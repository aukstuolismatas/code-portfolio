using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NT_Csharp.Models;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace NT_Csharp.Repos
{
    public class NTAgenturaRepository
    {
        public List<NTAgentura> getNTAgenturas()
        {
            List<NTAgentura> agenturos = new List<NTAgentura>();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "SELECT * FROM nt_agentura";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                agenturos.Add(new NTAgentura
                {
                    pavadinimas = Convert.ToString(item["pavadinimas"]),
                    imonesKodas = Convert.ToString(item["imones_kodas"]),
                    adresas = Convert.ToString(item["adresas"]),
                    telefonas = Convert.ToString(item["telefonas"]),
                    elPastas = Convert.ToString(item["el_pastas"])
                });
            }
            return agenturos;
        }
        public bool updateAgentura(NTAgentura ntAgentura)
        {
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
                MySqlConnection mySqlConnection = new MySqlConnection(conn);
                string sqlquery = @"UPDATE nt_agentura a SET a.pavadinimas=?pavadinimas, a.adresas=?adresas, a.telefonas=?tel, a.el_pastas=?email WHERE a.imones_kodas=?imoneskod";
                MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
                mySqlCommand.Parameters.Add("?pavadinimas", MySqlDbType.VarChar).Value = ntAgentura.pavadinimas;
                mySqlCommand.Parameters.Add("?imoneskod", MySqlDbType.VarChar).Value = ntAgentura.imonesKodas;
                mySqlCommand.Parameters.Add("?adresas", MySqlDbType.VarChar).Value = ntAgentura.adresas;
                mySqlCommand.Parameters.Add("?tel", MySqlDbType.VarChar).Value = ntAgentura.telefonas;
                mySqlCommand.Parameters.Add("?email", MySqlDbType.VarChar).Value = ntAgentura.elPastas;
                mySqlConnection.Open();
                mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public NTAgentura getNTAgentura(string imoneskodas)
        {
            NTAgentura ntAgentura = new NTAgentura();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "select * from nt_agentura where imones_kodas=?imoneskod";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlCommand.Parameters.Add("?imoneskod", MySqlDbType.VarChar).Value = imoneskodas;
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                ntAgentura.pavadinimas = Convert.ToString(item["pavadinimas"]);
                ntAgentura.imonesKodas = Convert.ToString(item["imones_kodas"]);
                ntAgentura.adresas = Convert.ToString(item["adresas"]);
                ntAgentura.telefonas = Convert.ToString(item["telefonas"]);
                ntAgentura.elPastas = Convert.ToString(item["el_pastas"]);
            }
            return ntAgentura;
        }
        public int getAgenturaSutarciuCount(string id)
        {
            int naudota = 0;
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"SELECT count(nr) as kiekis from sutartis where fk_NT_AGENTURAimones_kodas=" + id;
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                naudota = Convert.ToInt32(item["kiekis"] == DBNull.Value ? 0 : item["kiekis"]);
            }
            return naudota;
        }
        public void deleteAgentura(string id)
        {
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"DELETE FROM nt_agentura where imones_kodas=?id";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlCommand.Parameters.Add("?id", MySqlDbType.VarChar).Value = id;
            mySqlConnection.Open();
            mySqlCommand.ExecuteNonQuery();
            mySqlConnection.Close();
        }
    }
}
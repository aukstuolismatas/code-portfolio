﻿using System.ComponentModel.DataAnnotations;

namespace Bridge.Data.Dtos.Tables
{
    public record UpdateTableDto([Required] int BoardId);

}
#pragma once
#include "HDDWithComputedValue.h"
#include <omp.h>
#include <vector>


class SortedResultMonitor
{
private:
    omp_lock_t* lock;
public:
    HDDWithComputedValue** resultArray;
    int counter;
    SortedResultMonitor(int n);
    void addHDDSorted(HDDWithComputedValue* item);
    std::vector<HDDWithComputedValue*> getItems();
};


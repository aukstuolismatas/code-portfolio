import 'package:flutter/material.dart';
import 'package:layout_practice/art_route.dart';
import 'package:layout_practice/art_util.dart';
import '../shared/menu_drawer.dart';

class MainScreen extends StatelessWidget {
  const MainScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ArtRoute(
        art: ArtUtil.IMG_CARAVAGGIO,
      ),
    );
  }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NT_Csharp.Models;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace NT_Csharp.Repos
{
    public class NTTipasRepository
    {
        public List<NTTipas> getTipai()
        {
            List<NTTipas> tipai = new List<NTTipas>();

            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "SELECT * FROM nt_tipas";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                tipai.Add(new NTTipas
                {
                    id = Convert.ToInt32(item["id_NT_tipas"]),
                    pavadinimas = Convert.ToString(item["name"])
                });
            }

            return tipai;
        }
    }
}
﻿using CarServicePlatform.Entities;

namespace CarServicePlatform.Services;

public interface IFileService
{
    Task<FileInformation> UploadImage(IFormFile file, string userId);
    Task<List<FileInformation>> UploadRequestImages(IFormFileCollection files, string userId, int clientRequestId);
    Task<FileInformation> UploadCompanyDocuments(IFormFile file, string userId);
    Task<FileInformation?> DownloadProfileImage(string userId);
    Task<List<FileInformation>?> DownloadClientRequestImages(int requestId);
    Task<FileInformation?> DownloadDocument(string userId, int fileInformationId);
    Task<FileInformation?> DownloadDocumentForAdmin(int fileInformationId);
    Task<List<FileInformation>> RetrieveAllDocumentsInformationByUserId(string userId);
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Csharp.Models;
using NT_Csharp.Repos;

namespace NT_Csharp.Controllers
{
    public class VietaController : Controller
    {
        VietaRepository vietaRepository = new VietaRepository();
        // GET: Vieta
        public ActionResult Index()
        {
            return View(vietaRepository.getVietos());
        }
        // GET: Vieta/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Vieta/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Vieta/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Vieta/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Vieta/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Vieta/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

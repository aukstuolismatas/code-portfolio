import 'package:flutter/material.dart';
import 'package:layout_practice/art_route.dart';
import 'package:layout_practice/art_util.dart';
import './screens/main_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Color(0xff009688),
        textTheme: TextTheme(
            bodyText2: TextStyle(fontSize: 24, fontStyle: FontStyle.italic)),
      ),
    
      routes: {'/': (context) => MainScreen()},
    );
  }
}

﻿import { Component, OnInit } from "@angular/core";
import {TournamentService} from "../services/tournamentService";
import { Router, ActivatedRoute } from "@angular/router";
import { Tournament, UpdateTournament } from "../shared/Tournament";

@Component({
    selector: "tournament-edit",
    templateUrl: "tournamentEditView.html",
    styleUrls: []
})
export class TournamentEditView{
    private id: number;
    constructor(private tournament: TournamentService, private router: Router, private route: ActivatedRoute) { }
    ngOnInit() {
        this.id = this.route.snapshot.params['id'];
    }
    public tournamentItem: UpdateTournament = {
        date: new Date(Date.now())
    }
    public errorMessage = "";

    onTournamentUpdate(){
        this.tournament.updateTournament(this.id, this.tournamentItem).subscribe(() => {
            this.router.navigate([""]);
        }, error => {
            console.log(error);
            this.errorMessage = "Failed to update";
        })
    }

}
﻿using System.ComponentModel.DataAnnotations.Schema;
using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Entities.Request;

public class TransferOffer
{
    public TransferOffer()
    {
        Status = OfferStatus.Waiting;
    }

    public int Id { get; set; }
    public int Price { get; set; }
    public DateTime DateAndTime { get; set; }
    public OfferStatus Status { get; set; }

    [ForeignKey(nameof(TrawlAccount))]
    public string TrawlAccountId { get; set; }

    [ForeignKey(nameof(ClientRequest))]
    public int ClientRequestId { get; set; }
}
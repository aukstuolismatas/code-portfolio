﻿export interface Tournament {
    id: number;
    name: string;
    date: Date;
}
export interface UpdateTournament {
    date: Date;
}
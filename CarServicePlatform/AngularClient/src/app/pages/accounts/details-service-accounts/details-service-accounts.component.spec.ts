import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsServiceAccountsComponent } from './details-service-accounts.component';

describe('DetailsServiceAccountsComponent', () => {
  let component: DetailsServiceAccountsComponent;
  let fixture: ComponentFixture<DetailsServiceAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsServiceAccountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsServiceAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

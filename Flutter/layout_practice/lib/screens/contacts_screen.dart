import 'package:flutter/material.dart';
import 'package:layout_practice/screens/grid_screen.dart';

class ContactScreen extends StatelessWidget {
  const ContactScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Contacts')),
      body: ListView(children: showContacts(context)),
    );
  }

  List<Contact> buildContacts() {
    List<Contact> contacts = [];
    contacts
        .add(Contact('Thomas Anderson', 'The Matrix', Icons.sentiment_neutral));
    contacts.add(
        Contact('Frank Slade', 'Scent of a woman', Icons.sentiment_satisfied));
    contacts.add(Contact(
        'Mildred Hayes',
        'Three Billboards outside Ebbing, NMissouri',
        Icons.sentiment_dissatisfied));
    contacts.add(
        Contact('Bruce Wayne', 'The Dark Knight', Icons.sentiment_neutral));
    contacts.add(
        Contact('Jamal Malik', 'The Millionaire', Icons.sentiment_neutral));
    return contacts;
  }

  List<ListTile> showContacts(BuildContext context) {
    List<Contact> contacts = buildContacts();
    for (int i = 0; i < 20; i++) {
      contacts.addAll(buildContacts());
    }
    List<ListTile> list = [];
    contacts.forEach((contact) {
      list.add(ListTile(
        title: Text(contact.name),
        subtitle: Text(
          contact.subtitle,
          style: TextStyle(fontSize: 10),
        ),
        leading: CircleAvatar(
          child: Icon(contact.icon),
          backgroundColor: Colors.amber[600],
        ),
        trailing: Icon(Icons.keyboard_arrow_right),
        onTap: () => {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => GridScreen()))
        },
      ));
    });
    return list;
  }
}

class Contact {
  String name;
  String subtitle;
  IconData icon;
  Contact(this.name, this.subtitle, this.icon);
}

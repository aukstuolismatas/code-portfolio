import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { AppConstants } from "src/app/app-constants";
import { RolesType } from "../enum.service";
import { AuthService } from "./auth.service";


@Injectable({
    providedIn: 'root'
})
export class ClientGuard implements CanActivate {
    constructor(private auth: AuthService, private router: Router){}
    canActivate(): boolean | Observable<boolean>{
        if (this.auth.isLoggedIn() && this.auth.getRole()?.match(RolesType.Client) && !this.auth.getRole()?.match(RolesType.Admin)) {
            return true;
        } else {
            this.router.navigate([AppConstants.Login]);
            return false;
        }
    } 
}

﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.Request;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class PriceOfferRepository : IPriceOfferRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public PriceOfferRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<PriceOffer?> GetPriceOfferAsync(int id)
    {
        return await _carPlatformDbContext.PriceOffers.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
    }

    public async Task<List<PriceOffer>> GetAllClientRequestPriceOffersAsync(int clientRequestId)
    {
        return await _carPlatformDbContext.PriceOffers.Where(x => x.ClientRequestId == clientRequestId).ToListAsync().ConfigureAwait(false);
    }

    public async Task<PriceOffer?> GetClientRequestApprovedPriceOfferAsync(int clientRequestId)
    {
        return await _carPlatformDbContext.PriceOffers.FirstOrDefaultAsync(x => x.ClientRequestId == clientRequestId && x.Status == OfferStatus.Approved).ConfigureAwait(false);
    }

    public async Task<PriceOffer?> GetPriceOfferByServiceIdAndRequestIdAsync(string serviceAccountId, int clientRequestId)
    {
        return await _carPlatformDbContext.PriceOffers.FirstOrDefaultAsync(x => x.ServiceAccountId == serviceAccountId && x.ClientRequestId == clientRequestId).ConfigureAwait(false);
    }

    public async Task InsertPriceOfferAsync(PriceOffer priceOffer)
    {
        _carPlatformDbContext.PriceOffers.Add(priceOffer);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task UpdatePriceOfferAsync(PriceOffer priceOffer)
    {
        _carPlatformDbContext.PriceOffers.Update(priceOffer);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task UpdatePriceOffersRangeAsync(List<PriceOffer> priceOffers)
    {
        _carPlatformDbContext.PriceOffers.UpdateRange(priceOffers);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeletePriceOfferAsync(PriceOffer priceOffer)
    {
        _carPlatformDbContext.PriceOffers.Remove(priceOffer);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
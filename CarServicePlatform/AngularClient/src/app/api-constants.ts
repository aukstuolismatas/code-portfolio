export class ApiConstants {
    public static login = '/login';
    public static registerClient = '/register/client';
    public static registerService = '/register/service';
    public static registerTrawl = '/register/trawl';
    public static pusherAuth = '/pusher/auth';
    public static conversationsGroups = '/conversation/groups';
    public static clientAccount = '/account/clients/';
    public static adminAccount = '/account/admins/';
    public static serviceAccount = '/account/services/';
    public static trawlAccount = '/account/trawls/';
    public static activateAccount = '/account/activate/';
    public static deactivateAccount = '/account/deactivate/';
    public static uploadProfileImage = '/images/profiles';
    public static companyDocument = '/images/documents/';
    public static companyDocumentData = '/images/documents/data/';
    public static companyDocumentDataForAdmin = '/images/documents/admin/data/';
    public static uploadRepairRequestImage = '/images/clientRequest';
    public static citiesEnum = '/enum/cities';
    public static repairServiceGroupByCategories = '/repair/services/';
    public static repairServiceAvailableForService = '/repair/services/available/';
    public static clientRepairRequest = '/requests/';
    public static clientRepairRequestStart = '/requests/start/';
    public static clientRepairRequestEnd = '/requests/done/';
    public static clientRepairRequestTransferring = '/requests/transferring/';
    public static clientRepairRequestArrived = '/requests/arrived/';
    public static clientRepairRequestDelivered = '/requests/delivered/';

    public static availableClientRequestsForService = '/requests/service/offering/';
    public static clientRequestsWaitingApprovalForService = '/requests/service/waiting/';
    public static assignedClientRequestsForService = '/requests/service/assigned/';
    public static rejectedClientRequestsForService = '/requests/service/rejected/';

    public static availableClientRequestsForTrawl = '/requests/trawl/offering/';
    public static clientRequestsWaitingApprovalForTrawl = '/requests/trawl/waiting/';
    public static assignedClientRequestsForTrawl = '/requests/trawl/assigned/';
    public static rejectedClientRequestsForTrawl = '/requests/trawl/rejected/';
    public static priceOffer = '/offers/';
    public static transferOffer = '/offers/transfer/';

}

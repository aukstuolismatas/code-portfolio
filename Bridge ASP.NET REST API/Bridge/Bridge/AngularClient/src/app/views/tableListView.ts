﻿import { Component, OnInit } from "@angular/core";
import { TournamentService } from "../services/tournamentService";
import { AuthService } from "../services/authService";
import { Router, ActivatedRoute } from "@angular/router";
import { Table } from "../shared/Tables";

@Component({
    selector: "tables-list",
    templateUrl: "tableListView.html",
    styleUrls: []
})
export default class TableListView implements OnInit{
    constructor(public tournamentService: TournamentService, private router: Router, public authService: AuthService, private route: ActivatedRoute) {
    }
    public tournamnetId: number;
    ngOnInit(): void {
        this.tournamnetId = this.route.snapshot.params['id'];
        this.tournamentService.loadTables(this.tournamnetId).subscribe();
    }

    onDelete(tableId: number) {
        this.tournamentService.deleteTable(this.tournamnetId, tableId ).subscribe(() => {
            this.ngOnInit();
        });
    }
    onEdit(tournamentId: number) {
        this.router.navigate(['edit/' + tournamentId]);
    }
    goTo(tournamentId: number) {
        this.router.navigate(['tournaments/', tournamentId, '/tables']);
    }
    goToCreate() {
        this.router.navigate(['tournaments/', this.tournamnetId, '/tables/tableCreate'])
    }

}
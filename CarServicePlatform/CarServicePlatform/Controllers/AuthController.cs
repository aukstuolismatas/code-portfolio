﻿using AutoMapper;
using CarServicePlatform.Auth;
using CarServicePlatform.Data.Dtos;
using CarServicePlatform.Data.Dtos.Auth;
using CarServicePlatform.Data.Dtos.Auth.User;
using CarServicePlatform.Data.Mappings;
using CarServicePlatform.Entities;
using CarServicePlatform.Entities.RepairServices;
using CarServicePlatform.Entities.Users;
using CarServicePlatform.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarServicePlatform.Controllers;

[ApiController]
[Route("/")]
public class AuthController : ControllerBase
{
    private readonly UserManager<AppUser> _userManager;
    private readonly IMapper _mapper;
    private readonly ITokenManager _tokenManager;
    private readonly IAuthorizationService _authorizationService;
    private readonly IRepairServiceRepository _repairServiceRepository;
    private readonly IAvailableRepairServiceRepository _availableRepairServiceRepository;
    private readonly IClientRepository _clientRepository;

    public AuthController(UserManager<AppUser> userManager, IMapper mapper, ITokenManager tokenManager, IAuthorizationService authorizationService, IRepairServiceRepository repairServiceRepository, IAvailableRepairServiceRepository availableRepairServiceRepository, IClientRepository clientRepository)
    {
        _userManager = userManager;
        _mapper = mapper;
        _tokenManager = tokenManager;
        _authorizationService = authorizationService;
        _repairServiceRepository = repairServiceRepository;
        _availableRepairServiceRepository = availableRepairServiceRepository;
        _clientRepository = clientRepository;
    }

    [HttpPost]
    [Route("register/client")]
    public async Task<IActionResult> RegisterClient(RegisterClientUserDto registerClientUserDto)
    {
        AppUser user = await _userManager.FindByEmailAsync(registerClientUserDto.Email).ConfigureAwait(false);
        if (user != null)
        {
            return BadRequest("Email already exists");
        }

        string id = Guid.NewGuid().ToString();
        ClientAccount clientAccount = _mapper.Map<ClientAccount>(registerClientUserDto);
        clientAccount.Id = id;
        AppUser newAppUser = new()
        {
            Id = id,
            UserName = (registerClientUserDto.Email).Split('@')[0],
            Email = registerClientUserDto.Email,
            PhoneNumber = registerClientUserDto.PhoneNumber,
            IsAccountActivated = true,
            DomainUser = clientAccount
        };

        IdentityResult createUserResult = await _userManager.CreateAsync(newAppUser, registerClientUserDto.Password).ConfigureAwait(false);
        if (!createUserResult.Succeeded)
        {
            return BadRequest(createUserResult.Errors);
        }

        await _userManager.AddToRoleAsync(newAppUser, AppUserRoles.Client).ConfigureAwait(false);
        return CreatedAtAction(nameof(RegisterClient), _mapper.Map<AppClientUserDto>(newAppUser));
    }

    [HttpPost]
    [Route("register/service")]
    public async Task<IActionResult> RegisterService(RegisterServiceUserDto registerServiceUserDto)
    {
        AppUser serviceUser = await _userManager.FindByEmailAsync(registerServiceUserDto.Email).ConfigureAwait(false);
        if (serviceUser != null)
        {
            return BadRequest("Email already exists");
        }

        string id = Guid.NewGuid().ToString();
        ServiceAccount serviceAccount = _mapper.Map<ServiceAccount>(registerServiceUserDto);
        serviceAccount.Id = id;
        AppUser newAppUser = new()
        {
            Id = id,
            UserName = (registerServiceUserDto.Email).Split('@')[0],
            Email = registerServiceUserDto.Email,
            PhoneNumber = registerServiceUserDto.PhoneNumber,
            DomainUser = serviceAccount
        };
        City city = Enum.Parse<City>(registerServiceUserDto.City);
        List<AvailableRepairService> availableRepairServices =
            await ValidateAvailableRepairService(registerServiceUserDto.AvailableRepairServiceIds, id, city).ConfigureAwait(false);
        if (!availableRepairServices.Any())
        {
            return BadRequest("None of provided repair services exist");
        }

        IdentityResult createServiceUserResult = await _userManager.CreateAsync(newAppUser, registerServiceUserDto.Password).ConfigureAwait(false);
        if (!createServiceUserResult.Succeeded)
        {
            return BadRequest(createServiceUserResult.Errors);
        }

        await _userManager.AddToRoleAsync(newAppUser, AppUserRoles.Service).ConfigureAwait(false);
        await _availableRepairServiceRepository.InsertAvailableRepairServiceAsync(availableRepairServices).ConfigureAwait(false);
        return CreatedAtAction(nameof(RegisterService), _mapper.Map<AppServiceUserDto>(newAppUser));
    }

    private async Task<List<AvailableRepairService>> ValidateAvailableRepairService(Dictionary<int, int> availableRepairServiceIds, string userId, City city)
    {
        List<AvailableRepairService> availableRepairServices = new();
        foreach (KeyValuePair<int, int> id in availableRepairServiceIds)
        {
            RepairService? repairService = await _repairServiceRepository.GetServiceAsync(id.Value, id.Key).ConfigureAwait(false);
            if (repairService != null)
            {
                AvailableRepairService service = repairService.ToAvailableRepairService(userId, city);
                availableRepairServices.Add(service);
            }
        }

        return availableRepairServices;
    }

    [HttpPost]
    [Route("register/trawl")]
    public async Task<IActionResult> RegisterTrawl(RegisterTrawlUserDto registerTrawlUserDto)
    {
        AppUser serviceUser = await _userManager.FindByEmailAsync(registerTrawlUserDto.Email).ConfigureAwait(false);
        if (serviceUser != null)
        {
            return BadRequest("Email already exists");
        }

        string id = Guid.NewGuid().ToString();
        TrawlAccount trawlAccount = _mapper.Map<TrawlAccount>(registerTrawlUserDto);
        trawlAccount.Id = id;
        AppUser newAppUser = new()
        {
            Id = id,
            UserName = (registerTrawlUserDto.Email).Split('@')[0],
            Email = registerTrawlUserDto.Email,
            PhoneNumber = registerTrawlUserDto.PhoneNumber,
            DomainUser = trawlAccount
        };

        IdentityResult createServiceUserResult = await _userManager.CreateAsync(newAppUser, registerTrawlUserDto.Password).ConfigureAwait(false);
        if (!createServiceUserResult.Succeeded)
        {
            return BadRequest(createServiceUserResult.Errors);
        }

        await _userManager.AddToRoleAsync(newAppUser, AppUserRoles.Trawl).ConfigureAwait(false);
        return CreatedAtAction(nameof(RegisterTrawl), _mapper.Map<AppTrawlUserDto>(newAppUser));
    }

    [HttpPost]
    [Route("login")]
    public async Task<IActionResult> Login(LoginDto loginDto)
    {
        AppUser? user = await _userManager.FindByEmailAsync(loginDto.Email).ConfigureAwait(false);
        if (user == null)
        {
            return BadRequest("Credentials are incorrect");
        }

        bool isPasswordValid = await _userManager.CheckPasswordAsync(user, loginDto.Password).ConfigureAwait(false);
        if (!isPasswordValid)
        {
            return BadRequest("Credentials are incorrect");
        }

        DomainUser? domainUser = await _clientRepository.GetDomainUserAsync(user.Id).ConfigureAwait(false);
        if (domainUser == null)
        {
            return BadRequest("Domain user does not exists");
        }

        user.DomainUser = domainUser;
        return Ok(await _tokenManager.CreateAccessTokenAsync(user).ConfigureAwait(false));
    }

    [HttpGet]
    [Route("enum/cities")]
    public async Task<ActionResult<List<CityDto>>> RetrieveCities()
    {
        Array enumNames = Enum.GetNames(typeof(City));
        List<CityDto> cities = enumNames.Cast<object>().Select((_, i) => new CityDto { Name = enumNames.GetValue(i)?.ToString() }).ToList();
        return Ok(cities.Select(x => x));
    }
}
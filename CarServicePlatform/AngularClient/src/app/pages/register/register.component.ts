import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppConstants, RegexPatterns } from 'src/app/app-constants';
import { AppComponent } from 'src/app/app.component';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { mustMatch } from 'src/app/shared/form-validation';
import { RegisterClient } from 'src/app/shared/login-data';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public clientRegisterForm : FormGroup = new FormGroup({
    name: new FormControl(''),
    surname: new FormControl(''),
    email: new FormControl(''),
    phone: new FormControl(''),
    password: new FormControl(''),
    confirmPassword: new FormControl(''),
  });

  public submitted =false;
  public errorMessage = '';
  public successMessage = '';
  constructor(private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit(): void {
    this.clientRegisterForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]],
      email: ['', [Validators.required ,Validators.email, Validators.pattern(RegexPatterns.Email)]],
      phone: ['', [Validators.required, Validators.pattern(RegexPatterns.Phone)]],
      password: ['', [Validators.required, Validators.minLength(6),Validators.maxLength(40), Validators.pattern(RegexPatterns.Passowrd)]],
      confirmPassword: ['', [Validators.required]],
    }, {
      validator: mustMatch
    });
  }
  get f(): {[key:string]:AbstractControl} { return this.clientRegisterForm.controls; }

  register(){
    const registerRequest: RegisterClient = {
      name: this.f['name'].value,
      surname: this.f['surname'].value,
      email: this.f['email'].value,
      phoneNumber: this.f['phone'].value,
      password: this.f['password'].value,
    };
    this.submitted=false;
     if (this.clientRegisterForm.invalid) {
       this.submitted = true;
        return;
     }
    this.authService.register(registerRequest).subscribe({
      next: (user) => this.router.navigate([AppConstants.Login], {queryParams: { registered: 'true' } }),
      error: (err) => this.errorMessage = MessageConstants.LoginError
    })
    this.clientRegisterForm.reset;
    this.errorMessage =''
  }
}

﻿using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Repository;

public interface IAdministratorRepository
{
    Task<AdministratorAccount?> GetAdministratorUserAsync();
}
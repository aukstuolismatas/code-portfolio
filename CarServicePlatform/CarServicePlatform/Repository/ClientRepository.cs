﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.Users;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class ClientRepository : IClientRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public ClientRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<DomainUser?> GetDomainUserAsync(string userId)
    {
        return await _carPlatformDbContext.DomainUsers.FirstOrDefaultAsync(x => x.Id == userId).ConfigureAwait(false);
    }

    public async Task<ClientAccount?> GetClientAccountAsync(string userId)
    {
        return await _carPlatformDbContext.ClientAccounts.FirstOrDefaultAsync(x => x.Id == userId).ConfigureAwait(false);
    }

    public async Task<AdministratorAccount?> GetAdminAccountAsync(string userId)
    {
        return await _carPlatformDbContext.AdministratorAccounts.FirstOrDefaultAsync(x => x.Id == userId).ConfigureAwait(false);
    }

    public async Task<Dictionary<string, ClientAccount>> GetAllClientAccountsAsync()
    {
        return await _carPlatformDbContext.ClientAccounts.ToDictionaryAsync(key => key.Id, x => x).ConfigureAwait(false);
    }

    public async Task UpdateClientAccountAsync(ClientAccount clientAccount)
    {
        _carPlatformDbContext.ClientAccounts.Update(clientAccount);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteClientAccountAsync(ClientAccount clientAccount)
    {
        _carPlatformDbContext.ClientAccounts.Remove(clientAccount);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
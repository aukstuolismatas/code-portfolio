import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver';
import { MessageConstants } from 'src/app/message-constants';
import { CategoriesService } from 'src/app/services/categories.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FileService } from 'src/app/services/file.service';
import { ProfileService } from 'src/app/services/profile.service';
import { ServiceAccount, UserAccount } from 'src/app/shared/account';
import { City } from 'src/app/shared/city';
import { FileInformation } from 'src/app/shared/file-information';
import { RepairService } from 'src/app/shared/repair-services';

@Component({
  selector: 'app-details-service-accounts',
  templateUrl: './details-service-accounts.component.html',
  styleUrls: ['./details-service-accounts.component.css']
})
export class DetailsServiceAccountsComponent implements OnInit {
  index:number=0
  id=''
  public errorMessage ='';
  public successMessage ='';
  account!: ServiceAccount;
  repairServices: RepairService[] =[]
  documentsInformation:FileInformation[]=[]
  constructor(private dataSharingService: DataSharingService, private activatedRoute: ActivatedRoute, private profileService: ProfileService,
    private fileService: FileService, private categoriesService: CategoriesService) { }

  ngOnInit(): void {
    this.index = this.activatedRoute.snapshot.params['index'];
    this.id = this.activatedRoute.snapshot.params['id'];
    this.categoriesService.loadRepairServicesAvailableForService(this.id).subscribe((data:RepairService[]) =>{
      this.repairServices = data;
    }) 
    this.account = this.dataSharingService.serviceAccountsList.value[this.index];
    if(this.account===undefined){
      this.profileService.loadServiceAccount(this.id).subscribe((data:ServiceAccount)=>{
        this.account=data
      });
    }
    this.fileService.retrieveAllDocumentsByUserId(this.id).subscribe((data: FileInformation[])=>this.documentsInformation=data); 
    
  } 
  downloadDocument(index: number){
    this.fileService.downloadDocumentAdmin(this.documentsInformation[index].id).subscribe((data:any) =>{
      let blob:any = new Blob([data], { type: `${data.type}` });
      const url = window.URL.createObjectURL(blob);
      saveAs(blob, this.documentsInformation[index].name);
    }), (error: any) => this.errorMessage = "Parsiūsti nepavyko",
    () => console.info('Successfully downloaded');
  }

  getCity(enumNumber:number){
    return City[enumNumber];
  }

  activateAccount(userId:string){
    this.profileService.activateAccount(userId).subscribe({
        next:(data:UserAccount)=>{
          this.account.isAccountActivated=data.isAccountActivated
          this.successMessage = MessageConstants.AccountActivated
        },
        error: () => this.errorMessage = MessageConstants.AccountActivatedFailed
      });
    this.successMessage='';
    this.errorMessage ='';
  }

  deactivateAccount(userId:string){
    this.profileService.deactivateAccount(userId).subscribe({
      next:(data:UserAccount)=>{
        this.account.isAccountActivated=data.isAccountActivated
        this.successMessage = MessageConstants.AccountDeactivated
      },
      error: () => this.errorMessage = MessageConstants.AccountActivatedFailed
    });
    this.successMessage='';
    this.errorMessage ='';
  }
}

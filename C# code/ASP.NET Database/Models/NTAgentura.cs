using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NT_Csharp.Models
{
    public class NTAgentura
    {
        [DisplayName("Pavadinimas")]
        [Required]
        public string pavadinimas { get; set; }
        [DisplayName("Įmones kodas")]
        [Required]
        public string imonesKodas { get; set; }
        [DisplayName("Adresas")]
        [Required]
        public string adresas { get; set; }
        [DisplayName("Telefonas")]
        [Required]
        public string telefonas { get; set; }
        [DisplayName("El. Paštas")]
        [Required]
        public string elPastas { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarServicePlatform.Entities.RepairServices;

public class RepairService
{
    public int Id { get; set; }
    public string ServiceName { get; set; }

    [Required]
    public int RepairServiceCategoryId { get; set; }

    public RepairServiceCategory RepairServiceCategory { get; set; }

    [NotMapped]
    public City City { get; set; }
}
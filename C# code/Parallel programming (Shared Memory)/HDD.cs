using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Shared_Memory
{
    [DataContract]
    public class HDD
    {
        [DataMember]
        public string brand { get; set; }
        [DataMember]
        public int capacity { get; set; }
        [DataMember]
        public double price { get; set; }
    }
}

﻿namespace CarServicePlatform.Data.Dtos.Account;

public record ServiceAccountDto(string Id, string Name, string Surname, string CompanyCode, string CompanyName, string Address);
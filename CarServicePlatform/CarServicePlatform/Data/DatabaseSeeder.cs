﻿using CarServicePlatform.Auth;
using CarServicePlatform.Entities.Users;
using Microsoft.AspNetCore.Identity;

namespace CarServicePlatform.Data;

public class DatabaseSeeder
{
    private readonly UserManager<AppUser> _userManager;
    private readonly RoleManager<IdentityRole> _roleManager;

    public DatabaseSeeder(UserManager<AppUser> userManager, RoleManager<IdentityRole> roleManager)
    {
        _userManager = userManager;
        _roleManager = roleManager;
    }

    public async Task SeedAsync()
    {
        foreach (string role in AppUserRoles.All)
        {
            bool isRoleExist = await _roleManager.RoleExistsAsync(role).ConfigureAwait(false);
            if (!isRoleExist)
            {
                await _roleManager.CreateAsync(new IdentityRole(role)).ConfigureAwait(false);
            }
        }

        string id = Guid.NewGuid().ToString();
        AdministratorAccount admin = new()
        {
            Id = id,
            Surname = "Admin",
            Name = "Admin",
        };
        AppUser newAdmin = new()
        {
            Id = id,
            UserName = "admin",
            Email = "admin@gmail.com",
            PhoneNumber = "865555585",
            IsAccountActivated = true,
            DomainUser = admin
        };

        AppUser checkAdminUser = await _userManager.FindByNameAsync(newAdmin.UserName).ConfigureAwait(false);
        if (checkAdminUser == null)
        {
            IdentityResult createAdminUserResult = await _userManager.CreateAsync(newAdmin, "Super!22").ConfigureAwait(false);
            if (createAdminUserResult.Succeeded)
            {
                await _userManager.AddToRolesAsync(newAdmin, AppUserRoles.All).ConfigureAwait(false);
            }
        }
    }
}
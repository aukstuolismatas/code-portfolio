using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Csharp.Repos;
using NT_Csharp.Models;
using NT_Csharp.ViewModels;

namespace NT_Csharp.Controllers
{
    public class NamasController : Controller
    {
        NamasRepository namasRepository = new NamasRepository();
        NamuTipasRepository namuTipasRepository = new NamuTipasRepository();
        // GET: Namas
        public ActionResult Index()
        {
            return View(namasRepository.getNamai());
        }

        // GET: Namas/Create
        public ActionResult Create()
        {
            NamasEditViewModel namas = new NamasEditViewModel();
            PopulateSelections(namas);
            return View(namas);
        }

        // POST: Namas/Create
        [HttpPost]
        public ActionResult Create(NamasEditViewModel collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    namasRepository.addNamas(collection);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                PopulateSelections(collection);
                return View(collection);
            }
        }

        // GET: Namas/Edit/5
        public ActionResult Edit(int id)
        {
            NamasEditViewModel namas = namasRepository.getNamas(id);
            PopulateSelections(namas);
            return View(namas);
        }

        // POST: Namas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, NamasEditViewModel collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    namasRepository.updateNamas(collection);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                PopulateSelections(collection);
                return View(collection);
            }
        }

        // GET: Namas/Delete/5
        public ActionResult Delete(int id)
        {
            NamasEditViewModel namas = namasRepository.getNamas(id);
            return View(namas);
        }

        // POST: Namas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, NamasEditViewModel collection)
        {
            try
            {
                NamasEditViewModel namas = namasRepository.getNamas(id);
                bool naudojama = false;

                if (namasRepository.getNamasObjektuCount(id) > 0)
                {
                    naudojama = true;
                    ViewBag.naudojama = "Namas yra naudojamas Objekto lentelėje todėl negali būti pašalintas";
                    return View(namas);
                }

                if (!naudojama)
                {
                    namasRepository.deleteNamas(id);
                }


                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public void PopulateSelections(NamasEditViewModel namai)
        {
            var tipai = namuTipasRepository.getTipai();
            List<SelectListItem> selectListTipai = new List<SelectListItem>();
            foreach (var item in tipai)
            {
                selectListTipai.Add(new SelectListItem() { Value = Convert.ToString(item.id), Text = item.pavadinimas });
            }
            namai.TipaiList = selectListTipai;
        }
    }
}

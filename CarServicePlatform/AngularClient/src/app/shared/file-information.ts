export interface FileInformation {
    id: number;
    name: string;
    size: number;
    fileExtension: string;
}

export interface FileData {
    size: number;
    fileExtension: string;
    fileBase64: string;
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bridge.Data;
using Bridge.Data.Dtos.Auth.Users;
using Bridge.Data.Dtos.Users;
using Bridge.Data.Entities;
using Bridge.Data.Repositories;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UserController : ControllerBase
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;

        public UserController(IUsersRepository usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<UserDto>> GetAll()
        {
            List<User> users = await _usersRepository.GetAllAsync().ConfigureAwait(false);
            return users.Select(x => _mapper.Map<UserDto>(x));
        }

        [HttpGet("{username}")]
        public async Task<ActionResult<UserDto>> Get(string username)
        {
            User user = await _usersRepository.GetAsync(username).ConfigureAwait(false);
            if (user == null)
            {
                return NotFound($"User with username {username} not exist");
            }

            return Ok(_mapper.Map<UserDto>(user));
        }

        [HttpPost]
        public async Task<ActionResult<UserDto>> Post(CreateUserDto userDto)
        {
            User user = _mapper.Map<User>(userDto);
            User oldUser = await _usersRepository.GetAsync(user.Username).ConfigureAwait(false);
            if (oldUser != null)
            {
                return Conflict($"User with username {user.Username} already exists");
            }

            user.Password = Hasher.HashPassword(user.Password);
            await _usersRepository.InsertAsync(user).ConfigureAwait(false);
            return Created($"/api/users/{user.Username}", _mapper.Map<UserDto>(user));
        }

        [HttpPut("{username}")]
        public async Task<ActionResult<UserDto>> Put(string username, UpdateUserDto userDto)
        {
            User user = await _usersRepository.GetAsync(username).ConfigureAwait(false);
            if (user == null)
            {
                return NotFound($"User with username {username} not exist");
            }

            _mapper.Map(userDto, user);
            user.Password = Hasher.HashPassword(user.Password);

            await _usersRepository.UpdateAsync(user).ConfigureAwait(false);

            return Ok(_mapper.Map<UserDto>(user));
        }

        [HttpDelete("{username}")]
        public async Task<ActionResult<UserDto>> Delete(string username)
        {
            User user = await _usersRepository.GetAsync(username).ConfigureAwait(false);
            if (user == null)
            {
                return NotFound($"User with username {username} not exist");
            }

            await _usersRepository.DeleteAsync(user).ConfigureAwait(false);

            return NoContent();
        }
    }
}
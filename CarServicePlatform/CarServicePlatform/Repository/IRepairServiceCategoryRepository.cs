﻿using CarServicePlatform.Entities.RepairServices;

namespace CarServicePlatform.Repository;

public interface IRepairServiceCategoryRepository
{
    Task<RepairServiceCategory?> GetCategoryAsync(int id);
    Task<List<RepairServiceCategory>> GetAllCategoriesAsync();
    Task InsertCategoryAsync(RepairServiceCategory repairServiceCategory);
    Task UpdateCategoryAsync(RepairServiceCategory repairServiceCategory);
    Task DeleteCategoryAsync(RepairServiceCategory repairServiceCategory);
}
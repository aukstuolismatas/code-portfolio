import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-trawl-tab',
  templateUrl: './trawl-tab.component.html',
  styleUrls: ['./trawl-tab.component.css']
})
export class TrawlTabComponent implements OnInit {

  @Input('tab') tab = '';
  readonly waitingOfferUrl = AppConstants.AllWaitingTrawlRequestForOffer;
  readonly waitingApproveUrl = AppConstants.AllWaitingClientRequestToTransferForClientApproval;
  readonly assignedUrl = AppConstants.AllAssignedClientRequestForTrawl;
  readonly inactiveUrl = AppConstants.AllInactiveClientRequestForTrawl;


  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  isTrawl(){
    return this.authService.isTrawl();
  }

  isWaitingOffer(){
    return this.tab==HeaderConstants.WaitingPriceOfferTab;
  }

  isWaitingApprove(){
    return this.tab==HeaderConstants.WaitingClientApprovalTab;
  }

  isAssign(){
    return this.tab==HeaderConstants.AssignedTab;
  }

  isInActive(){
    return this.tab==HeaderConstants.InActiveTab;
  }

  toOffers(){
    this.router.navigate([this.waitingOfferUrl]);
  }

  toApproval(){
    this.router.navigate([this.waitingApproveUrl]);
  }
  
  toAssigned(){
    this.router.navigate([this.assignedUrl]);
  }

  toInActive(){
    this.router.navigate([this.inactiveUrl]);
  }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.Auth;

public record RegisterTrawlUserDto([Required] string Name, [Required] string Surname, [Required][EmailAddress] string Email,
    [Required] string Password, [Required][Phone] string PhoneNumber, [Required] int MinimalPrice, [Required] string CompanyCode, [Required] string CompanyName, 
    [Required] string City);
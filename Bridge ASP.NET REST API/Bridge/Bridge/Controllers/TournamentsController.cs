﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bridge.Auth;
using Bridge.Data.Dtos.Tournaments;
using Bridge.Data.Entities;
using Bridge.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.Controllers
{
    [ApiController]
    [Route("api/tournaments")]
    public class TournamentsController : ControllerBase
    {
        private readonly ITournamentsRepository _tournamentsRepository;
        private readonly IMapper _mapper;

        public TournamentsController(ITournamentsRepository tournamentsRepository, IMapper mapper)
        {
            _tournamentsRepository = tournamentsRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<TournamentDto>> GetAllAsync()
        {
            List<Tournament> tournaments = await _tournamentsRepository.GetAllAsync().ConfigureAwait(false);
            return tournaments.Select(x => _mapper.Map<TournamentDto>(x));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Tournament>> Get(int id)
        {
            Tournament tournament = await _tournamentsRepository.GetAsync(id).ConfigureAwait(false);
            if (tournament == null)
            {
                return NotFound($"Tournaments with id {id} not exists");
            }

            return Ok(_mapper.Map<TournamentDto>(tournament));
        }

        [HttpPost]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<TournamentDto>> Post(CreateTournamentDto tournamentDto)
        {
            Tournament tournament = _mapper.Map<Tournament>(tournamentDto);
            await _tournamentsRepository.InsertAsync(tournament).ConfigureAwait(false);
            return Created($"/api/tournaments/{tournament.Id}", _mapper.Map<TournamentDto>(tournament));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<TournamentDto>> Put(int id, UpdateTournamentDto tournamentDto)
        {
            Tournament tournament = await _tournamentsRepository.GetAsync(id).ConfigureAwait(false);
            if (tournament == null)
            {
                return NotFound($"Tournaments with id {id} not exists");
            }

            _mapper.Map(tournamentDto, tournament);

            await _tournamentsRepository.UpdateAsync(tournament).ConfigureAwait(false);

            return Ok(_mapper.Map<TournamentDto>(tournament));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<TournamentDto>> Delete(int id)
        {
            Tournament tournament = await _tournamentsRepository.GetAsync(id).ConfigureAwait(false);
            if (tournament == null)
            {
                return NotFound($"Tournaments with id {id} not exists");
            }

            await _tournamentsRepository.DeleteAsync(tournament).ConfigureAwait(false);

            return NoContent();
        }
    }
}

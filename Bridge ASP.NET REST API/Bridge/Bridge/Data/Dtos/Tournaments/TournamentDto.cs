﻿using System;

namespace Bridge.Data.Dtos.Tournaments
{
    public record TournamentDto(int Id, string Name, DateTime Date);
}
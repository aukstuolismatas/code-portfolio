﻿using CarServicePlatform.Entities;

namespace CarServicePlatform.Data.Dtos.RepairService;

public record RepairServiceDto(int Id, int RepairServiceCategoryId, string ServiceName, Entities.RepairServices.RepairServiceCategory RepairServiceCategory, City City);
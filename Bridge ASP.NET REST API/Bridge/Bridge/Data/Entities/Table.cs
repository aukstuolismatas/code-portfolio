﻿using System.ComponentModel.DataAnnotations;
using Bridge.Auth;
using Bridge.Data.Dtos.Auth;

namespace Bridge.Data.Entities
{
    public class Table : IUserOwnedResources
    {
        public int Id { get; set; }
        public int TableNr { get; set; }
        public int BoardId { get; set; }
        public Board Board { get; set; }
        public int TournamentId { get; set; }
        public Tournament Tournament { get; set; }

        [Required]
        public string UserId { get; set; }
        public BridgeRestUser User { get; set; }
    }
}
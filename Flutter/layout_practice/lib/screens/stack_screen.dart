import 'dart:html';

import 'package:flutter/material.dart';
import 'package:layout_practice/screens/contacts_screen.dart';

class StackScreen extends StatelessWidget {
  const StackScreen({super.key});
  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(title: Text('Stack window')),
      body: Container(
        width: sizeX,
        height: sizeY,
        child: Stack(
            children: showPizzaLayout(sizeX, sizeY,
                context)), //ListView(children:showContacts()), //,
      ),
    );
  }

  List<Widget> showPizzaLayout(
      double sizeX, double sizeY, BuildContext context) {
    List<Widget> layoutChildren = [];
    Container backGround = Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(
                  'https://images.unsplash.com/photo-1594007654729-407eedc4be65?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=728&q=80'),
              fit: BoxFit.fill)),
    );
    layoutChildren.add(backGround);
    Positioned pizzaCard = Positioned(
      child: Card(
        elevation: 12,
        color: Colors.white70,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        child: Column(children: [
          Text(
            "Pizza Margherita",
            style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.deepOrange[800]),
          ),
          Padding(
            padding: EdgeInsets.all(12),
            child: Text(
              'This delicious pizza is made of Tomato, Mozzarella and Basil.\n\nSeriously you can\'t miss it.',
              style: TextStyle(fontSize: 18, color: Colors.grey[800]),
            ),
          )
        ]),
      ),
      top: sizeY / 20,
      left: sizeX / 20,
    );
    layoutChildren.add(pizzaCard);
    Positioned buttonOrder = Positioned(
        bottom: sizeY / 20,
        left: sizeX / 20,
        width: sizeX - sizeX / 10,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            elevation: 12,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
            ),
            backgroundColor: Colors.orange[900],
          ),
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => ContactScreen(),
            ));
          },
          child: Text(
            'Order Now!',
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
        ));
    layoutChildren.add(buttonOrder);
    return layoutChildren;
  }

  // Widget createSeparator(int position) {  }

  
}

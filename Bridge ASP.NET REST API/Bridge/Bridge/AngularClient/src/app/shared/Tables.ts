﻿export interface Table {
    id: number;
    tournamentId: number;
    tableNr: number;
    boardId: number;
    userId: string;
}
export interface UpdateTable {
    boardId: number;
}
export interface CreateTable {
    tableNr: number;
    boardId: number;
}

﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.Request;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class TransferOfferRepository : ITransferOfferRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public TransferOfferRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<TransferOffer?> GetTransferOfferAsync(int id)
    {
        return await _carPlatformDbContext.TransferOffers.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
    }

    public async Task<List<TransferOffer>> GetAllClientRequestTransferOffersAsync(int clientRequestId)
    {
        return await _carPlatformDbContext.TransferOffers.Where(x => x.ClientRequestId == clientRequestId).ToListAsync().ConfigureAwait(false);
    }

    public async Task<TransferOffer?> GetTransferOfferByTrawlIdAndRequestIdAsync(string trawlAccountId, int clientRequestId)
    {
        return await _carPlatformDbContext.TransferOffers.FirstOrDefaultAsync(x => x.TrawlAccountId == trawlAccountId && x.ClientRequestId == clientRequestId).ConfigureAwait(false);
    }

    public async Task InsertTransferOfferAsync(TransferOffer transferOffer)
    {
        _carPlatformDbContext.TransferOffers.Add(transferOffer);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task UpdateTransferOfferAsync(TransferOffer transferOffer)
    {
        _carPlatformDbContext.TransferOffers.Update(transferOffer);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task UpdateTransferOffersRangeAsync(List<TransferOffer> transferOffers)
    {
        _carPlatformDbContext.TransferOffers.UpdateRange(transferOffers);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteTransferOfferAsync(TransferOffer transferOffer)
    {
        _carPlatformDbContext.TransferOffers.Remove(transferOffer);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
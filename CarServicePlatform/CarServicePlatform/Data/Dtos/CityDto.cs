﻿namespace CarServicePlatform.Data.Dtos;

public class CityDto
{
    public string? Name { get; set; }
}
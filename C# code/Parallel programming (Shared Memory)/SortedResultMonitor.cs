using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Shared_Memory
{
    class SortedResultMonitor
    {
        public HDDWithComputedValue[] resultArray;
        public int counter;
        private object mutex;
        public SortedResultMonitor(int n)
        {
            resultArray = new HDDWithComputedValue[n];
            counter = 0;
            mutex = new object();
        }
        public void addHDDSorted(HDDWithComputedValue item)
        {
            double computed = item.computedData;
            lock (mutex)
            {
                if (counter == 0)
                {
                    resultArray[0] = item;
                    counter++;
                }
                else
                {
                    for (int i = 0; i < counter; i++)
                    {
                        if (i == counter -1)
                        {
                            
                            if (computed > resultArray[i].computedData)
                            {
                                resultArray[counter] = item;
                            }
                            else
                            {
                                
                                resultArray[counter] = resultArray[counter - 1];
                                resultArray[counter - 1] = item;
                            }
                            break;
                        }
                        else if (computed <= resultArray[i].computedData)
                        {
                            for (int j = counter; j > i; j--)
                            {
                                resultArray[j] = resultArray[j - 1];
                            }
                            resultArray[i] = item;
                            break;
                        }
                    }
                    counter++;
                }
            }
        }
        public List<HDDWithComputedValue> getItems()
        {
            List<HDDWithComputedValue> result = new List<HDDWithComputedValue>();
            for (int i=0; i<counter; i++)
            {
                result.Add(resultArray[i]);
            }
            return result;
        }
    }
}

import { City } from "./city";

export interface RepairService {
    id: number;
    serviceName: string;
    repairServiceCategory:{
        id: number;
        categoryName: string;
    }
    city: City;
}

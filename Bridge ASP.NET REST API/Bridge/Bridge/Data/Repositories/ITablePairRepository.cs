﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;

namespace Bridge.Data.Repositories
{
    public interface ITablePairRepository
    {
        Task<TablePair> GetAsync(int tableId, int tablePairId);
        Task<List<TablePair>> GetAllAsync(int tableId);
        Task InsertAsync(TablePair tablePair);
        Task UpdateAsync(TablePair tablePair);
        Task DeleteAsync(TablePair tablePair);
    }
}
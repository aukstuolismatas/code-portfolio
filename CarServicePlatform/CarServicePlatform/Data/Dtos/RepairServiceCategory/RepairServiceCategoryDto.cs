﻿namespace CarServicePlatform.Data.Dtos.RepairServiceCategory;

public record RepairServiceCategoryDto(int Id, string CategoryName);
export enum RequestStatus {
    Waiting = "Laukiama pasiūlymų",
    Accepted = "Pasiūlymas priimtas",
    InProgress = "Vykdomas remontas",
    Done = "Remonto darbai baigti",
    Canceled = "Atšauktas",
    WaitingTransfer = "Laukiama pervežimo pasiūlymų",
    Transferring = "Automobilis vežamas",
    Arrived = "Automobilis atvyko",
    Delivered = "Automobilis grąžintas klientui"
}

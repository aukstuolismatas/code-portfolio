﻿namespace CarServicePlatform.Data.Dtos.File;

public record FileDataDto(double Size, string FileExtension, string FileBase64);
﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bridge.Data.Dtos.Tournaments
{
    public record UpdateTournamentDto([Required] DateTime Date);
}
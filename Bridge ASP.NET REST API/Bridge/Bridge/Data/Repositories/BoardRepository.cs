﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bridge.Data.Repositories
{
    public class BoardRepository : IBoardRepository
    {
        private readonly BridgeRestContext _bridgeRestContext;

        public BoardRepository(BridgeRestContext bridgeRestContext)
        {
            _bridgeRestContext = bridgeRestContext;
        }

        public async Task<Board> GetAsync(int boardId)
        {
            return await _bridgeRestContext.Boards.FirstOrDefaultAsync(x => x.Id == boardId).ConfigureAwait(false);
        }

        public async Task<List<Board>> GetAllAsync()
        {
            return await _bridgeRestContext.Boards.ToListAsync().ConfigureAwait(false);
        }

        public async Task InsertAsync(Board board)
        {
            _bridgeRestContext.Boards.Add(board);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task UpdateAsync(Board board)
        {
            _bridgeRestContext.Boards.Update(board);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAsync(Board board)
        {
            _bridgeRestContext.Boards.Remove(board);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { catchError, map } from 'rxjs';
import { ApiConstants } from 'src/app/api-constants';
import { EnumService } from 'src/app/services/enum.service';
import { ErrorHandleService } from 'src/app/services/error-handle.service';
import { City } from 'src/app/shared/city';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css']
})
export class DropdownComponent implements OnInit {

  show: boolean = false;
  @Input('value') value = '-1';
  @Output() OnChange: EventEmitter<number> = new EventEmitter<number>();
  // cities =[{ id: "1", name: "VIlno"}];
  // cities = [
  //   {id: 0, name: 'Kaunas'},
  //   {id: 1, name: 'Vilnius'},
  //   {id: 2, name: 'Klaipėda'},
  //   {id: 3, name: 'Šiauliai'},
  //   {id: 4, name: 'Panevežys'},
  // ]
  selectedLevel:any
  constructor(private enumService: EnumService) { 
    // this.enumService.loadCities().subscribe((data: City[]) => {
    //   this.cities = data;
    // });
    // console.log(this.cities);
  }

  ngOnInit(): void {
  }

  // toggle() {
  //   this.show = !this.show;
  // }
  selected(event: any){
    this.selectedLevel = event.target.value;
    this.OnChange.emit(this.selectedLevel)
  }
  // getSelectedDropdown(id: number){
  //   console.log(id);
  // }
  // onSelect(event: any) {
  //   console.log(`car id: `, event.target.id);
  //   console.log(`car`, event.target.id ? this.cities.find(c => c.id == event.target.id): "None")
  // }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.RepairService;

public record UpdateRepairServiceDto([Required] string ServiceName);
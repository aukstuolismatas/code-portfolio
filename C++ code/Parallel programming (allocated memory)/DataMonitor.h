#pragma once
#include <string>
#include "HDD1.h"
class DataMonitor
{
public:
	    HDD1 ** array;
	    int counter;
	private:
	    const int MIN_VALUE = 0;
	    int MAX_VALUE;
	    bool isLast;
	public:
		DataMonitor(int n);
		bool addHDD(HDD1* hdd, bool last);
		HDD1* removeFirst();
		bool getIsLast();
};


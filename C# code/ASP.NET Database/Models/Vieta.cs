using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NT_Csharp.Models
{
    public class Vieta
    {
        [DisplayName("Miestas")]
        [Required]
        public string miestas { get; set; }
        [DisplayName("Gatvė")]
        [Required]
        public string gatve { get; set; }
        [DisplayName("Namo numeris")]
        [Required]
        public int namoNR { get; set; }
        [DisplayName("Rajonas")]
        [Required]
        public string rajonas { get; set; }
        public double atstumasIkiCentro { get; set; }
        public double atstumasIkiParko { get; set; }
        public int idVieta { get; set; }
        
    }
}
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-confirmation-alert',
  templateUrl: './confirmation-alert.component.html',
  styleUrls: ['./confirmation-alert.component.css']
})
export class ConfirmationAlertComponent  {
  @ViewChild('modalComponent') modal:
  ModalComponent<ConfirmationAlertComponent> | undefined

  constructor() { }

  async deleteRecord(): Promise<void> {
    await this.modal?.confirm();
  }

  async close(): Promise<void> {
    await this.modal?.close();
  }

}

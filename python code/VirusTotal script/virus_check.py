import sys
import requests
import hashlib
import os
import json
import time

def check_file(filePath):
    url = "https://www.virustotal.com/vtapi/v2/file/report"
    BLOCKSIZE = 65536

    hasher = hashlib.sha1()
    with open(filePath, 'rb') as afolder:
        buf = afolder.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = afolder.read(BLOCKSIZE)
    file_hash = str(hasher.hexdigest())

    parameters = {'apikey': '1ad75392e4d83b247a3d2e619254f236f3e0235c4dcbbca31774a34fee3da93d', 'resource': file_hash}
    response = requests.get(url, params=parameters)
    response = response.json()
    result=json.dumps(response, sort_keys=True, indent=4, separators=(',', ': '))
    with open("virus.txt", "a+") as outfile:
        outfile.write(str(filePath))
        outfile.write(" Detected: " + str(response['positives']) + "/" + str(response['total']) + "\n")

    with open("virus.json", "a+") as outfile:
        outfile.write(result)

    print(filePath)
    if response['response_code'] == 0:
        print(response['verbose_msg'])
    elif response['response_code'] == 1:
        print(response['scans'])
        print("Detected: " + str(response['positives']) + "/" + str(response['total']))


folder = input("Please enter the path to the file: ")
folder = folder.strip('"')
files=os.listdir(folder)
result={}
counter=0
if os.path.exists("virus.json"):
    os.remove("virus.json")
if os.path.exists("virus.txt"):
    os.remove("virus.txt")
while files:
    check_file(folder + '\\' + files.pop())
    time.sleep(1)


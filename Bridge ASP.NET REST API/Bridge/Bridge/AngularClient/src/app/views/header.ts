import { Component, OnInit } from "@angular/core";
import { AuthService } from "../services/authService";
import { Router } from "@angular/router";

@Component({
    selector: "app-header",
    templateUrl: "header.html",
    styleUrls: ["header.css"]
})
export default class Header  {
    constructor(public authService: AuthService, private router: Router) { }

    onLogOut(): void {
        this.authService.logout();
        this.router.navigate([""]);
    }
}
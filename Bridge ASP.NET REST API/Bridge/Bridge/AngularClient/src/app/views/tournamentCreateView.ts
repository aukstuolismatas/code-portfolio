﻿import { Component, OnInit } from "@angular/core";
import { TournamentService } from "../services/tournamentService";
import { Router } from "@angular/router";
import { Tournament } from "../shared/Tournament";

@Component({
    selector: "tournament-create",
    templateUrl: "tournamentCreateView.html",
    styleUrls: []
})
export class TournamentCreateView {
    constructor(private tournament: TournamentService, private router: Router) { }

    public tournamentItem: Tournament = {
        id: 0,
        name: "",
        date: new Date(Date.now())
    }
    public errorMessage = "";

    onTournamentCreate() {
        this.tournament.createTournaments(this.tournamentItem).subscribe(() => {
            this.router.navigate([""]);
        }, error => {
            console.log(error);
            this.errorMessage = "Failed to create";
        })
    }

}
﻿using CarServicePlatform.Entities;
using CarServicePlatform.Entities.RepairServices;

namespace CarServicePlatform.Data.Mappings;

public static class RepairServiceMapperExtentions
{
    public static RequestedRepairService ToRequestedRepairService(this RepairService repairService, int requestId)
    {
        return new()
        {
            RepairServiceId = repairService.Id,
            RepairService = repairService,
            ClientRequestId = requestId,
        };
    }

    public static AvailableRepairService ToAvailableRepairService(this RepairService repairService, string userId, City city)
    {
        return new()
        {
            RepairServiceId = repairService.Id,
            RepairService = repairService,
            City = city,
            DomainUserId = userId
        };
    }
}
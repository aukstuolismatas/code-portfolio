﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CarServicePlatform.Entities.Request;
using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Entities;

public class FileInformation
{
    [Required]
    public int Id { get; set; }

    [Required]
    public string Path { get; set; }

    [Required]
    public string Name { get; set; }

    [Required]
    public string FileExtension { get; set; }

    [Required]
    public double Size { get; set; }

    [Required]
    public FileType FileType { get; set; }

    [Required]
    public string DomainUserId { get; set; }

    [NotMapped]
    public byte[] FileContent { get; set; }

    [NotMapped]
    public string FileBase64 { get; set; }

    public DomainUser DomainUser { get; set; }

    public int? ClientRequestId { get; set; }
    public ClientRequest? ClientRequest { get; set; }
}
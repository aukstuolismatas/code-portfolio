import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestWaitingClientAcceptanceComponent } from './request-waiting-client-acceptance.component';

describe('RequestWaitingClientAcceptanceComponent', () => {
  let component: RequestWaitingClientAcceptanceComponent;
  let fixture: ComponentFixture<RequestWaitingClientAcceptanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestWaitingClientAcceptanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestWaitingClientAcceptanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

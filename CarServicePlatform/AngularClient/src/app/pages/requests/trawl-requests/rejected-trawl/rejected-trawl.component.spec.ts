import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectedTrawlComponent } from './rejected-trawl.component';

describe('RejectedTrawlComponent', () => {
  let component: RejectedTrawlComponent;
  let fixture: ComponentFixture<RejectedTrawlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RejectedTrawlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectedTrawlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

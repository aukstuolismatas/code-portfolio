import csv
COMPANIES_FILE = "companies_info.csv"
FILTERED_OUTPUT_FILE = "my_file_without_dupes.csv"
RESULT_FILE='../sentiment_analyzes/companies_info_with_reviews.csv'
fieldnames = ['Firmy.cz', 'City', 'Company', 'Category', 'Website', 'Phone', 'Email', 'Rate', 'Reviews']

def delete_dublicated_emails():
    with open(COMPANIES_FILE, 'r', newline='', encoding="utf-8") as input_file:
        with open(FILTERED_OUTPUT_FILE, 'w', newline='', encoding="utf-8") as output_file:
            duplicate_reader = csv.DictReader(input_file, fieldnames=fieldnames, delimiter=',')
            unique_write = csv.DictWriter(output_file, fieldnames=fieldnames, delimiter=',')
            keys_read = []
            for row in duplicate_reader:
                key = (row['Email'])
                if key not in keys_read:
                    keys_read.append(key)
                    unique_write.writerow(row)
def merge_reviews():
    for i in range(len(companies_sorted_list)-1,-1,-1):
        if companies_sorted_list[i-1]['Company']==companies_sorted_list[i]['Company'] :
            for j in range(i-1,-1,-1):
                if companies_sorted_list[j]['Company']==companies_sorted_list[i]['Company']:
                    temp=companies_sorted_list.pop(j)['Reviews']
                    for item in temp:
                        print(j)
                        companies_sorted_list[i]['Reviews'].append(item)
                else:
                    break

def read_file():
    with open(COMPANIES_FILE, 'r', newline='', encoding="utf-8") as inputfile:
        reader=csv.DictReader(inputfile, fieldnames=fieldnames, delimiter=',')
        data=list(reader)

    # Converting reviews to python array
    for i in range(len(data)-1,-1,-1 ):
        if data[i]['Reviews']=='[]':
            del data[i]
        else:
            arr = data[i]['Reviews'].strip('[\'“').strip('”\']').split('”\', \'“')
            data[i]['Reviews']=arr
    return data

def write_to_file():
    with open(RESULT_FILE, 'w', newline='', encoding="utf-8") as outputfile:
        writer=csv.DictWriter(outputfile, fieldnames=fieldnames, delimiter=',')
        for item in companies_sorted_list:
            writer.writerow(item)

# Use this function in case there is needed non dublicate email in the list
# delete_dublicated_emails()
companies_list=read_file()
companies_sorted_list=sorted(companies_list, key=lambda x: x['Company'],reverse=True)
merge_reviews()
write_to_file()

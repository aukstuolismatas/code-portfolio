﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Entities.Users;

public class TrawlAccount : DomainUser
{
    public int MinimalPrice { get; set; }
    public string? CompanyName { get; set; }
    public string? CompanyCode { get; set; }

    [EnumDataType(typeof(City))]
    public City? City { get; set; }
}
import { Component, OnInit } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { RequestStatusConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CategoriesService } from 'src/app/services/categories.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FileService } from 'src/app/services/file.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { ApprovedClientRequest, PriceOffer } from 'src/app/shared/client-request';
import { RepairService } from 'src/app/shared/repair-services';
import { RequestStatus } from 'src/app/shared/request-status';

@Component({
  selector: 'app-assigned-to-service-request-details',
  templateUrl: './assigned-to-service-request-details.component.html',
  styleUrls: ['./assigned-to-service-request-details.component.css']
})
export class AssignedToServiceRequestDetailsComponent implements OnInit {

  index:number=0;
  requestId:number=0;
  imageIndex:number=0;
  assignedClientRequestDetail!: ApprovedClientRequest;
  repairServices: RepairService[]=[]
  priceOffers:PriceOffer[]=[]
  imagesArray: any[]=[]
  imageUrl: SafeUrl | string =''

  successMessage=''
  errorMessage=''
  descriptionTab=true;
  processTab=false;
  viewerOpen=false;
  
  constructor(private activatedRoute: ActivatedRoute, private dataSharingService: DataSharingService, private repairRequestService: RepairRequestService, 
    public fileService: FileService, private categoryService: CategoriesService, private authService: AuthService) { }

  ngOnInit(): void {
    this.index = this.activatedRoute.snapshot.params['index'];
    this.requestId = this.activatedRoute.snapshot.params['id'];
    this.assignedClientRequestDetail = this.dataSharingService.approvedRequestList.value[this.index];
    if(this.assignedClientRequestDetail===undefined){
      this.repairRequestService.retrieveAssignedClientRequestById(this.requestId).subscribe((data:ApprovedClientRequest)=>this.assignedClientRequestDetail=data);
    }
    this.fileService.downloadRequestImages(this.requestId).subscribe(data=> {
      this.imagesArray=data      
    })
    this.categoryService.loadRepairServicesForClientRequest(this.requestId).subscribe((data:RepairService[]) => this.repairServices = data)
  }

  description(){
    this.descriptionTab=true;
    this.processTab = false
  }

  process(){
    this.descriptionTab=false;
    this.processTab = true;
  }
  isArrived(){
    return this.assignedClientRequestDetail.status==RequestStatusConstants.Arrived
  }

  isInProgress(){
    return this.assignedClientRequestDetail.status==RequestStatusConstants.InProgress
  }

  start(){
    this.repairRequestService.startClientRequest(this.requestId).subscribe({
      next: (data:ApprovedClientRequest) => {
        this.assignedClientRequestDetail.status=data.status;
        this.successMessage = MessageConstants.InProgressClientRequestSuccess
      },
      error: (err) => this.errorMessage = MessageConstants.ClientRequestUpdateFailed
    })
    this.successMessage='';
    this.errorMessage='';
  }

  end(){
    this.repairRequestService.endClientRequest(this.requestId).subscribe({
      next: (data:ApprovedClientRequest) => {
        this.assignedClientRequestDetail.status=data.status;
        this.successMessage = MessageConstants.DoneClientRequestSuccess
      },
      error: (err) => this.errorMessage = MessageConstants.ClientRequestUpdateFailed
    })
  }
  
  getStatusDescription(enumString:string){
    return (<any>RequestStatus)[enumString];
  }

}

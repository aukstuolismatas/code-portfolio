import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { RegisteredClientRequest, RejectedClientRequest } from 'src/app/shared/client-request';

@Component({
  selector: 'app-ended-service-requests',
  templateUrl: './ended-service-requests.component.html',
  styleUrls: ['./ended-service-requests.component.css']
})
export class EndedServiceRequestsComponent implements OnInit {
  tabName:string=HeaderConstants.InActiveTab;
  frontTitle:string='Nepatvirtinti užsakymai'
  titles:string[]=['Miestas', 'Data', 'Trukmė', 'Kaina','Statusas']
  rejectedRepairRequestList:RejectedClientRequest[]=[];

  constructor(private repairRequestService: RepairRequestService, private dataSharingService: DataSharingService, private router: Router) {
    dataSharingService.rejectedRequestList.subscribe(value => this.rejectedRepairRequestList = value);
      this.repairRequestService.retrieveAllRejectedClientRequestForService().subscribe((data:RejectedClientRequest[])=>{
        this.dataSharingService.rejectedRequestList.next(data)
      })
   }

  ngOnInit(): void {
  }

  // openItem(index:number){
  //   this.router.navigate([`${AppConstants.AllClientRequest}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  // }
}

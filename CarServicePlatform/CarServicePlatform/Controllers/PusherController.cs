﻿using System.Diagnostics.CodeAnalysis;
using System.Security.Claims;
using CarServicePlatform.Auth.Model;
using CarServicePlatform.Entities.Chat;
using CarServicePlatform.Repository;
using CarServicePlatform.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PusherServer;
using static System.Int32;

namespace CarServicePlatform.Controllers;

[ApiController]
[Route("/pusher/")]
// [Authorize]
public class PusherController : ControllerBase
{
    private IConversationRepository _conversationRepository;

    public PusherController(IConversationRepository conversationRepository)
    {
        _conversationRepository = conversationRepository;
    }
    // public async Task<ActionResult<ConversationDto>> SendMessage(CreateConversationDto createConversationDto)

    // [HttpPost]
    // [Route("pusher/send")]
    // public async Task<ActionResult> SendMessage(CreateConversationDto createConversationDto)
    // {
    //     string senderUserId = User.FindFirstValue(CustomClaims.UserId);
    //     Conversation conversation = _mapper.Map<Conversation>(createConversationDto);
    //     conversation.SenderId = senderUserId;
    //     await _conversationRepository.InsertConversationAsync(conversation).ConfigureAwait(false);
    //     string conversationChannel = "chat"; //"private-chat-" + conversation.ReceiverId + "-" + senderUserId;
    //     await _pusher.TriggerAsync(conversationChannel, "message", new
    //                  {
    //                      receiverId = conversation.ReceiverId,
    //                      message = conversation.Message
    //                  })
    //                  .ConfigureAwait(false); //new TriggerOptions { SocketId = createConversationDto.SocketId }).ConfigureAwait(false);
    //     return Created("/sendMessage", _mapper.Map<ConversationDto>(conversation));
    // }

    [HttpPost("auth")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public async Task<IActionResult> ChannelAuth([FromForm] string channel_name, [FromForm] string socket_id)
    {
        int groupId;
        try
        {
            groupId = Parse(channel_name.Replace("private-", ""));
        }
        catch (FormatException e)
        {
            return BadRequest(e.Message);
        }

        // string currentUserId = User.FindFirstValue(CustomClaims.UserId);
        // ConversationUser? isUserInGroup = await _conversationRepository.GetConversationUserWhichAssignedToGroup(groupId, currentUserId).ConfigureAwait(false);
        //
        // if (isUserInGroup == null)
        // {
        //     return Forbid();
        // }

        Pusher pusher = PusherService.PusherInitialization();
        string? auth = pusher.Authenticate(channel_name, socket_id).ToJson();
        return Ok(auth);
    }
}

// if (!User.Identity.IsAuthenticated)
// {
//     return new ContentResult { Content = "Access forbidden", ContentType = "application/json" };
// }

// PresenceChannelData channelData = new()
// {
//     user_id = currentUserId,
//     user_info = new
//     {
//         name = "Allways MATAS"
//     }
// };
//
// if (channelName.IndexOf(currentUserId, StringComparison.Ordinal) == -1)
// {
//     return Task.FromResult<IActionResult>(Forbid());
// }
//
// IAuthenticationData? auth = _pusher.Authenticate(channelName, socketId, channelData);
// return Task.FromResult<IActionResult>(Ok(auth));
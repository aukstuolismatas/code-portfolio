﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.Auth;

public record RegisterServiceUserDto([Required] string CompanyName, [Required] string Name, [Required] string Surname, [Required][EmailAddress] string Email,
    [Required] string Password, [Required][Phone] string PhoneNumber, [Required] string CompanyCode, [Required] string Address,
    [Required] string City, [Required] Dictionary<int, int> AvailableRepairServiceIds);
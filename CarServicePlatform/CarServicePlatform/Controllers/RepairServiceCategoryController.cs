﻿using AutoMapper;
using CarServicePlatform.Auth;
using CarServicePlatform.Data.Dtos.RepairServiceCategory;
using CarServicePlatform.Entities.RepairServices;
using CarServicePlatform.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarServicePlatform.Controllers;

[ApiController]
[Authorize(Roles = AppUserRoles.Admin)]
[Route("/categories")]
public class RepairServiceCategoryController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IRepairServiceCategoryRepository _repairServiceCategoryRepository;

    public RepairServiceCategoryController(IRepairServiceCategoryRepository repairServiceCategoryRepository, IMapper mapper)
    {
        _repairServiceCategoryRepository = repairServiceCategoryRepository;
        _mapper = mapper;
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IEnumerable<RepairServiceCategoryDto>> GetAllCategories()
    {
        List<RepairServiceCategory> categories = await _repairServiceCategoryRepository.GetAllCategoriesAsync().ConfigureAwait(false);
        return categories.Select(x => _mapper.Map<RepairServiceCategoryDto>(x));
    }

    [HttpGet("{id}")]
    [AllowAnonymous]
    public async Task<ActionResult<RepairServiceCategoryDto>> GetCategory(int id)
    {
        RepairServiceCategory? category = await _repairServiceCategoryRepository.GetCategoryAsync(id).ConfigureAwait(false);
        if (category == null)
        {
            return NotFound($"Category with id {id} was not found");
        }

        return Ok(_mapper.Map<RepairServiceCategoryDto>(category));
    }

    [HttpPost]
    public async Task<ActionResult<RepairServiceCategoryDto>> InsertCategory(CreateRepairServiceCategoryDto createRepairServiceCategoryDto)
    {
        RepairServiceCategory category = _mapper.Map<RepairServiceCategory>(createRepairServiceCategoryDto);
        await _repairServiceCategoryRepository.InsertCategoryAsync(category).ConfigureAwait(false);
        return Created($"/categories/{category.Id}", _mapper.Map<RepairServiceCategoryDto>(category));
    }

    [HttpPut("{id}")]
    public async Task<ActionResult<RepairServiceCategoryDto>> UpdateCategory(int id, UpdateRepairServiceCategoryDto updateRepairServiceCategoryDto)
    {
        RepairServiceCategory? category = await _repairServiceCategoryRepository.GetCategoryAsync(id).ConfigureAwait(false);
        if (category == null)
        {
            return NotFound($"Category with id {id} was not found");
        }

        _mapper.Map(updateRepairServiceCategoryDto, category);
        await _repairServiceCategoryRepository.UpdateCategoryAsync(category).ConfigureAwait(false);
        return Ok(_mapper.Map<RepairServiceCategoryDto>(category));
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<RepairServiceCategoryDto>> DeleteCategory(int id)
    {
        RepairServiceCategory? category = await _repairServiceCategoryRepository.GetCategoryAsync(id).ConfigureAwait(false);
        if (category == null)
        {
            return NotFound($"Category with id {id} was not found");
        }

        await _repairServiceCategoryRepository.DeleteCategoryAsync(category).ConfigureAwait(false);
        return NoContent();
    }
}
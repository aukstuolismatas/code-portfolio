﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarServicePlatform.Entities.Users;

public class ServiceAccount : DomainUser
{
    public string CompanyName { get; set; }
    public string CompanyCode { get; set; }
    public string Address { get; set; }

    [NotMapped]
    [EnumDataType(typeof(City))]
    public City? City { get; set; }
}
﻿namespace CarServicePlatform.Data.Dtos.TransferOffer;

public record TransferOfferDto(int Id, int Price, string DateAndTime, string Status, string TrawlAccountId, int ClientRequestId);
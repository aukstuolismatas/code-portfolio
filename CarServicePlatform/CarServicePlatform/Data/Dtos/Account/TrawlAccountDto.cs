﻿using CarServicePlatform.Entities;

namespace CarServicePlatform.Data.Dtos.Account;

public record TrawlAccountDto(string Id, string Name, string Surname, int MinimalPrice, string CompanyName, string CompanyCode, City City);
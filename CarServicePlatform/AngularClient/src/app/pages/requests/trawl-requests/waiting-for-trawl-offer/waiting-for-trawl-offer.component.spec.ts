import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingForTrawlOfferComponent } from './waiting-for-trawl-offer.component';

describe('WaitingForTrawlOfferComponent', () => {
  let component: WaitingForTrawlOfferComponent;
  let fixture: ComponentFixture<WaitingForTrawlOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WaitingForTrawlOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingForTrawlOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import 'package:cupertino_practice_ios/bmi_screen.dart';
import 'package:cupertino_practice_ios/settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      home: HomeScreen(),
      localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
        DefaultMaterialLocalizations.delegate
      ],
    );
  }
}

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return CupertinoTabScaffold(
      tabBar: CupertinoTabBar(items: [
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.person), label: 'BMI'),
        BottomNavigationBarItem(
            icon: Icon(CupertinoIcons.settings_solid), label: 'Settings'),
      ]),
      tabBuilder: (BuildContext context, int index) {
        if (index == 0) {
          return BmiScreen();
        } else {
          return Settings();
        }
      },
    );
  }
}

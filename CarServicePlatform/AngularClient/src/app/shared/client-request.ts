
export interface ClientRequest {
    description: string;
    city: number;
    address: string;
    requestedServiceIds: {[key: string]: number}
}
export interface RegisteredClientRequest{
    clientRequestId: number;
    title: string;
    description: string;
    address: string;
    city: string;
    created: Date;
    status: string;
    requestedServices: {
        id: number;
        repairServiceId: number;
        clientRequestId: number;
    }
}

export interface RegisteredClientRequestTrawl{
    clientRequestId: number;
    title: string;
    description: string;
    address: string;
    city: string;
    created: Date;
    status: string;
    serviceAddress: string;
    serviceCompanyName: string;
}

export interface PlacePriceOffer{
    price: number;
    clientRequestId: number;
    duration: number;
}

export interface UpdatePriceOffer{
    clientRequestId: number,
}

export interface PriceOffer{
    id: number;
    status: string;
    serviceAccountId: string;
    price: number;
    duration: number;
    clientRequestId: number
}

export interface PlaceTransferOffer{
    price: number;
    clientRequestId: number;
    dateAndTime: string;
}

export interface UpdateTransferOffer{
    clientRequestId: number,
}

export interface TransferOffer{
    id: number;
    status: string;
    trawlAccountId: string;
    price: number;
    dateAndTime: string;
    clientRequestId: number
}

export interface ApprovedClientRequest{
    clientRequestId: number;
    title: string;
    description: string;
    address: string;
    city: string;
    created: string;
    status: string;
    price: number;
    duration: number
}

export interface ApprovedClientRequestTrawl{
    clientRequestId: number;
    title: string;
    description: string;
    address: string;
    city: string;
    created: string;
    status: string;
    price: number;
    arrivalTime: string;
    offerStatus: string;
    serviceCompanyName: string;
    serviceAddress: string;
    servicePhone: string;
    clientPhone: string;
}

export interface RejectedClientRequest{
    city: string;
    created: string;
    offerStatus: string;
    price: number;
    duration: number
}

export interface RejectedClientRequestTrawl{
    city: string;
    created: string;
    offerStatus: string;
    price: number;
    arrivalTime: string
}
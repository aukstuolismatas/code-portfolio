import 'package:layout_practice/screens/contacts_screen.dart';

class ArtUtil {
  static const CARAVAGGIO = "Caravaggio";
  static const MONET = "Monet";
  static const VANGOGH = "Van Gogh";
  static const IMG_CARAVAGGIO =
      'https://images.unsplash.com/flagged/photo-1580079942799-b27f909bc765?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=997&q=80';
  static const IMG_MONET =
      'https://images.unsplash.com/photo-1530376918024-3ebe0672889c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80';
  static const IMG_VANGOGH =
      'https://images.unsplash.com/photo-1585416835650-252af4e43388?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1117&q=80';

  static const List<String> menuItems = [CARAVAGGIO, MONET, VANGOGH];
  static const List<String> paths = [IMG_CARAVAGGIO, IMG_MONET, IMG_VANGOGH];
}

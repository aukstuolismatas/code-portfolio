﻿namespace Bridge.Data.Dtos.Pair
{
    public record PairDto(int Id, string Player1, string Player2, int Place);
}
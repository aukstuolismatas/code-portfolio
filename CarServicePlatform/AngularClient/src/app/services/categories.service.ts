import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiConstants } from '../api-constants';
import { RepairService } from '../shared/repair-services';
import { AuthService } from './auth/auth.service';
import { ErrorHandleService } from './error-handle.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient, private errorHandle: ErrorHandleService, private authService: AuthService) { }

  loadRepairServices(): Observable<RepairService[]>{
    return this.http
    .get<RepairService[]>(environment.apiUrl + ApiConstants.repairServiceGroupByCategories)
    .pipe(map((data:RepairService[]) => data), catchError(this.errorHandle.errorHandle));
  }

  loadRepairServicesForClientRequest(clientRequestId: number): Observable<RepairService[]>{
    return this.http
    .get<RepairService[]>(environment.apiUrl + ApiConstants.repairServiceGroupByCategories + clientRequestId)
    .pipe(map((data:RepairService[]) => data), catchError(this.errorHandle.errorHandle));
  }

  loadRepairServicesAvailableForService(userId: string | null): Observable<RepairService[]>{
    return this.http
    .get<RepairService[]>(environment.apiUrl + ApiConstants.repairServiceAvailableForService + userId)
    .pipe(map((data:RepairService[]) => data), catchError(this.errorHandle.errorHandle));
  }
}

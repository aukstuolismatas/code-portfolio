import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import TournamentListView from './views/tournamentListView';
import TableListView from './views/tableListView';
import { TournamentCreateView } from './views/tournamentCreateView';
import { TournamentEditView } from './views/tournamentEditView';
import { TableCreateView } from './views/tableCreateView';
import Header  from './views/header';
import Footer  from './views/footer';
import Content  from './views/content';
import BoardListView from './views/boardListView';
import { TournamentPage } from './pages/tournamentPage';
import { TournamentCreatePage } from './pages/tournamentCreatePage';
import { TournamentService } from "./services/tournamentService";
import { AuthService } from "./services/authService";
import { AuthInterceptor } from "./services/authInterceptor";
import { JwtService } from "./services/jwtService";
import { Router } from './router';
import {BoardPage} from "./pages/boardPage";
import { LoginPage } from "./pages/loginPage";
import { AuthActivator } from "./services/authActivatorService";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

@NgModule(({
    declarations: [
        AppComponent,
        TournamentListView,
        TableListView,
        TableCreateView,
        TournamentCreateView,
        TournamentEditView,
        BoardListView,
        TournamentPage,
        TournamentCreatePage,
        BoardPage,
        LoginPage,
        Header,
        Footer,
        Content
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        Router,
        FormsModule,
        BrowserAnimationsModule
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }, JwtService,
        TournamentService,
        AuthActivator,
        AuthService
        ],
    bootstrap: [AppComponent]
}) as any)
export class AppModule { }

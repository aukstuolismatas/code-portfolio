﻿namespace CarServicePlatform.Data.Dtos.ClientRequest;

public record RejectedTrawlClientRequestDto(string City, string Created, string OfferStatus, int Price, DateTime ArrivalTime);
﻿using CarServicePlatform.Entities;

namespace CarServicePlatform.Repository;

public interface IFileInformationRepository
{
    Task<FileInformation?> GetFileInformationAsync(int id, string userId);
    Task<FileInformation?> GetFileInformationForAdminAsync(int id);
    Task<FileInformation?> GetFileInformationOfLastProfileImageAsync(string userId);
    Task<List<FileInformation>> GetAllDocumentsInformationForUserAsync(string userId);
    Task<List<FileInformation>> GetAllFileInformationForClientRequestAsync(int clientRequestId);
    Task InsertFileInformationAsync(FileInformation fileInformation);
    Task InsertFileInformationListAsync(List<FileInformation> fileInformation);
}
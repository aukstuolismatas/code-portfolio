import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, retry, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiConstants } from '../api-constants';
import { ConversationGroup, ConversationMessage, CreateConversationGroup } from '../shared/conversation';
import { AuthService } from './auth/auth.service';
import { ErrorHandleService } from './error-handle.service';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  readonly conversationGroupUrl =environment.apiUrl + ApiConstants.conversationsGroups
  constructor(private http : HttpClient, private authService: AuthService, private errorHandle: ErrorHandleService) { }

  loadUserConversationGroups(): Observable<ConversationGroup[]>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ConversationGroup[]>(this.conversationGroupUrl + '/users/' + this.authService.getUserId(), {headers: headers})
    .pipe(retry(1), catchError(this.errorHandle.errorHandle));
  }

  loadConversationMessages(conversationGroupId : number) : Observable<ConversationMessage[]>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ConversationMessage[]>( this.conversationGroupUrl + `/${conversationGroupId}/messages`, {headers: headers})
    .pipe(retry(1), catchError(this.errorHandle.errorHandle));
  }

  createChat(chatData: CreateConversationGroup){
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .post<ConversationGroup>( this.conversationGroupUrl, chatData, {headers: headers})
    .pipe(map((data:ConversationGroup)=>data), catchError(this.errorHandle.errorHandle));
  }
}

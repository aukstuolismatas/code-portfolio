using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NT_Csharp.Models
{
    public class Agentas
    {
        [DisplayName("ID")]
        [Required]
        public int id { get; set; }
        [DisplayName("Vardas")]
        [Required]
        public string vardas { get; set; }
        [DisplayName("Pavardė")]
        [Required]
        public string pavarde { get; set; }
        [DisplayName("Telefonas")]
        [Required]
        public string telefonas { get; set; }
        [DisplayName("Pardavimų skaičius")]
        [Required]
        public int pardavimai { get; set; }
        [DisplayName("Agentura")]
        public string agentura { get; set; }


    }
}
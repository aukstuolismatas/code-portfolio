﻿using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Repository;

public interface IServiceRepository
{
    Task<Dictionary<string, ServiceAccount>> GetAllServiceAccountsAsync();
    Task<ServiceAccount?> GetServiceAccountAsync(string userId);
    Task UpdateServiceAccountAsync(ServiceAccount serviceAccount);
    Task DeleteServiceAccountAsync(ServiceAccount serviceAccount);
}
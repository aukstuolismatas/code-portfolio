﻿namespace CarServicePlatform.Data.Dtos.ClientRequest;

public record RejectedClientRequestDto(string City, string Created, string OfferStatus, int Price, int Duration);
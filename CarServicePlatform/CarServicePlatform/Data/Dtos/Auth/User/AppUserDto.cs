﻿namespace CarServicePlatform.Data.Dtos.Auth.User;

public record AppUserDto(string Id, string Email, string PhoneNumber, bool IsAccountActivated);
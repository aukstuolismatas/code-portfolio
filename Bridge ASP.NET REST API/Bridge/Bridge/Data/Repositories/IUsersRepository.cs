﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;

namespace Bridge.Data.Repositories
{
    public interface IUsersRepository
    {
        Task<List<User>> GetAllAsync();
        Task<User> GetAsync(string username);
        Task InsertAsync(User user);
        Task UpdateAsync(User user);
        Task DeleteAsync(User user);
    }
}
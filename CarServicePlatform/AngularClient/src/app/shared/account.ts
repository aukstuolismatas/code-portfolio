import { City } from "./city";

export interface Account {
}

export interface ClientAccount{
    email: string;
    phoneNumber: string;
    domainUser:{
        name: string;
        surname: string;
    }
}
export interface UpdateClientAccount{
    email: string;
    phoneNumber: string;
    name: string;
    surname: string;
    currentPassword: string | undefined;
    newPassword: string | undefined;
}

export interface UpdateServiceAccount{
    email: string;
    phoneNumber: string;
    name: string;
    surname: string;
    address: string;
    city: string;
    currentPassword: string | undefined;
    newPassword: string | undefined;
}

export interface UserAccount{
    id: string;
    email: string;
    phoneNumber: string;
    isAccountActivated: boolean;
}
export interface ServiceAccount{
    id: string;
    email: string;
    phoneNumber: string;
    isAccountActivated: boolean;
    domainUser:{
        id: string;
        name: string;
        surname: string;
        companyCode: string;
        companyName: string;
        address: string;
    }
}

export interface TrawlAccount{
    id: string;
    email: string;
    phoneNumber: string;
    isAccountActivated: boolean;
    domainUser:{
        id: string;
        name: string;
        surname: string;
        companyCode: string;
        companyName: string;
        minimalPrice: string;
        city: City;
    }
}
export interface UpdateTrawlAccount{
    email: string;
    phoneNumber: string;
    name: string;
    surname: string;
    city: string;
    minimalPrice: number;
    currentPassword: string | undefined;
    newPassword: string | undefined;
}
﻿namespace CarServicePlatform.Entities.Chat;

public class Conversation
{
    public Conversation()
    {
        Status = MessageStatus.Sent;
        CreatedAt = DateTime.Now;
    }

    public enum MessageStatus
    {
        Sent,
        Delivered
    }

    public int Id { get; set; }
    public string SenderId { get; set; }
    public string ReceiverId { get; set; }
    public string Message { get; set; }
    public MessageStatus Status { get; set; }
    public DateTime CreatedAt { get; set; }
}
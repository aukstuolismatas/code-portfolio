﻿using CarServicePlatform.Entities.RepairServices;

namespace CarServicePlatform.Repository;

public interface IAvailableRepairServiceRepository
{
    Task<List<AvailableRepairService>> GetAllAvailableRepairServiceForServiceUserAsync(string userId);
    Task<List<AvailableRepairService>> GetAllAvailableRepairServiceAsync();
    Task InsertAvailableRepairServiceAsync(List<AvailableRepairService> availableRepairServices);
    Task UpdateAvailableRepairServiceAsync(List<AvailableRepairService> availableRepairServices);
    Task DeleteAvailableRepairServiceAsync(List<AvailableRepairService> availableRepairServices);
}
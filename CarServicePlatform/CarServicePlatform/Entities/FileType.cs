﻿namespace CarServicePlatform.Entities;

public enum FileType
{
    Profile,
    Request,
    Document
}
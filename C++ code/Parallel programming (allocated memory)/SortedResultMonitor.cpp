#include "SortedResultMonitor.h"
#include <omp.h>
#include <vector>
#include <iostream>

SortedResultMonitor::SortedResultMonitor(int n)
{
    resultArray = new HDDWithComputedValue * [n];
    counter = 0;
    lock =  new omp_lock_t;
    omp_init_lock(lock);
}
void SortedResultMonitor::addHDDSorted(HDDWithComputedValue* item)
{
    double computed = item->computedData;
    omp_set_lock(lock);
    
    if (counter == 0)
    {
        resultArray[0] = item;
        counter++;
    }
    else
    {
        for (int i = 0; i < counter; i++)
        {
            if (i == counter - 1)
            {

                if (computed > resultArray[i]->computedData)
                {
                    resultArray[counter] = item;
                }
                else
                {

                    resultArray[counter] = resultArray[counter - 1];
                    resultArray[counter - 1] = item;
                }
                break;
            }
            else if (computed <= resultArray[i]->computedData)
            {
                for (int j = counter; j > i; j--)
                {
                    resultArray[j] = resultArray[j - 1];
                }
                resultArray[i] = item;
                break;
            }
        }
        counter++;
    }
    omp_unset_lock(lock);
}
std::vector<HDDWithComputedValue*> SortedResultMonitor::getItems()
{
    std::vector<HDDWithComputedValue*> result = std::vector<HDDWithComputedValue*>();
    for (int i = 0; i < counter; i++)
    {
        result.push_back(resultArray[i]);
    }
    return result;
}

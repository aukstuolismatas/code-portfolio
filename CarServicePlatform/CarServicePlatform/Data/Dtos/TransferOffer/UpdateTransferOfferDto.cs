﻿namespace CarServicePlatform.Data.Dtos.TransferOffer;

public record UpdateTransferOfferDto(int ClientRequestId);
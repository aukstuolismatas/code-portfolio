﻿using AutoMapper;
using CarServicePlatform.Auth;
using CarServicePlatform.Auth.Model;
using CarServicePlatform.Data.Dtos.Account;
using CarServicePlatform.Data.Dtos.Auth.User;
using CarServicePlatform.Entities;
using CarServicePlatform.Entities.RepairServices;
using CarServicePlatform.Entities.Users;
using CarServicePlatform.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarServicePlatform.Controllers;

[ApiController]
[Authorize]
[Route("/account/")]
public class AccountController : ControllerBase
{
    private readonly UserManager<AppUser> _userManager;
    private readonly IAuthorizationService _authorizationService;
    private readonly IMapper _mapper;
    private readonly IClientRepository _clientRepository;
    private readonly IServiceRepository _serviceRepository;
    private readonly ITrawlRepository _trawlRepository;
    private readonly IAvailableRepairServiceRepository _availableRepairServiceRepository;

    public AccountController(UserManager<AppUser> userManager, IAuthorizationService authorizationService, IMapper mapper, IClientRepository clientRepository, IServiceRepository serviceRepository, IAvailableRepairServiceRepository availableRepairServiceRepository, ITrawlRepository trawlRepository)
    {
        _userManager = userManager;
        _authorizationService = authorizationService;
        _mapper = mapper;
        _clientRepository = clientRepository;
        _serviceRepository = serviceRepository;
        _availableRepairServiceRepository = availableRepairServiceRepository;
        _trawlRepository = trawlRepository;
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpGet("clients")]
    public async Task<IEnumerable<AppClientUserDto>> GetAllClients()
    {
        List<AppUser> users = _userManager.Users.ToList();
        Dictionary<string, ClientAccount> clientAccounts = await _clientRepository.GetAllClientAccountsAsync().ConfigureAwait(false);
        List<AppUser> sorted = new();
        foreach (AppUser user in users.Where(user => clientAccounts.ContainsKey(user.Id)))
        {
            user.DomainUser = clientAccounts[user.Id];
            sorted.Add(user);
        }

        return sorted.Select(x => _mapper.Map<AppClientUserDto>(x));
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpGet("services")]
    public async Task<IEnumerable<AppServiceUserDto>> GetAllServices()
    {
        List<AppUser> users = _userManager.Users.ToList();
        Dictionary<string, ServiceAccount> serviceAccounts = await _serviceRepository.GetAllServiceAccountsAsync().ConfigureAwait(false);
        List<AppUser> sorted = new();
        foreach (AppUser user in users.Where(user => serviceAccounts.ContainsKey(user.Id)))
        {
            user.DomainUser = serviceAccounts[user.Id];
            sorted.Add(user);
        }

        return sorted.Select(x => _mapper.Map<AppServiceUserDto>(x));
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpGet("trawls")]
    public async Task<IEnumerable<AppTrawlUserDto>> GetAllTrawl()
    {
        List<AppUser> users = _userManager.Users.ToList();
        Dictionary<string, TrawlAccount> trawlAccounts = await _trawlRepository.GetAllTrawlAccountsAsync().ConfigureAwait(false);
        List<AppUser> sorted = new();
        foreach (AppUser user in users.Where(user => trawlAccounts.ContainsKey(user.Id)))
        {
            user.DomainUser = trawlAccounts[user.Id];
            sorted.Add(user);
        }

        return sorted.Select(x => _mapper.Map<AppTrawlUserDto>(x));
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpGet("admins/{userId}")]
    public async Task<ActionResult<AppClientUserDto>> GetAdmin(string userId)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        AdministratorAccount? administratorAccount = await _clientRepository.GetAdminAccountAsync(userId).ConfigureAwait(false);
        if (administratorAccount == null)
        {
            return NotFound($"Admin with id {userId} was not found");
        }

        user.DomainUser = administratorAccount;
        return Ok(_mapper.Map<AppClientUserDto>(user));
    }

    [Authorize(Roles = AppUserRoles.Client)]
    [HttpGet("clients/{userId}")]
    public async Task<ActionResult<AppClientUserDto>> GetClient(string userId)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        ClientAccount? clientAccount = await _clientRepository.GetClientAccountAsync(userId).ConfigureAwait(false);
        if (clientAccount == null)
        {
            return NotFound($"Client with id {userId} was not found");
        }

        user.DomainUser = clientAccount;
        return Ok(_mapper.Map<AppClientUserDto>(user));
    }

    [Authorize(Roles = AppUserRoles.Service)]
    [HttpGet("services/{userId}")]
    public async Task<ActionResult<AppServiceUserDto>> GetService(string userId)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        ServiceAccount? serviceAccount = await _serviceRepository.GetServiceAccountAsync(userId).ConfigureAwait(false);
        if (serviceAccount == null)
        {
            return NotFound($"Service with id {userId} was not found");
        }

        user.DomainUser = serviceAccount;
        return Ok(_mapper.Map<AppServiceUserDto>(user));
    }

    [Authorize(Roles = AppUserRoles.Trawl)]
    [HttpGet("trawls/{email}")]
    public async Task<ActionResult<AppTrawlUserDto>> GetTrawl(string email)
    {
        AppUser user = await _userManager.FindByEmailAsync(email).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with email {email} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        TrawlAccount? trawlAccount = await _trawlRepository.GetTrawlAccountAsync(user.Id).ConfigureAwait(false);
        if (trawlAccount == null)
        {
            return NotFound($"Service with id {user.Id} was not found");
        }

        user.DomainUser = trawlAccount;
        return Ok(_mapper.Map<AppTrawlUserDto>(user));
    }

    [Authorize(Roles = AppUserRoles.Client)]
    [HttpPut("clients/{userId}")]
    public async Task<ActionResult<AppClientUserDto>> UpdateClient(string userId, UpdateClientAccountDto updateClientAccountDto)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        ClientAccount? clientAccount = await _clientRepository.GetClientAccountAsync(userId).ConfigureAwait(false);
        if (clientAccount == null)
        {
            return NotFound($"Client with id {userId} was not found");
        }

        _mapper.Map(updateClientAccountDto, user);
        _mapper.Map(updateClientAccountDto, clientAccount);
        user.DomainUser = clientAccount;
        user.UserName = (updateClientAccountDto.Email).Split('@')[0];
        await _userManager.UpdateAsync(user).ConfigureAwait(false);
        if (!string.IsNullOrEmpty(updateClientAccountDto.CurrentPassword) && !string.IsNullOrEmpty(updateClientAccountDto.NewPassword))
        {
            IdentityResult? result = await _userManager.ChangePasswordAsync(user, updateClientAccountDto.CurrentPassword, updateClientAccountDto.NewPassword).ConfigureAwait(false);
            if (!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }
        }

        return Ok(_mapper.Map<AppClientUserDto>(user));
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpPut("admins/{userId}")]
    public async Task<ActionResult<AppClientUserDto>> UpdateAdmin(string userId, UpdateClientAccountDto updateClientAccountDto)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        AdministratorAccount? administratorAccount = await _clientRepository.GetAdminAccountAsync(userId).ConfigureAwait(false);
        if (administratorAccount == null)
        {
            return NotFound($"Admin with id {userId} was not found");
        }

        _mapper.Map(updateClientAccountDto, user);
        _mapper.Map(updateClientAccountDto, administratorAccount);
        user.DomainUser = administratorAccount;
        user.UserName = (updateClientAccountDto.Email).Split('@')[0];
        await _userManager.UpdateAsync(user).ConfigureAwait(false);
        if (!string.IsNullOrEmpty(updateClientAccountDto.CurrentPassword) && !string.IsNullOrEmpty(updateClientAccountDto.NewPassword))
        {
            IdentityResult? result = await _userManager.ChangePasswordAsync(user, updateClientAccountDto.CurrentPassword, updateClientAccountDto.NewPassword).ConfigureAwait(false);
            if (!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }
        }

        return Ok(_mapper.Map<AppClientUserDto>(user));
    }

    [Authorize(Roles = AppUserRoles.Service)]
    [HttpPut("services/{userId}")]
    public async Task<ActionResult<AppServiceUserDto>> UpdateService(string userId, UpdateServiceAccountDto updateServiceAccountDto)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        ServiceAccount? serviceAccount = await _serviceRepository.GetServiceAccountAsync(userId).ConfigureAwait(false);
        if (serviceAccount == null)
        {
            return NotFound($"Service with id {userId} was not found");
        }

        _mapper.Map(updateServiceAccountDto, user);
        _mapper.Map(updateServiceAccountDto, serviceAccount);
        user.UserName = (updateServiceAccountDto.Email).Split('@')[0];
        user.DomainUser = serviceAccount;
        await _userManager.UpdateAsync(user).ConfigureAwait(false);
        if (serviceAccount.City != null)
        {
            List<AvailableRepairService> availableRepairServices =
                await _availableRepairServiceRepository.GetAllAvailableRepairServiceForServiceUserAsync(userId).ConfigureAwait(false);
            availableRepairServices.ForEach(x => x.City = (City)serviceAccount.City);
            await _availableRepairServiceRepository.UpdateAvailableRepairServiceAsync(availableRepairServices).ConfigureAwait(false);
        }

        if (!string.IsNullOrEmpty(updateServiceAccountDto.CurrentPassword) && !string.IsNullOrEmpty(updateServiceAccountDto.NewPassword))
        {
            IdentityResult? result = await _userManager.ChangePasswordAsync(user, updateServiceAccountDto.CurrentPassword, updateServiceAccountDto.NewPassword).ConfigureAwait(false);
            if (!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }
        }

        return Ok(_mapper.Map<AppServiceUserDto>(user));
    }

    [Authorize(Roles = AppUserRoles.Trawl)]
    [HttpPut("trawls/{userId}")]
    public async Task<ActionResult<AppTrawlUserDto>> UpdateTrawl(string userId, UpdateTrawlAccountDto updateTrawlAccountDto)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        TrawlAccount? trawlAccount = await _trawlRepository.GetTrawlAccountAsync(userId).ConfigureAwait(false);
        if (trawlAccount == null)
        {
            return NotFound($"Trawl with id {userId} was not found");
        }

        _mapper.Map(updateTrawlAccountDto, user);
        _mapper.Map(updateTrawlAccountDto, trawlAccount);
        user.UserName = (updateTrawlAccountDto.Email).Split('@')[0];
        user.DomainUser = trawlAccount;
        await _userManager.UpdateAsync(user).ConfigureAwait(false);

        if (!string.IsNullOrEmpty(updateTrawlAccountDto.CurrentPassword) && !string.IsNullOrEmpty(updateTrawlAccountDto.NewPassword))
        {
            IdentityResult? result = await _userManager.ChangePasswordAsync(user, updateTrawlAccountDto.CurrentPassword, updateTrawlAccountDto.NewPassword).ConfigureAwait(false);
            if (!result.Succeeded)
            {
                return BadRequest(result.Errors);
            }
        }

        return Ok(_mapper.Map<AppTrawlUserDto>(user));
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpDelete("clients/{userId}")]
    public async Task<IActionResult> DeleteClient(string userId)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        ClientAccount? clientAccount = await _clientRepository.GetClientAccountAsync(userId).ConfigureAwait(false);
        if (clientAccount == null)
        {
            return NotFound($"Client with id {userId} was not found");
        }

        await _clientRepository.DeleteClientAccountAsync(clientAccount).ConfigureAwait(false);
        await _userManager.DeleteAsync(user).ConfigureAwait(false);
        return NoContent();
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpDelete("services/{userId}")]
    public async Task<IActionResult> DeleteService(string userId)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        ServiceAccount? serviceAccount = await _serviceRepository.GetServiceAccountAsync(userId).ConfigureAwait(false);
        if (serviceAccount == null)
        {
            return NotFound($"Service with id {userId} was not found");
        }

        await _serviceRepository.DeleteServiceAccountAsync(serviceAccount).ConfigureAwait(false);
        await _userManager.DeleteAsync(user).ConfigureAwait(false);
        return NoContent();
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpDelete("trawls/{userId}")]
    public async Task<IActionResult> DeleteTrawl(string userId)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return NotFound($"User with id {userId} was not found");
        }

        TrawlAccount? trawlAccount = await _trawlRepository.GetTrawlAccountAsync(userId).ConfigureAwait(false);
        if (trawlAccount == null)
        {
            return NotFound($"Trawl with id {userId} was not found");
        }

        await _trawlRepository.DeleteTrawlAccountAsync(trawlAccount).ConfigureAwait(false);
        await _userManager.DeleteAsync(user).ConfigureAwait(false);
        return NoContent();
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpPut("activate/{userId}")]
    public async Task<IActionResult> ActivateAccount(string userId)
    {
        return await AccountActivation(userId, true).ConfigureAwait(false);
    }

    [Authorize(Roles = AppUserRoles.Admin)]
    [HttpPut("deactivate/{userId}")]
    public async Task<IActionResult> DeactivateAccount(string userId)
    {
        return await AccountActivation(userId, false).ConfigureAwait(false);
    }

    private async Task<IActionResult> AccountActivation(string userId, bool isActive)
    {
        AppUser user = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (user == null)
        {
            return BadRequest("User does not exist");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, user.Id, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        user.IsAccountActivated = isActive;
        await _userManager.UpdateAsync(user).ConfigureAwait(false);
        return Ok(_mapper.Map<AppUserDto>(user));
    }
}
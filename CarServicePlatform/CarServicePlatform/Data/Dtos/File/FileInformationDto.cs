﻿namespace CarServicePlatform.Data.Dtos.File;

public record FileInformationDto(int Id, string Path, string Name, double Size, string FileExtension, string DomainUserId, int ClientRequestId);
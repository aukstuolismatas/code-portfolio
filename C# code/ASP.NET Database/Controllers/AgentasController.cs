using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Csharp.Repos;
using NT_Csharp.ViewModels;

namespace NT_Csharp.Controllers
{
    public class AgentasController : Controller
    {
        AgentasRepository agentasRepository = new AgentasRepository();
        NTAgenturaRepository nTAgenturaRepository = new NTAgenturaRepository();
        // GET: Agentas
        public ActionResult Index()
        {
            return View(agentasRepository.getAgentai());
        }

        // GET: Agentas/Create
        public ActionResult Create()
        {
            AgentasEditViewModel agentas = new AgentasEditViewModel();
            PopulateSelections(agentas);
            return View(agentas);
        }

        // POST: Agentas/Create
        [HttpPost]
        public ActionResult Create(AgentasEditViewModel collection)
        {
            try
            {
               if(ModelState.IsValid)
                {
                    agentasRepository.addAgentas(collection);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                PopulateSelections(collection);
                return View(collection);
            }
        }

        // GET: Agentas/Edit/5
        public ActionResult Edit(int id)
        {
            AgentasEditViewModel agentasEditViewModel = agentasRepository.getAgentas(id);
            PopulateSelections(agentasEditViewModel);
            return View(agentasEditViewModel);
        }

        // POST: Agentas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, AgentasEditViewModel collection)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    agentasRepository.updateAgentas(collection);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                PopulateSelections(collection);
                return View(collection);
            }
        }

        // GET: Agentas/Delete/5
        public ActionResult Delete(int id)
        {
            AgentasEditViewModel agentasEditViewModel = agentasRepository.getAgentas(id);
            return View(agentasEditViewModel);
        }

        // POST: Agentas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, AgentasEditViewModel collection)
        {
            try
            {
                AgentasEditViewModel agentasEditViewModel = agentasRepository.getAgentas(id);
                bool naudojama = false;

                if (agentasRepository.getKlientasSutarciuCount(id) > 0)
                {
                    naudojama = true;
                    ViewBag.naudojama = "Negalima pašalinti agento, yra sukurtų sutarčių su šiuo agentu.";
                    return View(agentasEditViewModel);
                }

                if (!naudojama)
                {
                    agentasRepository.deleteKlientas(id);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public void PopulateSelections(AgentasEditViewModel agentas)
        {
            var agenturos = nTAgenturaRepository.getNTAgenturas();
            List<SelectListItem> selectListagenturos = new List<SelectListItem>();

            foreach (var item in agenturos)
            {
                selectListagenturos.Add(new SelectListItem() { Value = Convert.ToString(item.imonesKodas), Text = item.pavadinimas });
            }

            agentas.AgenturosList = selectListagenturos;
        }
    }
}

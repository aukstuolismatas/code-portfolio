﻿namespace CarServicePlatform.Data.Dtos.Chat;

public record ConversationMessageDto(string SendBy, string Message, int ConversationGroupId);
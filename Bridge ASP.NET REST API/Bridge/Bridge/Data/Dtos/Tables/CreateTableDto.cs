﻿using System.ComponentModel.DataAnnotations;

namespace Bridge.Data.Dtos.Tables
{
    public record CreateTableDto([Required] int TableNr, [Required] int BoardId);
}

﻿using System.ComponentModel.DataAnnotations;

namespace Bridge.Data.Entities
{
    public class User
    {
        [Key]
        public string Username { get; set; }

        public string Password { get; set; }
        public UserRole Role { get; set; }
    }
}
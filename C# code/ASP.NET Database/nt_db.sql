-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2020 m. Kov 04 d. 12:27
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nt_db`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `agentas`
--

CREATE TABLE `agentas` (
  `vardas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `pavarde` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `pardavimu_skaicius` int(11) DEFAULT NULL,
  `telefonas` int(11) DEFAULT NULL,
  `id_AGENTAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `agentas`
--

INSERT INTO `agentas` (`vardas`, `pavarde`, `pardavimu_skaicius`, `telefonas`, `id_AGENTAS`) VALUES
('Arnoldas', 'Grevas', 5, 869999990, 1),
('Edvardas', 'Ivanauskas', 20, 869999991, 2),
('Michail', 'Resetov', 45, 864445565, 3),
('Patrikas', 'Pauza', 3, 864556900, 4),
('Domas', 'Sestakauskas', 15, 860000580, 5),
('Romas', 'Mikalauskas', 169, 865327600, 6),
('Liutauras', 'Liufas', 34, 867777744, 7),
('Mindaugas', 'Jurgaitis', 43, 861111181, 8),
('Simonas', 'Stanislovaitis', 1, 861234588, 9),
('Egidijus', 'Kumza', 200, 869009009, 10),
('Lukas', 'Marcininkus', 27, 865757585, 11),
('Saulius', 'Eidrigevicius', 2, 868686555, 12),
('Arnoldas', 'Bozdzinskas', 57, 867766688, 13),
('Tomas', 'Kundrotas', 155, 861123490, 14);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `butas`
--

CREATE TABLE `butas` (
  `laiptines_numeris` int(11) DEFAULT NULL,
  `buto_numeris` int(11) DEFAULT NULL,
  `buto_aukstas` int(11) DEFAULT NULL,
  `isskirta_parkavimo_vieta` tinyint(1) DEFAULT NULL,
  `vaiku_zaidimu_aikstele` tinyint(1) DEFAULT NULL,
  `rusys` tinyint(1) DEFAULT NULL,
  `id_BUTAS` int(11) NOT NULL,
  `fk_NT_OBJEKTASnr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `butas`
--

INSERT INTO `butas` (`laiptines_numeris`, `buto_numeris`, `buto_aukstas`, `isskirta_parkavimo_vieta`, `vaiku_zaidimu_aikstele`, `rusys`, `id_BUTAS`, `fk_NT_OBJEKTASnr`) VALUES
(2, 39, 4, 0, 0, 0, 1, 2),
(1, 22, 5, 1, 0, 1, 2, 3),
(4, 5, 2, 1, 0, 0, 3, 6),
(2, 3, 1, 0, 1, 1, 4, 10),
(4, 22, 7, 1, 0, 1, 5, 11),
(1, 9, 3, 1, 1, 1, 6, 12),
(2, 15, 5, 0, 0, 0, 7, 13),
(1, 1, 1, 1, 0, 0, 8, 14),
(4, 6, 2, 1, 0, 0, 9, 15),
(5, 8, 3, 0, 0, 1, 10, 16),
(3, 12, 4, 0, 1, 1, 11, 18),
(2, 10, 4, 0, 1, 1, 12, 19),
(2, 3, 1, 1, 1, 0, 13, 20),
(4, 2, 1, 0, 0, 1, 14, 21),
(1, 5, 2, 0, 0, 0, 15, 22);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `irengimo_busena`
--

CREATE TABLE `irengimo_busena` (
  `id_irengimo_busena` int(11) NOT NULL,
  `name` char(10) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `irengimo_busena`
--

INSERT INTO `irengimo_busena` (`id_irengimo_busena`, `name`) VALUES
(1, 'pilnai'),
(2, 'dalinis'),
(3, 'neirengtas');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `klientas`
--

CREATE TABLE `klientas` (
  `asmens_kodas` decimal(20,0) NOT NULL,
  `vardas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `pavarde` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `gimimo_data` date DEFAULT NULL,
  `telefonas` int(11) DEFAULT NULL,
  `el_pastas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `klientas`
--

INSERT INTO `klientas` (`asmens_kodas`, `vardas`, `pavarde`, `gimimo_data`, `telefonas`, `el_pastas`) VALUES
('37002011789', 'Renatas', 'Svelnys', '1970-02-01', 868677460, 'renatas.svelnys@juta.lt'),
('37708210890', 'Audrius', 'Klangauskas', '1977-08-21', 863456710, 'audrius.klang@gmail.com'),
('38107197722', 'Darius', 'Aukstuolis', '1981-07-19', 865555560, 'darius.aukstuolis@gmail.com'),
('39004207201', 'Daiva', 'Svelniene', '1990-04-20', 867543555, 'daiva.svelniene@gmail.com'),
('39512236543', 'Domas', 'Svelniukas', '1995-12-23', 866664890, 'domas.svelniukas@katu.lt'),
('39806071432', 'Matas', 'Jauceras', '1998-06-07', 864411420, 'matas.matys@gmail.com'),
('39808176677', 'Ignas', 'Repecka', '1998-08-17', 868547323, 'repauz@gmail.com'),
('39902031090', 'Laurynas', 'Varnas', '1999-02-03', 863495409, 'laurvar@yahoo.com'),
('39905220610', 'Matas', 'Aukstuolis', '1999-05-22', 865555585, 'mataniakas@gmail.com'),
('39907061532', 'Gintaras', 'Narvilas', '1999-07-06', 867766554, 'gintor@gmail.com'),
('60010263435', 'Daniele', 'Svelnyte', '2000-10-26', 867786459, 'daniele.svelnyte@gmail.com'),
('66011146789', 'Elvyra', 'Mikaliune', '1960-11-14', 863254098, 'mikelvyra@gmail.com'),
('67402170420', 'Jurgita', 'Aukstuoliene', '1974-02-17', 868636096, 'jurgita.aukstuoliene@judeja.lt');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `mokejimas`
--

CREATE TABLE `mokejimas` (
  `data` date DEFAULT NULL,
  `suma` double DEFAULT NULL,
  `id_MOKEJIMAS` int(11) NOT NULL,
  `fk_KLIENTASasmens_kodas` decimal(20,0) NOT NULL,
  `fk_SUTARTISnr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `mokejimas`
--

INSERT INTO `mokejimas` (`data`, `suma`, `id_MOKEJIMAS`, `fk_KLIENTASasmens_kodas`, `fk_SUTARTISnr`) VALUES
('2020-03-03', 500000, 1, '39905220610', 1),
('2020-03-06', 5000, 2, '39905220610', 2),
('2020-03-06', 120000, 3, '39512236543', 3),
('2020-02-17', 130000, 4, '38107197722', 6),
('2020-03-13', 130000, 5, '38107197722', 6),
('2020-02-24', 1000, 6, '39512236543', 6),
('2020-01-06', 1200, 7, '39004207201', 4),
('2019-04-26', 40000, 8, '60010263435', 7),
('2019-06-27', 60000, 9, '60010263435', 7),
('2019-06-26', 60000, 10, '60010263435', 7),
('2019-04-21', 3200, 11, '60010263435', 8),
('2020-02-27', 1500, 12, '39905220610', 10),
('2020-02-04', 100000, 13, '37002011789', 11),
('2020-02-26', 110000, 14, '37002011789', 11),
('2020-09-02', 110000, 15, '37002011789', 11),
('2020-02-03', 2000, 16, '37002011789', 12),
('2019-10-28', 250000, 17, '38107197722', 13),
('2020-01-09', 200000, 18, '38107197722', 13),
('2019-10-31', 4500, 19, '39808176677', 14),
(NULL, 2200, 20, '38107197722', 15),
('2020-02-21', 140000, 21, '39808176677', 16),
('2020-06-01', 100000, 22, '39808176677', 16),
('2020-01-07', 2400, 23, '39808176677', 17),
('2020-05-28', 4800, 24, '67402170420', 18),
('2020-03-02', 200000, 25, '37708210890', 19),
('2020-04-30', 180000, 26, '37708210890', 19),
('2020-04-30', 180000, 27, '37708210890', 19),
('2020-04-30', 180000, 28, '37708210890', 19);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `namas`
--

CREATE TABLE `namas` (
  `rekuperacija` tinyint(1) DEFAULT NULL,
  `apzeldinimas` tinyint(1) DEFAULT NULL,
  `kiemas` tinyint(1) DEFAULT NULL,
  `namo_tipas` int(11) DEFAULT NULL,
  `id_NAMAS` int(11) NOT NULL,
  `fk_NT_OBJEKTASnr` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `namas`
--

INSERT INTO `namas` (`rekuperacija`, `apzeldinimas`, `kiemas`, `namo_tipas`, `id_NAMAS`, `fk_NT_OBJEKTASnr`) VALUES
(1, 1, 1, 1, 1, 1),
(1, 0, 1, 2, 2, 4),
(0, 1, 1, 1, 3, 5),
(0, 1, 1, 1, 4, 7),
(0, 1, 1, 2, 5, 8),
(0, 1, 1, 1, 6, 9),
(1, 0, 1, 2, 7, 17),
(0, 1, 1, 1, 8, 23),
(0, 0, 1, 1, 9, 24),
(1, 0, 1, 3, 10, 25),
(0, 1, 1, 1, 11, 26),
(1, 0, 1, 3, 12, 27);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `namu_tipai`
--

CREATE TABLE `namu_tipai` (
  `id_namu_tipai` int(11) NOT NULL,
  `name` char(22) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `namu_tipai`
--

INSERT INTO `namu_tipai` (`id_namu_tipai`, `name`) VALUES
(1, 'nuosavas,_standartinis'),
(2, 'kotedzas'),
(3, 'koperatinis_bustas');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `nt_agentura`
--

CREATE TABLE `nt_agentura` (
  `pavadinimas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `imones_kodas` decimal(20,0) NOT NULL,
  `adresas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `telefonas` int(11) DEFAULT NULL,
  `el_pastas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `nt_agentura`
--

INSERT INTO `nt_agentura` (`pavadinimas`, `imones_kodas`, `adresas`, `telefonas`, `el_pastas`) VALUES
('RE/MAX', '132907143', 'Konstitucijos pr. 23, Vilnius', 852102767, 'info@remax.com'),
('Patera', '134594596', 'Laisvės al. 59, Kaunas ', 867777779, 'info@patea.lt'),
('Proreal', '192554479', 'Lukiškių g. 5, Vilnius', 860348615, 'info@proreal.eu'),
('Capital Kaunas', '300653680', 'Savanorių pr. 241, Kaunas', 837787776, 'info.kaunas@capital.com'),
('FIKS', '302647007', 'Savanorių g. 34, Varėna', 868200029, 'info@fiks.lt'),
('Capital', '302692664', 'A. Gustaičio g. 23-255A, Vilnius', 868589010, 'info@capital.com'),
('NT-TAU', '304476715', 'Kaunakiemio g. 40, Kaunas', 868672271, 'info@nttau.lt'),
('OPPA', '313245999', 'P. Lukšio g. 32, Vilnius', 866666667, 'info@oppa.lt'),
('INREAL', '365788722', 'T. Narbuto g. 5, Vilnius', 852730000, 'info@inreal.lt'),
('Ober-Haus', '667909162', 'Didžioji g. 20, Vilnius', 852666555, 'info@oberhaus.com'),
('Vilniaus turtas', '3908567513', 'Ulonų g. 5, Vilnius', 860000010, 'info.turtas@gmail.com');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `nt_objektas`
--

CREATE TABLE `nt_objektas` (
  `nr` int(11) NOT NULL,
  `pastatymo_metai` year(4) DEFAULT NULL,
  `kambariu_skaicius` int(11) DEFAULT NULL,
  `bendras_plotas` double DEFAULT NULL,
  `aukstu_skaicius` int(11) DEFAULT NULL,
  `baldai` tinyint(1) DEFAULT NULL,
  `signalizacija` tinyint(1) DEFAULT NULL,
  `garazas` tinyint(1) DEFAULT NULL,
  `irengimo_busena` int(11) DEFAULT NULL,
  `tipas` int(11) DEFAULT NULL,
  `fk_VIETAid_VIETA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `nt_objektas`
--

INSERT INTO `nt_objektas` (`nr`, `pastatymo_metai`, `kambariu_skaicius`, `bendras_plotas`, `aukstu_skaicius`, `baldai`, `signalizacija`, `garazas`, `irengimo_busena`, `tipas`, `fk_VIETAid_VIETA`) VALUES
(1, 2018, 10, 250, 2, 1, 1, 1, 1, 2, 1),
(2, 1990, 4, 50, 1, 1, 0, 0, 1, 1, 8),
(3, 1970, 5, 100, 5, 0, 0, 0, 2, 1, 9),
(4, 2019, 9, 150, 2, 0, 1, 1, 2, 2, 2),
(5, 1965, 12, 300, 3, 1, 0, 1, 1, 2, 3),
(6, 1954, 5, 80, 4, 1, 1, 1, 1, 1, 7),
(7, 1996, 14, 310, 3, 1, 1, 1, 1, 2, 4),
(8, 1998, 8, 180, 2, 1, 1, 0, 1, 2, 5),
(9, 2006, 9, 300, 2, 1, 1, 1, 1, 2, 6),
(10, 1980, 3, 50, 9, 1, 0, 0, 1, 1, 10),
(11, 2000, 5, 90, 12, 0, 0, 1, 2, 1, 11),
(12, 1989, 6, 100, 5, 1, 1, 0, 1, 1, 12),
(13, 1995, 6, 110, 7, 1, 0, 0, 1, 1, 13),
(14, 1991, 3, 50, 3, 0, 0, 0, 2, 1, 14),
(15, 2009, 7, 140, 5, 1, 1, 1, 1, 1, 15),
(16, 1969, 5, 80, 7, 1, 1, 0, 1, 1, 16),
(17, 2017, 8, 180, 2, 0, 0, 0, 2, 2, 17),
(18, 1979, 4, 70, 9, 1, 0, 0, 1, 1, 18),
(19, 1990, 5, 80, 7, 0, 0, 0, 1, 1, 19),
(20, 1971, 6, 100, 3, 1, 1, 0, 1, 1, 20),
(21, 1958, 3, 57, 3, 0, 0, 0, 1, 1, 21),
(22, 1960, 5, 69, 3, 1, 1, 0, 1, 1, 22),
(23, 1994, 11, 190, 1, 1, 1, 1, 1, 2, 23),
(24, 1987, 10, 200, 2, 1, 1, 1, 1, 2, 24),
(25, 2020, 7, 120, 2, 0, 0, 0, 2, 2, 25),
(26, 1967, 6, 148, 2, 1, 1, 1, 1, 2, 26),
(27, 2020, 7, 137, 2, 0, 0, 1, 3, 2, 27);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `nt_tipas`
--

CREATE TABLE `nt_tipas` (
  `id_NT_tipas` int(11) NOT NULL,
  `name` char(5) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `nt_tipas`
--

INSERT INTO `nt_tipas` (`id_NT_tipas`, `name`) VALUES
(1, 'butas'),
(2, 'namas');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `paslaugu`
--

CREATE TABLE `paslaugu` (
  `procentas_nuo_NT_kainos` double DEFAULT NULL,
  `sutartits_galioja_iki` date DEFAULT NULL,
  `tipas` int(11) DEFAULT NULL,
  `id_PASLAUGU` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `paslaugu`
--

INSERT INTO `paslaugu` (`procentas_nuo_NT_kainos`, `sutartits_galioja_iki`, `tipas`, `id_PASLAUGU`) VALUES
(1, '2020-03-19', 1, 1),
(2, '2020-03-19', 1, 2),
(1, '2020-04-30', 2, 3),
(2, '2019-07-26', 1, 4),
(1, '2020-04-30', 2, 5),
(NULL, '2020-03-31', 1, 6),
(1, '2020-01-01', 2, 7),
(NULL, '2020-01-31', 1, 8),
(1, '2020-04-30', 1, 9),
(2, '2020-05-31', 2, 10);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `paslaugu_tipai`
--

CREATE TABLE `paslaugu_tipai` (
  `id_paslaugu_tipai` int(11) NOT NULL,
  `name` char(9) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `paslaugu_tipai`
--

INSERT INTO `paslaugu_tipai` (`id_paslaugu_tipai`, `name`) VALUES
(1, 'pirkimo'),
(2, 'pardavimo');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `priklauso`
--

CREATE TABLE `priklauso` (
  `fk_NT_AGENTURAimones_kodas` decimal(20,0) NOT NULL,
  `fk_AGENTASid_AGENTAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `sutarties_busena`
--

CREATE TABLE `sutarties_busena` (
  `id_sutarties_busena` int(11) NOT NULL,
  `name` char(17) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `sutarties_busena`
--

INSERT INTO `sutarties_busena` (`id_sutarties_busena`, `name`) VALUES
(1, 'pasirasyta'),
(2, 'laukia_pasirasymo'),
(3, 'negalioja');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `sutarties_tipas`
--

CREATE TABLE `sutarties_tipas` (
  `id_sutarties_tipas` int(11) NOT NULL,
  `name` char(17) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `sutarties_tipas`
--

INSERT INTO `sutarties_tipas` (`id_sutarties_tipas`, `name`) VALUES
(1, 'pirkimo-pardavimo'),
(2, 'paslaugu');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `sutartis`
--

CREATE TABLE `sutartis` (
  `nr` int(11) NOT NULL,
  `sutarties_data` date DEFAULT NULL,
  `kaina` decimal(20,0) DEFAULT NULL,
  `busena` int(11) DEFAULT NULL,
  `tipas` int(11) DEFAULT NULL,
  `fk_VIETAid_VIETA` int(11) DEFAULT NULL,
  `fk_NT_AGENTURAimones_kodas` decimal(20,0) NOT NULL,
  `fk_KLIENTASasmens_kodas` decimal(20,0) NOT NULL,
  `fk_NT_OBJEKTASnr` int(11) DEFAULT NULL,
  `fk_PASLAUGUid_PASLAUGU` int(11) DEFAULT NULL,
  `fk_AGENTASid_AGENTAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `sutartis`
--

INSERT INTO `sutartis` (`nr`, `sutarties_data`, `kaina`, `busena`, `tipas`, `fk_VIETAid_VIETA`, `fk_NT_AGENTURAimones_kodas`, `fk_KLIENTASasmens_kodas`, `fk_NT_OBJEKTASnr`, `fk_PASLAUGUid_PASLAUGU`, `fk_AGENTASid_AGENTAS`) VALUES
(1, '2020-03-02', '500000', 1, 1, 1, '302692664', '39905220610', 1, NULL, 1),
(2, '2020-02-19', '5000', 1, 2, NULL, '302692664', '39905220610', NULL, 1, 1),
(3, '2020-02-28', '120000', 2, 1, 8, '304476715', '39512236543', 2, NULL, 2),
(4, '2020-01-02', '1200', 1, 2, NULL, '302647007', '39004207201', NULL, 3, 2),
(5, '2020-02-20', '1000', 1, 2, NULL, '302647007', '39512236543', NULL, 2, 2),
(6, '2020-02-11', '260000', 1, 1, 9, '300653680', '38107197722', 3, NULL, 3),
(7, '2019-04-22', '160000', 1, 1, 2, '300653680', '60010263435', 4, NULL, 3),
(8, '2019-04-19', '3200', 3, 2, NULL, '300653680', '60010263435', NULL, NULL, 3),
(9, '2020-03-04', '1500000', 2, 1, 3, '134594596', '39902031090', 5, NULL, 5),
(10, '2020-01-23', '1500', 1, 2, NULL, '134594596', '39905220610', NULL, NULL, 5),
(11, '2020-02-02', '320000', 1, 1, 7, '302692664', '37002011789', 6, NULL, 1),
(12, '2020-01-31', '2000', 1, 2, NULL, '302692664', '37002011789', NULL, 6, 1),
(13, '2019-10-25', '450000', 1, 1, 4, '302647007', '38107197722', 7, NULL, 4),
(14, '2019-09-02', '4500', 3, 2, NULL, '302647007', '39808176677', NULL, 7, 4),
(15, '2019-10-01', '2200', 3, 2, NULL, '302647007', '38107197722', NULL, NULL, 4),
(16, '2020-02-19', '240000', 1, 1, 5, '304476715', '39808176677', 8, NULL, 6),
(17, '2020-01-02', '2400', 1, 2, NULL, '304476715', '39808176677', NULL, 9, 6),
(18, '2020-02-05', '4800', 3, 2, NULL, '304476715', '67402170420', NULL, NULL, 6),
(19, '2020-02-28', '380000', 1, 1, 6, '302692664', '37708210890', 9, NULL, 7);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `vieta`
--

CREATE TABLE `vieta` (
  `miestas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `gatve` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `namo_numeris` int(11) DEFAULT NULL,
  `rajonas` varchar(255) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `atstumas_iki_centro` double DEFAULT NULL,
  `atstumas_iki_parko` double DEFAULT NULL,
  `id_VIETA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `vieta`
--

INSERT INTO `vieta` (`miestas`, `gatve`, `namo_numeris`, `rajonas`, `atstumas_iki_centro`, `atstumas_iki_parko`, `id_VIETA`) VALUES
('Kaunas', 'Sviesos g.', 7, 'Zaliakalnis', 2, 0.3, 1),
('Kaunas', 'Aukstutiniu Kaniuku g.', 45, 'Aukstieji Kaniukai', 12, 0.1, 2),
('Kaunas', 'Sukileliu g.', 33, 'Eiguliai', 4, 1.5, 3),
('Kaunas', 'Hipodromo g.', 57, 'Sanciai', 5, 0.3, 4),
('Kaunas', 'Pilieciu g.', 13, 'Sanciai', 5, 0.5, 5),
('Kaunas', 'Uzsiliu g.', 14, 'Sanciai', 5, 0.6, 6),
('Vilnius', 'Gedimino g.', 34, 'Centras', 0, 0.7, 7),
('Kaunas', 'Saules g.', 22, 'Dainava', 2, 0.5, 8),
('Kaunas', 'Maironio g.', 55, 'Centras', 0, 1.5, 9),
('Vilnius', 'Taikos pr.', 120, 'Naujininkai', 3, 3, 10),
('Kaunas', 'V. Kreves pr.', 129, 'Dainava', 18, 2, 11),
('Vilnius', 'Kauno g.', 30, 'Stoties', 1.6, 8, 12),
('Vilnius', 'Nemencines pl.', 150, 'Nemencine', 10, 0.5, 13),
('Vilnius', 'Dauksos g.', 11, 'Senamiestis', 1, 5, 14),
('Vilnius', 'Svitrigalos g.', 55, 'Centras', 0, 8, 15),
('Kaunas', 'Studentu g.', 1, 'Dainava', 3, 1, 16),
('Kaunas', 'Giraites g.', 41, 'Giraite', 14, 1, 17),
('Kaunas', 'Nemuno g.', 37, 'Kleboniskis', 13, 0, 18),
('Kaunas', 'Neries kr.', 33, 'Vilijampole', 1, 4, 19),
('Kaunas', 'Kauno g.', 12, 'Stoties', 2, 0.5, 20),
('Kaunas', 'Griunvaldo g.', 10, 'Stoties', 2, 0.3, 21),
('Kaunas', 'Gedimino g.', 35, 'Stoties', 1.9, 0.4, 22),
('Kaunas', 'Siulu g.', 4, 'Sanciai', 5, 1, 23),
('Kaunas', 'Siaures pr.', 45, 'Eiguliai', 5, 2, 24),
('Kaunas', 'Giraites g.', 155, 'Giraite', 10, 0.6, 25),
('Kaunas', 'Naujalio g.', 23, 'Vilijampole', 2, 4, 26),
('Kaunas', 'Lapo g.', 111, 'Ringaudai', 15, 2, 27);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `įrengimo_būsena`
--

CREATE TABLE `įrengimo_būsena` (
  `id_įrengimo_būsena` int(11) NOT NULL,
  `name` char(10) COLLATE utf8_lithuanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agentas`
--
ALTER TABLE `agentas`
  ADD PRIMARY KEY (`id_AGENTAS`);

--
-- Indexes for table `butas`
--
ALTER TABLE `butas`
  ADD PRIMARY KEY (`id_BUTAS`),
  ADD UNIQUE KEY `fk_NT_OBJEKTASnr` (`fk_NT_OBJEKTASnr`);

--
-- Indexes for table `irengimo_busena`
--
ALTER TABLE `irengimo_busena`
  ADD PRIMARY KEY (`id_irengimo_busena`);

--
-- Indexes for table `klientas`
--
ALTER TABLE `klientas`
  ADD PRIMARY KEY (`asmens_kodas`);

--
-- Indexes for table `mokejimas`
--
ALTER TABLE `mokejimas`
  ADD PRIMARY KEY (`id_MOKEJIMAS`),
  ADD KEY `padaro` (`fk_KLIENTASasmens_kodas`),
  ADD KEY `pagal_sutarti` (`fk_SUTARTISnr`);

--
-- Indexes for table `namas`
--
ALTER TABLE `namas`
  ADD PRIMARY KEY (`id_NAMAS`),
  ADD UNIQUE KEY `fk_NT_OBJEKTASnr` (`fk_NT_OBJEKTASnr`),
  ADD KEY `namo_tipas` (`namo_tipas`);

--
-- Indexes for table `namu_tipai`
--
ALTER TABLE `namu_tipai`
  ADD PRIMARY KEY (`id_namu_tipai`);

--
-- Indexes for table `nt_agentura`
--
ALTER TABLE `nt_agentura`
  ADD PRIMARY KEY (`imones_kodas`);

--
-- Indexes for table `nt_objektas`
--
ALTER TABLE `nt_objektas`
  ADD PRIMARY KEY (`nr`),
  ADD UNIQUE KEY `fk_VIETAid_VIETA` (`fk_VIETAid_VIETA`),
  ADD KEY `irengimo_busena` (`irengimo_busena`),
  ADD KEY `tipas` (`tipas`);

--
-- Indexes for table `nt_tipas`
--
ALTER TABLE `nt_tipas`
  ADD PRIMARY KEY (`id_NT_tipas`);

--
-- Indexes for table `paslaugu`
--
ALTER TABLE `paslaugu`
  ADD PRIMARY KEY (`id_PASLAUGU`),
  ADD KEY `tipas` (`tipas`);

--
-- Indexes for table `paslaugu_tipai`
--
ALTER TABLE `paslaugu_tipai`
  ADD PRIMARY KEY (`id_paslaugu_tipai`);

--
-- Indexes for table `priklauso`
--
ALTER TABLE `priklauso`
  ADD PRIMARY KEY (`fk_NT_AGENTURAimones_kodas`,`fk_AGENTASid_AGENTAS`);

--
-- Indexes for table `sutarties_busena`
--
ALTER TABLE `sutarties_busena`
  ADD PRIMARY KEY (`id_sutarties_busena`);

--
-- Indexes for table `sutarties_tipas`
--
ALTER TABLE `sutarties_tipas`
  ADD PRIMARY KEY (`id_sutarties_tipas`);

--
-- Indexes for table `sutartis`
--
ALTER TABLE `sutartis`
  ADD PRIMARY KEY (`nr`),
  ADD UNIQUE KEY `fk_VIETAid_VIETA` (`fk_VIETAid_VIETA`),
  ADD UNIQUE KEY `fk_NT_OBJEKTASnr` (`fk_NT_OBJEKTASnr`),
  ADD UNIQUE KEY `fk_PASLAUGUid_PASLAUGU` (`fk_PASLAUGUid_PASLAUGU`),
  ADD KEY `busena` (`busena`),
  ADD KEY `tipas` (`tipas`),
  ADD KEY `sudaro` (`fk_NT_AGENTURAimones_kodas`),
  ADD KEY `pasiraso` (`fk_KLIENTASasmens_kodas`),
  ADD KEY `itruktas_i` (`fk_AGENTASid_AGENTAS`);

--
-- Indexes for table `vieta`
--
ALTER TABLE `vieta`
  ADD PRIMARY KEY (`id_VIETA`);

--
-- Indexes for table `įrengimo_būsena`
--
ALTER TABLE `įrengimo_būsena`
  ADD PRIMARY KEY (`id_įrengimo_būsena`);

--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `butas`
--
ALTER TABLE `butas`
  ADD CONSTRAINT `yra_1` FOREIGN KEY (`fk_NT_OBJEKTASnr`) REFERENCES `nt_objektas` (`nr`);

--
-- Apribojimai lentelei `mokejimas`
--
ALTER TABLE `mokejimas`
  ADD CONSTRAINT `padaro` FOREIGN KEY (`fk_KLIENTASasmens_kodas`) REFERENCES `klientas` (`asmens_kodas`),
  ADD CONSTRAINT `pagal_sutarti` FOREIGN KEY (`fk_SUTARTISnr`) REFERENCES `sutartis` (`nr`);

--
-- Apribojimai lentelei `namas`
--
ALTER TABLE `namas`
  ADD CONSTRAINT `namas_ibfk_1` FOREIGN KEY (`namo_tipas`) REFERENCES `namu_tipai` (`id_namu_tipai`),
  ADD CONSTRAINT `yra_2` FOREIGN KEY (`fk_NT_OBJEKTASnr`) REFERENCES `nt_objektas` (`nr`);

--
-- Apribojimai lentelei `nt_objektas`
--
ALTER TABLE `nt_objektas`
  ADD CONSTRAINT `nt_objektas_ibfk_1` FOREIGN KEY (`irengimo_busena`) REFERENCES `irengimo_busena` (`id_irengimo_busena`),
  ADD CONSTRAINT `nt_objektas_ibfk_2` FOREIGN KEY (`tipas`) REFERENCES `nt_tipas` (`id_NT_tipas`),
  ADD CONSTRAINT `turi` FOREIGN KEY (`fk_VIETAid_VIETA`) REFERENCES `vieta` (`id_VIETA`);

--
-- Apribojimai lentelei `paslaugu`
--
ALTER TABLE `paslaugu`
  ADD CONSTRAINT `paslaugu_ibfk_1` FOREIGN KEY (`tipas`) REFERENCES `paslaugu_tipai` (`id_paslaugu_tipai`);

--
-- Apribojimai lentelei `priklauso`
--
ALTER TABLE `priklauso`
  ADD CONSTRAINT `priklauso` FOREIGN KEY (`fk_NT_AGENTURAimones_kodas`) REFERENCES `nt_agentura` (`imones_kodas`);

--
-- Apribojimai lentelei `sutartis`
--
ALTER TABLE `sutartis`
  ADD CONSTRAINT `itraukta_i` FOREIGN KEY (`fk_VIETAid_VIETA`) REFERENCES `vieta` (`id_VIETA`),
  ADD CONSTRAINT `itrauktas` FOREIGN KEY (`fk_NT_OBJEKTASnr`) REFERENCES `nt_objektas` (`nr`),
  ADD CONSTRAINT `itruktas_i` FOREIGN KEY (`fk_AGENTASid_AGENTAS`) REFERENCES `agentas` (`id_AGENTAS`),
  ADD CONSTRAINT `pasiraso` FOREIGN KEY (`fk_KLIENTASasmens_kodas`) REFERENCES `klientas` (`asmens_kodas`),
  ADD CONSTRAINT `sudaro` FOREIGN KEY (`fk_NT_AGENTURAimones_kodas`) REFERENCES `nt_agentura` (`imones_kodas`),
  ADD CONSTRAINT `sutartis_ibfk_1` FOREIGN KEY (`busena`) REFERENCES `sutarties_busena` (`id_sutarties_busena`),
  ADD CONSTRAINT `sutartis_ibfk_2` FOREIGN KEY (`tipas`) REFERENCES `sutarties_tipas` (`id_sutarties_tipas`),
  ADD CONSTRAINT `yra` FOREIGN KEY (`fk_PASLAUGUid_PASLAUGU`) REFERENCES `paslaugu` (`id_PASLAUGU`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

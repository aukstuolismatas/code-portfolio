import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppConstants, RegexPatterns } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CategoriesService } from 'src/app/services/categories.service';
import { mustMatch } from 'src/app/shared/form-validation';
import { RegisterService } from 'src/app/shared/login-data';
import { RepairService } from 'src/app/shared/repair-services';

@Component({
  selector: 'app-register-service',
  templateUrl: './register-service.component.html',
  styleUrls: ['./register-service.component.css']
})
export class RegisterServiceComponent implements OnInit {
  public registerForm : FormGroup = new FormGroup({
    name: new FormControl(''),
    surname: new FormControl(''),
    email: new FormControl(''),
    phone: new FormControl(''),
    password: new FormControl(''),
    confirmPassword: new FormControl(''),
    city: new FormControl(''),
    address: new FormControl(''),
    companyCode: new FormControl(''),
    companyName: new FormControl(''),
    availableRepairServiceIds: new FormArray([])
  });

  public submitted =false;
  public errorMessage = '';
  public successMessage = '';
  public repairServices: RepairService[]=[];
  public selectedRepairServices: RepairService[]=[];
  
  constructor(private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private categoryService: CategoriesService) {
      this.registerForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        surname: ['', [Validators.required]],
        email: ['', [Validators.required ,Validators.email, Validators.pattern(RegexPatterns.Email)]],
        phone: ['', [Validators.required, Validators.pattern(RegexPatterns.Phone)]],
        password: ['', [Validators.required, Validators.minLength(6),Validators.maxLength(40), Validators.pattern(RegexPatterns.Passowrd)]],
        confirmPassword: ['', [Validators.required]],
        city: ['', [Validators.required]],
        address: ['', [Validators.required]],
        companyCode: ['', [Validators.required]],
        companyName: ['', [Validators.required]],
      }, {
        validator: mustMatch
      });
      this.categoryService.loadRepairServices().subscribe((data:RepairService[])=>{
        this.repairServices=data;
      });
     }
    

  ngOnInit(): void {
    // console.log(this.repairServicesFormArray.value)

  }
  get f(): {[key:string]:AbstractControl} { return this.registerForm.controls; }
  // get repairServicesFormArray(){ return this.registerForm.controls['availableRepairServiceIds'] as FormArray; }

  // private addCheckboxes(services: RepairServices[]) {
  //   services.forEach((service) => this.repairServicesFormArray.push(new FormControl(service)));
  // }
  // minSelectedCheckboxes(min = 1) {
  //   const validator: ValidatorFn = (formArray: FormArray) => {
  //     const totalSelected = formArray.controls
  //       // get a list of checkbox values (boolean)
  //       .map(control => control.value)
  //       // total up the number of checked checkboxes
  //       .reduce((prev, next) => next ? prev + next : prev, 0);
  
  //     // if the total is not greater than the minimum, return the error message
  //     return totalSelected >= min ? null : { required: true };
  //   };
  
  //   return validator;
  // }

  onChange(repairService: RepairService, isChecked: any) {

    if (isChecked.checked) {
      this.selectedRepairServices.push(repairService)
    } else {
      const index = this.selectedRepairServices.findIndex(x => x.id === repairService.id);
      delete this.selectedRepairServices[index];
      this.selectedRepairServices.length--;
    }
    console.log(this.selectedRepairServices);
  }
  register(){
    const availableServices: { [key: string]: number; } = {};
    this.selectedRepairServices.forEach(x=> availableServices[x.id.toString()]= x.repairServiceCategory.id);
    console.log(availableServices)
    const registerRequest: RegisterService = {
      name: this.f['name'].value,
      surname: this.f['surname'].value,
      email: this.f['email'].value,
      phoneNumber: this.f['phone'].value,
      password: this.f['password'].value,
      address: this.f['address'].value,
      city: this.f['city'].value,
      companyCode: this.f['companyCode'].value,
      companyName: this.f['companyName'].value,
      availableRepairServiceIds: availableServices
    };
    
    this.submitted=false;
     if (this.registerForm.invalid) {
       this.submitted = true;
        return;
     }
    this.authService.registerService(registerRequest).subscribe({
      next: (user) => {
        this.router.navigate([AppConstants.Login], {queryParams: { registered: 'true' } });
      },
      error: (err) => {
        this.errorMessage = MessageConstants.RegisterError
      }
    })
    this.registerForm.reset;
    this.errorMessage =''
  }
  changeCity(event:number){
    this.f['city'].setValue(event);
    console.log(this.f['city'].value);
  }
}

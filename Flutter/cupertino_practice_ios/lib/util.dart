import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Util {
  static const Padding paddingTop = Padding(padding: EdgeInsets.only(top: 36));
  static const Padding paddingLeft =
      Padding(padding: EdgeInsets.only(left: 12));
  static const TextStyle textStyle = TextStyle(fontSize: 24);

  static double calculateBMI(double height, double weight, int unit) {
    double bmi = 0;
    if (unit == 0) {
      bmi = weight / (height * height);
    } else {
      bmi = weight * 703 / (height * height);
    }
    return bmi;
  }

  static void saveSettings(int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('unit', value);
  }

  static Future<int> getSettings() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int result = prefs.getInt('unit') ?? 0;
    return result;
  }
}

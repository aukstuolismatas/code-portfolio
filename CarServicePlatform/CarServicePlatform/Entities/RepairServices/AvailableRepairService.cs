﻿using System.ComponentModel.DataAnnotations;
using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Entities.RepairServices;

public class AvailableRepairService
{
    [Required]
    public int Id { get; set; }

    [Required]
    public int RepairServiceId { get; set; }

    public RepairService RepairService { get; set; }

    [Required]
    public string DomainUserId { get; set; }

    public DomainUser DomainUser { get; set; }

    [Required]
    [EnumDataType(typeof(City))]
    public City City { get; set; }
}
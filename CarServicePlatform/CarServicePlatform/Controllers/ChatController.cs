﻿using System.Security.Claims;
using AutoMapper;
using CarServicePlatform.Auth.Model;
using CarServicePlatform.Data.Dtos.Chat;
using CarServicePlatform.Entities.Chat;
using CarServicePlatform.Entities.Request;
using CarServicePlatform.Entities.Users;
using CarServicePlatform.Repository;
using CarServicePlatform.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PusherServer;

namespace CarServicePlatform.Controllers;

[ApiController]
[Route("/conversation/groups")]
[Authorize]
public class ChatController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IConversationRepository _conversationRepository;
    private readonly IAuthorizationService _authorizationService;
    private readonly IClientRequestRepository _clientRequestRepository;
    private readonly IAdministratorRepository _administratorRepository;
    private readonly IClientRepository _clientRepository;

    public ChatController(IMapper mapper, IConversationRepository conversationRepository, IAuthorizationService authorizationService, IClientRequestRepository clientRequestRepository, IAdministratorRepository administratorRepository, IClientRepository clientRepository)
    {
        _mapper = mapper;
        _conversationRepository = conversationRepository;
        _authorizationService = authorizationService;
        _clientRequestRepository = clientRequestRepository;
        _administratorRepository = administratorRepository;
        _clientRepository = clientRepository;
    }

    [HttpGet("users/{userId}")]
    public async Task<ActionResult<IEnumerable<ConversationGroupDto>>> GetAllUserConversations(string userId)
    {
        if (User.FindFirstValue(CustomClaims.UserId) != userId)
        {
            return Forbid();
        }

        List<ConversationGroup> allUserConversationGroups = await _conversationRepository.GetAllConversationGroupsWhichUserAssignedTo(userId).ConfigureAwait(false);
        return Ok(allUserConversationGroups.Select(x => _mapper.Map<ConversationGroupDto>(x)));
    }

    [HttpPost]
    public async Task<ActionResult<ConversationGroupDto>> CreateConversationGroup(CreateConversationGroupDto createConversationGroupDto)
    {
        ConversationGroup? ifConversationGroupExists = await _conversationRepository.GetConversationGroupByClientRequest(createConversationGroupDto.ClientRequestId).ConfigureAwait(false);
        if (ifConversationGroupExists != null)
        {
            return BadRequest("Conversation group already exists");
        }

        ClientRequest? request = await _clientRequestRepository.GetRequestAsync(createConversationGroupDto.ClientRequestId).ConfigureAwait(false);
        if (request == null)
        {
            return NotFound($"Request with request id {createConversationGroupDto.ClientRequestId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, request, PolicyNames.SameUser).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        //TODO VALIDATE USERS - HERE SHOULD BE VALIDATION ON USER ROLES WHO COULD COMMUNICATE
        ConversationGroup conversationGroup = new()
        {
            ConversationTitle = $"#{request.ClientRequestId} {request.Title}",
            ClientRequestId = request.ClientRequestId
        };

        await _conversationRepository.InsertConversationGroupAsync(conversationGroup).ConfigureAwait(false);

        List<ConversationUser> conversationUser = new();
        AdministratorAccount? adminUser = await _administratorRepository.GetAdministratorUserAsync().ConfigureAwait(false);
        ClientAccount? clientUser = await _clientRepository.GetClientAccountAsync(createConversationGroupDto.userId).ConfigureAwait(false);
        if (adminUser == null || clientUser == null)
        {
            return BadRequest("Users does not exists. It is not possible to create conversation");
        }

        conversationUser.Add(new ConversationUser
        {
            ConversationGroupId = conversationGroup.Id,
            DomainUserId = adminUser.Id,
            UserName = adminUser.Name
        });

        conversationUser.Add(new ConversationUser
        {
            ConversationGroupId = conversationGroup.Id,
            DomainUserId = clientUser.Id,
            UserName = clientUser.Name
        });
        await _conversationRepository.InsertConversationUsersAsync(conversationUser).ConfigureAwait(false);
        return Created($"/conversations/groups/{conversationGroup.Id}", _mapper.Map<ConversationGroupDto>(conversationGroup));
    }

    [HttpGet("{groupId}/messages")]
    public async Task<ActionResult<IEnumerable<ConversationMessageDto>>> GetAllConversationGroupMessages(int groupId)
    {
        //TODO Should only assigned user can get of their results
        List<ConversationMessage> conversationMessages = await _conversationRepository.GetAllConversationMessagesWhichAssignedToGroup(groupId).ConfigureAwait(false);
        return Ok(conversationMessages.Select(x => _mapper.Map<ConversationMessageDto>(x)));
    }

    [HttpPost("{groupId}/messages")]
    public async Task<ActionResult<ConversationMessageDto>> InsertNewConversationMessage([FromBody] CreateConversationMessageDto createConversationMessageDto, int groupId)
    {
        ConversationGroup? ifConversationGroupExists = await _conversationRepository.GetConversationGroupByGroupId(groupId).ConfigureAwait(false);
        if (ifConversationGroupExists == null)
        {
            return BadRequest($"Conversation with id {groupId} does not exists");
        }

        ConversationMessage conversationMessage = _mapper.Map<ConversationMessage>(createConversationMessageDto);
        conversationMessage.ConversationGroupId = groupId;
        await _conversationRepository.InsertConversationMessageAsync(conversationMessage).ConfigureAwait(false);
        Pusher pusher = PusherService.PusherInitialization();
        await pusher.TriggerAsync("private-" + conversationMessage.ConversationGroupId,
                        "message",
                        new
                        {
                            sendBy = conversationMessage.SendBy,
                            message = conversationMessage.Message,
                            conversationGroupId = groupId
                        },
                        new TriggerOptions { SocketId = createConversationMessageDto.SocketId })
                    .ConfigureAwait(false);
        // Ok(_mapper.Map<ConversationMessageDto>(conversationMessage));
        return Ok(_mapper.Map<ConversationMessageDto>(conversationMessage)); //Created($"conversation/groups/{groupId}/messages/{conversationMessage.Id}", _mapper.Map<ConversationMessageDto>(conversationMessage));
    }
}
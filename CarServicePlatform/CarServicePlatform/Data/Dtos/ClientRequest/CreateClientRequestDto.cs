﻿namespace CarServicePlatform.Data.Dtos.ClientRequest;

public record CreateClientRequestDto(string Description, Dictionary<int, int> RequestedServiceIds, string City, string Address);

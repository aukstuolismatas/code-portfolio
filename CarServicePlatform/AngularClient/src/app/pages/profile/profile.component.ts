import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RegexPatterns } from 'src/app/app-constants';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { RolesType } from 'src/app/services/enum.service';
import { FileService } from 'src/app/services/file.service';
import { ProfileService } from 'src/app/services/profile.service';
import { ClientAccount, ServiceAccount, UpdateClientAccount } from 'src/app/shared/account';
import { FileInformation } from 'src/app/shared/file-information';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public errorMessage ='';
  public successMessage ='';
  showValidation:boolean = false;
  profileForm : FormGroup = new FormGroup({
    name: new FormControl(''),
    surname: new FormControl(''),
    email: new FormControl(''),
    phone: new FormControl(''),
    currentPassword: new FormControl(''),
    newPassword: new FormControl(''),
  });

  fileUploadForm = new FormGroup({
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required])
  });
  public clientDetails!: ClientAccount;
  uploadedImageInformation!: FileInformation;
  constructor(private formBuilder: FormBuilder, private profileService: ProfileService, 
    private fileService: FileService, private dataSharingService: DataSharingService, private authService: AuthService) { 
      this.profileForm =  this.formBuilder.group({
        name: ['', [Validators.required]],
        surname: ['', [Validators.required]],
        email: ['', [Validators.required ,Validators.email, Validators.pattern(RegexPatterns.Email)]],
        phone: ['', [Validators.required, Validators.pattern(RegexPatterns.Phone)]],
        currentPassword: ['', [Validators.required, Validators.minLength(6),Validators.maxLength(40)]],
        newPassword: ['', [Validators.required, Validators.minLength(6),Validators.maxLength(40)]],
      });
    if(this.authService.isClient()){
      this.profileService.loadClientAccount().subscribe((data:ClientAccount)=> {
        this.clientDetails = data
      });
    } else if(this.authService.isAdmin()){
      this.profileService.loadAdminAccount().subscribe((data:ClientAccount)=> {
        this.clientDetails = data
      });
    }
  }

  ngOnInit(): void {
  }
  get f(): {[key:string]:AbstractControl} { return this.profileForm.controls; }
  update(){
    const updateRequest: UpdateClientAccount = {
      name: this.f['name'].value,
      surname: this.f['surname'].value,
      email: this.f['email'].value,
      phoneNumber: this.f['phone'].value,
      currentPassword: this.f['currentPassword'].value,
      newPassword: this.f['newPassword'].value,
    };
    if(this.authService.isClient()){
      this.profileService.updateClientAccount(updateRequest).subscribe({
        next: (user) => this.successMessage = MessageConstants.ProfileUpdateSuccess,
        error: (err) => this.errorMessage = MessageConstants.ProfileUpdateError
      })
    }else if(this.authService.isAdmin()){
      this.profileService.updateAdminAccount(updateRequest).subscribe({
        next: (user) => this.successMessage = MessageConstants.ProfileUpdateSuccess,
        error: (err) => this.errorMessage = MessageConstants.ProfileUpdateError
      })
    }
    
    this.successMessage ='';
    this.errorMessage='';
  }

  get fileFormControl() { return this.fileUploadForm.controls; }
  onFileChange(event: any) {
  
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.fileUploadForm.patchValue({
        fileSource: file
      });
    }
  }

  onFileUpload() {
    if( this.fileUploadForm.invalid){
      this.showValidation = true;
      return false
    }
    const formData = new FormData();
    formData.append('file', this.fileFormControl['fileSource'].value);
    this.fileService.readFile(this.fileFormControl['fileSource'].value).subscribe(value => this.dataSharingService.avatarImage.next(value));
    this.fileService.uploadFile(formData).subscribe((data:FileInformation) => this.uploadedImageInformation = data);
    this.fileUploadForm.reset;
    return true;
  }
}

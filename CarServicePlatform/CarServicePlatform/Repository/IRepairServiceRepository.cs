﻿using CarServicePlatform.Entities.RepairServices;

namespace CarServicePlatform.Repository;

public interface IRepairServiceRepository
{
    Task<List<RepairService>> GetAllServicesGroupByCategoriesAsync();
    Task<RepairService?> GetServiceAsync(int categoryId, int serviceId);
    Task<List<RepairService>> GetAllServicesAsync(int categoryId);
    Task<List<RepairService>> GetAllServicesForClientRequestAsync(int clientRequestId);
    Task<List<RepairService>> GetAllRepairServicesAvailableForServiceAsync(string serviceUserId);
    Task InsertServiceAsync(RepairService repairService);
    Task UpdateServiceAsync(RepairService repairService);
    Task DeleteServiceAsync(RepairService repairService);
}
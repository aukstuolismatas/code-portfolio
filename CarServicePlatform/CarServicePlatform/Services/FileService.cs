﻿using CarServicePlatform.Entities;
using CarServicePlatform.Repository;

namespace CarServicePlatform.Services;

public class FileService : IFileService
{
    private readonly IFileInformationRepository _fileInformationRepository;

    public FileService(IFileInformationRepository fileInformationRepository)
    {
        _fileInformationRepository = fileInformationRepository;
    }

    public async Task<List<FileInformation>> RetrieveAllDocumentsInformationByUserId(string userId)
    {
        return await _fileInformationRepository.GetAllDocumentsInformationForUserAsync(userId).ConfigureAwait(false);
    }

    public async Task<FileInformation> UploadImage(IFormFile file, string userId)
    {
        string filePath = FilePathFactory.ConstructProfileImagePath(userId);
        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }

        FileInformation fileInformation = new()
        {
            Path = filePath,
            Name = file.FileName,
            Size = file.Length,
            FileExtension = file.ContentType,
            FileType = FileType.Profile,
            DomainUserId = userId
        };
        CopyFileContentToFileSystem(fileInformation, file);
        await _fileInformationRepository.InsertFileInformationAsync(fileInformation).ConfigureAwait(false);
        return fileInformation;
    }

    public async Task<FileInformation> UploadCompanyDocuments(IFormFile file, string userId)
    {
        string filePath = FilePathFactory.ConstructCompanyDocumentsPath(userId);
        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }

        FileInformation fileInformation = new()
        {
            Path = filePath,
            Name = file.FileName,
            Size = file.Length,
            FileExtension = file.ContentType,
            FileType = FileType.Document,
            DomainUserId = userId
        };
        CopyFileContentToFileSystem(fileInformation, file);
        await _fileInformationRepository.InsertFileInformationAsync(fileInformation).ConfigureAwait(false);
        return fileInformation;
    }

    public async Task<List<FileInformation>> UploadRequestImages(IFormFileCollection files, string userId, int clientRequestId)
    {
        string filePath = FilePathFactory.ConstructClientRequestImagesPath(userId, clientRequestId);
        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }

        List<FileInformation> filesInformation = new();
        foreach (IFormFile file in files)
        {
            FileInformation fileInformation = new()
            {
                Path = filePath,
                Name = file.FileName,
                Size = file.Length,
                FileExtension = file.ContentType,
                FileType = FileType.Request,
                DomainUserId = userId,
                ClientRequestId = clientRequestId,
            };
            filesInformation.Add(fileInformation);
            CopyFileContentToFileSystem(fileInformation, file);
        }

        await _fileInformationRepository.InsertFileInformationListAsync(filesInformation).ConfigureAwait(false);
        return filesInformation;
    }

    private static async void CopyFileContentToFileSystem(FileInformation fileInformation, IFormFile file)
    {
        await using FileStream stream = new(fileInformation.Path + "/" + fileInformation.Name, FileMode.Create);
        await file.CopyToAsync(stream).ConfigureAwait(false);
    }

    public async Task<FileInformation?> DownloadProfileImage(string userId)
    {
        FileInformation? profileImageInformation = await _fileInformationRepository.GetFileInformationOfLastProfileImageAsync(userId).ConfigureAwait(false);
        return await ReadFile(profileImageInformation).ConfigureAwait(false);
    }

    public async Task<FileInformation?> DownloadDocument(string userId, int fileInformationId)
    {
        FileInformation? documentInformation = await _fileInformationRepository.GetFileInformationAsync(fileInformationId, userId).ConfigureAwait(false);
        return await ReadFile(documentInformation).ConfigureAwait(false);
    }

    public async Task<FileInformation?> DownloadDocumentForAdmin(int fileInformationId)
    {
        FileInformation? documentInformation = await _fileInformationRepository.GetFileInformationForAdminAsync(fileInformationId).ConfigureAwait(false);
        return await ReadFile(documentInformation).ConfigureAwait(false);
    }

    private static async Task<FileInformation?> ReadFile(FileInformation? fileInformation)
    {
        if (fileInformation == null)
        {
            return null;
        }

        string filePath = Path.Combine(fileInformation.Path, fileInformation.Name);
        fileInformation.FileContent = await File.ReadAllBytesAsync(filePath).ConfigureAwait(false);
        return fileInformation;
    }

    public async Task<List<FileInformation>?> DownloadClientRequestImages(int requestId)
    {
        List<FileInformation>? requestImagesInformation = await _fileInformationRepository.GetAllFileInformationForClientRequestAsync(requestId).ConfigureAwait(false);
        if (!requestImagesInformation.Any())
        {
            return null;
        }

        foreach (FileInformation imageInformation in requestImagesInformation)
        {
            string filePath = Path.Combine(imageInformation.Path, imageInformation.Name);
            imageInformation.FileContent = await File.ReadAllBytesAsync(filePath).ConfigureAwait(false);
            imageInformation.FileBase64 = Convert.ToBase64String(imageInformation.FileContent);
        }

        return requestImagesInformation;
    }
}
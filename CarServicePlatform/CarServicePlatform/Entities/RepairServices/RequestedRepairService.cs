﻿using System.ComponentModel.DataAnnotations;
using CarServicePlatform.Entities.Request;

namespace CarServicePlatform.Entities.RepairServices;

public class RequestedRepairService
{
    [Required]
    public int Id { get; set; }

    [Required]
    public int RepairServiceId { get; set; }

    public RepairService RepairService { get; set; }

    [Required]
    public int ClientRequestId { get; set; }

    public ClientRequest ClientRequest { get; set; }
}
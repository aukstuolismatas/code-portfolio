import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailTrawlAccountsComponent } from './detail-trawl-accounts.component';

describe('DetailTrawlAccountsComponent', () => {
  let component: DetailTrawlAccountsComponent;
  let fixture: ComponentFixture<DetailTrawlAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailTrawlAccountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailTrawlAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

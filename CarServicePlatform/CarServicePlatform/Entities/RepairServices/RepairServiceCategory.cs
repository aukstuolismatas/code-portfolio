﻿namespace CarServicePlatform.Entities.RepairServices;

public class RepairServiceCategory
{
    public int Id { get; set; }
    public string CategoryName { get; set; }
}
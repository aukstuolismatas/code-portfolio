﻿using System.Security.Claims;
using AutoMapper;
using CarServicePlatform.Auth;
using CarServicePlatform.Auth.Model;
using CarServicePlatform.Data.Dtos.File;
using CarServicePlatform.Data.Response;
using CarServicePlatform.Entities;
using CarServicePlatform.Entities.Request;
using CarServicePlatform.Entities.Users;
using CarServicePlatform.Repository;
using CarServicePlatform.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarServicePlatform.Controllers;

[ApiController]
[Authorize]
[Route("/images")]
public class FileController : ControllerBase
{
    private IMapper _mapper;
    private readonly IFileService _fileService;
    private readonly IClientRequestRepository _clientRequestRepository;
    private readonly IAuthorizationService _authorizationService;
    private UserManager<AppUser> _userManager;

    public FileController(IMapper mapper, IFileService fileService, UserManager<AppUser> userManager, IAuthorizationService authorizationService, IClientRequestRepository clientRequestRepository)
    {
        _mapper = mapper;
        _fileService = fileService;
        _userManager = userManager;
        _authorizationService = authorizationService;
        _clientRequestRepository = clientRequestRepository;
    }

    [HttpPost("profiles")]
    public async Task<ActionResult<FileInformationDto>> InsertImage([FromForm] UploadFile uploadFile)
    {
        if (uploadFile.File.Length <= 0)
        {
            return BadRequest($"Not supported file size {uploadFile.File.Length}");
        }

        ResponseModel response = FileValidationService.ValidateFile(uploadFile.File);
        if (!response.IsSuccess)
        {
            return BadRequest(response.Message);
        }

        string userId = User.FindFirst(CustomClaims.UserId)!.Value;
        FileInformation createdFileInformation = await _fileService.UploadImage(uploadFile.File, userId).ConfigureAwait(false);
        return CreatedAtAction(nameof(InsertImage), _mapper.Map<FileInformationDto>(createdFileInformation));
    }

    [HttpGet("profiles/{userId}")]
    public async Task<IActionResult> GetProfileImageData(string userId)
    {
        if (userId != User.FindFirst(CustomClaims.UserId)!.Value)
        {
            return Forbid();
        }

        FileInformation? profileImageInformation = await _fileService.DownloadProfileImage(userId).ConfigureAwait(false);
        if (profileImageInformation == null)
        {
            return NotFound($"The profile picture for this user id {userId} does not exists");
        }

        Stream stream = new MemoryStream(profileImageInformation.FileContent);
        return File(stream, profileImageInformation.FileExtension);
    }

    [HttpGet("clientRequest/{requestId}")]
    public async Task<ActionResult<FileDataDto>> GetRequestImagesData(int requestId)
    {
        ClientRequest? request = await _clientRequestRepository.GetRequestAsync(requestId).ConfigureAwait(false);
        if (request == null)
        {
            return NotFound($"Request with request id {requestId} was not found");
        }

        // AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, request, PolicyNames.SameUser).ConfigureAwait(false);
        // if (!authorizationResult.Succeeded)
        // {
        //     return Forbid();
        // }

        List<FileInformation>? requestImagesInformation = await _fileService.DownloadClientRequestImages(requestId).ConfigureAwait(false);
        if (requestImagesInformation == null)
        {
            return NotFound($"The requests pictures for this user id {requestId} does not exists");
        }

        return Ok(requestImagesInformation.Select(x => _mapper.Map<FileDataDto>(x)));
    }

    [HttpPost("clientRequest/{clientRequestId}")]
    public async Task<ActionResult<FileInformation>> InsertClientRequestImages(int clientRequestId)
    {
        IFormFileCollection files = Request.Form.Files;
        if (!files.Any())
        {
            return NoContent();
        }

        ResponseModel filesCountValidation = FileValidationService.ValidateFilesCount(files);
        if (!filesCountValidation.IsSuccess)
        {
            return BadRequest(filesCountValidation.Message);
        }

        ResponseModel? filesValidationResponse = files
                                                 .Select(FileValidationService.ValidateFile)
                                                 .FirstOrDefault(x => x.IsSuccess == false);
        if (filesValidationResponse is { IsSuccess: false })
        {
            return BadRequest(filesValidationResponse.Message);
        }

        string userId = User.FindFirstValue(CustomClaims.UserId);
        List<FileInformation> filesInformation = await _fileService.UploadRequestImages(files, userId, clientRequestId).ConfigureAwait(false);
        return Created($"/images/clientRequest/{clientRequestId}", _mapper.Map<List<FileInformationDto>>(filesInformation));
    }

    [HttpPost("documents")]
    [Authorize(Roles = AppUserRoles.Service + "," + AppUserRoles.Trawl)]
    public async Task<ActionResult<FileInformationDto>> InsertDocuments([FromForm] UploadFile uploadFile)
    {
        if (uploadFile.File.Length <= 0)
        {
            return BadRequest($"Not supported file size {uploadFile.File.Length}");
        }

        ResponseModel response = FileValidationService.ValidateDocumentFile(uploadFile.File);
        if (!response.IsSuccess)
        {
            return BadRequest(response.Message);
        }

        string userId = User.FindFirst(CustomClaims.UserId)!.Value;
        FileInformation createdFileInformation = await _fileService.UploadCompanyDocuments(uploadFile.File, userId).ConfigureAwait(false);
        return CreatedAtAction(nameof(InsertImage), _mapper.Map<FileInformationDto>(createdFileInformation));
    }

    [HttpGet("documents/data/{id}")]
    [Authorize(Roles = AppUserRoles.Service + "," + AppUserRoles.Trawl)]
    public async Task<IActionResult> GetDocumentData(int id)
    {
        string userId = User.FindFirstValue(CustomClaims.UserId);
        FileInformation? documentInformation = await _fileService.DownloadDocument(userId, id).ConfigureAwait(false);
        if (documentInformation == null)
        {
            return NotFound($"The document for this user id {userId} does not exists");
        }

        Stream stream = new MemoryStream(documentInformation.FileContent);
        return File(stream, documentInformation.FileExtension, documentInformation.Name);
    }

    [HttpGet("documents/admin/data/{id}")]
    [Authorize(Roles = AppUserRoles.Admin)]
    public async Task<IActionResult> GetDocumentDataForAdmin(int id)
    {
        FileInformation? documentInformation = await _fileService.DownloadDocumentForAdmin(id).ConfigureAwait(false);
        if (documentInformation == null)
        {
            return NotFound($"The document with id {id} does not exists");
        }

        Stream stream = new MemoryStream(documentInformation.FileContent);
        return File(stream, documentInformation.FileExtension, documentInformation.Name);
    }

    [HttpGet("documents/{userId}")]
    [Authorize(Roles = AppUserRoles.Service + "," + AppUserRoles.Trawl)]
    public async Task<ActionResult<IEnumerable<FileInformation>>> GetDocumentsInformation(string userId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, userId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<FileInformation> documentsInformation = await _fileService.RetrieveAllDocumentsInformationByUserId(userId).ConfigureAwait(false);
        if (!documentsInformation.Any())
        {
            return NotFound($"Documents for this user id {userId} does not exists");
        }

        return Ok(documentsInformation.Select(x => _mapper.Map<FileInformationDto>(x)));
    }
}
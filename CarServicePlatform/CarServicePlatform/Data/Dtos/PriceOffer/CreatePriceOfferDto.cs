﻿namespace CarServicePlatform.Data.Dtos.PriceOffer;

public record CreatePriceOfferDto(int Price, int Duration, int ClientRequestId);
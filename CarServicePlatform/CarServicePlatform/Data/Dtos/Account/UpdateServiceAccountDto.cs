﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.Account;

public record UpdateServiceAccountDto([EmailAddress] string Email, [Phone] string PhoneNumber, string Name, string Surname, string Address, string? CurrentPassword, string? NewPassword, string City);
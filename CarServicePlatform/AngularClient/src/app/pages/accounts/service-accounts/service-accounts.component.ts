import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { ProfileService } from 'src/app/services/profile.service';
import { ServiceAccount, UserAccount } from 'src/app/shared/account';

@Component({
  selector: 'app-service-accounts',
  templateUrl: './service-accounts.component.html',
  styleUrls: ['./service-accounts.component.css']
})
export class ServiceAccountsComponent implements OnInit {

  public errorMessage ='';
  public successMessage ='';
  serviceAccounts: ServiceAccount[]=[];
  tabName = HeaderConstants.ServiceTab
  constructor(private profileService: ProfileService, private route: ActivatedRoute, private router: Router, private dataSharingService: DataSharingService) { 
    dataSharingService.serviceAccountsList.subscribe(value => this.serviceAccounts = value);
    profileService.loadServiceAccounts().subscribe((data:ServiceAccount[])=>this.dataSharingService.serviceAccountsList.next(data))
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params =>{
      if(params['login'] !== undefined && params['login'] === 'true'){
        this.successMessage = MessageConstants.LoginSuccess;
      }
    });
  }
  openItem(index:number){
    this.router.navigate([`${AppConstants.AccountsService}/${this.serviceAccounts[index].id}/${index}`]);
  }

  activateAccount(arrayIndex: number, userId:string){
    this.profileService.activateAccount(userId).subscribe({
        next:(data:UserAccount)=>{
          this.serviceAccounts[arrayIndex].isAccountActivated=data.isAccountActivated
          this.successMessage = MessageConstants.AccountActivated
        },
        error: () => this.errorMessage = MessageConstants.AccountActivatedFailed
      });
    this.successMessage='';
    this.errorMessage ='';
  }

  deactivateAccount(arrayIndex: number, userId:string){
    this.profileService.deactivateAccount(userId).subscribe({
      next:(data:UserAccount)=>{
        this.serviceAccounts[arrayIndex].isAccountActivated=data.isAccountActivated
        this.successMessage = MessageConstants.AccountDeactivated
      },
      error: () => this.errorMessage = MessageConstants.AccountActivatedFailed
    });
    this.successMessage='';
    this.errorMessage ='';
  }

  deleteService(arrayIndex: number, userId:string){
    this.profileService.deleteServiceAccount(userId).subscribe({
      next: () => {
        this.successMessage = MessageConstants.DeleteAccountSuccess;
        delete this.serviceAccounts[arrayIndex];
      },
      error: () => this.errorMessage = MessageConstants.DeleteAccountFailed
    });
    this.successMessage='';
    this.errorMessage ='';
  }

}

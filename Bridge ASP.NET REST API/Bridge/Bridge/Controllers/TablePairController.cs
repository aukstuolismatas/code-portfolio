﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bridge.Auth;
using Bridge.Auth.Model;
using Bridge.Data.Dtos.TablePair;
using Bridge.Data.Entities;
using Bridge.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.Controllers
{
    [ApiController]
    [Route("api/tournaments/{tournamentId}/tables/{tableId}/tablePairs")]
    public class TablePairController : ControllerBase
    {
        private readonly ITablesRepository _tablesRepository;
        private readonly IPairsRepository _pairsRepository;
        private readonly ITablePairRepository _tablePairsRepository;
        private readonly IAuthorizationService _authorizationService;
        private readonly IMapper _mapper;

        public TablePairController(ITablesRepository tablesRepository, IMapper mapper, IPairsRepository pairsRepository, ITablePairRepository tablePairsRepository, IAuthorizationService authorizationService)
        {
            _tablePairsRepository = tablePairsRepository;
            _authorizationService = authorizationService;
            _tablesRepository = tablesRepository;
            _pairsRepository = pairsRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TablePairDto>>> GetAllAsync(int tournamentId, int tableId)
        {
            Table table = await _tablesRepository.GetAsync(tournamentId, tableId).ConfigureAwait(false);
            if (table == null)
            {
                return NotFound($"Tournament with id {tournamentId} and Table with id {tableId} not exists");
            }

            List<TablePair> tablePairs = await _tablePairsRepository.GetAllAsync(tableId).ConfigureAwait(false);
            return Ok(tablePairs.Select(x => _mapper.Map<TablePairDto>(x)));
        }

        [HttpGet("{tablePairId}")]
        public async Task<ActionResult<TablePairDto>> GetAsync(int tournamentId, int tableId, int tablePairId)
        {
            Table table = await _tablesRepository.GetAsync(tournamentId, tableId).ConfigureAwait(false);
            if (table == null)
            {
                return NotFound($"Tournament with id {tournamentId} and Table with id {tableId} not exists");
            }

            TablePair tablePair = await _tablePairsRepository.GetAsync(tableId, tablePairId).ConfigureAwait(false);
            if (tablePair == null)
            {
                return NotFound($"TablePair with id {tablePairId} not exists");
            }

            return Ok(_mapper.Map<TablePairDto>(tablePair));
        }

        [HttpPost]
        [Authorize(Roles = BridgeRestUserRoles.Table)]
        public async Task<ActionResult<TablePairDto>> PostAsync(int tournamentId, int tableId, CreateTablePairDto tablePairDto)
        {
            Table table = await _tablesRepository.GetAsync(tournamentId, tableId).ConfigureAwait(false);
            if (table == null)
            {
                return NotFound($"Tournament with id {tournamentId} and Table with id {tableId} not exists");
            }

            AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, table, PolicyNames.SameUser).ConfigureAwait(false);
            if (!authorizationResult.Succeeded)
            {
                return Forbid();
            }

            Pair pair = await _pairsRepository.GetAsync(tablePairDto.PairId).ConfigureAwait(false);
            if (pair == null)
            {
                return UnprocessableEntity($"Pair with id {tablePairDto.PairId} not exists");
            }

            TablePair tablePair = _mapper.Map<TablePair>(tablePairDto);
            tablePair.TableId = tableId;

            await _tablePairsRepository.InsertAsync(tablePair).ConfigureAwait(false);

            return Created($"/api/tournaments/{tournamentId}/tables/{table.Id}/tablePairs/{tablePair.Id}", _mapper.Map<TablePairDto>(tablePair));
        }

        [HttpPut("{tablePairId}")]
        [Authorize(Roles = BridgeRestUserRoles.Table)]
        public async Task<ActionResult<TablePairDto>> Put(int tournamentId, int tableId, int tablePairId, UpdateTablePairDto tablePairDto)
        {
            Table table = await _tablesRepository.GetAsync(tournamentId, tableId).ConfigureAwait(false);
            if (table == null)
            {
                return NotFound($"Tournament with id {tournamentId} and Table with id {tableId} not exists");
            }

            AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, table, PolicyNames.SameUser).ConfigureAwait(false);
            if (!authorizationResult.Succeeded)
            {
                return Forbid();
            }

            TablePair oldTablePair = await _tablePairsRepository.GetAsync(tableId, tablePairId).ConfigureAwait(false);
            if (oldTablePair == null)
            {
                return NotFound($"Table with id {tableId} and TablePair with {tablePairId} not exists");
            }

            Pair pair = await _pairsRepository.GetAsync(tablePairDto.PairId).ConfigureAwait(false);
            if (pair == null)
            {
                return UnprocessableEntity($"Pair with id {tablePairDto.PairId} not exists");
            }

            _mapper.Map(tablePairDto, oldTablePair);

            await _tablePairsRepository.UpdateAsync(oldTablePair).ConfigureAwait(false);

            return Ok(_mapper.Map<TablePairDto>(oldTablePair));
        }

        [HttpDelete("{tablePairId}")]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult> Delete(int tournamentId, int tableId, int tablePairId)
        {
            Table table = await _tablesRepository.GetAsync(tournamentId, tableId).ConfigureAwait(false);
            if (table == null)
            {
                return NotFound($"Tournament with id {tournamentId} and Table with id {tableId} not exists");
            }

            TablePair tablePair = await _tablePairsRepository.GetAsync(tableId, tablePairId).ConfigureAwait(false);
            if (tablePair == null)
            {
                return NotFound($"Table with id {tableId} and TablePair with id {tablePairId} not exists");
            }

            await _tablePairsRepository.DeleteAsync(tablePair).ConfigureAwait(false);

            return NoContent();
        }
    }
}
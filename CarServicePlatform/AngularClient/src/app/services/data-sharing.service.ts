import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ServiceAccount, TrawlAccount } from '../shared/account';
import { ApprovedClientRequest, ApprovedClientRequestTrawl, RegisteredClientRequest, RegisteredClientRequestTrawl, RejectedClientRequest, RejectedClientRequestTrawl } from '../shared/client-request';

@Injectable()
export class DataSharingService {
  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public userName: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public avatarImage: BehaviorSubject<any> = new BehaviorSubject<any>('../../assets/avatar.jpg');
  public requestList: BehaviorSubject<RegisteredClientRequest[]> = new BehaviorSubject<RegisteredClientRequest[]>([]);
  public trawlRequestList: BehaviorSubject<RegisteredClientRequestTrawl[]> = new BehaviorSubject<RegisteredClientRequestTrawl[]>([]);
  public approvedRequestList: BehaviorSubject<ApprovedClientRequest[]> = new BehaviorSubject<ApprovedClientRequest[]>([]);
  public approvedTrawlRequestList: BehaviorSubject<ApprovedClientRequestTrawl[]> = new BehaviorSubject<ApprovedClientRequestTrawl[]>([]);
  public rejectedTrawlRequestList: BehaviorSubject<RejectedClientRequestTrawl[]> = new BehaviorSubject<RejectedClientRequestTrawl[]>([]);
  public rejectedRequestList: BehaviorSubject<RejectedClientRequest[]> = new BehaviorSubject<RejectedClientRequest[]>([]);
  
  public trawlAccountsList: BehaviorSubject<TrawlAccount[]> = new BehaviorSubject<TrawlAccount[]>([]);
  public serviceAccountsList: BehaviorSubject<ServiceAccount[]> = new BehaviorSubject<ServiceAccount[]>([]);

  public cleanUserVariables(){
    this.userName.next('');
    this.avatarImage.next('');
  }
}

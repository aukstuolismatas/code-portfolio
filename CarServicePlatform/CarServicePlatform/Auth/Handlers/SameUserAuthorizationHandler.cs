﻿using System.Security.Claims;
using CarServicePlatform.Auth.Model;
using Microsoft.AspNetCore.Authorization;

namespace CarServicePlatform.Auth.Handlers;

public class SameUserAuthorizationHandler : AuthorizationHandler<SameUserRequirement, IUserOwnedResources>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, SameUserRequirement requirement, IUserOwnedResources resource)
    {
        if (context.User.IsInRole(AppUserRoles.Admin) || context.User.FindFirstValue(CustomClaims.UserId) == resource.DomainUserId)
        {
            context.Succeed(requirement);
        }

        return Task.CompletedTask;
    }
}

public record SameUserRequirement : IAuthorizationRequirement;
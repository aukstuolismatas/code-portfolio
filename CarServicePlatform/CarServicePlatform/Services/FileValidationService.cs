﻿using CarServicePlatform.Data.Response;

namespace CarServicePlatform.Services;

public static class FileValidationService
{
    private static readonly long MaxFileSize = 6 * 1024 * 1024;
    private static readonly int MaxFileCount = 5;
    private static readonly string[] SupportedExtensions = new[] { ".jpg", ".png", "jpeg" };
    private static readonly string[] SupportedDocumentExtensions = new[] { ".pdf" };

    private static bool IsFileSizeValid(IFormFile file)
    {
        return file.Length <= MaxFileSize;
    }

    private static bool IsFileFormatValid(IFormFile file)
    {
        string extension = Path.GetExtension(file.FileName).ToLower();
        return SupportedExtensions.Contains(extension);
    }

    private static bool IsDocumentFormatValid(IFormFile file)
    {
        string extension = Path.GetExtension(file.FileName).ToLower();
        return SupportedDocumentExtensions.Contains(extension);
    }

    public static ResponseModel ValidateFile(IFormFile file)
    {
        ResponseModel model = new();
        if (!IsFileSizeValid(file))
        {
            model.Message = $"File size {file.Length} is exceeding the limit ({MaxFileSize})";
            return model;
        }

        if (!IsFileFormatValid(file))
        {
            model.Message = $"{Path.GetExtension(file.FileName)} file format is not supported. Supported formats {string.Join(", ", SupportedExtensions)}";
            return model;
        }

        model.IsSuccess = true;
        return model;
    }

    public static ResponseModel ValidateDocumentFile(IFormFile file)
    {
        ResponseModel model = new();
        if (!IsFileSizeValid(file))
        {
            model.Message = $"File size {file.Length} is exceeding the limit ({MaxFileSize})";
            return model;
        }

        if (!IsDocumentFormatValid(file))
        {
            model.Message = $"{Path.GetExtension(file.FileName)} file format is not supported. Supported formats {string.Join(", ", SupportedDocumentExtensions)}";
            return model;
        }

        model.IsSuccess = true;
        return model;
    }

    public static ResponseModel ValidateFilesCount(IFormFileCollection files)
    {
        ResponseModel model = new();
        if (files.Count > MaxFileCount)
        {
            model.IsSuccess = false;
            model.Message = $"Exceeding file count limit {MaxFileCount}";
            return model;
        }

        model.IsSuccess = true;
        return model;
    }
}
﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.Request;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class ClientRequestRepository : IClientRequestRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public ClientRequestRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<ClientRequest?> GetRequestAsync(int requestId)
    {
        return await _carPlatformDbContext.ClientRequests.FirstOrDefaultAsync(x => x.ClientRequestId == requestId).ConfigureAwait(false);
    }

    public async Task<ClientRequest?> GetRequestForTrawlAsync(int requestId)
    {
        return await _carPlatformDbContext.PriceOffers.Where(x => x.ClientRequestId == requestId && x.Status == OfferStatus.Approved)
                                          .Join(_carPlatformDbContext.ClientRequests, offer => offer.ClientRequestId, request => request.ClientRequestId, (offer, request) =>
                                              new
                                              {
                                                  offer,
                                                  request
                                              })
                                          .Join(_carPlatformDbContext.ServiceAccounts, offerWithRequest => offerWithRequest.offer.ServiceAccountId, account => account.Id, (offerWithRequest, account) => new ClientRequest
                                          {
                                              ClientRequestId = offerWithRequest.request.ClientRequestId,
                                              DomainUserId = offerWithRequest.request.DomainUserId,
                                              Title = offerWithRequest.request.Title,
                                              Status = offerWithRequest.request.Status,
                                              Address = offerWithRequest.request.Address,
                                              City = offerWithRequest.request.City,
                                              Created = offerWithRequest.request.Created,
                                              Description = offerWithRequest.request.Description,
                                              ServiceAccountId = account.Id,
                                              ServiceAddress = account.Address,
                                              ServiceCompanyName = account.CompanyName,
                                          })
                                          .FirstOrDefaultAsync(x => x.ClientRequestId == requestId)
                                          .ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRequestsAsync()
    {
        return await _carPlatformDbContext.ClientRequests.ToListAsync().ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRequestsAvailableForService(string serviceId)
    {
        return await _carPlatformDbContext.AvailableRepairServices.Where(x => x.DomainUserId == serviceId)
                                          .Join(_carPlatformDbContext.RequestedRepairServices, availableService => availableService.RepairServiceId,
                                              requestedService => requestedService.RepairServiceId,
                                              (availableService, requestedService) => new { availableService.City, requestedService.ClientRequestId })
                                          .Join(_carPlatformDbContext.ClientRequests, requestedService => requestedService.ClientRequestId, request => request.ClientRequestId,
                                              (requestedService, request) => new { request, requestedService.City })
                                          .Where(arg => arg.request.City == arg.City &&
                                                        arg.request.Status == RequestStatus.Waiting &&
                                                        !_carPlatformDbContext.PriceOffers.Any(x => x.ClientRequestId == arg.request.ClientRequestId && x.ServiceAccountId == serviceId))
                                          .Select(arg => arg.request)
                                          .Distinct()
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRequestAvailableForTrawl(string trawlId)
    {
        return await _carPlatformDbContext.TrawlAccounts.Where(x => x.Id == trawlId)
                                          .Join(_carPlatformDbContext.ClientRequests, account => account.City, request => request.City, (account, request) => request)
                                          .Where(x => x.Status == RequestStatus.WaitingTransfer && !_carPlatformDbContext.TransferOffers.Any(o => o.ClientRequestId == x.ClientRequestId && o.TrawlAccountId == trawlId))
                                          .Join(_carPlatformDbContext.PriceOffers, request => request.ClientRequestId, offer => offer.ClientRequestId, (request, offer) => new { request, offer })
                                          .Where(x => x.offer.Status == OfferStatus.Approved)
                                          .Join(_carPlatformDbContext.ServiceAccounts, requestsWithOffers => requestsWithOffers.offer.ServiceAccountId, account => account.Id, (requestsWithOffers, account) => new ClientRequest
                                          {
                                              ClientRequestId = requestsWithOffers.request.ClientRequestId,
                                              DomainUserId = requestsWithOffers.request.DomainUserId,
                                              Title = requestsWithOffers.request.Title,
                                              Status = requestsWithOffers.request.Status,
                                              Address = requestsWithOffers.request.Address,
                                              City = requestsWithOffers.request.City,
                                              Created = requestsWithOffers.request.Created,
                                              Description = requestsWithOffers.request.Description,
                                              ServiceAddress = account.Address,
                                              ServiceCompanyName = account.CompanyName
                                          })
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRequestsWaitingApprovalForService(string serviceId)
    {
        return await _carPlatformDbContext.PriceOffers.Where(x => x.ServiceAccountId == serviceId && x.Status == OfferStatus.Waiting)
                                          .Join(_carPlatformDbContext.ClientRequests, offer => offer.ClientRequestId, request => request.ClientRequestId,
                                              (offer, request) => request)
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRequestsWaitingApprovalForTrawl(string trawlId)
    {
        return await _carPlatformDbContext.TransferOffers.Where(x => x.TrawlAccountId == trawlId && x.Status == OfferStatus.Waiting)
                                          .Join(_carPlatformDbContext.ClientRequests, offer => offer.ClientRequestId, request => request.ClientRequestId,
                                              (offer, request) => request)
                                          .Join(_carPlatformDbContext.PriceOffers, request => request.ClientRequestId, priceOffer => priceOffer.ClientRequestId, (request, priceOffer) => new
                                          {
                                              request,
                                              priceOffer
                                          })
                                          .Where(x => x.priceOffer.Status == OfferStatus.Approved)
                                          .Join(_carPlatformDbContext.ServiceAccounts, args => args.priceOffer.ServiceAccountId, account => account.Id, (args, account) => new ClientRequest
                                          {
                                              ClientRequestId = args.request.ClientRequestId,
                                              DomainUserId = args.request.DomainUserId,
                                              Title = args.request.Title,
                                              Status = args.request.Status,
                                              Address = args.request.Address,
                                              City = args.request.City,
                                              Created = args.request.Created,
                                              Description = args.request.Description,
                                              ServiceAddress = account.Address,
                                              ServiceCompanyName = account.CompanyName
                                          })
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRequestsAssignedForService(string serviceId)
    {
        return await _carPlatformDbContext.PriceOffers.Where(x => x.ServiceAccountId == serviceId && x.Status == OfferStatus.Approved)
                                          .Join(_carPlatformDbContext.ClientRequests, offer => offer.ClientRequestId, request => request.ClientRequestId,
                                              (offer, request) => new ClientRequest
                                              {
                                                  ClientRequestId = request.ClientRequestId,
                                                  DomainUserId = request.DomainUserId,
                                                  Title = request.Title,
                                                  Status = request.Status,
                                                  Address = request.Address,
                                                  City = request.City,
                                                  Created = request.Created,
                                                  Description = request.Description,
                                                  Duration = offer.Duration,
                                                  Price = offer.Price
                                              })
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRequestsAssignedForTrawl(string trawlId)
    {
        return await _carPlatformDbContext.TransferOffers.Where(x => x.TrawlAccountId == trawlId && x.Status == OfferStatus.Approved)
                                          .Join(_carPlatformDbContext.ClientRequests, offer => offer.ClientRequestId, request => request.ClientRequestId,
                                              (offer, request) => new { offer, request })
                                          .Join(_carPlatformDbContext.PriceOffers, arg => arg.request.ClientRequestId, priceOffer => priceOffer.ClientRequestId, (arg, priceOffer)=>
                                              new { arg, priceOffer.ServiceAccountId, priceOffer.Status })
                                          .Where(x => x.Status == OfferStatus.Approved)
                                          .Join(_carPlatformDbContext.ServiceAccounts, requestTransferAndPriceOffer => requestTransferAndPriceOffer.ServiceAccountId, account => account.Id, (requestTransferAndPriceOffer, account) =>
                                              new ClientRequest
                                              {
                                                  ClientRequestId = requestTransferAndPriceOffer.arg.request.ClientRequestId,
                                                  DomainUserId = requestTransferAndPriceOffer.arg.request.DomainUserId,
                                                  Title = requestTransferAndPriceOffer.arg.request.Title,
                                                  Status = requestTransferAndPriceOffer.arg.request.Status,
                                                  Description = requestTransferAndPriceOffer.arg.request.Description,
                                                  Address = requestTransferAndPriceOffer.arg.request.Address,
                                                  City = requestTransferAndPriceOffer.arg.request.City,
                                                  Created = requestTransferAndPriceOffer.arg.request.Created,
                                                  ArrivalTime = requestTransferAndPriceOffer.arg.offer.DateAndTime,
                                                  Price = requestTransferAndPriceOffer.arg.offer.Price,
                                                  ServiceCompanyName = account.CompanyName,
                                                  ServiceAddress = account.Address
                                              })
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRejectedClientRequestsForService(string serviceId)
    {
        return await _carPlatformDbContext.PriceOffers.Where(x => x.ServiceAccountId == serviceId && x.Status == OfferStatus.Rejected)
                                          .Join(_carPlatformDbContext.ClientRequests, offer => offer.ClientRequestId, request => request.ClientRequestId,
                                              (offer, request) => new ClientRequest
                                              {
                                                  ClientRequestId = request.ClientRequestId,
                                                  DomainUserId = request.DomainUserId,
                                                  Title = request.Title,
                                                  Status = request.Status,
                                                  Address = request.Address,
                                                  City = request.City,
                                                  Created = request.Created,
                                                  Description = request.Description,
                                                  Duration = offer.Duration,
                                                  Price = offer.Price,
                                                  OfferStatus = offer.Status
                                              })
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<ClientRequest>> GetAllRejectedClientRequestsForTrawl(string trawlId)
    {
        return await _carPlatformDbContext.TransferOffers.Where(x => x.TrawlAccountId == trawlId && x.Status == OfferStatus.Rejected)
                                          .Join(_carPlatformDbContext.ClientRequests, offer => offer.ClientRequestId, request => request.ClientRequestId,
                                              (offer, request) => new ClientRequest
                                              {
                                                  ClientRequestId = request.ClientRequestId,
                                                  DomainUserId = request.DomainUserId,
                                                  Title = request.Title,
                                                  Status = request.Status,
                                                  Address = request.Address,
                                                  City = request.City,
                                                  Created = request.Created,
                                                  Description = request.Description,
                                                  ArrivalTime = offer.DateAndTime,
                                                  Price = offer.Price,
                                                  OfferStatus = offer.Status
                                              })
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task InsertRequestAsync(ClientRequest clientRequest)
    {
        _carPlatformDbContext.ClientRequests.Add(clientRequest);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task UpdateRequestAsync(ClientRequest clientRequest)
    {
        _carPlatformDbContext.ClientRequests.Update(clientRequest);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteRequestAsync(ClientRequest clientRequest)
    {
        _carPlatformDbContext.ClientRequests.Remove(clientRequest);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
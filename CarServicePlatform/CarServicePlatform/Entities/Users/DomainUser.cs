﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CarServicePlatform.Entities.Users;

public abstract class DomainUser
{
    [ForeignKey(nameof(AppUser))]
    public string Id { get; set; }

    public string Name { get; set; }
    public string Surname { get; set; }
}
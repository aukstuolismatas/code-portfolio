﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.Account;

public record UpdateClientAccountDto([EmailAddress] string Email, [Phone] string PhoneNumber, string Name, string Surname, string? CurrentPassword, string? NewPassword);
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConfirmationAlertComponent } from 'src/app/components/confirmation-alert/confirmation-alert.component';
import { ModalComponent } from 'src/app/components/modal/modal.component';
import { MessageConstants } from 'src/app/message-constants';
import { ModalService } from 'src/app/services/modal.service';
import { ProfileService } from 'src/app/services/profile.service';
import { ServiceAccount, UserAccount } from 'src/app/shared/account';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
 
  public errorMessage ='';
  public successMessage ='';
  serviceAccounts: ServiceAccount[]=[];
  
  constructor(private profileService: ProfileService, private modalService: ModalService<ConfirmationAlertComponent>, private route: ActivatedRoute) { 
    profileService.loadServiceAccounts().subscribe((data:ServiceAccount[])=>this.serviceAccounts=data)
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params =>{
      if(params['login'] !== undefined && params['login'] === 'true'){
        this.successMessage = MessageConstants.LoginSuccess;
      }
    });
  }

  activateAccount(arrayIndex: number, userId:string){
    this.profileService.activateAccount(userId).subscribe((data:UserAccount)=>this.serviceAccounts[arrayIndex].isAccountActivated=data.isAccountActivated);
  }

  deactivateAccount(arrayIndex: number, userId:string){
    this.profileService.deactivateAccount(userId).subscribe((data:UserAccount)=>this.serviceAccounts[arrayIndex].isAccountActivated=data.isAccountActivated);
  }

  deleteService(arrayIndex: number, userId:string){
    this.profileService.deleteServiceAccount(userId).subscribe({
      next: () => {
        this.successMessage = MessageConstants.DeleteAccountSuccess;
        delete this.serviceAccounts[arrayIndex];
      },
      error: () => this.errorMessage = MessageConstants.DeleteAccountFailed
    });
    this.successMessage='';
    this.errorMessage ='';
  }

  async showAlert(): Promise<void> {
    const {ConfirmationAlertComponent} = await import(
      'src/app/components/confirmation-alert/confirmation-alert.component'
    );

    await this.modalService.open(ConfirmationAlertComponent);
    console.log(this.modalService.confirmation);
  }
}

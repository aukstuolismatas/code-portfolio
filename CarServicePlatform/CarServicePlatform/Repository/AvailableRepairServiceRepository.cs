﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.RepairServices;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class AvailableRepairServiceRepository : IAvailableRepairServiceRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public AvailableRepairServiceRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<List<AvailableRepairService>> GetAllAvailableRepairServiceForServiceUserAsync(string userId)
    {
        return await _carPlatformDbContext.AvailableRepairServices
                                          .Where(x => userId == x.DomainUserId)
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<AvailableRepairService>> GetAllAvailableRepairServiceAsync()
    {
        return await _carPlatformDbContext.AvailableRepairServices.ToListAsync().ConfigureAwait(false);
    }

    public async Task InsertAvailableRepairServiceAsync(List<AvailableRepairService> availableRepairServices)
    {
        _carPlatformDbContext.AvailableRepairServices.AddRange(availableRepairServices);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task UpdateAvailableRepairServiceAsync(List<AvailableRepairService> availableRepairServices)
    {
        _carPlatformDbContext.AvailableRepairServices.UpdateRange(availableRepairServices);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteAvailableRepairServiceAsync(List<AvailableRepairService> availableRepairServices)
    {
        _carPlatformDbContext.AvailableRepairServices.RemoveRange(availableRepairServices);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
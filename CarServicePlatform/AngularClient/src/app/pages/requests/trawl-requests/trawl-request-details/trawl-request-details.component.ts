import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { RequestStatusConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FileService } from 'src/app/services/file.service';
import { ProfileService } from 'src/app/services/profile.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { TrawlTransferRequestService } from 'src/app/services/trawl-transfer-request.service';
import { TrawlAccount } from 'src/app/shared/account';
import { ApprovedClientRequestTrawl, PlaceTransferOffer, PriceOffer, RegisteredClientRequest, RegisteredClientRequestTrawl, TransferOffer } from 'src/app/shared/client-request';

@Component({
  selector: 'app-trawl-request-details',
  templateUrl: './trawl-request-details.component.html',
  styleUrls: ['./trawl-request-details.component.css']
})
export class TrawlRequestDetailsComponent implements OnInit {

  index:number=0;
  requestId:number=0;
  imageIndex:number=0;
  clientRequestDetail!: RegisteredClientRequestTrawl;
  transferOffer!:TransferOffer
  imagesArray: any[]=[]
  imageUrl: SafeUrl | string =''

  offerForm : FormGroup = new FormGroup({
    price: new FormControl('', [Validators.required]),
    arrivalDate: new FormControl('', [Validators.required]),
    arrivalTime: new FormControl('', [Validators.required])
  });
  submitted=false
  successMessage=''
  errorMessage=''
  titles=['Kaina', 'Atvykimo laikas', '']
  descriptionTab=true;
  offersTab=false;
  viewerOpen=false;
  date:Date= new Date(Date.now())
  trawlDetails!:TrawlAccount
  constructor(private activatedRoute: ActivatedRoute, private dataSharingService: DataSharingService, private trawlTransferRequestService: TrawlTransferRequestService, 
    private repairRequestService: RepairRequestService, public fileService: FileService, private authService: AuthService, private profileService: ProfileService) {
    }
  ngOnInit(): void {
    this.index = this.activatedRoute.snapshot.params['index'];
    this.requestId = this.activatedRoute.snapshot.params['id'];
    this.clientRequestDetail = this.dataSharingService.trawlRequestList.value[this.index];
    if(this.clientRequestDetail===undefined){
      this.trawlTransferRequestService.retrieveClientRequestForTrawlById(this.requestId).subscribe((data:RegisteredClientRequestTrawl)=>this.clientRequestDetail=data);
    }
    this.profileService.loadTrawlAccountByEmail(this.authService.getEmail()).subscribe((data:TrawlAccount)=> {
      this.trawlDetails = data;
    });
    this.fileService.downloadRequestImages(this.requestId).subscribe(data=> {
      this.imagesArray=data      
    })
    this.offerForm.patchValue({
      arrivalDate: this.date
    })

    this.trawlTransferRequestService.retrieveTrawlTransferOffer(this.requestId).subscribe((data:TransferOffer) => this.transferOffer = data)
  }

  get f(): {[key:string]:AbstractControl} { return this.offerForm.controls; }


  sendOffer(){
    console.log(this.offerForm.controls['arrivalDate'].value.h);
    console.log(this.offerForm.controls['arrivalTime'].value);
    this.date=  this.offerForm.controls['arrivalDate'].value+this.offerForm.controls['arrivalTime'].value;//new Date(this.offerForm.controls['arrivalDate'].value+this.offerForm.controls['arrivalTime'].value)
    console.log(this.date);
    const format: Date = new Date(this.date);
    console.log(format)
    const transferOffer: PlaceTransferOffer = {
      clientRequestId: this.requestId,
      price: this.f['price'].value,
      dateAndTime: this.offerForm.controls['arrivalDate'].value+" "+this.offerForm.controls['arrivalTime'].value
    }
    this.submitted=false;
    if (this.offerForm.invalid) {
      this.submitted = true;
       return;
    }
    this.trawlTransferRequestService.placeTransferOffer(transferOffer).subscribe({
      next: (data:TransferOffer) => {
        this.transferOffer=data;
        this.successMessage = MessageConstants.PlacePriceOfferSuccess
      },
      error: (err) => this.errorMessage = MessageConstants.PlacePriceOfferFailed
    })
    this.errorMessage=''
    this.successMessage=''
    this.f['price'].setValue(''); 
    this.f['time'].setValue(''); 
  }

  isTrawl(){
    return this.authService.isTrawl();
  }

  description(){
    this.descriptionTab=true;
    this.offersTab =false
  }

  offers(){
    this.descriptionTab=false;
    this.offersTab =true
  }

  isRequestSent(){
    return this.transferOffer===undefined
  }

  
}

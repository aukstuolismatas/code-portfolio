import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrawlTabComponent } from './trawl-tab.component';

describe('TrawlTabComponent', () => {
  let component: TrawlTabComponent;
  let fixture: ComponentFixture<TrawlTabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrawlTabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrawlTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

﻿namespace CarServicePlatform.Data.Dtos.Auth;

public record SuccessfulLoginResponseDto(string AccessToken, DateTime Expiration);
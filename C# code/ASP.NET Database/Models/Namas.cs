using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
namespace NT_Csharp.Models
{
    public class Namas
    {
        [DisplayName("Rekuperacija")]
        public bool rekuperacija { get; set; }
        [DisplayName("Apzeldinimas")]
        public bool apzeldinimas { get; set; }
        [DisplayName("Kiemas")]
        public bool kiemas { get; set; }
        [DisplayName("Namo tipas")]
        public string namoTipas { get; set; }
        [DisplayName("ID")]
        public int id { get; set; }
    }
}
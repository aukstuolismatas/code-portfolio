﻿namespace Bridge.Data.Entities
{
    public class TablePair
    {
        public int Id { get; set; }
        public double FinalResult { get; set; }
        public int RoundNr { get; set; }
        public PairPosition PairPosition { get; set; }
        public int TableId { get; set; }
        public Table Table { get; set; }
        public int PairId { get; set; }
        public Pair Pair { get; set; }
    }
}
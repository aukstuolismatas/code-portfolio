﻿using CarServicePlatform.Entities.Chat;

namespace CarServicePlatform.Repository;

public interface IConversationRepository
{
    // Task<List<Conversation>> GetAllConversationsOfCurrentUserAsync(string currentUserId);
    // Task<List<Conversation>> GetAllConversationsWithSpecificUserAsync(string currentUserId, string contactUserId);
    Task<List<ConversationGroup>> GetAllConversationGroupsWhichUserAssignedTo(string userId);
    Task<List<ConversationMessage>> GetAllConversationMessagesWhichAssignedToGroup(int groupId);
    Task<ConversationGroup?> GetConversationGroupByClientRequest(int clientRequestId);
    Task<ConversationGroup?> GetConversationGroupByGroupId(int groupId);
    Task<ConversationUser?> GetConversationUserWhichAssignedToGroup(int groupId, string userId);
    Task InsertConversationGroupAsync(ConversationGroup conversationGroup);
    Task InsertConversationUsersAsync(List<ConversationUser> conversationUsers);

    Task InsertConversationMessageAsync(ConversationMessage conversationMessage);
    // Task DeleteConversationAsync(Conversation conversation);
}
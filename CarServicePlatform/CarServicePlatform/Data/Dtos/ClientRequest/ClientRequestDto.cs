﻿using CarServicePlatform.Data.Dtos.RequestedRepairService;

namespace CarServicePlatform.Data.Dtos.ClientRequest;

public record ClientRequestDto(int ClientRequestId, string Title, string Description, string Address, string City, List<RequestedRepairServiceDto> RequestedServices, string Created, string Status);
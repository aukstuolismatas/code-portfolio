﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.RepairServices;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class RequestedRepairServiceRepository : IRequestedRepairServiceRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public RequestedRepairServiceRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<List<RequestedRepairService>> GetAllRequestedRepairServiceToClientRequestAsync(int clientRequestId)
    {
        return await _carPlatformDbContext.RequestedRepairServices.Where(x => clientRequestId == x.ClientRequestId).ToListAsync().ConfigureAwait(false);
    }

    public async Task<List<RequestedRepairService>> GetAllRequestedRepairServiceAsync()
    {
        return await _carPlatformDbContext.RequestedRepairServices.ToListAsync().ConfigureAwait(false);
    }

    public async Task InsertRequestedRepairServiceAsync(List<RequestedRepairService> requestedRepairServices)
    {
        _carPlatformDbContext.RequestedRepairServices.AddRange(requestedRepairServices);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteRequestedRepairServiceAsync(List<RequestedRepairService> requestedRepairServices)
    {
        _carPlatformDbContext.RequestedRepairServices.RemoveRange(requestedRepairServices);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiConstants } from '../api-constants';
import { ApprovedClientRequestTrawl, PlaceTransferOffer, RegisteredClientRequest, RegisteredClientRequestTrawl, RejectedClientRequestTrawl, TransferOffer } from '../shared/client-request';
import { AuthService } from './auth/auth.service';
import { ErrorHandleService } from './error-handle.service';

@Injectable({
  providedIn: 'root'
})
export class TrawlTransferRequestService {
  availableRequestForTrawlUrl=environment.apiUrl+ApiConstants.availableClientRequestsForTrawl;
  requestsWaitingApprovalForTrawlUrl=environment.apiUrl+ApiConstants.clientRequestsWaitingApprovalForTrawl;
  assignedRequestForTrawlUrl=environment.apiUrl+ApiConstants.assignedClientRequestsForTrawl;
  rejectedRequestForTrawlUrl=environment.apiUrl+ApiConstants.rejectedClientRequestsForTrawl;
  requests=environment.apiUrl+ApiConstants.clientRepairRequest;
  transferOfferUrl = environment.apiUrl+ApiConstants.transferOffer;

  constructor(private authService: AuthService, private http: HttpClient, private errorHandle: ErrorHandleService) { }

  retrieveClientRequestForTrawlById(requestId: number): Observable<RegisteredClientRequestTrawl>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RegisteredClientRequestTrawl>(`${this.requests}${requestId}/trawl/${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:RegisteredClientRequestTrawl) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAssignedClientRequestForTrawlById(requestId: number): Observable<ApprovedClientRequestTrawl>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ApprovedClientRequestTrawl>(`${this.requests}${requestId}/trawl/assigned/${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequestTrawl) => data), catchError(this.errorHandle.errorHandle));
  }
  
  retrieveClientRequestAllTransferOffers(requestId: number): Observable<TransferOffer[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<TransferOffer[]>(`${this.transferOfferUrl}${requestId}`, {headers: headers})
    .pipe(map((data:TransferOffer[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllAvailableClientRequestForTrawl(): Observable<RegisteredClientRequestTrawl[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RegisteredClientRequestTrawl[]>(`${this.availableRequestForTrawlUrl}${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:RegisteredClientRequestTrawl[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllWaitingApprovalClientRequestForTrawl(): Observable<RegisteredClientRequestTrawl[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RegisteredClientRequestTrawl[]>(`${this.requestsWaitingApprovalForTrawlUrl}${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:RegisteredClientRequestTrawl[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllAssignedClientRequestForTrawl(): Observable<ApprovedClientRequestTrawl[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ApprovedClientRequestTrawl[]>(`${this.assignedRequestForTrawlUrl}${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequestTrawl[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllRejectedClientRequestForTrawl(): Observable<RejectedClientRequestTrawl[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RejectedClientRequestTrawl[]>(`${this.rejectedRequestForTrawlUrl}${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:RejectedClientRequestTrawl[]) => data), catchError(this.errorHandle.errorHandle));
  }
  
  placeTransferOffer(transferOffer: PlaceTransferOffer){
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .post<TransferOffer>(this.transferOfferUrl, transferOffer, {headers: headers})
    .pipe(map((data:TransferOffer)=>data), catchError(this.errorHandle.errorHandle));
  }

  retrieveTrawlTransferOffer(requestId: number): Observable<TransferOffer>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<TransferOffer>(`${this.transferOfferUrl}${requestId}/trawl/${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:TransferOffer) => data), catchError(this.errorHandle.errorHandle));
  }

}

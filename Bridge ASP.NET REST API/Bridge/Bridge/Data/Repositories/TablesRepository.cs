﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bridge.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bridge.Data.Repositories
{
    public class TablesRepository : ITablesRepository
    {
        private readonly BridgeRestContext _bridgeRestContext;

        public TablesRepository(BridgeRestContext bridgeRestContext)
        {
            _bridgeRestContext = bridgeRestContext;
        }

        public async Task<Table> GetAsync(int tournamentId, int tableId)
        {
            return await _bridgeRestContext.Tables.FirstOrDefaultAsync(x => x.TournamentId == tournamentId && x.Id == tableId).ConfigureAwait(false);
        }

        public async Task<List<Table>> GetAllAsync(int tournamentId)
        {
            return await _bridgeRestContext.Tables.Where(x => x.TournamentId == tournamentId).ToListAsync().ConfigureAwait(false);
        }

        public async Task InsertAsync(Table table)
        {
            _bridgeRestContext.Tables.Add(table);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task UpdateAsync(Table table)
        {
            _bridgeRestContext.Tables.Update(table);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAsync(Table table)
        {
            _bridgeRestContext.Tables.Remove(table);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
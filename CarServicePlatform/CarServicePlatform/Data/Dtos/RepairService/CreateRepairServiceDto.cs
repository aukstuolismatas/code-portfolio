﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.RepairService;

public record CreateRepairServiceDto([Required] string ServiceName);

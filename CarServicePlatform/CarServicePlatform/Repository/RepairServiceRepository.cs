﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.RepairServices;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class RepairServiceRepository : IRepairServiceRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public RepairServiceRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<RepairService?> GetServiceAsync(int categoryId, int serviceId)
    {
        return await _carPlatformDbContext.RepairServices
                                          .FirstOrDefaultAsync(x => x.RepairServiceCategoryId == categoryId && x.Id == serviceId)
                                          .ConfigureAwait(false);
    }

    public async Task<List<RepairService>> GetAllServicesAsync(int categoryId)
    {
        return await _carPlatformDbContext.RepairServices.Where(x => x.RepairServiceCategoryId == categoryId).ToListAsync().ConfigureAwait(false);
    }

    public async Task<List<RepairService>> GetAllServicesForClientRequestAsync(int clientRequestId)
    {
        return await _carPlatformDbContext.RequestedRepairServices.Where(x => x.ClientRequestId == clientRequestId)
                                          .Join(_carPlatformDbContext.RepairServices, requestedService => requestedService.RepairServiceId, repairService => repairService.Id,
                                              (requestedService, repairService) => repairService)
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<RepairService>> GetAllRepairServicesAvailableForServiceAsync(string serviceUserId)
    {
        return await _carPlatformDbContext.AvailableRepairServices.Where(x => x.DomainUserId == serviceUserId)
                                          .Join(_carPlatformDbContext.RepairServices, availableRepairService => availableRepairService.RepairServiceId, repairService => repairService.Id,
                                              (availableRepairService, repairService) => new RepairService
                                              {
                                                  Id = repairService.Id,
                                                  RepairServiceCategoryId = repairService.RepairServiceCategoryId,
                                                  ServiceName = repairService.ServiceName,
                                                  RepairServiceCategory = repairService.RepairServiceCategory,
                                                  City = availableRepairService.City
                                              })
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<RepairService>> GetAllServicesGroupByCategoriesAsync()
    {
        return await _carPlatformDbContext.RepairServices.Join(_carPlatformDbContext.RepairServiceCategories, service => service.RepairServiceCategoryId,
                                              category => category.Id, (service, category) => new RepairService
                                              {
                                                  Id = service.Id,
                                                  RepairServiceCategoryId = category.Id,
                                                  RepairServiceCategory = category,
                                                  ServiceName = service.ServiceName
                                              })
                                          .OrderBy(service => service.RepairServiceCategoryId)
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task InsertServiceAsync(RepairService repairService)
    {
        _carPlatformDbContext.RepairServices.Add(repairService);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task UpdateServiceAsync(RepairService repairService)
    {
        _carPlatformDbContext.RepairServices.Update(repairService);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteServiceAsync(RepairService repairService)
    {
        _carPlatformDbContext.RepairServices.Remove(repairService);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
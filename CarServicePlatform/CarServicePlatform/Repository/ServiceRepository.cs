﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.Users;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class ServiceRepository : IServiceRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public ServiceRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<Dictionary<string, ServiceAccount>> GetAllServiceAccountsAsync()
    {
        return await _carPlatformDbContext.ServiceAccounts.ToDictionaryAsync(key => key.Id, x => x).ConfigureAwait(false);
    }

    public async Task<ServiceAccount?> GetServiceAccountAsync(string userId)
    {
        return await _carPlatformDbContext.ServiceAccounts.FirstOrDefaultAsync(x => x.Id == userId).ConfigureAwait(false);
    }

    public async Task UpdateServiceAccountAsync(ServiceAccount serviceAccount)
    {
        _carPlatformDbContext.ServiceAccounts.Update(serviceAccount);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteServiceAccountAsync(ServiceAccount serviceAccount)
    {
        _carPlatformDbContext.ServiceAccounts.Remove(serviceAccount);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
﻿using Bridge.Data.Entities;

namespace Bridge.Data.Dtos.TablePair
{
    public record TablePairDto(int Id, int TableId, int PairId, double FinalResult, int RoundNr, PairPosition PairPosition);

}
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, shareReplay, tap } from 'rxjs';
import { ApiConstants } from 'src/app/api-constants';
import { LoginData, LoginRequest, RegisterClient, RegisterService, RegisterTrawl } from 'src/app/shared/login-data';
import { environment } from 'src/environments/environment';
import { DataSharingService } from '../data-sharing.service';
import { RolesType } from '../enum.service';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  decodedToken!: { [key: string]: string };
  constructor(private http: HttpClient, private jwtService: JwtService, private dataSharingService: DataSharingService) { 
    dataSharingService.userName.next(localStorage['name']);
   }
  login(loginRequest: LoginRequest): Observable<LoginData> {
    const headers = { 'content-type': 'application/json' };
    const body = JSON.stringify(loginRequest);
    return this.http.post<LoginData>(environment.apiUrl + ApiConstants.login, body, { headers: headers })
    .pipe(tap((res) => { this.setSession(res) }), shareReplay());
  }
  register(clientRequest: RegisterClient) {
    return this.http.post<RegisterClient>(environment.apiUrl + ApiConstants.registerClient, clientRequest);
  }

  registerService(serviceRequest: RegisterService) {
    const headers = { 'content-type': 'application/json' };
    const body = JSON.stringify(serviceRequest);
    return this.http.post<RegisterService>(environment.apiUrl + ApiConstants.registerService, body, {headers: headers});
  }

  registerTrawl(trawlRequest: RegisterTrawl) {
    const headers = { 'content-type': 'application/json' };
    const body = JSON.stringify(trawlRequest);
    return this.http.post<RegisterTrawl>(environment.apiUrl + ApiConstants.registerTrawl, body, {headers: headers});
  }

  private setSession(authResult: LoginData) {
    this.decodedToken = this.jwtService.DecodeToken(authResult.accessToken);
    this.dataSharingService.userName.next(this.decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name']);
    localStorage.setItem('id_token', authResult.accessToken);
    localStorage.setItem('role', this.decodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role']);
    localStorage.setItem('user_id', this.decodedToken['userId']);
    localStorage.setItem('exp', this.decodedToken['exp']);
    localStorage.setItem('email', this.decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress']);
    localStorage.setItem('name', this.dataSharingService.userName.value);
    localStorage.setItem('surname', this.decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname']);
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('role');
    localStorage.removeItem('user_id');
    localStorage.removeItem('exp');
    localStorage.removeItem('email');
    localStorage.removeItem('name');
    localStorage.removeItem('surname');
    this.dataSharingService.cleanUserVariables();
  }

public isLoggedIn(): boolean {
    return localStorage.getItem("id_token") != null && this.getExpiration() > (Date.now() / 1000);
}

public isLoggedOut(): boolean {
    return !this.isLoggedIn();
}

public getAccessToken(): string | null {
  return localStorage.getItem("id_token");
}

public getUserId(): string | null {
    return localStorage.getItem("user_id");
}

public getRole(): string | null {
    return localStorage.getItem("role");
}

public getEmail(): string | null {
  return localStorage.getItem("email");
}

public getName(): string | null {
  return localStorage.getItem("name");;
}
public getSurname(): string | null {
  return localStorage.getItem("surname");
}

public isAdmin(){
  return this.getRole()?.match(RolesType.Admin);
}

public isClient(){
  return !this.getRole()?.match(RolesType.Admin) && this.getRole()?.match(RolesType.Client);
}

public isService(){
  return !this.getRole()?.match(RolesType.Admin) && this.getRole()?.match(RolesType.Service);
}

public isTrawl(){
  return !this.getRole()?.match(RolesType.Admin) && this.getRole()?.match(RolesType.Trawl);
}

public updateSession(email: string, name: string, surname: string){
  this.dataSharingService.userName.next(name);
  localStorage.setItem('email', email);
  localStorage.setItem('name', name);
  localStorage.setItem('surname', surname);
}

getExpiration(): number {
    return parseInt(localStorage.getItem('exp')!);
}
}
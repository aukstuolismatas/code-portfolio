﻿using System.ComponentModel.DataAnnotations;
using Bridge.Data.Entities;

namespace Bridge.Data.Dtos.Users
{
    public record CreateUserDto([Required] string Username, [Required] string Password, [Required] UserRole Role);
}
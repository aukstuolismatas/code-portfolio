﻿namespace CarServicePlatform.Data.Dtos.RequestedRepairService;

public record RequestedRepairServiceDto(int Id, int RepairServiceId, int ClientRequestId);
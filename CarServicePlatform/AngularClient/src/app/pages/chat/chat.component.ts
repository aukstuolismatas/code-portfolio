import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { ChatService } from 'src/app/services/chat.service';
import { PusherService } from 'src/app/services/pusher.service';
import { ConversationGroup, ConversationMessage } from 'src/app/shared/conversation';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  // userConversationGroups : ConversationGroup = [];
  conversationsGroups: ConversationGroup[] =[];
  messages: Array<ConversationMessage> =[];
  currentUserId: string |null='';
  currentUserName: string|null ='';
  currentConversationId =0;
  messageText = '';
  constructor(private pusherService: PusherService, private authService: AuthService, private chatService : ChatService) { 
    

    
  }
  ngOnInit(): void {
    this.currentUserId = this.authService.getUserId();
    this.currentUserName = this.authService.getName();
    this.chatService.loadUserConversationGroups().subscribe((resultConversationGroups: ConversationGroup[]) => {
      this.conversationsGroups = resultConversationGroups;
      this.currentConversationId = resultConversationGroups[0].id
      this.pusherService.changeChannel(resultConversationGroups[0].id);
      this.pusherService.messagesChannel.bind('message', (message: ConversationMessage) =>{
        this.messages.push(message);
      });
      this.chatService.loadConversationMessages(resultConversationGroups[0].id).subscribe((data: Array<ConversationMessage>) =>{
        this.messages = data;
      });
    });
    
  }
  
  sendMessage(messageText: string) {
    if(this.currentUserId!=null){
      const message: ConversationMessage = {
        sendBy: this.currentUserId,
        message: messageText,
        conversationGroupId: this.currentConversationId
     } //find a way to pass conversationGroupID
      // this.pusherService.messagesChannel.trigger('new_message', message);
      // this.messages.push(message);
      this.messageText=''
      this.messages.push(message);
      this.pusherService.postMessage(message).subscribe(()=> this.pusherService.messagesChannel.bind('message', (message: ConversationMessage) =>{
        this.messages.push(message);
    }));
  }
}

  isCurrentUser(userId: string) : boolean{
    return userId==this.currentUserId;
  }

  loadConversation(index: number){
    this.currentConversationId = this.conversationsGroups[index].id;
    this.chatService.loadConversationMessages(this.conversationsGroups[index].id).subscribe((data: Array<ConversationMessage>) =>{
      this.messages = data;
    });
    this.pusherService.changeChannel(this.conversationsGroups[index].id);
  }

}

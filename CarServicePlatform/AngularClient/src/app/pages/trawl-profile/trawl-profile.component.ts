import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { saveAs } from 'file-saver';
import { RegexPatterns } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FileService } from 'src/app/services/file.service';
import { ProfileService } from 'src/app/services/profile.service';
import { TrawlAccount, UpdateTrawlAccount } from 'src/app/shared/account';
import { FileInformation } from 'src/app/shared/file-information';

@Component({
  selector: 'app-trawl-profile',
  templateUrl: './trawl-profile.component.html',
  styleUrls: ['./trawl-profile.component.css']
})
export class TrawlProfileComponent implements OnInit {

  public errorMessage ='';
  public successMessage ='';
  showValidation:boolean = false;
  showImageValidation:boolean = false;
  trawlProfileForm : FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    surname: new FormControl('', [Validators.required]),
    city: new FormControl('', [Validators.required]),
    minimalPrice: new FormControl('', [Validators.required]),
    email: new FormControl('',[Validators.required ,Validators.email, Validators.pattern(RegexPatterns.Email)]),
    phone: new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.Phone)]),
    currentPassword: new FormControl('', [Validators.minLength(6),Validators.maxLength(40)]),
    newPassword: new FormControl('', [Validators.minLength(6),Validators.maxLength(40)]),
  });

  uploadedImageInformation!: FileInformation;
  public trawlDetails!: TrawlAccount;
  public documentsInformation: FileInformation[]=[];
  constructor(private profileService: ProfileService, private fileService: FileService, private dataSharingService: DataSharingService,
    private authService: AuthService) { 
      this.fileService.retrieveAllDocumentsForUser().subscribe((data: FileInformation[])=>this.documentsInformation=data);
      this.profileService.loadTrawlAccountByEmail(authService.getEmail()).subscribe((data:TrawlAccount)=> {
        this.trawlDetails = data;
        this.trawlProfileForm.patchValue({
          name: data.domainUser.name,
          surname: data.domainUser.surname,
          email: data.email,
          phone: data.phoneNumber,
          city: data.domainUser.city,
          minimalPrice: data.domainUser.minimalPrice,
        })
      });  
  }

  ngOnInit(): void {
  }

  get f(): {[key:string]:AbstractControl} { return this.trawlProfileForm.controls; }

  changeCity(event:number){
    this.f['city'].setValue(event);
    console.log(this.f['city'].value);
  }

  updateProfile(){
    const updateRequest: UpdateTrawlAccount = {
      name: this.f['name'].value,
      surname: this.f['surname'].value,
      email: this.f['email'].value,
      phoneNumber: this.f['phone'].value,
      minimalPrice: this.f['minimalPrice'].value,
      city: this.f['city'].value.toString(),
      currentPassword: this.f['currentPassword'].value,
      newPassword: this.f['newPassword'].value,
    };

     if (this.trawlProfileForm.invalid) {
        return;
     }
     this.profileService.updateTrawlAccount(updateRequest).subscribe({
      next: (user) => this.successMessage = MessageConstants.ProfileUpdateSuccess,
      error: (err) => this.errorMessage = MessageConstants.ProfileUpdateError
    })
    
    this.successMessage ='';
    this.errorMessage='';
  }


  imageUploadForm = new FormGroup({
    image: new FormControl('', [Validators.required]),
    imageSource: new FormControl('', [Validators.required])
  });

  documentUploadForm = new FormGroup({
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required])
  });

  get imageFormControl() { return this.imageUploadForm.controls; }
  get documentFormControl() { return this.documentUploadForm.controls; }

  onImageChange(event: any) {
    if (event.target.files.length > 0) {
      this.showImageValidation = false;
      const file = event.target.files[0];
      this.imageUploadForm.patchValue({
        imageSource: file
      });
    }
  }

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      this.showValidation = false;
      const file = event.target.files[0];
      this.documentUploadForm.patchValue({
        fileSource: file
      });
    }
  }

  onProfileImageUpload() {
    if( this.imageUploadForm.invalid){
      this.showImageValidation = true;
      return false
    }
    const formData = new FormData();
    formData.append('file', this.imageFormControl['imageSource'].value);
    this.fileService.readFile(this.imageFormControl['imageSource'].value).subscribe(value => this.dataSharingService.avatarImage.next(value));
    this.fileService.uploadFile(formData).subscribe({
      next: (data:FileInformation) => {
        this.uploadedImageInformation = data
        this.successMessage = MessageConstants.ProfilePictureSuccess
      },
      error: () => this.errorMessage = MessageConstants.ProfilePictureFailed
      
    });
    this.imageUploadForm.reset;
    this.imageFormControl['image'].setValue(''); 
    this.imageFormControl['imageSource'].setValue(''); 
    return true;
  }

  onDocsUpload(){
    if( this.documentUploadForm.invalid){
      this.showValidation = true;
      return false
    }
    const formData = new FormData();
    formData.append('file', this.documentFormControl['fileSource'].value);
    // this.fileService.readFile(this.documentFormControl['fileSource'].value).subscribe(value => this.dataSharingService.avatarImage.next(value));
    this.fileService.uploadDocument(formData).subscribe({
      next: (data:FileInformation) => {
        this.documentsInformation.push(data);
        this.successMessage = MessageConstants.CompanyDocumentsSuccess
      },
      error: () => this.errorMessage = MessageConstants.CompanyDocumentsFailed
      
    });
    this.documentUploadForm.reset;
    this.documentFormControl['file'].setValue(''); 
    this.documentFormControl['fileSource'].setValue(''); 
    return true;
  }

  downloadDocument(index: number){
    this.fileService.downloadDocument(this.documentsInformation[index].id).subscribe((data:any) =>{
      let blob:any = new Blob([data], { type: `${data.type}` });
      const url = window.URL.createObjectURL(blob);
      saveAs(blob, this.documentsInformation[index].name);
    }), (error: any) => this.errorMessage = "Parsiūsti nepavyko",
    () => console.info('Successfully downloaded');
  }

  isEmpty(){
    return this.documentsInformation.length<=0;
  }

}

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { data } from 'autoprefixer';
import { AppConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { CategoriesService } from 'src/app/services/categories.service';
import { ChatService } from 'src/app/services/chat.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FileService } from 'src/app/services/file.service';
import { ProfileService } from 'src/app/services/profile.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { TrawlTransferRequestService } from 'src/app/services/trawl-transfer-request.service';
import { ServiceAccount } from 'src/app/shared/account';
import { ClientRequest, PlacePriceOffer, PriceOffer, RegisteredClientRequest, TransferOffer } from 'src/app/shared/client-request';
import { ConversationGroup, CreateConversationGroup } from 'src/app/shared/conversation';
import { RepairService } from 'src/app/shared/repair-services';

@Component({
  selector: 'app-request-details',
  templateUrl: './request-details.component.html',
  styleUrls: ['./request-details.component.css']
})
export class RequestDetailsComponent implements OnInit {
  @ViewChild('requestImage')
  private imageRef!: ElementRef;

  index:number=0;
  requestId:number=0;
  clientRequestDetail!: RegisteredClientRequest;
  repairServices: RepairService[]=[]
  priceOffers:PriceOffer[]=[]
  transferOffers:TransferOffer[]=[]
  imagesArray: SafeUrl[]=[]
  imageIndex=0
  viewerOpen:boolean=false

  offerForm : FormGroup = new FormGroup({
    price: new FormControl('', [Validators.required]),
    time: new FormControl('', [Validators.required])
  });
  submitted=false
  successMessage=''
  errorMessage=''
  priceTitles=['Preliminari kaina', 'Preliminari darbų trukmė', '']
  transferTitles = ['Kaina', 'Paėmimo data ir laikas', '']

  descriptionTab=true;
  priceOffersTab=false;
  transferOffersTab=false;
  serviceDetails!: ServiceAccount
  constructor(private activatedRoute: ActivatedRoute, private dataSharingService: DataSharingService, private repairRequestService: RepairRequestService, 
    private trawlTransferService: TrawlTransferRequestService, private chatService: ChatService, private router: Router, private profileService: ProfileService,
    public fileService: FileService, private sanitizer: DomSanitizer, private categoryService: CategoriesService, private authService: AuthService) {
    }
  ngOnInit(): void {
    this.index = this.activatedRoute.snapshot.params['index'];
    this.requestId = this.activatedRoute.snapshot.params['id'];
    this.clientRequestDetail = this.dataSharingService.requestList.value[this.index];
    if(this.clientRequestDetail===undefined){
      this.repairRequestService.retrieveClientRequestById(this.requestId).subscribe((data:RegisteredClientRequest)=>this.clientRequestDetail=data);
    }
    this.fileService.downloadRequestImages(this.requestId).subscribe(data=> {
      this.imagesArray=data      
    })
    this.categoryService.loadRepairServicesForClientRequest(this.requestId).subscribe((data:RepairService[]) => this.repairServices = data)

    if(this.authService.isClient()){
      this.repairRequestService.retrieveClientPriceOffers(this.requestId).subscribe((data:PriceOffer[]) => this.priceOffers = data)
      this.trawlTransferService.retrieveClientRequestAllTransferOffers(this.requestId).subscribe((data:TransferOffer[])=> this.transferOffers = data)
    } else if(this.authService.isService()){
      this.profileService.loadServiceAccount(this.authService.getUserId()).subscribe((data:ServiceAccount)=> {
        this.serviceDetails = data;
      });
      this.repairRequestService.retrieveServicePriceOffer(this.requestId).subscribe((data:PriceOffer) => this.priceOffers[0] = data)
    }
  }

  get f(): {[key:string]:AbstractControl} { return this.offerForm.controls; }

  startChat(){
    const chatData: CreateConversationGroup = {
      clientRequestId: this.requestId,
      userId: this.authService.getUserId()
    }
    this.chatService.createChat(chatData).subscribe({
      next: (data:ConversationGroup) => {
        this.router.navigate([AppConstants.Chat]);
        // this.successMessage = MessageConstants.PlacePriceOfferSuccess
      },
      error: (err) => this.errorMessage = MessageConstants.ChatFailed
    })
  }

  sendOffer(){
    const priceOffer: PlacePriceOffer = {
      clientRequestId: this.requestId,
      price: this.f['price'].value,
      duration: this.f['time'].value
    }
    this.submitted=false;
    if (this.offerForm.invalid) {
      this.submitted = true;
       return;
    }
    this.repairRequestService.placePriceOffer(priceOffer).subscribe({
      next: (data:PriceOffer) => {
        this.priceOffers[0]=data;
        this.toPriceOffers();
        this.successMessage = MessageConstants.PlacePriceOfferSuccess
      },
      error: (err) => this.errorMessage = MessageConstants.PlacePriceOfferFailed
    })
    this.errorMessage=''
    this.successMessage=''
    this.f['price'].setValue(''); 
    this.f['time'].setValue(''); 
  }

  approveOffer(offerId: number){
    this.repairRequestService.approvePriceOffer(offerId, this.requestId).subscribe((data:PriceOffer[])=> this.priceOffers = data);
  }

  approveTransferOffer(offerId: number){
    this.repairRequestService.approveTransferOffer(offerId, this.requestId).subscribe((data:TransferOffer[])=> this.transferOffers = data);
  }
  
  isService(){
    return this.authService.isService();
  }

  isClient(){
    return this.authService.isClient()
  }

  toDescription(){
    this.descriptionTab=true;
    this.priceOffersTab =false
    this.transferOffersTab =false
  }

  toPriceOffers(){
    this.descriptionTab=false;
    this.priceOffersTab =true
    this.transferOffersTab =false
  }

  toTransferOffers(){
    this.descriptionTab=false;
    this.priceOffersTab =false;
    this.transferOffersTab =true;
  }
}

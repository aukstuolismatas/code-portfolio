import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationCategoryComponent } from './registration-category.component';

describe('RegistrationCategoryComponent', () => {
  let component: RegistrationCategoryComponent;
  let fixture: ComponentFixture<RegistrationCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

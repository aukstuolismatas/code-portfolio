#include <string>
#include <iostream>
#include <omp.h>
#include <vector>
#include <nlohmann/json.hpp>
#include <fstream>
#include "HDD1.h"
#include "DataMonitor.h"
#include "SortedResultMonitor.h"
#include <exception>
#include <iomanip>

using json = nlohmann::json;
using namespace std;

void WriteToFile(vector<HDDWithComputedValue*> result);

int main() {
    vector<HDD1*> hddList;
    std::ifstream i("IFF-8_2_AukstuolisM_L1_dat_1.json");
    json js;
    i >> js;
    for (auto j : js)
    {
        hddList.push_back(new HDD1(j["brand"], j["capacity"], j["price"]));
    }
    omp_set_num_threads(7);
    int threadNumber = 0;
    DataMonitor dataMonitor = DataMonitor(hddList.size());
    SortedResultMonitor result = SortedResultMonitor(hddList.size());
    
    #pragma omp parallel private(threadNumber)
    {
        threadNumber = omp_get_thread_num();
        if (threadNumber == 0)
        {
            for (int i=0; i<hddList.size(); i++)
            {
                bool isAdded = false;
                while (!isAdded)
                {
                    if (i == hddList.size()-1)
                    {
                        isAdded=dataMonitor.addHDD(hddList[i], true);
                    }
                    else
                    {
                        isAdded=dataMonitor.addHDD(hddList[i], false);
                    }
                }
            }
        }
        else
        {
            while (true)
            {
                //HDD1* removedItem;
                HDD1* removedItem = dataMonitor.removeFirst();
                //try
                //{
                //     removedItem=dataMonitor.removeFirst();
                //}
                //catch (exception)
                //{
                //    //std::cout << exception << endl;
                //    break;
                //}
                if (removedItem == NULL)
                {
                    break;
                }
                if(removedItem!=nullptr)
                {
                    HDDWithComputedValue* item = new HDDWithComputedValue(removedItem);
                    if (item->computedData > 10870)
                    {
                        result.addHDDSorted(item);
                    }
                }
            }
        }
    }

    for (int i = 0; i < result.counter; i++) {
        std::cout << result.resultArray[i]->computedData << "\n";
    }
    WriteToFile(result.getItems());
    cout << "Programa baig� darb�" << endl;
}
void WriteToFile(vector<HDDWithComputedValue*> result)
{
    ofstream outfile;
    outfile.open("IFF - 8_2_AukstuolisM_L1_rez.txt");
    outfile << setw(31) << "Brand |" << setw(16) << "Capacity |" << setw(16) << "Price |" << setw(21) << "Value |"<< endl;
    outfile << std::string( 85, '-' ) << endl;
    for (auto item : result)
    {
        outfile << fixed;
        outfile << setw(31) << item->item->brand + " |" << setw(14) << item->item->capacity << " |" << setprecision(2) << setw(14) << item->item->price << " |" << setprecision(1) << setw(19) << item->computedData << " |" << endl;
    }
    
    outfile.close();
}

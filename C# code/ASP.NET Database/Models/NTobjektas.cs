using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NT_Csharp.Models
{
    public class NTobjektas
    {
        [DisplayName("ID")]
        [Required]
        public int id { get; set; }
        [DisplayName("Pastatymo metai")]
        [Required]
        public int pastatymoMetai { get; set; }
        [DisplayName("Kambariu skaicius")]
        [Required]
        public int kambariuSkaicius { get; set; }
        [DisplayName("Bendras plotas")]
        [Required]
        public double bendrasPlotas { get; set; }
        //public int aukstuSkaicius { get; set; }
        //public Boolean baldai { get; set; }
        //public Boolean signalizacija { get; set; }
        //public Boolean garazas { get; set; }
        [DisplayName("Irengimo būsena")]
        [Required]
        public string irengimoBusena { get; set; }
        [DisplayName("Tipas")]
        [Required]
        public string tipas { get; set; }
        //public int fk_butas { get; set; }
        //public int fk_vieta { get; set; }
        //public int fk_namas { get; set; }
    }
}
﻿using System.Threading.Tasks;
using AutoMapper;
using Bridge.Auth;
using Bridge.Data.Dtos.Auth;
using Bridge.Data.Dtos.Auth.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.Controllers
{
    [ApiController]
    [AllowAnonymous]
    [Route("api")]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<BridgeRestUser> _userManager;
        private readonly IMapper _mapper;
        private readonly ITokenManager _tokenManager;

        public AuthController(UserManager<BridgeRestUser> userManage, IMapper mapper, ITokenManager tokenManager)
        {
            _userManager = userManage;
            _mapper = mapper;
            _tokenManager = tokenManager;
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register(RegisterUserDto registerUserDto)
        {
            BridgeRestUser user = await _userManager.FindByNameAsync(registerUserDto.UserName).ConfigureAwait(false);
            if (user != null)
            {
                return BadRequest("Request invalid.");
            }

            BridgeRestUser newUser = new BridgeRestUser
            {
                UserName = registerUserDto.UserName
            };

            IdentityResult createUserResult = await _userManager.CreateAsync(newUser, registerUserDto.Password).ConfigureAwait(false);
            if (!createUserResult.Succeeded)
            {
                return BadRequest(createUserResult.Errors);
            }

            await _userManager.AddToRoleAsync(newUser, BridgeRestUserRoles.Table).ConfigureAwait(false);
            return CreatedAtAction(nameof(Register), _mapper.Map<UserDto>(newUser));
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login(LoginDto loginDto)
        {
            BridgeRestUser user = await _userManager.FindByNameAsync(loginDto.UserName).ConfigureAwait(false);
            if (user == null)
            {
                return BadRequest("Credentials are incorrect");
            }

            bool isPasswordValid = await _userManager.CheckPasswordAsync(user, loginDto.Password).ConfigureAwait(false);
            if (!isPasswordValid)
            {
                return BadRequest("Credentials are incorrect");
            }

            return Ok(await _tokenManager.CreateAccessTokenAsync(user).ConfigureAwait(false));
        }
    }
}
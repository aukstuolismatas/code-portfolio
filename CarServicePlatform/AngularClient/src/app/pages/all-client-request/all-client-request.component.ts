import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants } from 'src/app/app-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { RolesType } from 'src/app/services/enum.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { RegisteredClientRequest } from 'src/app/shared/client-request';
import { RequestStatus } from 'src/app/shared/request-status';

@Component({
  selector: 'app-all-client-request',
  templateUrl: './all-client-request.component.html',
  styleUrls: ['./all-client-request.component.css']
})
export class AllClientRequestComponent implements OnInit {
  frontTitle:string='Pateiktos užklausos'
  titles:string[]=['Užsakymo nr.', 'Miestas', 'Data', 'Statusas']
  repairRequestList:RegisteredClientRequest[]=[];
  constructor(private repairRequestService: RepairRequestService, private authService: AuthService, private router: Router, private dataSharingService: DataSharingService) {
    dataSharingService.requestList.subscribe(value => this.repairRequestList = value);
    if(this.authService.isClient()){
      this.frontTitle='Pateiktos užklausos'
      this.repairRequestService.retrieveAllClientRequest().subscribe((data:RegisteredClientRequest[])=>{
        this.dataSharingService.requestList.next(data)
      })
    } 
    
  }

  ngOnInit(): void {
  }

  openItem(index:number){
    this.router.navigate([`${AppConstants.AllClientRequest}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  }

  isService(){
    return this.authService.isService();
  }

  getStatusDescription(enumString:string){
    return (<any>RequestStatus)[enumString];
  }
}

import 'dart:async';
import 'dart:html';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  _MyAppState state = _MyAppState();
  runApp(MyApp(state: state));
}

const List<String> urls = [
  'https://images.unsplash.com/photo-1542553458-79a13aebfda6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=723&q=80',
  'https://images.unsplash.com/photo-1542438408-abb260104ef3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80',
  'https://images.unsplash.com/photo-1542466500-dccb2789cbbb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=686&q=80',
  'https://images.unsplash.com/photo-1542370285-b8eb8317691c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1126&q=80'
];

class MyApp extends StatelessWidget {
  final _MyAppState state;
  MyApp({required this.state});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Photo Viewer',
        home: GalleryPage(
          title: 'Image Gallery',
          state: state,
        ));
  }
}

class _MyAppState {
  Stream<bool> get isTagging => _taggingController.stream;
  Stream<List<PhotoState>> get photoStates => _photoStateController.stream;
  Sink<String> get photoLongPressSink => _photoLongPressController.sink;
  Sink<PhotoState> get photoTapSink => _photoTapController.sink;
  Sink<String> get tagTapSink => _tagTapController.sink;

  bool _isTagging = false;
  List<PhotoState> _photoStates =
      List.of(urls.map((url) => PhotoState(url: url)));
  Set<String> tags = {"all", "nature", "city"};
  StreamController<bool> _taggingController = StreamController.broadcast();
  StreamController<List<PhotoState>> _photoStateController =
      StreamController.broadcast();

  StreamController<String> _photoLongPressController = StreamController();
  StreamController<PhotoState> _photoTapController = StreamController();
  StreamController<String> _tagTapController = StreamController();

  _MyAppState() {
    _photoStateController.onListen = () {
      _photoStateController.add(_photoStates);
    };
    _taggingController.onListen = () {
      _taggingController.add(_isTagging);
    };

    _photoLongPressController.stream.listen((url) {
      _toggelTagging(url);
    });

    _photoTapController.stream.listen((ps) {
      _onPhotoSelect(ps.url, !ps.selected);
    });

    _tagTapController.stream.listen((tag) {
      _selectTag(tag);
    });
  }
  void _selectTag(String tag) {
    if (_isTagging) {
      if (tag != 'all') {
        _photoStates.forEach((element) {
          if (element.selected) {
            element.tags.add(tag);
          }
        });
      }
      _toggelTagging(null);
    } else {
      _photoStates.forEach((element) {
        element.display = tag == 'all' ? true : element.tags.contains(tag);
      });
      _photoStateController.add(_photoStates);
    }
  }

  void _toggelTagging(String? url) {
    _isTagging = !_isTagging;
    _photoStates.forEach((element) {
      if (_isTagging && element.url == url) {
        element.selected = true;
      } else {
        element.selected = false;
      }
    });
    _taggingController.add(_isTagging);
    _photoStateController.add(_photoStates);
  }

  void _onPhotoSelect(String url, bool? selected) {
    _photoStates.forEach((element) {
      if (element.url == url) {
        element.selected = selected ?? true;
      }
    });
    _photoStateController.add(_photoStates);
  }
}

class PhotoState {
  String url;
  bool selected;
  bool display;
  Set<String> tags = {};
  PhotoState(
      {required this.url, this.selected = false, this.display = true, tags});
}

class GalleryPage extends StatelessWidget {
  final String title;
  final _MyAppState state;
  GalleryPage({required this.title, required this.state});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title)),
      body: StreamBuilder<List<PhotoState>>(
        initialData: [],
        stream: state.photoStates,
        builder: ((context, snapshot) {
          return GridView.count(
              crossAxisCount: 2,
              primary: false,
              children: List.of(snapshot.data!
                  .where((ps) => ps.display ?? true)
                  .map((ps) => Photo(
                        photoState: ps,
                        state: state,
                      ))));
        }),
      ),
      drawer: Drawer(
        child: ListView(
            children: List.of((state.tags.map((e) => ListTile(
                  title: Text(e),
                  onTap: () {
                    state.tagTapSink.add(e);
                    Navigator.of(context).pop();
                  },
                ))))),
      ),
    );
  }
}

class Photo extends StatelessWidget {
  final _MyAppState state;
  final PhotoState photoState;
  const Photo({required this.photoState, required this.state});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        initialData: false,
        stream: state.isTagging,
        builder: (context, snapshot) {
          List<Widget> children = [
            GestureDetector(
              child: Image.network(
                photoState.url,
                fit: BoxFit.cover,
              ),
              onLongPress: () => state.photoLongPressSink.add(photoState.url),
            )
          ];
          if (snapshot.data!) {
            children.add(Positioned(
              child: Theme(
                data: Theme.of(context)
                    .copyWith(unselectedWidgetColor: Colors.grey[200]),
                child: Checkbox(
                  onChanged: (value) {
                    state.photoTapSink.add(photoState);
                  },
                  value: photoState.selected,
                  activeColor: Colors.white,
                  checkColor: Colors.black,
                ),
              ),
              left: 20,
              top: 0,
            ));
          }
          return Container(
              padding: EdgeInsets.all(5),
              child: Stack(
                alignment: Alignment.center,
                children: children,
              ));
        });
  }
}

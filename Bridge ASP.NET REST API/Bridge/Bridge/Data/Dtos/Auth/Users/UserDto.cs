﻿namespace Bridge.Data.Dtos.Auth.Users
{
    public record UserDto(string UserName);
}
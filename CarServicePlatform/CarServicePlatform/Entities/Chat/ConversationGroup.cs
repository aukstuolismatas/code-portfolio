﻿using System.ComponentModel.DataAnnotations.Schema;
using CarServicePlatform.Entities.Request;

namespace CarServicePlatform.Entities.Chat;

public class ConversationGroup
{
    public int Id { get; set; }
    public string ConversationTitle { get; set; }
    public int ClientRequestId { get; set; }
    public ClientRequest ClientRequest { get; set; }

    [NotMapped]
    public string? UserName { get; set; }
}
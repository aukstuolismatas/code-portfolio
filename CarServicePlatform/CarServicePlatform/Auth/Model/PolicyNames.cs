﻿namespace CarServicePlatform.Auth.Model;

public static class PolicyNames
{
    public const string SameUser = nameof(SameUser);
    public const string User = nameof(User);
    public const string Chat = nameof(Chat);
}
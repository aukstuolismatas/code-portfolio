﻿using System.Threading.Tasks;
using Bridge.Auth;
using Bridge.Data.Dtos.Auth;
using Microsoft.AspNetCore.Identity;

namespace Bridge.Data
{
    public class DatabaseSeeder
    {
        private readonly UserManager<BridgeRestUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;

        public DatabaseSeeder(UserManager<BridgeRestUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task SeedAsync()
        {
            foreach (string role in BridgeRestUserRoles.All)
            {
                bool isRoleExist = await _roleManager.RoleExistsAsync(role).ConfigureAwait(false);
                if (!isRoleExist)
                {
                    await _roleManager.CreateAsync(new IdentityRole(role)).ConfigureAwait(false);
                }
            }

            BridgeRestUser newAdmin = new()
            {
                UserName = "director"
            };

            BridgeRestUser checkAdminUser = await _userManager.FindByNameAsync(newAdmin.UserName).ConfigureAwait(false);
            if (checkAdminUser == null)
            {
                IdentityResult createAdminUserResult = await _userManager.CreateAsync(newAdmin, "Super!22").ConfigureAwait(false);
                if (createAdminUserResult.Succeeded)
                {
                    await _userManager.AddToRolesAsync(newAdmin, BridgeRestUserRoles.All).ConfigureAwait(false);
                }
            }
        }
    }
}
﻿namespace CarServicePlatform.Entities.Chat;

public class ConversationUser
{
    public int Id { get; set; }
    public string UserName { get; set; }
    public string DomainUserId { get; set; }
    public int ConversationGroupId { get; set; }
    public ConversationGroup ConversationGroup { get; set; }
}
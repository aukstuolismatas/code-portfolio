﻿using System.ComponentModel;

namespace CarServicePlatform.Entities.Request;

public enum RequestStatus
{
    [Description("Laukiama pasiūlymų")] Waiting = 0,

    [Description("Pasiūlymas priimtas")] Accepted = 1,

    [Description("Vykdomas remontas")] InProgress = 2,

    [Description("Įvykdytas")] Done = 3,

    [Description("Atšauktas")] Canceled = 4,

    // [Description("Pasiūlymas išsiųstas")] Sent = 5,

    [Description("Laukiama pervežimo pasiūlymų")]
    WaitingTransfer = 5,
    [Description("Automobilis vežamas")] Transferring = 6,
    [Description("Automobilis atvyko")] Arrived = 7,
    [Description("Automobilis grąžintas")] Delivered = 8,

}
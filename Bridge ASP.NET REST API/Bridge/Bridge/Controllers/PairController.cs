﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bridge.Auth;
using Bridge.Data.Dtos.Pair;
using Bridge.Data.Entities;
using Bridge.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.Controllers
{
    [ApiController]
    [Route("api/pairs")]
    public class PairController : ControllerBase
    {
        private readonly IPairsRepository _pairsRepository;
        private readonly IMapper _mapper;

        public PairController(IPairsRepository pairsRepository, IMapper mapper)
        {
            _pairsRepository = pairsRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<PairDto>> GetAll()
        {
            List<Pair> pairs = await _pairsRepository.GetAllAsync().ConfigureAwait(false);
            return pairs.Select(x => _mapper.Map<PairDto>(x));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PairDto>> Get(int id)
        {
            Pair pair = await _pairsRepository.GetAsync(id).ConfigureAwait(false);
            if (pair == null)
            {
                return NotFound($"Pair with id {id} not exist");
            }

            return Ok(_mapper.Map<PairDto>(pair));
        }

        [HttpPost]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<PairDto>> Post(CreatePairDto pairDto)
        {
            Pair pair = _mapper.Map<Pair>(pairDto);
            await _pairsRepository.InsertAsync(pair).ConfigureAwait(false);
            return Created($"/api/pairs/{pair.Id}", _mapper.Map<PairDto>(pair));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<PairDto>> Put(int id, UpdatePairDto pairDto)
        {
            Pair pair = await _pairsRepository.GetAsync(id).ConfigureAwait(false);
            if (pair == null)
            {
                return NotFound($"Pair with id {id} not exist");
            }

            _mapper.Map(pairDto, pair);
            await _pairsRepository.UpdateAsync(pair).ConfigureAwait(false);

            return Ok(_mapper.Map<PairDto>(pair));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<PairDto>> Delete(int id)
        {
            Pair pair = await _pairsRepository.GetAsync(id).ConfigureAwait(false);
            if (pair == null)
            {
                return NotFound($"Pair with id {id} not exist");
            }

            await _pairsRepository.DeleteAsync(pair).ConfigureAwait(false);

            return NoContent();
        }
    }
}
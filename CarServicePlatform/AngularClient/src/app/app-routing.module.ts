import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppConstants } from './app-constants';
import { AppComponent } from './app.component';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { DetailTrawlAccountsComponent } from './pages/accounts/detail-trawl-accounts/detail-trawl-accounts.component';
import { DetailsServiceAccountsComponent } from './pages/accounts/details-service-accounts/details-service-accounts.component';
import { ServiceAccountsComponent } from './pages/accounts/service-accounts/service-accounts.component';
import { TrawlAccountsComponent } from './pages/accounts/trawl-accounts/trawl-accounts.component';
import { AllClientRequestComponent } from './pages/all-client-request/all-client-request.component';
import { ChatComponent } from './pages/chat/chat.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { RegisterServiceComponent } from './pages/register-service/register-service.component';
import { RegisterTrawlComponent } from './pages/register-trawl/register-trawl.component';
import { RegisterComponent } from './pages/register/register.component';
import { RegistrationCategoryComponent } from './pages/registration-category/registration-category.component';
import { RepairRequestComponent } from './pages/repair-request/repair-request.component';
import { RequestDetailsComponent } from './pages/request-details/request-details.component';
import { ApprovedServiceRequestsComponent as AssignedServiceRequestsComponent } from './pages/requests/approved-service-requests/approved-service-requests.component';
import { AssignedToServiceRequestDetailsComponent } from './pages/requests/assigned-to-service-request-details/assigned-to-service-request-details.component';
import { EndedServiceRequestsComponent } from './pages/requests/ended-service-requests/ended-service-requests.component';
import { RequestWaitingClientAcceptanceComponent } from './pages/requests/request-waiting-client-acceptance/request-waiting-client-acceptance.component';
import { RequestWaitingServiceOfferComponent } from './pages/requests/request-waiting-service-offer/request-waiting-service-offer.component';
import { AssignedToTrawlComponent } from './pages/requests/trawl-requests/assigned-to-trawl/assigned-to-trawl.component';
import { AssignedTrawlRequestDetailsComponent } from './pages/requests/trawl-requests/assigned-trawl-request-details/assigned-trawl-request-details.component';
import { RejectedTrawlComponent } from './pages/requests/trawl-requests/rejected-trawl/rejected-trawl.component';
import { TrawlRequestDetailsComponent } from './pages/requests/trawl-requests/trawl-request-details/trawl-request-details.component';
import { WaitingForClientResponseComponent } from './pages/requests/trawl-requests/waiting-for-client-response/waiting-for-client-response.component';
import { WaitingForTrawlOfferComponent } from './pages/requests/trawl-requests/waiting-for-trawl-offer/waiting-for-trawl-offer.component';
import { ServiceProfileComponent } from './pages/service-profile/service-profile.component';
import { TrawlProfileComponent } from './pages/trawl-profile/trawl-profile.component';
import { AdminGuard } from './services/auth/admin-guard';
import { AuthGuard } from './services/auth/auth-guard';
import { ClientGuard } from './services/auth/client-guard';
import { LoginGuard } from './services/auth/login-guard';
import { ServiceGuard } from './services/auth/service-guard';
import { TrawlGuard } from './services/auth/trawl-guard';

const routes: Routes = [
  { path: AppConstants.Home, component: HomeComponent },
  { path: AppConstants.Chat, component: ChatComponent, canActivate: [AuthGuard] },
  { path: AppConstants.Login, component: LoginComponent, canActivate: [LoginGuard]  },
  { path: AppConstants.RegisterClient, component: RegisterComponent, canActivate: [LoginGuard]  },
  { path: AppConstants.RegisterService, component: RegisterServiceComponent, canActivate: [LoginGuard]  },
  { path: AppConstants.RegisterTrawl, component: RegisterTrawlComponent, canActivate: [LoginGuard]  },
  { path: AppConstants.RegisterCategories, component: RegistrationCategoryComponent, canActivate: [LoginGuard]  },
  { path: AppConstants.Profile, component: ProfileComponent, canActivate: [AuthGuard] },
  { path: AppConstants.CompanyServiceProfile, component: ServiceProfileComponent, canActivate: [ServiceGuard] },
  { path: AppConstants.CompanyTrawlProfile, component: TrawlProfileComponent, canActivate: [TrawlGuard] },

  // { path: AppConstants.Accounts, component: AccountsComponent, canActivate: [AdminGuard] },
  { path: AppConstants.AccountsService, component: ServiceAccountsComponent, canActivate: [AdminGuard] },
  { path: AppConstants.AccountsTrawl, component: TrawlAccountsComponent, canActivate: [AdminGuard] },
  { path: AppConstants.AccountsTrawl+"/:id/:index", component: DetailTrawlAccountsComponent, canActivate: [AdminGuard] },
  { path: AppConstants.AccountsService+"/:id/:index", component: DetailsServiceAccountsComponent, canActivate: [AdminGuard] },

  { path: AppConstants.RepairRequest, component: RepairRequestComponent, canActivate: [ClientGuard] },
  { path: AppConstants.AllClientRequest, component: AllClientRequestComponent, canActivate: [AuthGuard] },
  { path: AppConstants.AllWaitingClientRequestForServiceOffer, component: RequestWaitingServiceOfferComponent, canActivate: [ServiceGuard] },
  { path: AppConstants.AllWaitingClientRequestForClientApproval, component: RequestWaitingClientAcceptanceComponent, canActivate: [ServiceGuard] },
  { path: AppConstants.AllAssignedClientRequestForService, component: AssignedServiceRequestsComponent, canActivate: [ServiceGuard] },
  { path: AppConstants.AllInactiveClientRequestForService, component: EndedServiceRequestsComponent, canActivate: [ServiceGuard] },

  { path: AppConstants.AllWaitingTrawlRequestForOffer, component: WaitingForTrawlOfferComponent, canActivate: [TrawlGuard] },
  { path: AppConstants.AllWaitingClientRequestToTransferForClientApproval, component: WaitingForClientResponseComponent, canActivate: [TrawlGuard] },
  { path: AppConstants.AllAssignedClientRequestForTrawl, component: AssignedToTrawlComponent, canActivate: [TrawlGuard] },
  { path: AppConstants.AllInactiveClientRequestForTrawl, component: RejectedTrawlComponent, canActivate: [TrawlGuard] },

  { path: AppConstants.AllClientRequest+"/:id/:index", component: RequestDetailsComponent, canActivate: [AuthGuard] },
  { path: AppConstants.AllAssignedClientRequestForService+"/:id/:index", component: AssignedToServiceRequestDetailsComponent, canActivate: [ServiceGuard] },
  { path: AppConstants.AllTrawlRequests+"/:id/:index", component: TrawlRequestDetailsComponent, canActivate: [TrawlGuard] },
  { path: AppConstants.AllAssignedClientRequestForTrawl+"/:id/:index", component: AssignedTrawlRequestDetailsComponent, canActivate: [TrawlGuard] },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

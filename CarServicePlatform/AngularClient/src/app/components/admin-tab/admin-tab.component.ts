import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-admin-tab',
  templateUrl: './admin-tab.component.html',
  styleUrls: ['./admin-tab.component.css']
})
export class AdminTabComponent implements OnInit {

  @Input('tab') tab = '';
  readonly serviceAccountsUrl = AppConstants.AccountsService;
  readonly trawlAccountsUrl = AppConstants.AccountsTrawl;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  isServiceAccounts(){
    return this.tab==HeaderConstants.ServiceTab;
  }

  isTrawlAccounts(){
    return this.tab==HeaderConstants.TrawlTab;
  }

  toTrawl(){
    this.router.navigate([this.trawlAccountsUrl]);
  }

  toService(){
    this.router.navigate([this.serviceAccountsUrl]);
  }

}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiConstants } from '../api-constants';
import { ClientAccount, ServiceAccount, TrawlAccount, UpdateClientAccount, UpdateServiceAccount, UpdateTrawlAccount, UserAccount } from '../shared/account';
import { AuthService } from './auth/auth.service';
import { ErrorHandleService } from './error-handle.service';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  clientUrl = environment.apiUrl + ApiConstants.clientAccount;
  adminUrl = environment.apiUrl + ApiConstants.adminAccount;
  serviceUrl = environment.apiUrl + ApiConstants.serviceAccount;
  trawlUrl = environment.apiUrl + ApiConstants.trawlAccount;
  activateUrl = environment.apiUrl + ApiConstants.activateAccount;
  deactivateUrl = environment.apiUrl + ApiConstants.deactivateAccount;
  constructor(private http: HttpClient, private authService: AuthService, private errorHandle: ErrorHandleService) { }
  loadClientAccount(): Observable<ClientAccount>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ClientAccount>( this.clientUrl + this.authService.getUserId(), {headers: headers})
    .pipe(map((data:ClientAccount) => data), catchError(this.errorHandle.errorHandle));
  }

  loadAdminAccount(): Observable<ClientAccount>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ClientAccount>( this.adminUrl + this.authService.getUserId(), {headers: headers})
    .pipe(map((data:ClientAccount) => data), catchError(this.errorHandle.errorHandle));
  }

  loadServiceAccount(id: string| null): Observable<ServiceAccount>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ServiceAccount>( this.serviceUrl + id, {headers: headers})
    .pipe(map((data:ServiceAccount) => data), catchError(this.errorHandle.errorHandle));
  }

  loadTrawlAccountByEmail(email: string | null): Observable<TrawlAccount>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<TrawlAccount>( this.trawlUrl + email, {headers: headers})
    .pipe(map((data:TrawlAccount) => data), catchError(this.errorHandle.errorHandle));
  }

  loadServiceAccounts(): Observable<ServiceAccount[]>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ServiceAccount[]>( this.serviceUrl, {headers: headers})
    .pipe(map((data:ServiceAccount[]) => data), catchError(this.errorHandle.errorHandle));
  }

  loadTrawlAccounts(): Observable<TrawlAccount[]>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<TrawlAccount[]>( this.trawlUrl, {headers: headers})
    .pipe(map((data:TrawlAccount[]) => data), catchError(this.errorHandle.errorHandle));
  }

  updateClientAccount(clientAccount: UpdateClientAccount){
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    this.authService.updateSession(clientAccount.email, clientAccount.name, clientAccount.surname);
    return this.http.put<UpdateClientAccount>(this.clientUrl + this.authService.getUserId(), clientAccount, {headers: headers})
  }

  updateAdminAccount(clientAccount: UpdateClientAccount){
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    this.authService.updateSession(clientAccount.email, clientAccount.name, clientAccount.surname);
    return this.http.put<UpdateClientAccount>(this.adminUrl + this.authService.getUserId(), clientAccount, {headers: headers})
  }

  updateServiceAccount(serviceAccount: UpdateServiceAccount){
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    this.authService.updateSession(serviceAccount.email, serviceAccount.name, serviceAccount.surname);
    return this.http.put<UpdateServiceAccount>(this.serviceUrl + this.authService.getUserId(), serviceAccount, {headers: headers})
  }

  updateTrawlAccount(trawlAccount: UpdateTrawlAccount){
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    this.authService.updateSession(trawlAccount.email, trawlAccount.name, trawlAccount.surname);
    return this.http.put<UpdateTrawlAccount>(this.trawlUrl + this.authService.getUserId(), trawlAccount, {headers: headers})
  }

  activateAccount(userId: string):Observable<UserAccount>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.put<UserAccount>(this.activateUrl+userId, {headers: headers})
    .pipe(map((data:UserAccount)=>data), catchError(this.errorHandle.errorHandle));
  }

  deactivateAccount(userId: string):Observable<UserAccount>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.put<UserAccount>(this.deactivateUrl+userId, {headers: headers})
    .pipe(map((data:UserAccount)=>data), catchError(this.errorHandle.errorHandle));
  }
  
  deleteServiceAccount(userId: string){
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.delete(this.serviceUrl+userId, {headers: headers});
  }

  deleteTrawlAccount(userId: string){
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.delete(this.trawlUrl+userId, {headers: headers});
  }
}

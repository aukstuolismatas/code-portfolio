#include "DataMonitor.h"
#include <iostream>
#include <exception>
#include <nlohmann\json.hpp>

DataMonitor::DataMonitor(int n)
{
	MAX_VALUE = 4;
	array = new HDD1*[MAX_VALUE];
	counter = 0;
	isLast = false;
}
bool DataMonitor::addHDD(HDD1* hdd, bool last)
{
	bool isSuccessful;
	#pragma omp critical (count_critical)
	{
		//std::cout << "add" + counter;
		if (counter >= MAX_VALUE)
		{
			isSuccessful = false;
		}
		else
		{
			array[counter] = hdd;
			counter++;
			isLast = last;
			isSuccessful = true;
		}
	}
	return isSuccessful;
}
HDD1* DataMonitor::removeFirst()
{
	HDD1* removedItem = new HDD1();
	#pragma omp critical (count_critical)
	{
		//std::cout << "remove" + counter;
		if (counter <= 0)
		{
			removedItem = nullptr;
		}
		else if (counter <= 0 && isLast)
		{
			//throw 10;
			//throw std::exception::exception();
			removedItem = NULL;
		}
		else
		{
			removedItem = array[0];
			for (int i = 0; i < counter - 1; i++)
			{
				array[i] = array[i + 1];
			}
			counter--;
		}
	}
	return removedItem;
}
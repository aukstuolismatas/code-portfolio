﻿using AutoMapper;
using Bridge.Data.Dtos.Auth;
using Bridge.Data.Dtos.Auth.Users;
using Bridge.Data.Dtos.Board;
using Bridge.Data.Dtos.Pair;
using Bridge.Data.Dtos.TablePair;
using Bridge.Data.Dtos.Tables;
using Bridge.Data.Dtos.Tournaments;
using Bridge.Data.Entities;

namespace Bridge.Data
{
    public class BridgeRestProfile : Profile
    {
        public BridgeRestProfile()
        {
            CreateMap<Tournament, TournamentDto>();
            CreateMap<CreateTournamentDto, Tournament>();
            CreateMap<UpdateTournamentDto, Tournament>();

            CreateMap<Table, TableDto>();
            CreateMap<CreateTableDto, Table>();
            CreateMap<UpdateTableDto, Table>();

            CreateMap<TablePair, TablePairDto>();
            CreateMap<CreateTablePairDto, TablePair>();
            CreateMap<UpdateTablePairDto, TablePair>();

            // CreateMap<User, UserDto>();
            // CreateMap<CreateUserDto, User>();
            // CreateMap<UpdateUserDto, User>();

            CreateMap<Pair, PairDto>();
            CreateMap<CreatePairDto, Pair>();
            CreateMap<UpdatePairDto, Pair>();

            CreateMap<Board, BoardDto>();
            CreateMap<CreateBoardDto, Board>();
            CreateMap<UpdateBoardDto, Board>();

            CreateMap<BridgeRestUser, UserDto>();
        }
    }
}
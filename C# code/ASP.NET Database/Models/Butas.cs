using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NT_Csharp.Models
{
    public class Butas
    {
        public int laiptine { get; set; }
        public int butoNR { get; set; }
        public int butoAukstas { get; set; }
        public bool vietaPark { get; set; }
        public bool zaidimuAikst { get; set; }
        public bool rusys { get; set; }
        public int id { get; set; }
    }
}
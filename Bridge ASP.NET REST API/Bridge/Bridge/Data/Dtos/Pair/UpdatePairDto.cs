﻿using System.ComponentModel.DataAnnotations;

namespace Bridge.Data.Dtos.Pair
{
    public record UpdatePairDto([Required] string Player1, [Required] string Player2, int Place);
}
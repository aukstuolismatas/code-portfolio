import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiConstants } from '../api-constants';
import { ApprovedClientRequest, ApprovedClientRequestTrawl, ClientRequest, PlacePriceOffer, PriceOffer, RegisteredClientRequest, RejectedClientRequest, TransferOffer, UpdatePriceOffer as UpdateOffer } from '../shared/client-request';
import { AuthService } from './auth/auth.service';
import { ErrorHandleService } from './error-handle.service';

@Injectable({
  providedIn: 'root'
})
export class RepairRequestService {
  requestUrl=environment.apiUrl + ApiConstants.clientRepairRequest;
  requestStartUrl=environment.apiUrl + ApiConstants.clientRepairRequestStart;
  requestEndUrl=environment.apiUrl + ApiConstants.clientRepairRequestEnd;
  requestTransferringUrl=environment.apiUrl + ApiConstants.clientRepairRequestTransferring;
  requestArrivedUrl=environment.apiUrl + ApiConstants.clientRepairRequestArrived;
  requestDeliveredUrl=environment.apiUrl + ApiConstants.clientRepairRequestDelivered;

  availableRequestForServiceUrl=environment.apiUrl+ApiConstants.availableClientRequestsForService;
  requestsWaitingApprovalForServiceUrl=environment.apiUrl+ApiConstants.clientRequestsWaitingApprovalForService;
  assignedRequestForServiceUrl=environment.apiUrl+ApiConstants.assignedClientRequestsForService;
  rejectedRequestForServiceUrl=environment.apiUrl+ApiConstants.rejectedClientRequestsForService;

  priceOfferUrl=environment.apiUrl+ApiConstants.priceOffer;
  constructor(private http: HttpClient, private authService: AuthService, private errorHandle: ErrorHandleService) { }

  placeClientRequest(clientRequest: ClientRequest){
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    headers.append('content-type','application/json');
    return this.http.post<RegisteredClientRequest>(this.requestUrl, clientRequest, {headers: headers})
    .pipe(map((data:RegisteredClientRequest)=>data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllClientRequest(): Observable<RegisteredClientRequest[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RegisteredClientRequest[]>( this.requestUrl, {headers: headers})
    .pipe(map((data:RegisteredClientRequest[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllAvailableClientRequestForService(): Observable<RegisteredClientRequest[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RegisteredClientRequest[]>(`${this.availableRequestForServiceUrl}${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:RegisteredClientRequest[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllClientRequestWaitingApprovalForService(): Observable<RegisteredClientRequest[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RegisteredClientRequest[]>(`${this.requestsWaitingApprovalForServiceUrl}${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:RegisteredClientRequest[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllAssignedClientRequestForService(): Observable<ApprovedClientRequest[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ApprovedClientRequest[]>(`${this.assignedRequestForServiceUrl}${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequest[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllRejectedClientRequestForService(): Observable<RejectedClientRequest[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RejectedClientRequest[]>(`${this.rejectedRequestForServiceUrl}${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:RejectedClientRequest[]) => data), catchError(this.errorHandle.errorHandle));
  }
  
  retrieveClientRequestById(id: number): Observable<RegisteredClientRequest>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<RegisteredClientRequest>(`${this.requestUrl}${id}`, {headers: headers})
    .pipe(map((data:RegisteredClientRequest) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAssignedClientRequestById(id: number): Observable<ApprovedClientRequest>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<ApprovedClientRequest>(`${this.requestUrl}${id}/service/assigned/${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequest) => data), catchError(this.errorHandle.errorHandle));
  }

  placePriceOffer(priceOffer: PlacePriceOffer){
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .post<PriceOffer>(this.priceOfferUrl, priceOffer, {headers: headers})
    .pipe(map((data:PriceOffer)=>data), catchError(this.errorHandle.errorHandle));
  }

  retrieveClientPriceOffers(requestId: number): Observable<PriceOffer[]>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<PriceOffer[]>(`${this.priceOfferUrl}${requestId}`, {headers: headers})
    .pipe(map((data:PriceOffer[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveServicePriceOffer(requestId: number): Observable<PriceOffer>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .get<PriceOffer>(`${this.priceOfferUrl}${requestId}/service/${this.authService.getUserId()}`, {headers: headers})
    .pipe(map((data:PriceOffer) => data), catchError(this.errorHandle.errorHandle));
  }

  approvePriceOffer(offerId: number, clientRequestId: number): Observable<PriceOffer[]>{
    const updateRequest: UpdateOffer= {
      clientRequestId: clientRequestId
    }
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .put<PriceOffer[]>(`${this.priceOfferUrl}approve/${offerId}`, updateRequest, {headers: headers})
    .pipe(map((data:PriceOffer[])=>data), catchError(this.errorHandle.errorHandle));
  }

  approveTransferOffer(offerId: number, clientRequestId: number): Observable<TransferOffer[]>{
    const updateRequest: UpdateOffer= {
      clientRequestId: clientRequestId
    }
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .put<TransferOffer[]>(`${this.priceOfferUrl}approve/transfer/${offerId}`, updateRequest, {headers: headers})
    .pipe(map((data:TransferOffer[])=>data), catchError(this.errorHandle.errorHandle));
  }
  
  startClientRequest(requestId: number): Observable<ApprovedClientRequest>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .put<ApprovedClientRequest>(`${this.requestStartUrl}${requestId}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequest)=>data), catchError(this.errorHandle.errorHandle));
  }

  endClientRequest(requestId: number): Observable<ApprovedClientRequest>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .put<ApprovedClientRequest>(`${this.requestEndUrl}${requestId}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequest)=>data), catchError(this.errorHandle.errorHandle));
  }

  transferringClientRequest(requestId: number): Observable<ApprovedClientRequestTrawl>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .put<ApprovedClientRequestTrawl>(`${this.requestTransferringUrl}${requestId}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequestTrawl)=>data), catchError(this.errorHandle.errorHandle));
  }

  arrivedClientRequest(requestId: number): Observable<ApprovedClientRequestTrawl>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .put<ApprovedClientRequestTrawl>(`${this.requestArrivedUrl}${requestId}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequestTrawl)=>data), catchError(this.errorHandle.errorHandle));
  }

  deliveredClientRequest(requestId: number): Observable<ApprovedClientRequestTrawl>{
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    return this.http
    .put<ApprovedClientRequestTrawl>(`${this.requestDeliveredUrl}${requestId}`, {headers: headers})
    .pipe(map((data:ApprovedClientRequestTrawl)=>data), catchError(this.errorHandle.errorHandle));
  }
}

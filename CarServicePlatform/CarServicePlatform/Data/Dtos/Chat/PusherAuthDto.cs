﻿using Microsoft.AspNetCore.Mvc;

namespace CarServicePlatform.Data.Dtos.Chat;

public record PusherAuthDto([FromForm] string channel_name, [FromForm] string socket_id);
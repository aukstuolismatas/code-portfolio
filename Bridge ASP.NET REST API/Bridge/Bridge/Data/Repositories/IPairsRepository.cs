﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;

namespace Bridge.Data.Repositories
{
    public interface IPairsRepository
    {
        Task<List<Pair>> GetAllAsync();
        Task<Pair> GetAsync(int id);
        Task InsertAsync(Pair pair);
        Task UpdateAsync(Pair pair);
        Task DeleteAsync(Pair pair);
    }
}
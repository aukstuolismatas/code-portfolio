﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.RepairServices;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class RepairServiceCategoryRepository : IRepairServiceCategoryRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public RepairServiceCategoryRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<RepairServiceCategory?> GetCategoryAsync(int id)
    {
        return await _carPlatformDbContext.RepairServiceCategories.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
    }

    public async Task<List<RepairServiceCategory>> GetAllCategoriesAsync()
    {
        return await _carPlatformDbContext.RepairServiceCategories.ToListAsync().ConfigureAwait(false);
    }

    public async Task InsertCategoryAsync(RepairServiceCategory repairServiceCategory)
    {
        _carPlatformDbContext.RepairServiceCategories.Add(repairServiceCategory);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task UpdateCategoryAsync(RepairServiceCategory repairServiceCategory)
    {
        _carPlatformDbContext.RepairServiceCategories.Update(repairServiceCategory);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteCategoryAsync(RepairServiceCategory repairServiceCategory)
    {
        _carPlatformDbContext.RepairServiceCategories.Remove(repairServiceCategory);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
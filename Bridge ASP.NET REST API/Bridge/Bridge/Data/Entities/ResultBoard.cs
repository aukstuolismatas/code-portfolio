﻿namespace Bridge.Data.Entities
{
    public class ResultBoard
    {
        public int TableNr { get; set; }
        public int RoundNr { get; set; }
        public int BoardNr { get; set; }
        public int PairNr { get; set; }
        public string Contracts { get; set; }
        public int Tricks { get; set; }
        public int Score { get; set; }
    }
}
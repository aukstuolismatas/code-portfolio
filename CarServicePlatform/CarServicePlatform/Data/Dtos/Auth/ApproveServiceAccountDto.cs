﻿namespace CarServicePlatform.Data.Dtos.Auth;

public record ApproveServiceAccountDto(string Email);
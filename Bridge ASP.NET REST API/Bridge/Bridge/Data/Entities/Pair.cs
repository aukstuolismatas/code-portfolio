﻿namespace Bridge.Data.Entities
{
    public class Pair
    {
        public int Id { get; set; }
        public string Player1 { get; set; }
        public string Player2 { get; set; }
        public string Place { get; set; }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Csharp.Repos;
using NT_Csharp.Models;
using NT_Csharp.ViewModels;


namespace NT_Csharp.Controllers
{
    public class ObjektasController : Controller
    {
        NTobjektasRepository ntOjektasRepository = new NTobjektasRepository();
        IrengimoBusenaRepository irengimoBusenaRepository = new IrengimoBusenaRepository();
        NTTipasRepository ntTipasRepository = new NTTipasRepository();
        VietaRepository vietaRepository = new VietaRepository();
        NamasRepository namasRepository = new NamasRepository();
        ButasRepository butasRepository = new ButasRepository();
        NamuTipasRepository namuTipasRepository = new NamuTipasRepository();
        // GET: Objektas
        public ActionResult Index()
        {
            ModelState.Clear();
            return View(ntOjektasRepository.getNTobjektai());
        }


        // GET: Objektas/Create
        public ActionResult Create()
        {
            NTEditViewModel nTEditViewModel = new NTEditViewModel();
            PopulateSelections(nTEditViewModel);
            return View(nTEditViewModel);
        }

        // POST: Objektas/Create
        [HttpPost]
        public ActionResult Create(NTEditViewModel collection)
        {
            try
            {
                NTEditViewModel tmp = ntOjektasRepository.getNTobjektas(collection.id);
                if (tmp.id != 0)
                {
                    ModelState.AddModelError("id", "Objektas su tokiu id jau užregistruotas");
                    return View(collection);
                }
                if (ModelState.IsValid)
                {
                    ntOjektasRepository.addObjektas(collection);
                }
                

                return RedirectToAction("Index");
            }
            catch
            {
                PopulateSelections(collection);
                return View(collection);
            }
        }

        // GET: Objektas/Edit/5
        public ActionResult Edit(int id)
        {
            NTEditViewModel nTEditViewModel = ntOjektasRepository.getNTobjektas(id);
            PopulateSelections(nTEditViewModel);
            return View(nTEditViewModel);
        }

        // POST: Objektas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, NTEditViewModel collection)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    ntOjektasRepository.updateNTObjektas(collection);
                }
                return RedirectToAction("Index");
            }
            catch
            {
                PopulateSelections(collection);
                return View(collection);
            }
        }

        // GET: Objektas/Delete/5
        public ActionResult Delete(int id)
        {
            return View(ntOjektasRepository.getNTobjektas(id));
        }

        // POST: Objektas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                bool naudojama = false;
                if (ntOjektasRepository.getObjektasSutarciuCount(id) > 0)
                {
                    naudojama = true;
                    ViewBag.naudojama = "Negalima pašalinti objekto turėjo sudarytų sutarčių";
                    return View(ntOjektasRepository.getNTobjektas(id));
                }
                if (!naudojama)
                {
                    ntOjektasRepository.deleteObjektas(id);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public void PopulateSelections(NTEditViewModel ntEditViewModels)
        {
            var busenos = irengimoBusenaRepository.getBusenos();
            var ntTipai = ntTipasRepository.getTipai();
            var namai = namasRepository.getNamai();
            var butai = butasRepository.getButai();
            var namuTipai = namuTipasRepository.getTipai();
            var vietos = vietaRepository.getVietos();
            List<SelectListItem> selectListItems = new List<SelectListItem>();
            List<SelectListItem> selectListBusenos = new List<SelectListItem>();
            List<SelectListItem> selectListntTipai = new List<SelectListItem>();
            List<SelectListItem> selectListVietos = new List<SelectListItem>();
            List<SelectListItem> selectListButai = new List<SelectListItem>();
            List<SelectListItem> selectListNamai = new List<SelectListItem>();
            List<SelectListItem> selectListNamuTipai = new List<SelectListItem>();
            List<SelectListGroup> groups = new List<SelectListGroup>();
            bool yra = false;

            //Sukuriamos pasirinkimo grupės
            foreach (var item in vietos)
            {
                yra = false;
                foreach (var i in groups)
                {
                    if (i.Name.Equals(item.miestas))
                    {
                        yra = true;
                    }
                }
                if (!yra)
                {
                    groups.Add(new SelectListGroup() { Name = item.miestas });
                }
            }

            //Užpildomas pasirinkimo sąrašas pagal grupes(miestus) gatves
            foreach (var item in vietos)
            {
                var optGroup = new SelectListGroup() { Name = "--------" };
                foreach (var i in groups)
                {
                    if (i.Name.Equals(item.miestas))
                    {
                        optGroup = i;
                    }
                }
                selectListItems.Add(
                    new SelectListItem() { Value = Convert.ToString(item.idVieta), Text = item.gatve +" "+ item.namoNR, Group = optGroup }
                    );
            }


            //užpildomas irengimo busenu sąrašas iš duomenų bazės
            foreach (var item in busenos)
            {
                selectListBusenos.Add(new SelectListItem() { Value = Convert.ToString(item.id), Text = item.pavadinimas });
            }

            //Užpildomas nt tipu sąrašas iš duomenų bazės
            foreach (var item in ntTipai)
            {
                selectListntTipai.Add(new SelectListItem() { Value = Convert.ToString(item.id), Text = item.pavadinimas });
            }
            //foreach (var item in namuTipai)
            //{
            //    selectListNamuTipai.Add(new SelectListItem() { Value = Convert.ToString(item.id), Text = item.pavadinimas });
            //}

            //Užpildomas butu sąrašas iš duomenų bazės
            foreach (var item in butai)
            {
                selectListButai.Add(new SelectListItem() { Value = Convert.ToString(item.id), Text = "id: "+item.id.ToString()+", buto numeris: "+item.butoNR.ToString() +", aukštas: "+item.butoAukstas.ToString()});
            }

            //Užpildomas namu sąrašas iš duomenų bazės
            foreach (var item in namai)
            {
                selectListNamai.Add(new SelectListItem() { Value = Convert.ToString(item.id), Text = "id: "+item.id+", tipas: "+item.namoTipas + ", kiemas: "+item.kiemas});
            }


            //Sarašai priskiriami vaizdo objektui
            ntEditViewModels.VietaList = selectListItems;
            ntEditViewModels.IrengimoBusenosList = selectListBusenos;
            ntEditViewModels.TipasList = selectListntTipai;
            ntEditViewModels.ButasList = selectListButai;
            ntEditViewModels.NamasList = selectListNamai;
        }
    }
}

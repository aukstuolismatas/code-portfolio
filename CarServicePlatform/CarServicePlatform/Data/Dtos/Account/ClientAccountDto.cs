﻿namespace CarServicePlatform.Data.Dtos.Account;

public record ClientAccountDto(string Name, string Surname);
// public record ClientAccountDto(string Id,string Name, string Surname);
﻿namespace CarServicePlatform.Data.Dtos.PriceOffer;

public record UpdatePriceOfferDto(int ClientRequestId);
﻿using CarServicePlatform.Entities.RepairServices;

namespace CarServicePlatform.Repository;

public interface IRequestedRepairServiceRepository
{
    Task<List<RequestedRepairService>> GetAllRequestedRepairServiceToClientRequestAsync(int clientRequestId);
    Task<List<RequestedRepairService>> GetAllRequestedRepairServiceAsync();
    Task InsertRequestedRepairServiceAsync(List<RequestedRepairService> assignedRepairServices);
    Task DeleteRequestedRepairServiceAsync(List<RequestedRepairService> requestedRepairServices);
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;

namespace Bridge.Data.Repositories
{
    public interface IBoardRepository
    {
        Task<Board> GetAsync(int boardId);
        Task<List<Board>> GetAllAsync();
        Task InsertAsync(Board board);
        Task UpdateAsync(Board board);
        Task DeleteAsync(Board board);
    }
}
﻿namespace Bridge.Auth
{
    public interface IUserOwnedResources
    {
        string UserId { get; }
    }
}
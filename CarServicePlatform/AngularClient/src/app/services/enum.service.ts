import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, retry } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiConstants } from '../api-constants';
import { City } from '../shared/city';
import { ErrorHandleService } from './error-handle.service';

@Injectable({
  providedIn: 'root'
})
export class EnumService {


  constructor(private http: HttpClient, private errorHandle: ErrorHandleService) { }

  loadCities():Observable<City[]>{
    return this.http.get<City[]>(`${environment.apiUrl}${ApiConstants.citiesEnum}`)
    .pipe(retry(1), catchError(this.errorHandle.errorHandle));
  }
}
export enum RolesType {
  Admin = 'Admin',
  Service = 'Service',
  Client = 'Client',
  Trawl = 'Trawl'
}

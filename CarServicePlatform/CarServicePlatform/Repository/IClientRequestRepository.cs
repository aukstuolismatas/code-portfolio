﻿using CarServicePlatform.Entities.Request;

namespace CarServicePlatform.Repository;

public interface IClientRequestRepository
{
    Task<ClientRequest?> GetRequestAsync(int requestId);
    Task<ClientRequest?> GetRequestForTrawlAsync(int requestId);
    Task<List<ClientRequest>> GetAllRequestsAsync();
    Task<List<ClientRequest>> GetAllRequestsAvailableForService(string serviceId);
    Task<List<ClientRequest>> GetAllRequestAvailableForTrawl(string trawlId);
    Task<List<ClientRequest>> GetAllRequestsWaitingApprovalForService(string serviceId);
    Task<List<ClientRequest>> GetAllRequestsWaitingApprovalForTrawl(string trawlId);
    Task<List<ClientRequest>> GetAllRequestsAssignedForService(string serviceId);
    Task<List<ClientRequest>> GetAllRequestsAssignedForTrawl(string trawlId);
    Task<List<ClientRequest>> GetAllRejectedClientRequestsForService(string serviceId);
    Task<List<ClientRequest>> GetAllRejectedClientRequestsForTrawl(string trawlId);
    Task InsertRequestAsync(ClientRequest clientRequest);
    Task UpdateRequestAsync(ClientRequest clientRequest);
    Task DeleteRequestAsync(ClientRequest clientRequest);
}
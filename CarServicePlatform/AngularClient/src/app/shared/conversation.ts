export interface ConversationMessage {
    sendBy: string;
    message: string;
    conversationGroupId: number;
}

export interface CreateConversationMessage {
    sendBy: string;
    message: string;
    socketId: string;
}

export interface ConversationGroup {
    id : number;
    conversationTitle : string
    clientRequestId : number;
    userName: string;
}

export interface CreateConversationGroup {
    userId : string|null
    clientRequestId : number;
}
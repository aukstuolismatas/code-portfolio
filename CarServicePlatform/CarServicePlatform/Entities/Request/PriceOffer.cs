﻿using System.ComponentModel.DataAnnotations.Schema;
using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Entities.Request;

public class PriceOffer
{
    public int Id { get; set; }
    public int Price { get; set; }
    public int Duration { get; set; }
    public OfferStatus Status { get; set; }

    [ForeignKey(nameof(ServiceAccount))]
    public string ServiceAccountId { get; set; }

    [ForeignKey(nameof(ClientRequest))]
    public int ClientRequestId { get; set; }
}
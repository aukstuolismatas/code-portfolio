import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { saveAs } from 'file-saver';
import { MessageConstants } from 'src/app/message-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FileService } from 'src/app/services/file.service';
import { ProfileService } from 'src/app/services/profile.service';
import { TrawlAccount, UserAccount } from 'src/app/shared/account';
import { City } from 'src/app/shared/city';
import { FileInformation } from 'src/app/shared/file-information';

@Component({
  selector: 'app-detail-trawl-accounts',
  templateUrl: './detail-trawl-accounts.component.html',
  styleUrls: ['./detail-trawl-accounts.component.css']
})
export class DetailTrawlAccountsComponent implements OnInit {
  index:number=0
  email=''
  public errorMessage ='';
  public successMessage ='';
  account!: TrawlAccount;
  documentsInformation:FileInformation[]=[]
  constructor(private dataSharingService: DataSharingService, private activatedRoute: ActivatedRoute, private profileService: ProfileService,
    private fileService: FileService) { }

  ngOnInit(): void {
    this.index = this.activatedRoute.snapshot.params['index'];
    this.email = this.activatedRoute.snapshot.params['id'];
    this.account = this.dataSharingService.trawlAccountsList.value[this.index];
    if(this.account===undefined){
      this.profileService.loadTrawlAccountByEmail(this.email).subscribe((data:TrawlAccount)=>{
        this.account=data
        this.fileService.retrieveAllDocumentsByUserId(data.id).subscribe((data: FileInformation[])=>this.documentsInformation=data);
      });
    } else{
      this.fileService.retrieveAllDocumentsByUserId(this.account.id).subscribe((data: FileInformation[])=>this.documentsInformation=data); 

    }
  } 
  downloadDocument(index: number){
    this.fileService.downloadDocumentAdmin(this.documentsInformation[index].id).subscribe((data:any) =>{
      let blob:any = new Blob([data], { type: `${data.type}` });
      const url = window.URL.createObjectURL(blob);
      saveAs(blob, this.documentsInformation[index].name);
    }), (error: any) => this.errorMessage = "Parsiūsti nepavyko",
    () => console.info('Successfully downloaded');
  }

  getCity(enumNumber:number){
    return City[enumNumber];
  }

  activateAccount(userId:string){
    this.profileService.activateAccount(userId).subscribe({
        next:(data:UserAccount)=>{
          this.account.isAccountActivated=data.isAccountActivated
          this.successMessage = MessageConstants.AccountActivated
        },
        error: () => this.errorMessage = MessageConstants.AccountActivatedFailed
      });
    this.successMessage='';
    this.errorMessage ='';
  }

  deactivateAccount(userId:string){
    this.profileService.deactivateAccount(userId).subscribe({
      next:(data:UserAccount)=>{
        this.account.isAccountActivated=data.isAccountActivated
        this.successMessage = MessageConstants.AccountDeactivated
      },
      error: () => this.errorMessage = MessageConstants.AccountActivatedFailed
    });
    this.successMessage='';
    this.errorMessage ='';
  }
}

﻿using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Repository;

public interface IClientRepository
{
    Task<ClientAccount?> GetClientAccountAsync(string userId);
    Task<DomainUser?> GetDomainUserAsync(string userId);
    Task<AdministratorAccount?> GetAdminAccountAsync(string userId);
    Task<Dictionary<string, ClientAccount>> GetAllClientAccountsAsync();
    Task UpdateClientAccountAsync(ClientAccount clientAccount);
    Task DeleteClientAccountAsync(ClientAccount clientAccount);
}
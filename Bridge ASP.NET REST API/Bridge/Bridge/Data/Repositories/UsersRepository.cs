﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bridge.Data.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        private readonly BridgeRestContext _bridgeRestContext;

        public UsersRepository(BridgeRestContext bridgeRestContext)
        {
            _bridgeRestContext = bridgeRestContext;
        }

        public async Task<List<User>> GetAllAsync()
        {
            return await _bridgeRestContext.Users.ToListAsync().ConfigureAwait(false);
        }

        public async Task<User> GetAsync(string username)
        {
            return await _bridgeRestContext.Users.FirstOrDefaultAsync(x => x.Username == username).ConfigureAwait(false);
        }

        public async Task InsertAsync(User user)
        {
            _bridgeRestContext.Users.Add(user);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task UpdateAsync(User user)
        {
            _bridgeRestContext.Users.Update(user);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAsync(User user)
        {
            _bridgeRestContext.Users.Remove(user);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
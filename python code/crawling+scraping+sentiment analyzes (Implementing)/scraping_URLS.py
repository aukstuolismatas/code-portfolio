import sys
from bs4 import BeautifulSoup
from selenium import webdriver  #for show hidden reviews
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from collections import Counter
import time
from datetime import datetime
import os
import csv
import json


URL_FILE='url.json'
URL_FILE_NEW='url_scraping.json'
RESULTS_FILE='companies_info.csv'
WEBSITE_URL = "https://www.firmy.cz"
WINDOW_SIZE="1920,1080"
CHROME_DRIVER_PATH='C:/Users/lt0mtls/PycharmProjects/Crawling/webdriver/chromedriver.exe'
CHROME_PATH='C:/Users/lt0mtls/PycharmProjects/Crawling/webdriver'
ERROR_THRESHOLD=5

class URLScrapper():
    # name = 'xxx'
    # allowed_domains = [WEBSITE_URL]
    # start_urls = ['https://www.firmy.cz/detail/12973177-aaa-auto-praha-cimice.html']
    fieldnames = ['Firmy.cz', 'City', 'Company', 'Category', 'Website', 'Phone', 'Email', 'Rate', 'Reviews']

    def __init__(self):
        # For running chrome in the background
        chrome_options=Options()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--window-size=%s" % WINDOW_SIZE)
        # chrome_options.binary_location=CHROME_PATH
        self.driver = webdriver.Chrome(executable_path=CHROME_DRIVER_PATH, chrome_options=chrome_options)
        # self.driver=webdriver.Chrome(CHROME_DRIVER_PATH)

        self.error=[] #error list to avoid repeating error

        if os.path.isfile(URL_FILE):
            self.url_dictionary = load_to_dictionary(URL_FILE)
        else:
            print("File "+ URL_FILE +" not found")
            exit(0)


    def crawl_urls(self):
        with open(URL_FILE_NEW, mode='w+', encoding='utf-8') as url_file:
            for url in self.url_dictionary:
                try:
                    temp_url = {}
                    temp_url[url] = True
                    if not self.url_dictionary[url]:
                        if not self.is_url_exist_csv(url):
                            self.driver.get(url)
                            time.sleep(2)
                            # Checking url has at least one review
                            reviews = self.driver.find_elements_by_xpath(
                                "//div[@class='ratingText']/span[@class='info']")
                            if not reviews:
                                reviews = self.driver.find_elements_by_xpath("//div[@class='info infoPlus']")

                            # Expanding reviews in clicking the button show more
                            if reviews:
                                while True:
                                    try:
                                        next = WebDriverWait(self.driver, 10).until(
                                            EC.presence_of_element_located((By.XPATH, '//*[@id=\"nextPageBtn\"]')))
                                        next.click()
                                        date_pub = \
                                            self.driver.find_elements_by_xpath(
                                                "//span[@class='date']/meta[@itemprop='datePublished']")[
                                                -1].get_attribute("content")
                                        if datetime.strptime(date_pub, '%Y-%m-%d').year < 2018:
                                            break
                                        time.sleep(1)  # for avoiding exceptions of not loaded page
                                    except Exception as e:
                                        print(e)
                                        break

                            # Collecting a expanded reviews
                            try:
                                reviews = self.driver.find_elements_by_xpath(
                                    "//div[@class='ratingText']/span[@class='info']")
                                print(url)
                                review_arr = []
                                for r in reviews:
                                    review_arr.append(r.text)
                                    print(r.text)
                            except Exception as e:
                                try:
                                    reviews = self.driver.find_elements_by_xpath("//div[@class='info infoPlus']")
                                    for r in reviews:
                                        review_arr.append(r.text)
                                        print(r.text)
                                except Exception as e:
                                    print("Reviews")
                                    print(e)

                            # Collecting other important details about the company
                            try:
                                self.parse_details(self.driver.page_source, review_arr, url)
                                self.error = []  # reset array in every successful iteration
                                self.url_dictionary[url] = True
                            except MemoryError:
                                temp_url[url] = False
                                self.url_dictionary[url] = False
                                self.driver.close()
                                sys.exit()
                            except Exception as e:
                                print(e)
                                self.error.append(e)
                                temp_url[url] = False
                                self.url_dictionary[url] = False
                                if self.is_error_repeat():
                                    self.load_to_json_file()
                                    self.driver.close()
                                    sys.exit()

                    url_file.write(json.dumps(temp_url) + '\n')
                except Exception as e:
                    self.load_to_json_file()


        self.driver.close()


# extract the details of one page and store it in csv file
    def parse_details(self, body, reviews, firmy_url):
        soup = BeautifulSoup(body, "html.parser")

        #  So I take only the last not empty div element to get the city
        address = soup.find('h2', {'class': 'sAddress'})
        cities = address.findAll('div')
        for c in cities:
            if c.text != '':
                city = c.text

        # Checking every entry if it is exists in the page in order to avoid empty lines or errors
        # The most valueable entries are phone number and email address.
        if soup.find('h1', {'data-dot': 'premise/title'}):
            companyName = soup.find('h1', {'data-dot': 'premise/title'}).text.strip()
            if soup.find('li', {'class': 'category'}):
                category = soup.find('li', {'class': 'category'}).text.strip()
                if soup.find('a', {'class': 'url companyUrl'}, href=True):
                    webURL = soup.find('a', {'class': 'url companyUrl'}, href=True)['href']
                    with open(RESULTS_FILE, mode='a', newline='', encoding="utf-8") as csv_file:
                        writer = csv.DictWriter(csv_file, fieldnames=self.fieldnames)
                        if soup.find('span', {'data-dot': 'origin-phone-number'}):
                            tel = soup.find('span', {'data-dot': 'origin-phone-number'}).text
                            if soup.find('a', {'class': 'companyMail'}):
                                email = soup.find('a', {'class': 'companyMail'}).text
                                writer.writerow(
                                    {'Firmy.cz': firmy_url, 'City': city, 'Company': companyName, 'Category': category,
                                     'Website': webURL,
                                     'Phone': tel, 'Email': email, 'Rate':0, 'Reviews': reviews } )
                                # csv_file.write({companyName, category,  webURL, tel, email})
                                # print()
                                # print(soup.find('a', {'class': 'url companyUrl'}, href=True))
                                # print(soup.find('span', {'data-dot': 'origin-phone-number'}).text)
                                # print(soup.find('a', {'class': 'companyMail'}).text)
                                # print()
                        # else:
                        #     writer.writerow(
                        #         {'City': city, 'Company': companyName, 'Category': category,
                        #          'Website': webURL})

    # Check if the url is stored in the csv file
    def is_url_exist_csv(self, url):
        with open(RESULTS_FILE, 'r', newline='', encoding="utf-8") as input_file:
            reader = csv.DictReader(input_file, fieldnames=self.fieldnames, delimiter=',')
            for line in reader:
                if line['Firmy.cz']==url:
                    return True

            return False

    # Catch if the error repeats several time in the row
    def is_error_repeat(self):
        error=Counter(self.error).most_common(1)
        if error[0][1]>ERROR_THRESHOLD:
            return True
        return False

    # Load the url dictionary to json file
    def load_to_json_file(self):
        with open(URL_FILE, mode='w', encoding='utf-8') as output_file:
            for url in self.url_dictionary:
                temp={}
                temp[url]=self.url_dictionary[url]
                output_file.write(json.dumps(temp) + '\n')

# Load urls from json file to python dictionary
def load_to_dictionary(filename):
    d={}
    with open(filename, mode='r', encoding='utf-8') as file:
        for line in file:
            js = json.loads(line)
            d.update(js)
    return d

scrap=URLScrapper()
scrap.crawl_urls()
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { TrawlTransferRequestService } from 'src/app/services/trawl-transfer-request.service';
import { ApprovedClientRequestTrawl } from 'src/app/shared/client-request';
import { RequestStatus } from 'src/app/shared/request-status';

@Component({
  selector: 'app-assigned-to-trawl',
  templateUrl: './assigned-to-trawl.component.html',
  styleUrls: ['./assigned-to-trawl.component.css']
})
export class AssignedToTrawlComponent implements OnInit {
  tabName:string=HeaderConstants.AssignedTab;
  frontTitle:string='Priskirti užsakymai'
  titles:string[]=['Užsakymo nr.', 'Miestas', 'Atvykimo adresas', 'Pristatymo adresas', 'Atvykimo laikas',  'Kaina', 'Statusas']
  repairRequestList:ApprovedClientRequestTrawl[]=[];
  constructor(private trawlTransferService: TrawlTransferRequestService, private dataSharingService: DataSharingService, private router: Router) {
    dataSharingService.approvedTrawlRequestList.subscribe(value => this.repairRequestList = value);
      this.trawlTransferService.retrieveAllAssignedClientRequestForTrawl().subscribe((data:ApprovedClientRequestTrawl[])=>{
        this.dataSharingService.approvedTrawlRequestList.next(data)
      })
   }

  ngOnInit(): void {
  }

  openItem(index:number){
    this.router.navigate([`${AppConstants.AllAssignedClientRequestForTrawl}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  }

  getStatusDescription(enumString:string){
    return (<any>RequestStatus)[enumString];
  }
}

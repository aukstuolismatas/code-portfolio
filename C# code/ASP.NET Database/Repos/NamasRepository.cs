using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using NT_Csharp.Models;
using NT_Csharp.ViewModels;
using System.Configuration;


namespace NT_Csharp.Repos
{
    public class NamasRepository
    {
        public List<Namas> getNamai()
        {
            List<Namas> namai = new List<Namas>();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "SELECT namas.rekuperacija, namas.apzeldinimas, namas.kiemas, b.name as tipas, namas.id_NAMAS FROM namas " +
                "LEFT JOIN namu_tipai b on b.id_namu_tipai=namas.namo_tipas;";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                namai.Add(new Namas
                {
                    rekuperacija = Convert.ToBoolean(item["rekuperacija"]),
                    apzeldinimas = Convert.ToBoolean(item["apzeldinimas"]),
                    kiemas = Convert.ToBoolean(item["kiemas"]),
                    namoTipas = Convert.ToString(item["tipas"]),
                    id = Convert.ToInt32(item["id_NAMAS"]),
                });
            }
            return namai;
        }
        public NamasEditViewModel getNamas(int id)
        {
            NamasEditViewModel namas = new NamasEditViewModel();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "select * FROM namas where id_NAMAS="+id;
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                namas.rekuperacija = Convert.ToBoolean(item["rekuperacija"]);
                namas.apzeldinimas = Convert.ToBoolean(item["apzeldinimas"]);
                namas.kiemas = Convert.ToBoolean(item["kiemas"]);
                namas.namoTipas = Convert.ToInt32(item["namo_tipas"]);
                namas.id = Convert.ToInt32(item["id_NAMAS"]);
            }
            return namas;
        }
        public bool addNamas(NamasEditViewModel namas)
        {
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
                MySqlConnection mySqlConnection = new MySqlConnection(conn);
                string sqlquery = @"INSERT INTO namas(rekuperacija,apzeldinimas,kiemas,namo_tipas,id_NAMAS)VALUES(?rekup,?apzeld,?kiem,?tipas,?id);";
                MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
                mySqlCommand.Parameters.Add("?rekup", MySqlDbType.Int32).Value = namas.rekuperacija;
                mySqlCommand.Parameters.Add("?apzeld", MySqlDbType.Int32).Value = namas.apzeldinimas;
                mySqlCommand.Parameters.Add("?kiem", MySqlDbType.Int32).Value = namas.kiemas;
                mySqlCommand.Parameters.Add("?tipas", MySqlDbType.Int32).Value = namas.namoTipas;
                mySqlCommand.Parameters.Add("?id", MySqlDbType.Int32).Value = namas.id;
                mySqlConnection.Open();
                mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool updateNamas(NamasEditViewModel namas)
        {
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
                MySqlConnection mySqlConnection = new MySqlConnection(conn);
                string sqlquery = @"UPDATE namas a SET a.rekuperacija=?rekup, a.apzeldinimas=?apzeld, a.kiemas=?kiem, a.namo_tipas=?tipas WHERE a.id_NAMAS=?id";
                MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
                mySqlCommand.Parameters.Add("?rekup", MySqlDbType.Int32).Value = namas.rekuperacija;
                mySqlCommand.Parameters.Add("?apzeld", MySqlDbType.Int32).Value = namas.apzeldinimas;
                mySqlCommand.Parameters.Add("?kiem", MySqlDbType.Int32).Value = namas.kiemas;
                mySqlCommand.Parameters.Add("?tipas", MySqlDbType.Int32).Value = namas.namoTipas;
                mySqlCommand.Parameters.Add("?id", MySqlDbType.Int32).Value = namas.id;
                mySqlConnection.Open();
                mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public int getNamasObjektuCount(int id)
        {
            int naudota = 0;
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"SELECT count(nr) as kiekis from nt_objektas where fk_NAMASid_NAMAS=" + id;
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                naudota = Convert.ToInt32(item["kiekis"] == DBNull.Value ? 0 : item["kiekis"]);
            }
            return naudota;
        }
        public void deleteNamas(int id)
        {
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"DELETE FROM namas where id_NAMAS=?id";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlCommand.Parameters.Add("?id", MySqlDbType.Int32).Value = id;
            mySqlConnection.Open();
            mySqlCommand.ExecuteNonQuery();
            mySqlConnection.Close();
        }
    }
}

import 'package:flutter/material.dart';
import '../shared/menu_drawer.dart';

class ContainerScreen extends StatelessWidget {
  const ContainerScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(
        title: Text('Container'),
      ),
      body: Container(
        margin: EdgeInsets.all(50),
        width: 500,
        height: 500,
        decoration: BoxDecoration(
            color: Colors.orange,
            gradient: LinearGradient(colors: [Colors.white, Colors.black]),
            image: DecorationImage(
                image:
                    NetworkImage('https://openclipart.org/image/400px/339886')),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25),
                bottomRight: Radius.circular(25))),
      ),
    );
  }
}

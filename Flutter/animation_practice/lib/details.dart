import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'util.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

class Details extends StatelessWidget {
  final String drinkPath;
  final String drinkTag;
  Details(this.drinkPath, this.drinkTag);

  @override
  Widget build(BuildContext context) {
    pop() {
      Navigator.of(context).pop();
    }

    timeDilation = 10;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return PlatformScaffold(
      appBar: PlatformAppBar(title: Text('Hero Animation')),
      body: Center(
          child: Util.buildHeroRadialDestination(
              drinkPath,
              drinkTag,
              width,
              height,
              pop) //Util.buildHeroDestination(drinkPath, drinkTag, width),
          ),
    );
  }
}

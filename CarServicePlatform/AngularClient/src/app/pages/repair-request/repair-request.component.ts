import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { CategoriesService } from 'src/app/services/categories.service';
import { FileService } from 'src/app/services/file.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { ClientRequest, RegisteredClientRequest } from 'src/app/shared/client-request';
import { RepairService } from 'src/app/shared/repair-services';

@Component({
  selector: 'app-repair-request',
  templateUrl: './repair-request.component.html',
  styleUrls: ['./repair-request.component.css']
})
export class RepairRequestComponent implements OnInit {

  public errorMessage ='';
  public successMessage ='';
  submitted:boolean=false;
  selectedFiles:string[]=[]
  requestForm : FormGroup = new FormGroup({
    city: new FormControl('', [Validators.required]),
    address: new FormControl('', [Validators.required, Validators.minLength(9)]),
    description: new FormControl('', [Validators.required]),
    file: new FormControl(''),
    fileSource: new FormControl('')
  });

  repairServices: RepairService[]=[]
  selectedRepairServices: RepairService[]=[]
  registeredClientRequest!: RegisteredClientRequest;
  constructor(private categoryService: CategoriesService, private repairRequestService: RepairRequestService, private fileService: FileService,
    private router: Router, private route: ActivatedRoute) { 
    this.categoryService.loadRepairServices().subscribe((data:RepairService[])=>{
      this.repairServices=data;
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params =>{
      if(params['login'] !== undefined && params['login'] === 'true'){
        this.successMessage = MessageConstants.LoginSuccess;
      }
    });
  }
  get f(): {[key:string]:AbstractControl} { return this.requestForm.controls; }
  
  onFileChange(event: any){
    this.selectedFiles=[]
    for (var i = 0; i < event.target.files.length; i++) { 
      this.selectedFiles.push(event.target.files[i]);
    }
    console.log(this.selectedFiles);
  }

  onChange(repairService: RepairService, isChecked: any) {

    if (isChecked.checked) {
      this.selectedRepairServices.push(repairService)
    } else {
      const index = this.selectedRepairServices.findIndex(x => x.id === repairService.id);
      delete this.selectedRepairServices[index];
      this.selectedRepairServices.length--;
    }
    console.log(this.selectedRepairServices);
  }

  placeRequest(){
    const requestedServices: { [key: string]: number; } = {};
    this.selectedRepairServices.forEach(x=> requestedServices[x.id.toString()]= x.repairServiceCategory.id);
    console.log(requestedServices)
    const registerRequest: ClientRequest = {
      address: this.f['address'].value,
      city: this.f['city'].value,
      description: this.f['description'].value,
      requestedServiceIds: requestedServices
    };
    const formData = new FormData();
      for(var i=0; i<this.selectedFiles.length; i++){
        formData.append('file[]', this.selectedFiles[i]);
    }

    this.submitted=false;
     if (this.requestForm.invalid) {
       this.submitted = true;
        return;
     }
     this.repairRequestService.placeClientRequest(registerRequest).subscribe({
      next: (data:RegisteredClientRequest) => {
        this.registeredClientRequest=data;
        this.fileService.uploadRepairRequestImages(formData,this.registeredClientRequest.clientRequestId).subscribe({
          next: () => {
            this.router.navigate([AppConstants.AllClientRequest],{state: {data:MessageConstants.PlaceClientRequestSuccess}})
          },
          error: () => {
            this.errorMessage = MessageConstants.PlaceClientRequestFailed
          }})
      },
      error: () => {
        this.errorMessage = MessageConstants.PlaceClientRequestFailed
      }
    })
    this.requestForm.reset;
    this.errorMessage =''
  }
  changeCity(event:number){
    this.f['city'].setValue(event);
    console.log(this.f['city'].value);
  }
  
}
// @Pipe({ name: 'categoryFilter' })
// export class CategoryFilter implements PipeTransform {
//     transform(items: any[], selectedId: number): any {
//         if(selectedId === -1)
//           return items;
//         return items.filter(item => item.id === selectedId);
//     }
// }

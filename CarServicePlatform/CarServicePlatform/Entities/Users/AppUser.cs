﻿using Microsoft.AspNetCore.Identity;

namespace CarServicePlatform.Entities.Users;

public class AppUser : IdentityUser
{
    public DomainUser DomainUser { get; set; }
    public bool IsAccountActivated { get; set; }
}
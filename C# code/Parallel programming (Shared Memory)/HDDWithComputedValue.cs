using System;
using System.Collections.Generic;
using System.Text;

namespace Shared_Memory
{
    class HDDWithComputedValue
    {
        public HDD item { get; set; }
        public double computedData { get; set; }
        public HDDWithComputedValue(HDD _item)
        {
            item = _item;
            computedData = (item.capacity * item.price);
        }
    }
}

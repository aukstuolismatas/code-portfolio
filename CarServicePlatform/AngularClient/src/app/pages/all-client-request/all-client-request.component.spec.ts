import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllClientRequestComponent } from './all-client-request.component';

describe('AllClientRequestComponent', () => {
  let component: AllClientRequestComponent;
  let fixture: ComponentFixture<AllClientRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllClientRequestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllClientRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

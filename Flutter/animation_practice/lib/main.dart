import 'package:animation_practice/details.dart';
import 'package:animation_practice/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Animations',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HeroAnimation());
  }
}

class HeroAnimation extends StatelessWidget {
  const HeroAnimation({super.key});

  @override
  Widget build(BuildContext context) {
    final double maxRadius = MediaQuery.of(context).size.width;
    final double minRadius = 80.0;
    return PlatformScaffold(
      appBar: PlatformAppBar(
        title: Text('Hero animation demo'),
      ),
      body: Container(
        padding: EdgeInsets.all(24),
        alignment: Alignment.topLeft,
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          GestureDetector(
            child: Util.buildHeroIcon(Util.BEANS_URL, ' beans'),
            onTap: () {
              changeRoute(context, 'beans');
            },
          ),
          GestureDetector(
            child: Util.buildHeroIcon(Util.COFFE_URL, ' coffee'),
            onTap: () {
              changeRoute(context, 'coffee');
            },
          ),
          GestureDetector(
            child: Util.buildHeroIcon(Util.COCKTAIL_URL, ' cocktail'),
            onTap: () {
              changeRoute(context, 'cocktail');
            },
          ),
          GestureDetector(
            child: Util.buildHeroRadialICon(
                Util.BEANS_URL, ' beans', minRadius, maxRadius),
            onTap: () {
              changeRoute(context, 'beans');
            },
          ),
          GestureDetector(
            child: Util.buildHeroRadialICon(
                Util.COFFE_URL, ' coffee', minRadius, maxRadius),
            onTap: () {
              changeRoute(context, 'coffee');
            },
          ),
          GestureDetector(
            child: Util.buildHeroRadialICon(
                Util.COCKTAIL_URL, ' cocktail', minRadius, maxRadius),
            onTap: () {
              changeRoute(context, 'cocktail');
            },
          ),
        ]),
      ),
    );
  }

  void changeRoute(BuildContext context, String drink) {
    switch (drink) {
      case 'coffee':
        Navigator.push(
            context,
            platformPageRoute(
                context: context,
                builder: ((context) => Details(Util.COFFE_URL, drink))));
        break;
      case 'beans':
        Navigator.push(
            context,
            platformPageRoute(
                context: context,
                builder: ((context) => Details(Util.BEANS_URL, drink))));
        break;
      case 'cocktail':
        Navigator.push(
            context,
            platformPageRoute(
                context: context,
                builder: ((context) => Details(Util.COCKTAIL_URL, drink))));
        break;
    }
  }
}

import { ApplicationRef, ComponentFactoryResolver, ComponentRef, EmbeddedViewRef, Injectable, Injector, Type } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService<T> {
  private componentRef: ComponentRef<T> | undefined;
  confirmation: boolean=false;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector
  ) {}

  async open(component: Type<T>): Promise<void> {
    if (this.componentRef) {
      return;
    }

    this.componentRef = this.componentFactoryResolver
      .resolveComponentFactory<T>(component)
      .create(this.injector);
    this.componentRef.instance
    this.appRef.attachView(this.componentRef.hostView);

    const domElem = (this.componentRef.hostView as 
                     EmbeddedViewRef<any>)
                     .rootNodes[0] as HTMLElement;
    document.body.appendChild(domElem);
  }

  async close(): Promise<boolean> {
    if (!this.componentRef) {
      return false;
    }

    this.appRef.detachView(this.componentRef.hostView);
    this.componentRef.destroy();
    console.log(this.confirmation)
    this.componentRef = undefined;
    this.confirmation = false;
    return false;
  }
  async confirm(): Promise<boolean>{
    if (!this.componentRef) {
      return false;
    }

    this.appRef.detachView(this.componentRef.hostView);
    this.componentRef.destroy();
    console.log(this.confirmation)
    this.componentRef = undefined;
    this.confirmation=true;
    console.log(this.confirmation)
    return true;
  }
}

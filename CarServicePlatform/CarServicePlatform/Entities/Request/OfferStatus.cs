﻿namespace CarServicePlatform.Entities.Request;

public enum OfferStatus
{
    Waiting,
    Rejected,
    Approved
}
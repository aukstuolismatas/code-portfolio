import 'package:flutter/material.dart';
import 'package:flutter_application_1/shared/menu_drawer.dart';
import '../shared/menu_bottom.dart';

class IntroScreen extends StatelessWidget {
  const IntroScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Global fitness')),
        drawer: MenuDrawer(),
        bottomNavigationBar: MenuBottom(),
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
              image: AssetImage('assets/mountain.jpg'),
              fit: BoxFit.cover,
            )),
            child: Center(
                child: Container(
                    padding: EdgeInsets.all(24),
                    decoration: BoxDecoration(
                        color: Colors.white70,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: Text(
                      'Commit to be fit. City is beautiful. \nDaniele is studing. Matas is working!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 22,
                        shadows: [
                          Shadow(
                              offset: Offset(1.0, 1.0),
                              blurRadius: 2.0,
                              color: Colors.grey)
                        ],
                      ),
                    )))));
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterTrawlComponent } from './register-trawl.component';

describe('RegisterTrawlComponent', () => {
  let component: RegisterTrawlComponent;
  let fixture: ComponentFixture<RegisterTrawlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterTrawlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterTrawlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

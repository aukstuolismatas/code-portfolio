﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.Auth;

public record RegisterClientUserDto([Required] string Name, [Required] string Surname, [Required][EmailAddress] string Email, [Required] string Password, [Required][Phone] string PhoneNumber);
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using NT_Csharp.Models;

namespace NT_Csharp.ViewModels
{
    public class NamasEditViewModel
    {
        [DisplayName("Rekuperacija")]
        public bool rekuperacija { get; set; }
        [DisplayName("Apzeldinimas")]
        public bool apzeldinimas { get; set; }
        [DisplayName("Kiemas")]
        public bool kiemas { get; set; }
        [DisplayName("Namo tipas")]
        public int namoTipas { get; set; }
        [DisplayName("ID")]
        public int id { get; set; }

        public IList<SelectListItem> TipaiList { get; set; }
    }
}
﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.Auth;

public record LoginDto([Required] string Email, [Required] string Password);
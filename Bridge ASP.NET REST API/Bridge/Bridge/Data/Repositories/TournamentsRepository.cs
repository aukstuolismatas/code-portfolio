﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bridge.Data.Repositories
{
    public class TournamentsRepository : ITournamentsRepository
    {
        private readonly BridgeRestContext _bridgeRestContext;

        public TournamentsRepository(BridgeRestContext bridgeRestContext)
        {
            _bridgeRestContext = bridgeRestContext;
        }

        public async Task<List<Tournament>> GetAllAsync()
        {
            return await _bridgeRestContext.Tournaments.ToListAsync().ConfigureAwait(false);
        }

        public async Task<Tournament> GetAsync(int id)
        {
            return await _bridgeRestContext.Tournaments.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
        }

        public async Task InsertAsync(Tournament tournament)
        {
            _bridgeRestContext.Tournaments.Add(tournament);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task UpdateAsync(Tournament tournament)
        {
            _bridgeRestContext.Tournaments.Update(tournament);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAsync(Tournament tournament)
        {
            _bridgeRestContext.Tournaments.Remove(tournament);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
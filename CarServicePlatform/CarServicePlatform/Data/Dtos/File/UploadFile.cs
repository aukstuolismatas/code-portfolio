﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.File;

public record UploadFile([Required] IFormFile File);
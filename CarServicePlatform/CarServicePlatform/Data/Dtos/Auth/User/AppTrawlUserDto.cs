﻿using CarServicePlatform.Data.Dtos.Account;

namespace CarServicePlatform.Data.Dtos.Auth.User;

public record AppTrawlUserDto(string Id, string UserName, string Email, string PhoneNumber, bool IsAccountActivated, TrawlAccountDto DomainUser);
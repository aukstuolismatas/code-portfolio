﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;

namespace Bridge.Data.Repositories
{
    public interface ITournamentsRepository
    {
        Task<List<Tournament>> GetAllAsync();
        Task<Tournament> GetAsync(int id);
        Task InsertAsync(Tournament tournament);
        Task UpdateAsync(Tournament tournament);
        Task DeleteAsync(Tournament tournament);
    }
}
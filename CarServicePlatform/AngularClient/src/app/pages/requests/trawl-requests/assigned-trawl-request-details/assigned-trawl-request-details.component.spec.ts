import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedTrawlRequestDetailsComponent } from './assigned-trawl-request-details.component';

describe('AssignedTrawlRequestDetailsComponent', () => {
  let component: AssignedTrawlRequestDetailsComponent;
  let fixture: ComponentFixture<AssignedTrawlRequestDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignedTrawlRequestDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedTrawlRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

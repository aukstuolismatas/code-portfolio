﻿namespace CarServicePlatform.Data.Dtos.AvailableRepairService;

public record AvailableRepairServiceDto(int Id, int RepairServiceId, string DomainUserId, string City);

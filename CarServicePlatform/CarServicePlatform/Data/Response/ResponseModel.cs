﻿namespace CarServicePlatform.Data.Response;

public class ResponseModel
{
    public string? Message { get; set; }
    public bool IsSuccess { get; set; }
}
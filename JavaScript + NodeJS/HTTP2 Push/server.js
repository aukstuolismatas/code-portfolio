const http2 = require('http2');
const fs = require('fs');
const mime = require('mime');
const PORT=8080;


function sendF(stream, fileName){
    const fd=fs.openSync(fileName, "r");
    const stat=fs.fstatSync(fd);
    const headers = {
        "content-length": stat.size,
        "last-modified": stat.mtime.toUTCString(),
        "content-type": mime.getType(fileName)
      };
    stream.respondWithFD(fd, headers);
    stream.on("close", () =>{
        console.log("closing file", fileName);
        fs.closeSync(fd);
    });
    stream.end();
}
function pushF(stream, filePath, fileName){
    stream.pushStream({":path":filePath},(err,pushStream)=>{
        if(err){
            throw err;
        }
        sendF(pushStream, fileName);
    });
}

function pushAll(req, res){
    console.log(req.url);
    if(req.url==="/"){
        const files = fs.readdirSync(__dirname + "/data");
        for(let i=0; i<files.length; i++){
            const fileName = __dirname + "/data/" + files[i];
            const path = "/data/" + files[i];
            pushF(res.stream, path, fileName);
        }
        sendF(res.stream, "index.html");
        
    }
}
http2.createSecureServer({key: fs.readFileSync(__dirname+'/secret/privkey.pem'),cert: fs.readFileSync(__dirname+'/secret/cert.pem')}, pushAll).listen(PORT, ()=>{
    console.log("Start server on "+PORT);
})

﻿using System.Security.Claims;
using AutoMapper;
using CarServicePlatform.Auth;
using CarServicePlatform.Auth.Model;
using CarServicePlatform.Data.Dtos.ClientRequest;
using CarServicePlatform.Data.Mappings;
using CarServicePlatform.Entities.RepairServices;
using CarServicePlatform.Entities.Request;
using CarServicePlatform.Entities.Users;
using CarServicePlatform.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarServicePlatform.Controllers;

[ApiController]
[Route("/requests")]
[Authorize]
public class ClientRequestController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IClientRequestRepository _clientRequestRepository;
    private readonly ITrawlRepository _trawlRepository;
    private readonly IServiceRepository _serviceRepository;
    private readonly IRepairServiceRepository _repairServiceRepository;
    private readonly IRequestedRepairServiceRepository _requestedRepairServiceRepository;
    private readonly IPriceOfferRepository _priceOfferRepository;
    private readonly ITransferOfferRepository _transferOfferRepository;
    private readonly IAuthorizationService _authorizationService;
    private readonly UserManager<AppUser> _userManager;

    public ClientRequestController(IMapper mapper, IClientRequestRepository clientRequestRepository, IAuthorizationService authorizationHandler, IRepairServiceRepository repairServiceRepository, IRequestedRepairServiceRepository requestedRepairServiceRepository, IPriceOfferRepository priceOfferRepository, UserManager<AppUser> userManager, ITrawlRepository trawlRepository, IServiceRepository serviceRepository, ITransferOfferRepository transferOfferRepository)
    {
        _mapper = mapper;
        _clientRequestRepository = clientRequestRepository;
        _authorizationService = authorizationHandler;
        _repairServiceRepository = repairServiceRepository;
        _requestedRepairServiceRepository = requestedRepairServiceRepository;
        _priceOfferRepository = priceOfferRepository;
        _userManager = userManager;
        _trawlRepository = trawlRepository;
        _serviceRepository = serviceRepository;
        _transferOfferRepository = transferOfferRepository;
    }

    [HttpGet]
    [Authorize(Roles = AppUserRoles.Client)]
    public async Task<IEnumerable<ClientRequestDto>> GetAllRequests()
    {
        List<ClientRequest> requests = await _clientRequestRepository.GetAllRequestsAsync().ConfigureAwait(false);

        if (!User.IsInRole(AppUserRoles.Admin))
        {
            string userId = User.FindFirst(CustomClaims.UserId)!.Value;
            requests = requests.Where(x => x.DomainUserId == userId).ToList();
        }

        foreach (ClientRequest request in requests)
        {
            request.RequestedServices = await _requestedRepairServiceRepository
                                              .GetAllRequestedRepairServiceToClientRequestAsync(request.ClientRequestId)
                                              .ConfigureAwait(false);
        }

        return requests.Select(x => _mapper.Map<ClientRequestDto>(x));
    }

    [HttpGet("{requestId}")]
    public async Task<ActionResult<ClientRequestDto>> GetRequest(int requestId)
    {
        ClientRequest? request = await _clientRequestRepository.GetRequestAsync(requestId).ConfigureAwait(false);
        if (request == null)
        {
            return NotFound($"Request with request id {requestId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, request, PolicyNames.SameUser).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        request.RequestedServices = await _requestedRepairServiceRepository
                                          .GetAllRequestedRepairServiceToClientRequestAsync(request.ClientRequestId)
                                          .ConfigureAwait(false);
        return Ok(_mapper.Map<ClientRequestDto>(request));
    }

    [HttpGet("{requestId}/trawl/{trawlUserId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<ClientRequestForTrawlDto>> GetRequestForTrawl(int requestId, string trawlUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, trawlUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        ClientRequest? request = await _clientRequestRepository.GetRequestForTrawlAsync(requestId).ConfigureAwait(false);
        if (request == null)
        {
            return NotFound($"Request with request id {requestId} for trawl was not found");
        }

        TrawlAccount? trawlAccount = await _trawlRepository.GetTrawlAccountAsync(trawlUserId).ConfigureAwait(false);
        if (request.Status != RequestStatus.WaitingTransfer && trawlAccount?.City != request.City)
        {
            Forbid();
        }

        return Ok(_mapper.Map<ClientRequestForTrawlDto>(request));
    }

    [HttpGet("{requestId}/trawl/assigned/{trawlUserId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<AssignedClientRequestToTrawlDto>> GetAssignedRequestForTrawl(int requestId, string trawlUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, trawlUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        TransferOffer? transferOffer = await _transferOfferRepository.GetTransferOfferByTrawlIdAndRequestIdAsync(trawlUserId, requestId).ConfigureAwait(false);
        if (transferOffer == null)
        {
            return NotFound($"Transfer offer for trawl {trawlUserId} was not found");
        }

        if (transferOffer.Status != OfferStatus.Approved)
        {
            return Forbid();
        }

        ClientRequest? request = await _clientRequestRepository.GetRequestForTrawlAsync(requestId).ConfigureAwait(false);
        if (request == null)
        {
            return NotFound($"Request with request id {requestId} for trawl was not found");
        }

        AppUser clientUser = await _userManager.FindByIdAsync(request.DomainUserId).ConfigureAwait(false);
        AppUser serviceUser = await _userManager.FindByIdAsync(request.ServiceAccountId).ConfigureAwait(false);
        request.ServicePhone = serviceUser.PhoneNumber;
        request.ClientPhone = clientUser.PhoneNumber;
        request.Price = transferOffer.Price;
        request.ArrivalTime = transferOffer.DateAndTime;
        request.OfferStatus = transferOffer.Status;

        return Ok(_mapper.Map<AssignedClientRequestToTrawlDto>(request));
    }

    [HttpGet("{requestId}/service/assigned/{serviceUserId}")]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<ApprovedClientRequestDto>> GetAssignedRequest(int requestId, string serviceUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, serviceUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        PriceOffer? priceOffer = await _priceOfferRepository.GetPriceOfferByServiceIdAndRequestIdAsync(serviceUserId, requestId).ConfigureAwait(false);
        if (priceOffer == null)
        {
            return NotFound($"Price offer for service {serviceUserId} was not found");
        }

        if (priceOffer.Status != OfferStatus.Approved)
        {
            return Forbid();
        }

        ClientRequest? request = await _clientRequestRepository.GetRequestAsync(requestId).ConfigureAwait(false);
        if (request == null)
        {
            return NotFound($"Request with request id {requestId} was not found");
        }

        request.Price = priceOffer.Price;
        request.Duration = priceOffer.Duration;
        return Ok(_mapper.Map<ApprovedClientRequestDto>(request));
    }

    [HttpGet("service/offering/{serviceUserId}")]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<IEnumerable<ClientRequestDto>>> GetAllClientRequestWaitingForOfferForService(string serviceUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, serviceUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<ClientRequest> serviceClientRequests = await _clientRequestRepository.GetAllRequestsAvailableForService(serviceUserId).ConfigureAwait(false);
        foreach (ClientRequest request in serviceClientRequests)
        {
            request.RequestedServices = await _requestedRepairServiceRepository
                                              .GetAllRequestedRepairServiceToClientRequestAsync(request.ClientRequestId)
                                              .ConfigureAwait(false);
        }

        return Ok(serviceClientRequests.Select(x => _mapper.Map<ClientRequestDto>(x)));
    }

    [HttpGet("trawl/offering/{trawlUserId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<IEnumerable<ClientRequestForTrawlDto>>> GetAllClientRequestWaitingForTrawlOffer(string trawlUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, trawlUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<ClientRequest> trawlClientRequests = await _clientRequestRepository.GetAllRequestAvailableForTrawl(trawlUserId).ConfigureAwait(false);

        return Ok(trawlClientRequests.Select(x => _mapper.Map<ClientRequestForTrawlDto>(x)));
    }

    [HttpGet("trawl/waiting/{trawlUserId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<IEnumerable<ClientRequestForTrawlDto>>> GetAllClientRequestWaitingOfApprovalForTrawl(string trawlUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, trawlUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<ClientRequest> serviceClientRequests = await _clientRequestRepository.GetAllRequestsWaitingApprovalForTrawl(trawlUserId).ConfigureAwait(false);

        return Ok(serviceClientRequests.Select(x => _mapper.Map<ClientRequestForTrawlDto>(x)));
    }

    [HttpGet("service/waiting/{serviceUserId}")]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<IEnumerable<ClientRequestDto>>> GetAllClientRequestWaitingForApprovalForService(string serviceUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, serviceUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<ClientRequest> serviceClientRequests = await _clientRequestRepository.GetAllRequestsWaitingApprovalForService(serviceUserId).ConfigureAwait(false);

        return Ok(serviceClientRequests.Select(x => _mapper.Map<ClientRequestDto>(x)));
    }

    [HttpGet("trawl/assigned/{trawlUserId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<IEnumerable<AssignedClientRequestToTrawlDto>>> GetAllClientRequestAssignedForTrawl(string trawlUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, trawlUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<ClientRequest> trawlClientRequests = await _clientRequestRepository.GetAllRequestsAssignedForTrawl(trawlUserId).ConfigureAwait(false);

        return Ok(trawlClientRequests.Select(x => _mapper.Map<AssignedClientRequestToTrawlDto>(x)));
    }

    [HttpGet("service/assigned/{serviceUserId}")]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<IEnumerable<ApprovedClientRequestDto>>> GetAllClientRequestAssignedForService(string serviceUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, serviceUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<ClientRequest> serviceClientRequests = await _clientRequestRepository.GetAllRequestsAssignedForService(serviceUserId).ConfigureAwait(false);

        return Ok(serviceClientRequests.Select(x => _mapper.Map<ApprovedClientRequestDto>(x)));
    }

    [HttpGet("service/rejected/{serviceUserId}")]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<IEnumerable<RejectedClientRequestDto>>> GetAllClientRequestRejectedForService(string serviceUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, serviceUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<ClientRequest> serviceClientRequests = await _clientRequestRepository.GetAllRejectedClientRequestsForService(serviceUserId).ConfigureAwait(false);

        return Ok(serviceClientRequests.Select(x => _mapper.Map<RejectedClientRequestDto>(x)));
    }

    [HttpGet("trawl/rejected/{trawlUserId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<IEnumerable<RejectedTrawlClientRequestDto>>> GetAllClientRequestRejectedForTrawl(string trawlUserId)
    {
        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, trawlUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<ClientRequest> trawlClientRequests = await _clientRequestRepository.GetAllRejectedClientRequestsForTrawl(trawlUserId).ConfigureAwait(false);

        return Ok(trawlClientRequests.Select(x => _mapper.Map<RejectedTrawlClientRequestDto>(x)));
    }

    [HttpPost]
    [Authorize(Roles = AppUserRoles.Client)]
    public async Task<ActionResult<ClientRequestDto>> InsertRequest(CreateClientRequestDto createClientRequestDto)
    {
        ClientRequest clientRequest = _mapper.Map<ClientRequest>(createClientRequestDto);
        clientRequest.Title = $"{clientRequest.Address}, {clientRequest.City}";
        clientRequest.DomainUserId = User.FindFirst(CustomClaims.UserId)!.Value;
        await _clientRequestRepository.InsertRequestAsync(clientRequest).ConfigureAwait(false);
        List<RequestedRepairService> requestedRepairServices = await ValidateRequestedRepairService(createClientRequestDto.RequestedServiceIds, clientRequest.ClientRequestId).ConfigureAwait(false);
        //TODO possible dummy request creation (Rethink this logic)
        if (!requestedRepairServices.Any())
        {
            return NotFound("Such repair services does not exist");
        }

        await _requestedRepairServiceRepository.InsertRequestedRepairServiceAsync(requestedRepairServices).ConfigureAwait(false);
        //TODO REMOVE LINE BELOW
        clientRequest.RequestedServices = requestedRepairServices;
        return Created($"/requests/{clientRequest.ClientRequestId}", _mapper.Map<ClientRequestDto>(clientRequest));
    }

    private async Task<List<RequestedRepairService>> ValidateRequestedRepairService(Dictionary<int, int> requestedRepairServiceIds, int requestId)
    {
        List<RequestedRepairService> assignedRepairServiceList = new();
        foreach (KeyValuePair<int, int> id in requestedRepairServiceIds)
        {
            RepairService? repairService = await _repairServiceRepository.GetServiceAsync(id.Value, id.Key).ConfigureAwait(false);
            if (repairService != null)
            {
                RequestedRepairService service = repairService.ToRequestedRepairService(requestId);
                assignedRepairServiceList.Add(service);
            }
        }

        return assignedRepairServiceList;
    }

    [HttpPut("{requestId}")]
    [Authorize(Roles = AppUserRoles.Client)]
    public async Task<ActionResult<ClientRequestDto>> UpdateRequest(int requestId, UpdateClientRequestDto updateClientRequestDto)
    {
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(requestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return NotFound($"Request with request id {requestId} was not found");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, clientRequest, PolicyNames.SameUser).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        _mapper.Map(updateClientRequestDto, clientRequest);
        await _clientRequestRepository.UpdateRequestAsync(clientRequest).ConfigureAwait(false);
        return Ok(_mapper.Map<ClientRequestDto>(clientRequest));
    }

    [HttpPut("start/{requestId}")]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<ApprovedClientRequestDto>> StartProcessOfClientRequest(int requestId)
    {
        PriceOffer? validatedPriceOffer = await ValidatePriceOffer(requestId).ConfigureAwait(false);
        if (validatedPriceOffer == null)
        {
            return Forbid();
        }

        ClientRequest? clientRequest = await ChangeClientRequestStatus(requestId, RequestStatus.InProgress, RequestStatus.Arrived).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return Forbid();
        }

        clientRequest.Duration = validatedPriceOffer.Duration;
        clientRequest.Price = validatedPriceOffer.Price;
        return Ok(_mapper.Map<ApprovedClientRequestDto>(clientRequest));
    }

    [HttpPut("done/{requestId}")]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<ApprovedClientRequestDto>> DoneClientRequest(int requestId)
    {
        PriceOffer? validatedPriceOffer = await ValidatePriceOffer(requestId).ConfigureAwait(false);
        if (validatedPriceOffer == null)
        {
            return Forbid();
        }

        ClientRequest? clientRequest = await ChangeClientRequestStatus(requestId, RequestStatus.Done, RequestStatus.InProgress).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return Forbid();
        }

        clientRequest.Duration = validatedPriceOffer.Duration;
        clientRequest.Price = validatedPriceOffer.Price;
        return Ok(_mapper.Map<ApprovedClientRequestDto>(clientRequest));
    }

    [HttpPut("transferring/{requestId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<AssignedClientRequestToTrawlDto>> TransferringClientRequest(int requestId)
    {
        TransferOffer? validateTransferOffer = await ValidateTransferOffer(requestId).ConfigureAwait(false);
        if (validateTransferOffer == null)
        {
            return Forbid();
        }

        ClientRequest? clientRequest = await ChangeClientRequestStatus(requestId, RequestStatus.Transferring, RequestStatus.Accepted).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return Forbid();
        }

        clientRequest.ArrivalTime = validateTransferOffer.DateAndTime;
        clientRequest.Price = validateTransferOffer.Price;
        return Ok(_mapper.Map<AssignedClientRequestToTrawlDto>(clientRequest));
    }

    [HttpPut("arrived/{requestId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<AssignedClientRequestToTrawlDto>> ArrivedClientRequest(int requestId)
    {
        TransferOffer? validateTransferOffer = await ValidateTransferOffer(requestId).ConfigureAwait(false);
        if (validateTransferOffer == null)
        {
            return Forbid();
        }

        ClientRequest? clientRequest = await ChangeClientRequestStatus(requestId, RequestStatus.Arrived, RequestStatus.Transferring).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return Forbid();
        }

        clientRequest.ArrivalTime = validateTransferOffer.DateAndTime;
        clientRequest.Price = validateTransferOffer.Price;
        return Ok(_mapper.Map<AssignedClientRequestToTrawlDto>(clientRequest));
    }

    [HttpPut("delivered/{requestId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<AssignedClientRequestToTrawlDto>> DeliveredClientRequest(int requestId)
    {
        TransferOffer? validateTransferOffer = await ValidateTransferOffer(requestId).ConfigureAwait(false);
        if (validateTransferOffer == null)
        {
            return Forbid();
        }

        ClientRequest? clientRequest = await ChangeClientRequestStatus(requestId, RequestStatus.Delivered, RequestStatus.Done).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return Forbid();
        }

        clientRequest.ArrivalTime = validateTransferOffer.DateAndTime;
        clientRequest.Price = validateTransferOffer.Price;
        return Ok(_mapper.Map<AssignedClientRequestToTrawlDto>(clientRequest));
    }

    private async Task<PriceOffer?> ValidatePriceOffer(int requestId)
    {
        string userId = User.FindFirstValue(CustomClaims.UserId);
        AppUser serviceOrTrawlUser = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (!serviceOrTrawlUser.IsAccountActivated)
        {
            return null;
        }

        PriceOffer? priceOffer = await _priceOfferRepository.GetPriceOfferByServiceIdAndRequestIdAsync(userId, requestId).ConfigureAwait(false);
        if (priceOffer == null)
        {
            return null;
        }

        if (priceOffer.Status != OfferStatus.Approved)
        {
            return null;
        }

        return priceOffer;
    }

    private async Task<TransferOffer?> ValidateTransferOffer(int requestId)
    {
        string userId = User.FindFirstValue(CustomClaims.UserId);
        AppUser serviceOrTrawlUser = await _userManager.FindByIdAsync(userId).ConfigureAwait(false);
        if (!serviceOrTrawlUser.IsAccountActivated)
        {
            return null;
        }

        TransferOffer? transferOffer = await _transferOfferRepository.GetTransferOfferByTrawlIdAndRequestIdAsync(userId, requestId).ConfigureAwait(false);
        if (transferOffer == null)
        {
            return null;
        }

        if (transferOffer.Status != OfferStatus.Approved)
        {
            return null;
        }

        return transferOffer;
    }

    private async Task<ClientRequest?> ChangeClientRequestStatus(int requestId, RequestStatus status, RequestStatus statusBefore)
    {
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(requestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return null;
        }

        if (clientRequest.Status != statusBefore)
        {
            return null;
        }

        clientRequest.Status = status;

        await _clientRequestRepository.UpdateRequestAsync(clientRequest).ConfigureAwait(false);
        return clientRequest;
    }

    [HttpDelete("{requestId}")]
    [Authorize(Roles = AppUserRoles.Admin)]
    public async Task<ActionResult<ClientRequestDto>> DeleteRequest(int requestId)
    {
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(requestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return NotFound($"Request with request id {requestId} was not found");
        }

        List<RequestedRepairService> requestedRepairService = await _requestedRepairServiceRepository.GetAllRequestedRepairServiceToClientRequestAsync(requestId).ConfigureAwait(false);
        if (!requestedRepairService.Any())
        {
            return NotFound($"Requested service with request id {requestId} was not found");
        }

        await _clientRequestRepository.DeleteRequestAsync(clientRequest).ConfigureAwait(false);
        await _requestedRepairServiceRepository.DeleteRequestedRepairServiceAsync(requestedRepairService).ConfigureAwait(false);
        return NoContent();
    }
}
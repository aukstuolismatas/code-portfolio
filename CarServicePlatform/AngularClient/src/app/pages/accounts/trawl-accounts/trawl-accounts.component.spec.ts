import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrawlAccountsComponent } from './trawl-accounts.component';

describe('TrawlAccountsComponent', () => {
  let component: TrawlAccountsComponent;
  let fixture: ComponentFixture<TrawlAccountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrawlAccountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrawlAccountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

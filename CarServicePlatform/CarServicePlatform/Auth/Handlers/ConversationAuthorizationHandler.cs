﻿using System.Security.Claims;
using CarServicePlatform.Auth.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;

namespace CarServicePlatform.Auth.Handlers;

public class ConversationAuthorizationHandler : AuthorizationHandler<OperationAuthorizationRequirement, string>
{
    protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, OperationAuthorizationRequirement requirement, string userId)
    {
        if (context.User.FindFirstValue(CustomClaims.UserId) == userId)
        {
            context.Succeed(requirement);
        }

        return Task.CompletedTask;
    }
}

public record ChatConversationRequirement : IAuthorizationRequirement;
import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/app-constants';

@Component({
  selector: 'app-registration-category',
  templateUrl: './registration-category.component.html',
  styleUrls: ['./registration-category.component.css']
})
export class RegistrationCategoryComponent implements OnInit {

  trawlRegistration = '/'+AppConstants.RegisterTrawl;
  clientRegistration = '/'+AppConstants.RegisterClient;
  serviceRegistration = '/'+AppConstants.RegisterService;

  constructor() { }

  ngOnInit(): void {
  }

}

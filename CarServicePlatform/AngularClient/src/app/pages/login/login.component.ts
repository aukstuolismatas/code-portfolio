import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants, RegexPatterns } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { LoginRequest } from 'src/app/shared/login-data';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public submitted  = false;
  public emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  loginForm : FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });
  public errorMessage = '';
  public successMessage = '';
  constructor(private authService: AuthService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute) {
       this.loginForm.valueChanges.subscribe(console.log);
  
     }
    
  ngOnInit(): void {
    this.route.queryParams.subscribe(params =>{
      if(params['registered'] !== undefined && params['registered'] === 'true'){
        this.successMessage = MessageConstants.RegisterSuccess;
      }
    })
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required ,Validators.email, Validators.pattern(RegexPatterns.Email)]],
      password: ['', [Validators.required, Validators.minLength(6),Validators.maxLength(40), Validators.pattern(RegexPatterns.Passowrd)]]
    });
  }
  get f(): {[key:string]:AbstractControl} { return this.loginForm.controls; }

  login(){
    const loginRequest: LoginRequest = {
      email: this.f['email'].value,
      password: this.f['password'].value
    };
    this.submitted = false;
     if (this.loginForm.invalid) {
        this.submitted = true;
        return;
     }
    this.authService.login(loginRequest).subscribe({
      next: (user) => {
        if(this.authService.isClient()){
          this.router.navigate([AppConstants.RepairRequest], {queryParams: { login: 'true' } })
        } else if(this.authService.isAdmin()){
          this.router.navigate([AppConstants.AccountsService], {queryParams: { login: 'true' } })
        } else if(this.authService.isService()){
          this.router.navigate([AppConstants.AllWaitingClientRequestForServiceOffer], {queryParams: { login: 'true' } })
        } else if(this.authService.isTrawl()){
          this.router.navigate([AppConstants.AllWaitingTrawlRequestForOffer], {queryParams: { login: 'true' } })
        }
      },
      error: (err) => this.errorMessage = MessageConstants.LoginError
    })
    this.loginForm.reset;
    this.errorMessage =''
  }
}
// <div *ngIf="mobNumber.errors && userForm.submitted && !isValidFormSubmitted" [ngClass] = "'error'">  
//       <div *ngIf="mobNumber.errors.pattern">  
//         Mobile number not valid.  
//       </div>   
// account_validation_messages = {
//   'username': [
//     { type: 'required', message: 'Username is required' },
//     { type: 'minlength', message: 'Username must be at least 5 characters long' },
//     { type: 'maxlength', message: 'Username cannot be more than 25 characters long' },
//     { type: 'pattern', message: 'Your username must contain only numbers and letters' },
//     { type: 'validUsername', message: 'Your username has already been taken' }
//   ],
//   'email': [
//     { type: 'required', message: 'Email is required' },
//     { type: 'pattern', message: 'Enter a valid email' }
//   ],
//   'confirm_password': [
//     { type: 'required', message: 'Confirm password is required' },
//     { type: 'areEqual', message: 'Password mismatch' }
//   ],
//   'password': [
//     { type: 'required', message: 'Password is required' },
//     { type: 'minlength', message: 'Password must be at least 5 characters long' },
//     { type: 'pattern', message: 'Your password must contain at least one uppercase, one lowercase, and one number' }
//   ],
//   'terms': [
//     { type: 'pattern', message: 'You must accept terms and conditions' }
//   ]
//   }
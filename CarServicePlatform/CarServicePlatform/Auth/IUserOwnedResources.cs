﻿namespace CarServicePlatform.Auth;

public interface IUserOwnedResources
{
    string DomainUserId { get; }
}
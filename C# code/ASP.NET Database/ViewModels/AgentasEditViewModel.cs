using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace NT_Csharp.ViewModels
{
    public class AgentasEditViewModel
    {
        [DisplayName("ID")]
        public int id { get; set; }
        [DisplayName("Vardas")]
        public string vardas { get; set; }
        [DisplayName("Pavardė")]
        public string pavarde { get; set; }
        [DisplayName("Telefonas")]
        public string telefonas { get; set; }
        [DisplayName("Pardavimų skaičius")]
        public int pardavimai { get; set; }
        [DisplayName("Agentura")]
        [Required]
        public int fk_agentura { get; set; }
        public IList<SelectListItem> AgenturosList { get; set; }
    }
}
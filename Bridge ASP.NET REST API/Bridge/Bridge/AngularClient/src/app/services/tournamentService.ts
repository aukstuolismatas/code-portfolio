﻿import { Injectable, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Tournament, UpdateTournament } from "../shared/Tournament";
import { Table, UpdateTable, CreateTable } from "../shared/Tables";
import { Board } from "../shared/Board";

@Injectable()
export class TournamentService {
    constructor(private http: HttpClient) {

    }


    public tournaments: Tournament[] = [];
    public boards: Board[] = [];
    public tables: Table[] = [];
    private url: string = "https://bridge-tournament.azurewebsites.net/api/";
    public token = "";
    public expiration = new Date();

    loadTournaments(): Observable<void> {
        return this.http.get<[]>(this.url + "tournaments")
            .pipe(map(data => {
                this.tournaments = data;
                return;
            }));
    }

    createTournaments(tournament: Tournament) {
        const headers = new HttpHeaders().set("Authorization", `Bearer ${this.token}`);
        return this.http.post(this.url + "tournaments",
            tournament,
            {
                headers: headers
            });
    }
    deleteTournament(tournamentId: number) {
        const headers = new HttpHeaders().set("Authorization", `Bearer ${this.token}`);
        return this.http.delete(this.url + "tournaments/" + tournamentId, {headers: headers});
    }

    updateTournament(tournamentId: number, updatedTournamnet: UpdateTournament) {
        const headers = new HttpHeaders().set("Authorization", `Bearer ${this.token}`);
        return this.http.put(this.url + 'tournaments/' + tournamentId, updatedTournamnet, { headers: headers });
    }

    loadBoards(): Observable<void> {
        return this.http.get<[]>(this.url + "boards")
            .pipe(map(data => {
                this.boards = data;
                return;
            }));
    }
    loadTables(tournamentId: number): Observable<void> {
        return this.http.get<[]>(this.url + "tournaments/" +tournamentId+ "/tables")
            .pipe(map(data => {
                this.tables = data;
                return;
            }));
    }

    createTable(table: CreateTable, tournamentId: number) {
        const headers = new HttpHeaders().set("Authorization", `Bearer ${this.token}`);
        return this.http.post(this.url + "tournaments/" + tournamentId + "/tables",
            table,
            {
                headers: headers
            });
    }

    deleteTable(tournamentId: number, tableId: number) {
        const headers = new HttpHeaders().set("Authorization", `Bearer ${this.token}`);
        return this.http.delete(this.url + "tournaments/" + tournamentId + '/tables/'+ tableId, { headers: headers });
    }

    updateTable(tournamentId: number, tableId: number, updatedTable: UpdateTable) {
        const headers = new HttpHeaders().set("Authorization", `Bearer ${this.token}`);
        return this.http.put(this.url + 'tournaments/' + tournamentId + '/tables/' + tableId, updatedTable, { headers: headers });
    }
}
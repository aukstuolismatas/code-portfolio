import 'package:flutter/material.dart';
import 'package:layout_practice/screens/calculator_screen.dart';
import 'package:layout_practice/screens/container_screen.dart';
import 'package:layout_practice/screens/main_screen.dart';
import 'package:layout_practice/screens/stack_screen.dart';
import 'package:layout_practice/screens/timer_screen.dart';

class MenuDrawer extends StatelessWidget {
  const MenuDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(children: buildMenuItems(context)),
    );
  }

  List<Widget> buildMenuItems(BuildContext context) {
    final List<String> menuTitles = [
      'Main',
      'Container',
      'Calculator',
      'Timer',
      'Stack'
    ];
    List<Widget> menuItems = [];
    menuItems.add(DrawerHeader(
      child: Text(
        'Select screen',
        style: TextStyle(color: Colors.black, fontSize: 24),
      ),
      decoration: BoxDecoration(
          image: DecorationImage(
              image: NetworkImage(
                  'https://images.unsplash.com/photo-1581812182087-3b2aad1b9d9f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80'),
              fit: BoxFit.cover)),
    ));

    menuTitles.forEach((element) {
      Widget screen = Container();
      menuItems.add(ListTile(
        title: Text(
          element,
          style: TextStyle(fontSize: 18),
        ),
        trailing: Icon(Icons.menu_book),
        onTap: () {
          switch (element) {
            case 'Main':
              screen = MainScreen();
              break;
            case 'Container':
              screen = ContainerScreen();
              break;
            case 'Calculator':
              screen = CalculatorScreen();
              break;
            case 'Timer':
              screen = TimerScreen();
              break;
            case 'Stack':
              screen = StackScreen();
              break;
          }
          Navigator.of(context).pop();
          Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => screen,
          ));
        },
      ));
    });
    return menuItems;
  }
}

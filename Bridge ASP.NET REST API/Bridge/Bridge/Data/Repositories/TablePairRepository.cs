﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bridge.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bridge.Data.Repositories
{
    public class TablePairRepository : ITablePairRepository
    {
        private readonly BridgeRestContext _bridgeRestContext;

        public TablePairRepository(BridgeRestContext bridgeRestContext)
        {
            _bridgeRestContext = bridgeRestContext;
        }

        public async Task<TablePair> GetAsync(int tableId, int tablePairId)
        {
            return await _bridgeRestContext.TablePairs.FirstOrDefaultAsync(x => x.TableId == tableId && x.Id == tablePairId).ConfigureAwait(false);
        }

        public async Task<List<TablePair>> GetAllAsync(int tableId)
        {
            return await _bridgeRestContext.TablePairs.Where(x => x.TableId == tableId).ToListAsync().ConfigureAwait(false);
        }

        public async Task InsertAsync(TablePair tablePair)
        {
            _bridgeRestContext.TablePairs.Add(tablePair);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task UpdateAsync(TablePair tablePair)
        {
            _bridgeRestContext.TablePairs.Update(tablePair);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAsync(TablePair tablePair)
        {
            _bridgeRestContext.TablePairs.Remove(tablePair);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
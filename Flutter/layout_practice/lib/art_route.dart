import 'package:flutter/material.dart';
import 'package:layout_practice/shared/menu_drawer.dart';
import './art_util.dart';

class ArtRoute extends StatelessWidget {
  final String art;
  ArtRoute({required this.art});
  static int _currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MenuDrawer(),
      appBar: AppBar(
        title: Text('Matas learning flutter'),
        actions: <Widget>[
          PopupMenuButton(
              // icon: Icon(Icons.image),
              // child: Text('Change route'),
              itemBuilder: (BuildContext context) {
                return ArtUtil.menuItems.map((String item) {
                  return PopupMenuItem<String>(
                    child: Text(item),
                    value: item,
                  );
                }).toList();
              },
              onSelected: (String value) => changeRoute(context, value))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.lightbulb_outline),
        onPressed: () {
          print('What do you want from me?');
        },
      ),
      persistentFooterButtons: <Widget>[
        IconButton(onPressed: () {}, icon: Icon(Icons.add_comment)),
        IconButton(onPressed: () {}, icon: Icon(Icons.add_alarm)),
        IconButton(onPressed: () {}, icon: Icon(Icons.add_location)),
      ],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Colors.lime[900],
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.article), label: ArtUtil.CARAVAGGIO),
          BottomNavigationBarItem(
              icon: Icon(Icons.architecture), label: ArtUtil.MONET),
          BottomNavigationBarItem(
              icon: Icon(Icons.art_track_sharp), label: ArtUtil.VANGOGH),
        ],
        onTap: ((value) {
          String _artist = ArtUtil.menuItems[value];
          _currentIndex = value;
          changeRoute(context, _artist);
        }),
      ),
      body: Container(
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(art), fit: BoxFit.cover))),
    );
  }

  void changeRoute(BuildContext context, String menuItem) {
    String image = '';
    switch (menuItem) {
      case ArtUtil.CARAVAGGIO:
        {
          image = ArtUtil.IMG_CARAVAGGIO;
          break;
        }
      case ArtUtil.MONET:
        {
          image = ArtUtil.IMG_MONET;
          break;
        }
      case ArtUtil.VANGOGH:
        {
          image = ArtUtil.IMG_VANGOGH;
          break;
        }
    }
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ArtRoute(art: image)));
  }
}



// WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
// WebApplication app = builder.Build();
//
// IServiceCollection services = builder.Services;

// app.MapGet("/", () => "Hello World!");
// app.Run();

using CarServicePlatform.Data;

namespace CarServicePlatform;

public class Program
{
    public static async Task Main(string[] args)
    {
        IHost host = CreateHostBuilder(args).Build();
        using IServiceScope scope = host.Services.CreateScope();
        DatabaseSeeder? dbSeeder = (DatabaseSeeder)scope.ServiceProvider.GetService(typeof(DatabaseSeeder))!;
        await dbSeeder.SeedAsync().ConfigureAwait(false);
        await host.RunAsync().ConfigureAwait(false);
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>());
    }
}
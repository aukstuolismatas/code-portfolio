const http=require('http')
const hostname='localhost';
const port=4000;
const fs = require("fs");

http.createServer((req, res)=>{
    // Server side
    if(req.url==='/sse'){
        res.writeHead(200,{
            'Content-Type': 'text/event-stream',
            'Cache-Control': 'no-cache',
            'Access-Control-Allow-Origin': '*',
            'Connection':'keep-alive',
        });
 
        const refresh=2000 //one request per 2 seconds  
        return setInterval(()=>{
            var today=new Date()
            const id=today;
            const data=`${today.getHours()}:${today.getMinutes()}:${today.getSeconds()}`;
            const message= `retry: ${refresh}\nid: ${id}\ndata: ${data}\n\n`;
            res.write(message);
        },refresh);

    }
    // Client side
    fs.readFile('./index.html', function(err, data){
        if (err){
            res.writeHead(404);
            res.write(err);
            res.end();
        } else {
            res.writeHead(200,{'Content-Type':'text/html'});
            res.write(data);
            res.end();
        }
    });
        
}).listen(port, hostname, ()=>{
    console.log(`Server running at http://${hostname}:${port}`);
})

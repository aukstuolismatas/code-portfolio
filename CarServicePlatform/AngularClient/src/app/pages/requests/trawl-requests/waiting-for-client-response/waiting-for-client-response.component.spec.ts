import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WaitingForClientResponseComponent } from './waiting-for-client-response.component';

describe('WaitingForClientResponseComponent', () => {
  let component: WaitingForClientResponseComponent;
  let fixture: ComponentFixture<WaitingForClientResponseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WaitingForClientResponseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingForClientResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

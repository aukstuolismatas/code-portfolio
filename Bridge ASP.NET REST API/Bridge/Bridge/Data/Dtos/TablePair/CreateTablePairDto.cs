﻿using System.ComponentModel.DataAnnotations;
using Bridge.Data.Entities;

namespace Bridge.Data.Dtos.TablePair
{
    public record CreateTablePairDto([Required] int PairId, double FinalResult, int RoundNr, [Required] PairPosition PairPosition);

}
﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.RepairServiceCategory;

public record UpdateRepairServiceCategoryDto([Required] string CategoryName);
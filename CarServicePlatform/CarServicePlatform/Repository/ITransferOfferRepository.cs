﻿using CarServicePlatform.Entities.Request;

namespace CarServicePlatform.Repository;

public interface ITransferOfferRepository
{
    Task<TransferOffer?> GetTransferOfferAsync(int id);
    Task<List<TransferOffer>> GetAllClientRequestTransferOffersAsync(int clientRequestId);
    Task<TransferOffer?> GetTransferOfferByTrawlIdAndRequestIdAsync(string trawlAccountId, int clientRequestId);
    Task InsertTransferOfferAsync(TransferOffer transferOffer);
    Task UpdateTransferOfferAsync(TransferOffer transferOffer);
    Task UpdateTransferOffersRangeAsync(List<TransferOffer> transferOffers);
    Task DeleteTransferOfferAsync(TransferOffer transferOffer);
}
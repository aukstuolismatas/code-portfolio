﻿namespace CarServicePlatform.Auth;

public static class AppUserRoles
{
    public const string Admin = nameof(Admin);
    public const string Client = nameof(Client);
    public const string Service = nameof(Service);
    public const string Trawl = nameof(Trawl);

    public static readonly IReadOnlyCollection<string> All = new[] { Admin, Client, Service, Trawl };
}
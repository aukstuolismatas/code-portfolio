import { animate } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants } from 'src/app/app-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { RolesType } from 'src/app/services/enum.service';
import { FileService } from 'src/app/services/file.service';
import { FileInformation } from 'src/app/shared/file-information';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, public fileService: FileService, private sanitizer: DomSanitizer, private dataSharingService: DataSharingService,
    private route: ActivatedRoute) { 
    dataSharingService.userName.subscribe(value => this.user = value);
    dataSharingService.avatarImage.subscribe(value => this.profileImage = value);
    this.route.queryParams.subscribe(params =>{
      if((params['login'] !== undefined && params['login'] === 'true') || this.authService.isLoggedIn()){
        this.fileService.downloadProfilePicture().subscribe({
          next: imgData => this.profileImage = imgData,
          error: () => this.profileImage ="../../assets/avatar.jpg"
        });
      }
    });
      
      // (baseImage: any) =>{
      // let objectUrl = baseImage //'data:image/jpg;base64,'+baseImage;
      // var reader = new FileReader();
		  // reader.readAsArrayBuffer(baseImage);
      // this.profileImagePath = reader.result//baseImage //this.sanitizer.bypassSecurityTrustResourceUrl(objectUrl)
  }
  profileImageInformation: FileInformation | undefined
  profileImage: any ='' //"../../assets/avatar.jpg";
  readonly loginUrl = AppConstants.Login;
  readonly registerUrl = AppConstants.RegisterCategories;
  readonly profileUrl = AppConstants.Profile;
  readonly companyServiceProfileUrl = AppConstants.CompanyServiceProfile;
  readonly companyTrawlProfileUrl = AppConstants.CompanyTrawlProfile;
  readonly homeUrl = AppConstants.Home;
  readonly chatUrl = AppConstants.Chat;
  readonly accounts = AppConstants.AccountsService;
  readonly placeRepairRequest = AppConstants.RepairRequest;
  readonly allServiceRepairRequest = AppConstants.AllWaitingClientRequestForServiceOffer;
  readonly allTrawlRequest = AppConstants.AllWaitingTrawlRequestForOffer;
  readonly allClientRepairRequest = AppConstants.AllClientRequest;
  user:string|null ='';
  ngOnInit(): void {
    
  }
  onLogOut(){
    this.authService.logout();
    this.router.navigate([""]);
  }
  isLogin(){
    return this.authService.isLoggedIn();
  }

  isLogout(){
    return this.authService.isLoggedOut();
  }

  isAdmin(){
    return this.authService.isAdmin();
  }

  isClient(){
    return this.authService.isClient();
  }

  isService(){
    return this.authService.isService();
  }

  isTrawl(){
    return this.authService.isTrawl();
  }
  // changeProfileImage(){
  //   this.profileImage = this.profileImageInformation?.path+'/'+this.profileImageInformation?.name;
  // }
}

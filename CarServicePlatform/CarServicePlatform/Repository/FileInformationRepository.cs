﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class FileInformationRepository : IFileInformationRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public FileInformationRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<FileInformation?> GetFileInformationAsync(int id, string userId)
    {
        return await _carPlatformDbContext.FilesInformation
                                          .FirstOrDefaultAsync(x => x.Id == id && x.DomainUserId == userId)
                                          .ConfigureAwait(false);
    }

    public async Task<FileInformation?> GetFileInformationForAdminAsync(int id)
    {
        return await _carPlatformDbContext.FilesInformation
                                          .FirstOrDefaultAsync(x => x.Id == id)
                                          .ConfigureAwait(false);
    }

    public async Task<FileInformation?> GetFileInformationOfLastProfileImageAsync(string userId)
    {
        return await _carPlatformDbContext.FilesInformation.OrderBy(x => x.Id)
                                          .LastOrDefaultAsync(x => x.DomainUserId == userId && x.ClientRequestId == null && x.FileType == FileType.Profile)
                                          .ConfigureAwait(false);
    }

    public async Task<List<FileInformation>> GetAllDocumentsInformationForUserAsync(string userId)
    {
        return await _carPlatformDbContext.FilesInformation
                                          .Where(x => x.DomainUserId == userId && x.FileType == FileType.Document)
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<List<FileInformation>> GetAllFileInformationForClientRequestAsync(int clientRequestId)
    {
        return await _carPlatformDbContext.FilesInformation
                                          .Where(x => x.ClientRequestId == clientRequestId && x.FileType == FileType.Request)
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task InsertFileInformationAsync(FileInformation fileInformation)
    {
        _carPlatformDbContext.FilesInformation.Add(fileInformation);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task InsertFileInformationListAsync(List<FileInformation> fileInformation)
    {
        _carPlatformDbContext.FilesInformation.AddRange(fileInformation);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using NT_Csharp.Models;
using NT_Csharp.ViewModels;
using MySql.Data.MySqlClient;

namespace NT_Csharp.Repos
{
    public class NTobjektasRepository
    {
        public List<NTobjektas> getNTobjektai()
        {
            List<NTobjektas> ntObjektai = new List<NTobjektas>();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"SELECT nt_objektas.nr,
                                       nt_objektas.pastatymo_metai,
                                       nt_objektas.kambariu_skaicius,
                                       nt_objektas.bendras_plotas,
                                       b.name AS busena,
                                       m.name AS tipas
                                       FROM nt_objektas
                                         LEFT JOIN irengimo_busena b ON b.id_irengimo_busena = nt_objektas.irengimo_busena
                                         LEFT JOIN nt_tipas m ON m.id_NT_tipas =nt_objektas.tipas;"; 
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();
            
            foreach (DataRow item in dt.Rows)
            {
                ntObjektai.Add(new NTobjektas
                {
                    id = Convert.ToInt32(item["nr"]),
                    pastatymoMetai = Convert.ToInt32(item["pastatymo_metai"]),
                    kambariuSkaicius = Convert.ToInt32(item["kambariu_skaicius"]),
                    bendrasPlotas = Convert.ToDouble(item["bendras_plotas"]),
                    irengimoBusena = Convert.ToString(item["busena"]),
                    tipas = Convert.ToString(item["tipas"])
                });
            }
            return ntObjektai;
        }
        public NTEditViewModel getNTobjektas(int id)
        {
            NTEditViewModel NTEditViewModel  = new NTEditViewModel();

            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"SELECT a.nr, 
                                       a.pastatymo_metai,
                                       a.kambariu_skaicius,
                                       a.bendras_plotas,
                                       a.aukstu_skaicius,
                                       a.baldai,
                                       a.signalizacija,
                                       a.garazas,
                                       a.irengimo_busena,
                                       a.tipas,
                                       a.fk_BUTASid_BUTAS,
                                       a.fk_VIETAid_VIETA,
                                       a.fk_NAMASid_NAMAS
                                       FROM nt_objektas a
                                       WHERE a.nr=" + id;
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();
            foreach (DataRow item in dt.Rows)
            {
                NTEditViewModel.id = Convert.ToInt32(item["nr"]);
                NTEditViewModel.pastatymoMetai = Convert.ToInt32(item["pastatymo_metai"]);
                NTEditViewModel.kambariuSkaicius = Convert.ToInt32(item["kambariu_skaicius"]);
                NTEditViewModel.bendrasPlotas = Convert.ToDouble(item["bendras_plotas"]);
                NTEditViewModel.aukstuSkaicius = Convert.ToInt32(item["aukstu_skaicius"]);
                NTEditViewModel.baldai = Convert.ToBoolean(item["baldai"]);
                NTEditViewModel.signalizacija = Convert.ToBoolean(item["signalizacija"]);
                NTEditViewModel.garazas = Convert.ToBoolean(item["garazas"]);
                NTEditViewModel.busena = Convert.ToInt32(item["irengimo_busena"]);
                NTEditViewModel.tipas = Convert.ToInt32(item["tipas"]);
                if(item["fk_BUTASid_BUTAS"]==DBNull.Value)
                {
                    NTEditViewModel.fk_butas = null;
                }
                else
                {
                    NTEditViewModel.fk_butas = Convert.ToInt32(item["fk_BUTASid_BUTAS"]);
                }
                NTEditViewModel.fk_vieta = Convert.ToInt32(item["fk_VIETAid_VIETA"]);
                if (item["fk_NAMASid_NAMAS"] == DBNull.Value)
                {
                    NTEditViewModel.fk_namas = null;
                }
                else
                {
                    NTEditViewModel.fk_namas = Convert.ToInt32(item["fk_NAMASid_NAMAS"]);

                }
            }
            return NTEditViewModel;
        }
        public bool addObjektas(NTEditViewModel objektasEditViewModel)
        {
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"INSERT INTO `nt_objketas`
                                    (
                                    `nr`,
                                    `pastatymo_metai`,
                                    `kambariu_skaicius`,
                                    `bendras_plotas`,
                                    `aukstu_skaicius`,
                                    `baldai`,
                                    `signalizacija`,
                                    `garazas`,
                                    `irengimo_busena`,
                                    `tipas`,
                                    `fk_BUTASid_BUTAS`,
                                    `fk_VIETAid_VIETA`,
                                    `fk_NAMASid_NAMAS`) 
                                    VALUES (
                                    ?nr,
                                    ?past_metai,
                                    ?kbr_sk,
                                    ?bendr_plot,
                                    ?aukst_sk,
                                    ?bald,
                                    ?signal,
                                    ?garaz,
                                    ?irengim,
                                    ?tip,
                                    ?butas,
                                    ?vieta,
                                    ?namas)";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlCommand.Parameters.Add("?nr", MySqlDbType.Int32).Value = objektasEditViewModel.id;
            mySqlCommand.Parameters.Add("?past_metai", MySqlDbType.Int32).Value = objektasEditViewModel.pastatymoMetai;
            mySqlCommand.Parameters.Add("?kbr_sk", MySqlDbType.Int32).Value = objektasEditViewModel.kambariuSkaicius;
            mySqlCommand.Parameters.Add("?bendr_plot", MySqlDbType.Double).Value = objektasEditViewModel.bendrasPlotas;
            mySqlCommand.Parameters.Add("?aukst_sk", MySqlDbType.Int32).Value = objektasEditViewModel.aukstuSkaicius ;
            mySqlCommand.Parameters.Add("?bald", MySqlDbType.Int32).Value = (objektasEditViewModel.baldai ? 1 : 0);
            mySqlCommand.Parameters.Add("?signal", MySqlDbType.Int32).Value = (objektasEditViewModel.signalizacija ? 1 : 0);
            mySqlCommand.Parameters.Add("?garaz", MySqlDbType.Int32).Value = (objektasEditViewModel.garazas ? 1 : 0);
            mySqlCommand.Parameters.Add("?irengim", MySqlDbType.Int32).Value = objektasEditViewModel.busena;
            mySqlCommand.Parameters.Add("?tip", MySqlDbType.Int32).Value = objektasEditViewModel.tipas;
            if (Convert.ToInt32(objektasEditViewModel.fk_butas) > 0)
            {
                mySqlCommand.Parameters.Add("?butas", MySqlDbType.Int32).Value = objektasEditViewModel.fk_butas;
            }
            else
            {
                mySqlCommand.Parameters.AddWithValue("?butas", DBNull.Value);
            }
            if (Convert.ToInt32(objektasEditViewModel.fk_namas) > 0)
            {
                mySqlCommand.Parameters.Add("?namas", MySqlDbType.Int32).Value = objektasEditViewModel.fk_namas;
            }
            else
            {
                mySqlCommand.Parameters.AddWithValue("?namas", DBNull.Value);
            }
            //mySqlCommand.Parameters.Add("?butas", MySqlDbType.Int32).Value = objektasEditViewModel.fk_butas!=null ? objektasEditViewModel.fk_butas:null;
            mySqlCommand.Parameters.Add("?vieta", MySqlDbType.Int32).Value = objektasEditViewModel.fk_vieta;
            //mySqlCommand.Parameters.Add("?namas", MySqlDbType.Int32).Value = objektasEditViewModel.fk_namas != null ? objektasEditViewModel.fk_namas : null; ;
            mySqlConnection.Open();
            mySqlCommand.ExecuteNonQuery();
            mySqlConnection.Close();

            return true;
        }
        public bool updateNTObjektas(NTEditViewModel ntEditViewModel)
        {
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"UPDATE `nt_objektas` SET
                                    `pastatymo_metai` = ?past_metai,
                                    `kambariu_skaicius` = ?kbr_sk,
                                    `bendras_plotas` = ?bendr_plot,
                                    `aukstu_skaicius` = ?aukst_sk,
                                    `baldai` = ?bald,
                                    `signalizacija` = ?signal,
                                    `garazas` = ?garaz,
                                    `irengimo_busena` = ?irengim,
                                    `tipas` = ?tip,
                                    `fk_BUTASid_BUTAS` = ?butas,
                                    `fk_VIETAid_VIETA` = ?vieta,
                                    `fk_NAMASid_NAMAS` = ?namas
                                    WHERE nr=" + ntEditViewModel.id;
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlCommand.Parameters.Add("?past_metai", MySqlDbType.Int32).Value = ntEditViewModel.pastatymoMetai;
            mySqlCommand.Parameters.Add("?kbr_sk", MySqlDbType.Int32).Value = ntEditViewModel.kambariuSkaicius;
            mySqlCommand.Parameters.Add("?bendr_plot", MySqlDbType.Double).Value = ntEditViewModel.bendrasPlotas;
            mySqlCommand.Parameters.Add("?aukst_sk", MySqlDbType.Int32).Value = ntEditViewModel.aukstuSkaicius;
            mySqlCommand.Parameters.Add("?bald", MySqlDbType.Int32).Value = (ntEditViewModel.baldai ? 1 : 0);
            mySqlCommand.Parameters.Add("?signal", MySqlDbType.Int32).Value = (ntEditViewModel.signalizacija ? 1 : 0);
            mySqlCommand.Parameters.Add("?garaz", MySqlDbType.Int32).Value = (ntEditViewModel.garazas ? 1 : 0);
            mySqlCommand.Parameters.Add("?irengim", MySqlDbType.Int32).Value = ntEditViewModel.busena;
            mySqlCommand.Parameters.Add("?tip", MySqlDbType.Int32).Value = ntEditViewModel.tipas;
            mySqlCommand.Parameters.Add("?butas", MySqlDbType.Int32).Value = ntEditViewModel.fk_butas!=0 ? ntEditViewModel.fk_butas: -1;
            mySqlCommand.Parameters.Add("?vieta", MySqlDbType.Int32).Value = ntEditViewModel.fk_vieta;
            mySqlCommand.Parameters.Add("?namas", MySqlDbType.Int32).Value = ntEditViewModel.fk_namas;
            mySqlConnection.Open();
            mySqlCommand.ExecuteNonQuery();
            mySqlConnection.Close();

            return true;
        }

        public int getObjektasSutarciuCount(int id)
        {
            int naudota = 0;
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"SELECT count(nr) as kiekis from sutartis where fk_NT_OBJEKTASnr=" + id;
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                naudota = Convert.ToInt32(item["kiekis"] == DBNull.Value ? 0 : item["kiekis"]);
            }
            return naudota;
        }

        public void deleteObjektas(int id)
        {
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"DELETE FROM nt_objektas where nr=?id";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlCommand.Parameters.Add("?id", MySqlDbType.Int32).Value = id;
            mySqlConnection.Open();
            mySqlCommand.ExecuteNonQuery();
            mySqlConnection.Close();
        }
    }
}
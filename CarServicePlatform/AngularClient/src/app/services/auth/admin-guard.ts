import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { AppConstants } from "src/app/app-constants";
import { RolesType } from "../enum.service";
import { AuthService } from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class AdminGuard  implements CanActivate{
    constructor(private auth: AuthService, private router: Router){}
    canActivate(): boolean | Observable<boolean>{
        if (this.auth.isLoggedOut() || !this.auth.getRole()?.match(RolesType.Admin)) {
            this.router.navigate([AppConstants.Login]);
            return false;
        } else {
            return true;
        }
    } 
}

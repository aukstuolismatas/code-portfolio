﻿namespace CarServicePlatform.Services;

public static class FilePathFactory
{
    private static readonly string ProfileImageFolder = "Profile_Image";
    private static readonly string ClientServiceRequestFolder = "Client_Service_Request";
    private static readonly string CompanyDocumentsFolder = "Documents";
    private static readonly string FilesDirectory = "./Data/Resources";

    public static string ConstructProfileImagePath(string userId)
    {
        return $"{FilesDirectory}/{userId}/{ProfileImageFolder}";
    }

    public static string ConstructClientRequestImagesPath(string userId, int clientRequestId)
    {
        return $"{FilesDirectory}/{userId}/{ClientServiceRequestFolder}/{clientRequestId}";
    }

    public static string ConstructCompanyDocumentsPath(string userId)
    {
        return $"{FilesDirectory}/{userId}/{CompanyDocumentsFolder}";
    }
}
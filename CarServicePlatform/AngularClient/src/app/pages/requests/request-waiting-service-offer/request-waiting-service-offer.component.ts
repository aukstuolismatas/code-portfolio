import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { RegisteredClientRequest } from 'src/app/shared/client-request';
import { RequestStatus } from 'src/app/shared/request-status';

@Component({
  selector: 'app-request-waiting-service-offer',
  templateUrl: './request-waiting-service-offer.component.html',
  styleUrls: ['./request-waiting-service-offer.component.css']
})
export class RequestWaitingServiceOfferComponent implements OnInit {
  successMessage:string='';
  tabName:string=HeaderConstants.WaitingPriceOfferTab;
  frontTitle:string='Laukiantys užsakymai'
  titles:string[]=['Užsakymo nr.', 'Miestas', 'Data', 'Statusas']
  repairRequestList:RegisteredClientRequest[]=[];

  constructor(private repairRequestService: RepairRequestService, private dataSharingService: DataSharingService, private router: Router,
    private route: ActivatedRoute) {
    dataSharingService.requestList.subscribe(value => this.repairRequestList = value);
      this.repairRequestService.retrieveAllAvailableClientRequestForService().subscribe((data:RegisteredClientRequest[])=>{
        this.dataSharingService.requestList.next(data)
      })
   }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params =>{
      if(params['login'] !== undefined && params['login'] === 'true'){
        this.successMessage = MessageConstants.LoginSuccess;
      }
    });
  }

  openItem(index:number){
    this.router.navigate([`${AppConstants.AllClientRequest}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  }

  getStatusDescription(enumString:string){
    return (<any>RequestStatus)[enumString];
  }
}

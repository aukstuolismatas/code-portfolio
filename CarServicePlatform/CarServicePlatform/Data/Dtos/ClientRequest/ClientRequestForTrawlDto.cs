﻿namespace CarServicePlatform.Data.Dtos.ClientRequest;

public record ClientRequestForTrawlDto(int ClientRequestId, string Title, string Description, string Address, string City, string Created, string Status, string ServiceAddress, string ServiceCompanyName);
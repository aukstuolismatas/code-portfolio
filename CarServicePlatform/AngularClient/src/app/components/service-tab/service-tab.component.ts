import { Input, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-service-tab',
  templateUrl: './service-tab.component.html',
  styleUrls: ['./service-tab.component.css']
})
export class ServiceTabComponent implements OnInit {

  @Input('tab') tab = '';
  readonly waitingOfferUrl = AppConstants.AllWaitingClientRequestForServiceOffer;
  readonly waitingApproveUrl = AppConstants.AllWaitingClientRequestForClientApproval;
  readonly assignedUrl = AppConstants.AllAssignedClientRequestForService;
  readonly inactiveUrl = AppConstants.AllInactiveClientRequestForService;


  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  isService(){
    return this.authService.isService();
  }

  isWaitingOffer(){
    return this.tab==HeaderConstants.WaitingPriceOfferTab;
  }

  isWaitingApprove(){
    return this.tab==HeaderConstants.WaitingClientApprovalTab;
  }

  isAssign(){
    return this.tab==HeaderConstants.AssignedTab;
  }

  isInActive(){
    return this.tab==HeaderConstants.InActiveTab;
  }

  toOffers(){
    this.router.navigate([AppConstants.AllWaitingClientRequestForServiceOffer]);
  }

  toApproval(){
    this.router.navigate([AppConstants.AllWaitingClientRequestForClientApproval]);
  }
  
  toAssigned(){
    this.router.navigate([AppConstants.AllAssignedClientRequestForService]);
  }

  toInActive(){
    this.router.navigate([AppConstants.AllInactiveClientRequestForService]);
  }

}

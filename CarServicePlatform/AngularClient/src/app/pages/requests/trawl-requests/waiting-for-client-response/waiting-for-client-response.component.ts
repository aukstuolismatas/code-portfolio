import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { TrawlTransferRequestService } from 'src/app/services/trawl-transfer-request.service';
import { RegisteredClientRequest, RegisteredClientRequestTrawl } from 'src/app/shared/client-request';
import { RequestStatus } from 'src/app/shared/request-status';

@Component({
  selector: 'app-waiting-for-client-response',
  templateUrl: './waiting-for-client-response.component.html',
  styleUrls: ['./waiting-for-client-response.component.css']
})
export class WaitingForClientResponseComponent implements OnInit {
  tabName:string=HeaderConstants.WaitingClientApprovalTab;
  frontTitle:string='Laukiantys užsakymai pervežimui'
  titles:string[]=['Užsakymo nr.', 'Miestas', 'Atvykimo adresas', 'Data', 'Statusas']
  repairRequestList:RegisteredClientRequestTrawl[]=[];

  constructor(private trawlTransferService: TrawlTransferRequestService, private dataSharingService: DataSharingService, private router: Router) {
    dataSharingService.trawlRequestList.subscribe(value => this.repairRequestList = value);
      this.trawlTransferService.retrieveAllWaitingApprovalClientRequestForTrawl().subscribe((data:RegisteredClientRequestTrawl[])=>{
        this.dataSharingService.trawlRequestList.next(data)
      })
   }

  ngOnInit(): void {
  }

  openItem(index:number){
    this.router.navigate([`${AppConstants.AllTrawlRequests}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  }

  getStatusDescription(enumString:string){
    return (<any>RequestStatus)[enumString];
  }
}

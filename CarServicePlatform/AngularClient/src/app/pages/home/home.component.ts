import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'src/app/app-constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  clientRegisterUrl = AppConstants.RegisterClient;

  constructor() { }
  ngOnInit(): void {
  }
}

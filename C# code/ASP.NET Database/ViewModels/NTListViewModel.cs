using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace NT_Csharp.ViewModels
{
    public class NTListViewModel
    {
        [DisplayName("ID")]
        public int id { get; set; }
        [DisplayName("Pastatymo metai")]
        public int pastatymoMetai { get; set; }
        [DisplayName("Kambariu skaicius")]
        public int kambariuSkaicius { get; set; }
        [DisplayName("Bendras plotas")]
        public double bendrasPlotas { get; set; }
        [DisplayName("Irengimo būsena")]
        public string busena { get; set; }
        [DisplayName("Tipas")]
        public string tipas { get; set; }

        //[DisplayName("ID")]
        //[Required]
        //public int id { get; set; }
        //[DisplayName("Pastatymo metai")]
        //[Required]
        //public int pastatymoMetai { get; set; }
        //[DisplayName("Kambariu skaicius")]
        //[Required]
        //public int kambariuSkaicius { get; set; }
        //[DisplayName("Bendras plotas")]
        //[Required]
        //public double bendrasPlotas { get; set; }
        ////public int aukstuSkaicius { get; set; }
        ////public Boolean baldai { get; set; }
        ////public Boolean signalizacija { get; set; }
        ////public Boolean garazas { get; set; }
        //[DisplayName("Irengimo būsena")]
        //[Required]
        //public string irengimoBusena { get; set; }
        //[DisplayName("Tipas")]
        //[Required]
        //public string tipas { get; set; }
        ////public int fk_butas { get; set; }
        ////public int fk_vieta { get; set; }
        ////public int fk_namas { get; set; }
    }
}
import { Component, OnInit } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { RequestStatusConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { FileService } from 'src/app/services/file.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { TrawlTransferRequestService } from 'src/app/services/trawl-transfer-request.service';
import { ApprovedClientRequestTrawl, TransferOffer } from 'src/app/shared/client-request';
import { ApprovedServiceRequestsComponent } from '../../approved-service-requests/approved-service-requests.component';

@Component({
  selector: 'app-assigned-trawl-request-details',
  templateUrl: './assigned-trawl-request-details.component.html',
  styleUrls: ['./assigned-trawl-request-details.component.css']
})
export class AssignedTrawlRequestDetailsComponent implements OnInit {
  index:number=0;
  requestId:number=0;
  imageIndex:number=0;
  clientRequestDetail!: ApprovedClientRequestTrawl;
  imagesArray: any[]=[]
  imageUrl: SafeUrl | string =''
  viewerOpen=false
  successMessage=''
  errorMessage=''
  titles=['Kaina', 'Atvykimo laikas', '']
  constructor(private activatedRoute: ActivatedRoute, private dataSharingService: DataSharingService, private trawlTransferRequestService: TrawlTransferRequestService, 
    private repairRequestService: RepairRequestService, public fileService: FileService, private authService: AuthService) {
    }
  ngOnInit(): void {
    this.index = this.activatedRoute.snapshot.params['index'];
    this.requestId = this.activatedRoute.snapshot.params['id'];
    // this.clientRequestDetail = this.dataSharingService.approvedTrawlRequestList.value[this.index];
    // if(this.clientRequestDetail.clientPhone==''){
      
    // }
    this.trawlTransferRequestService.retrieveAssignedClientRequestForTrawlById(this.requestId).subscribe((data:ApprovedClientRequestTrawl)=>this.clientRequestDetail=data);
    this.fileService.downloadRequestImages(this.requestId).subscribe(data=> {
      this.imagesArray=data      
    })
  }

  isAccepted(){
    return this.clientRequestDetail.status == RequestStatusConstants.Accepted;
  }

  isTransferring(){
    return this.clientRequestDetail.status == RequestStatusConstants.Transferring;
  }

  isDone(){
    return this.clientRequestDetail.status == RequestStatusConstants.Done;
  }

  takeCar(){
    this.repairRequestService.transferringClientRequest(this.requestId).subscribe({
      next: (data:ApprovedClientRequestTrawl) => {
        this.clientRequestDetail.status=data.status;
        this.successMessage = MessageConstants.TransferringClientRequestSuccess
      },
      error: (err) => this.errorMessage = MessageConstants.ClientRequestUpdateFailed
    })
    this.successMessage='';
    this.errorMessage='';
  }

  carArrived(){
    this.repairRequestService.arrivedClientRequest(this.requestId).subscribe({
      next: (data:ApprovedClientRequestTrawl) => {
        this.clientRequestDetail.status=data.status;
        this.successMessage = MessageConstants.ArrivedClientRequestSuccess
      },
      error: (err) => this.errorMessage = MessageConstants.ClientRequestUpdateFailed
    })
    this.successMessage='';
    this.errorMessage='';
  }

  carReturned(){
    this.repairRequestService.deliveredClientRequest(this.requestId).subscribe({
      next: (data:ApprovedClientRequestTrawl) => {
        this.clientRequestDetail.status=data.status;
        this.successMessage = MessageConstants.DeliveredClientRequestSuccess
      },
      error: (err) => this.errorMessage = MessageConstants.ClientRequestUpdateFailed
    })
    this.successMessage='';
    this.errorMessage='';
  }
}

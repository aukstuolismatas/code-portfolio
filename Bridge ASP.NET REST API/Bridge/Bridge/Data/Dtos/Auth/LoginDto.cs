﻿namespace Bridge.Data.Dtos.Auth
{
    public record LoginDto(string UserName, string Password);
}
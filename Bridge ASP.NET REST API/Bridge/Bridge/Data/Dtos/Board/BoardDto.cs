﻿using Bridge.Data.Entities;

namespace Bridge.Data.Dtos.Board
{
    public record BoardDto(int Id, int BoardNr, string Deal, PairPosition Dealer, string Vulnerable, string Minimax, int MinimaxScore);
}
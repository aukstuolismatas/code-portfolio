import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovedServiceRequestsComponent } from './approved-service-requests.component';

describe('ApprovedServiceRequestsComponent', () => {
  let component: ApprovedServiceRequestsComponent;
  let fixture: ComponentFixture<ApprovedServiceRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApprovedServiceRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovedServiceRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

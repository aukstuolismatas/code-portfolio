﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.Chat;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class ConversationRepository : IConversationRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public ConversationRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    // public async Task<List<Conversation>> GetAllConversationsOfCurrentUserAsync(string currentUserId)
    // {
    //     return await _carPlatformDbContext.Conversations
    //                                       .Where(x => x.SenderId == currentUserId)
    //                                       .OrderBy(x => x.ReceiverId)
    //                                       .ThenBy(x => x.CreatedAt)
    //                                       .ToListAsync()
    //                                       .ConfigureAwait(false);
    // }
    //
    // public async Task<List<Conversation>> GetAllConversationsWithSpecificUserAsync(string currentUserId, string contactUserId)
    // {
    //     return await _carPlatformDbContext.Conversations
    //                                       .Where(x => 
    //                                           (x.ReceiverId == currentUserId && x.SenderId == contactUserId) ||
    //                                           (x.ReceiverId == contactUserId && x.SenderId == currentUserId))
    //                                       .OrderBy(x => x.CreatedAt)
    //                                       .ToListAsync()
    //                                       .ConfigureAwait(false);
    // }

    public async Task<List<ConversationGroup>> GetAllConversationGroupsWhichUserAssignedTo(string userId)
    {
        return await _carPlatformDbContext.ConversationUsers.Where(x => x.DomainUserId == userId)
                                          .Join(_carPlatformDbContext.ConversationGroups, user => user.ConversationGroupId, group => @group.Id,
                                              (user, group) => new ConversationGroup
                                              {
                                                  Id = @group.Id,
                                                  ConversationTitle = @group.ConversationTitle,
                                                  ClientRequestId = @group.ClientRequestId,
                                                  UserName = _carPlatformDbContext
                                                             .ConversationUsers
                                                             .FirstOrDefault(x => x.ConversationGroupId == @group.Id && x.DomainUserId != userId)!
                                                             .UserName
                                              })
                                          .ToListAsync()
                                          .ConfigureAwait(false);
    }

    public async Task<ConversationUser?> GetConversationUserWhichAssignedToGroup(int groupId, string userId)
    {
        return await _carPlatformDbContext.ConversationUsers.FirstOrDefaultAsync(x => x.ConversationGroupId == groupId && x.DomainUserId == userId).ConfigureAwait(false);
    }

    public async Task<List<ConversationMessage>> GetAllConversationMessagesWhichAssignedToGroup(int groupId)
    {
        return await _carPlatformDbContext.ConversationMessages.Where(x => x.ConversationGroupId == groupId).ToListAsync().ConfigureAwait(false);
    }

    public async Task<ConversationGroup?> GetConversationGroupByClientRequest(int clientRequestId)
    {
        return await _carPlatformDbContext.ConversationGroups.FirstOrDefaultAsync(x => x.ClientRequestId == clientRequestId).ConfigureAwait(false);
    }

    public async Task<ConversationGroup?> GetConversationGroupByGroupId(int groupId)
    {
        return await _carPlatformDbContext.ConversationGroups.FirstOrDefaultAsync(x => x.Id == groupId).ConfigureAwait(false);
    }

    public async Task InsertConversationGroupAsync(ConversationGroup conversationGroup)
    {
        _carPlatformDbContext.ConversationGroups.Add(conversationGroup);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task InsertConversationUsersAsync(List<ConversationUser> conversationUsers)
    {
        _carPlatformDbContext.ConversationUsers.AddRange(conversationUsers);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task InsertConversationMessageAsync(ConversationMessage conversationMessage)
    {
        _carPlatformDbContext.ConversationMessages.Add(conversationMessage);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    // public async Task DeleteConversationAsync(Conversation conversation)
    // {
    //     _carPlatformDbContext.Conversations.Remove(conversation);
    //     await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    // }
}
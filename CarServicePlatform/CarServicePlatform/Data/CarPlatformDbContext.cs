﻿using CarServicePlatform.Entities;
using CarServicePlatform.Entities.Chat;
using CarServicePlatform.Entities.RepairServices;
using CarServicePlatform.Entities.Request;
using CarServicePlatform.Entities.Users;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Data;

public class CarPlatformDbContext : IdentityDbContext<AppUser>
{
    public CarPlatformDbContext(DbContextOptions<CarPlatformDbContext> options) : base(options)
    {
    }

    public DbSet<ClientRequest> ClientRequests { get; set; }
    public DbSet<PriceOffer> PriceOffers { get; set; }
    public DbSet<TransferOffer> TransferOffers { get; set; }
    public DbSet<DomainUser> DomainUsers { get; set; }
    public DbSet<ClientAccount> ClientAccounts { get; set; }
    public DbSet<ServiceAccount> ServiceAccounts { get; set; }
    public DbSet<TrawlAccount> TrawlAccounts { get; set; }
    public DbSet<AdministratorAccount> AdministratorAccounts { get; set; }
    public DbSet<RepairServiceCategory> RepairServiceCategories { get; set; }
    public DbSet<RepairService> RepairServices { get; set; }
    public DbSet<RequestedRepairService> RequestedRepairServices { get; set; }
    public DbSet<AvailableRepairService> AvailableRepairServices { get; set; }
    public DbSet<FileInformation> FilesInformation { get; set; }
    public DbSet<ConversationGroup> ConversationGroups { get; set; }
    public DbSet<ConversationUser> ConversationUsers { get; set; }
    public DbSet<ConversationMessage> ConversationMessages { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=MyCarApp;Integrated Security=SSPI;");
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);
        // foreach (RequestStatus status in Enum.GetValues(typeof(RequestStatus)).Cast<RequestStatus>())
        // {
        //     RequestStatusEnum statusEnum = new RequestStatusEnum
        //     {
        //         Id = status,
        //         Status = status.
        //     };
        //
        //     modelBuilder.Entity<ColorDto>().HasData(colorDto);
        // }
        //

        builder.Entity<ClientAccount>(x => x.ToTable(nameof(ClientAccount)));
        builder.Entity<ServiceAccount>(x => x.ToTable(nameof(ServiceAccount)));
                                        // .Property(u => u.City)
                                             // .HasConversion<string>()
                                             // .HasMaxLength(30));
        builder.Entity<TrawlAccount>(x => x.ToTable(nameof(TrawlAccount)));
        builder.Entity<AdministratorAccount>(x => x.ToTable(nameof(AdministratorAccount)));
    }
}

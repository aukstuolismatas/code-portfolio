
export interface LoginData {
    accessToken: string;
    expiration: Date;
}
export interface LoginRequest{
    email : string;
    password: string;
}
export interface RegisterClient{
    name: string;
    surname: string;
    email: string;
    password: string;
    phoneNumber: string;
}

export interface RegisterService{
    name: string;
    surname: string;
    email: string;
    password: string;
    phoneNumber: string;
    companyName: string;
    companyCode: string;
    city: number;
    address: string;
    availableRepairServiceIds: {[key: string]: number}
}

export interface RegisterTrawl{
    name: string;
    surname: string;
    email: string;
    password: string;
    phoneNumber: string;
    companyName: string;
    companyCode: string;
    city: number;
    minimalPrice: number;
}
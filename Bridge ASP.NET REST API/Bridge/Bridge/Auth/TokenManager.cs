﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Bridge.Auth.Model;
using Bridge.Data.Dtos.Auth;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace Bridge.Auth
{
    public interface ITokenManager
    {
        Task<SuccessfulLoginResponseDto> CreateAccessTokenAsync(BridgeRestUser user);
    }

    public class TokenManager : ITokenManager
    {
        private readonly UserManager<BridgeRestUser> _userManager;
        private readonly string _validIssuer;
        private readonly string _validAudience;
        private readonly SymmetricSecurityKey _authSigningKey;

        public TokenManager(IConfiguration configuration, UserManager<BridgeRestUser> userManager)
        {
            _userManager = userManager;
            _validAudience = configuration["JWT:ValidAudience"];
            _validIssuer = configuration["JWT:ValidIssuer"];
            _authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));
        }

        public async Task<SuccessfulLoginResponseDto> CreateAccessTokenAsync(BridgeRestUser user)
        {
            IList<string> userRoles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
            List<Claim> authClaims = new List<Claim>
            {
                new(ClaimTypes.Name, user.UserName),
                new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new(CustomClaims.UserId, user.Id.ToString()),
            };

            authClaims.AddRange(userRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole)));
            JwtSecurityToken accessSecurityToken = new JwtSecurityToken(
                issuer: _validIssuer,
                audience: _validAudience,
                expires: DateTime.UtcNow.AddHours(1),
                claims: authClaims,
                signingCredentials: new SigningCredentials(_authSigningKey, SecurityAlgorithms.HmacSha256)
            );
            return new SuccessfulLoginResponseDto(new JwtSecurityTokenHandler().WriteToken(accessSecurityToken), accessSecurityToken.ValidTo);
        }
    }
}
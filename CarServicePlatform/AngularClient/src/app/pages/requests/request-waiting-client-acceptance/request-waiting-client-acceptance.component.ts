import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { RegisteredClientRequest } from 'src/app/shared/client-request';

@Component({
  selector: 'app-request-waiting-client-acceptance',
  templateUrl: './request-waiting-client-acceptance.component.html',
  styleUrls: ['./request-waiting-client-acceptance.component.css']
})
export class RequestWaitingClientAcceptanceComponent implements OnInit {
  tabName:string=HeaderConstants.WaitingClientApprovalTab;
  frontTitle:string='Laukiančios užklausos kliento patvirtinimo'
  titles:string[]=['Užsakymo nr.', 'Miestas', 'Data', 'Statusas']
  repairRequestList:RegisteredClientRequest[]=[];

  constructor(private repairRequestService: RepairRequestService, private dataSharingService: DataSharingService, private router: Router) {
    dataSharingService.requestList.subscribe(value => this.repairRequestList = value);
      this.repairRequestService.retrieveAllClientRequestWaitingApprovalForService().subscribe((data:RegisteredClientRequest[])=>{
        this.dataSharingService.requestList.next(data)
      })
   }

  ngOnInit(): void {
  }

  openItem(index:number){
    this.router.navigate([`${AppConstants.AllClientRequest}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  }
}

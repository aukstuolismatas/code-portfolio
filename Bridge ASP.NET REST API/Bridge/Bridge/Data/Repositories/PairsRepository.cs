﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Bridge.Data.Repositories
{
    public class PairsRepository : IPairsRepository
    {
        private readonly BridgeRestContext _bridgeRestContext;

        public PairsRepository(BridgeRestContext bridgeRestContext)
        {
            _bridgeRestContext = bridgeRestContext;
        }

        public async Task<List<Pair>> GetAllAsync()
        {
            return await _bridgeRestContext.Pairs.ToListAsync().ConfigureAwait(false);
        }

        public async Task<Pair> GetAsync(int id)
        {
            return await _bridgeRestContext.Pairs.FirstOrDefaultAsync(x => x.Id == id).ConfigureAwait(false);
        }

        public async Task InsertAsync(Pair pair)
        {
            _bridgeRestContext.Pairs.Add(pair);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task UpdateAsync(Pair pair)
        {
            _bridgeRestContext.Pairs.Update(pair);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAsync(Pair pair)
        {
            _bridgeRestContext.Pairs.Remove(pair);
            await _bridgeRestContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}
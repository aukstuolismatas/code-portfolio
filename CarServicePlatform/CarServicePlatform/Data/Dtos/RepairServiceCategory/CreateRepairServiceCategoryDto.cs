﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.RepairServiceCategory;

public record CreateRepairServiceCategoryDto([Required] string CategoryName);

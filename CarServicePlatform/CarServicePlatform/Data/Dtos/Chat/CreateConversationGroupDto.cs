﻿namespace CarServicePlatform.Data.Dtos.Chat;

public record CreateConversationGroupDto(int ClientRequestId, string userId);
import 'package:cupertino_practice_ios/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BmiScreen extends StatefulWidget {
  const BmiScreen();

  @override
  _BmiScreenState createState() => _BmiScreenState();
}

class _BmiScreenState extends State<BmiScreen> {
  final TextEditingController txtHeight = TextEditingController();
  final TextEditingController txtWeight = TextEditingController();
  int unit = 0;
  final double fontSize = 18;
  String result = '';
  bool isMetric = true;
  bool isImperial = false;
  // double? height = MediaQuery.of(context).size.height;
  // double? weight;
  late List<bool> isSelected;
  String heightMessage = '';
  String weightMessage = '';
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return CupertinoPageScaffold(
      child: SafeArea(
        child: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(maxHeight: height * 0.8),
            child: Column(children: <Widget>[
              Image.network(
                  'https://images.unsplash.com/photo-1554224155-8d04cb21cd6c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80'),
              Util.paddingTop,
              Row(
                children: <Widget>[
                  Util.paddingLeft,
                  Container(
                    child: Text('Height'),
                    width: width / 3,
                  ),
                  Expanded(
                      child: CupertinoTextField(
                    controller: txtHeight,
                  )),
                  Util.paddingLeft
                ],
              ),
              Util.paddingTop,
              Row(
                children: <Widget>[
                  Util.paddingLeft,
                  Container(
                    child: Text('Weight'),
                    width: width / 3,
                  ),
                  Expanded(
                      child: CupertinoTextField(
                    controller: txtWeight,
                  )),
                  Util.paddingLeft
                ],
              ),
              Expanded(child: Util.paddingTop),
              CupertinoButton.filled(
                  child: Text('Calculate BMI'),
                  onPressed: () {
                    showResult();
                  }),
              Util.paddingTop
            ]),
          ),
        ),
      ),
    );
  }

  void showResult() async {
    int unit = await Util.getSettings();
    double height = double.tryParse(txtHeight.text) ?? 0;
    double weight = double.tryParse(txtWeight.text) ?? 0;
    double result = Util.calculateBMI(height, weight, unit);
    String message = 'BMI is ' + result.toStringAsFixed(2);
    CupertinoAlertDialog dialog = CupertinoAlertDialog(
      title: Text('Resukt'),
      content: Text(message),
      actions: [
        CupertinoDialogAction(
          child: Text('OK'),
          onPressed: () {
            Navigator.pop(context);
          },
        )
      ],
    );
    showDialog(
        context: context,
        builder: (context) {
          return dialog;
        });
  }
}

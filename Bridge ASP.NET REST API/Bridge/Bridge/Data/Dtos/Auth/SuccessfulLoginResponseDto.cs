﻿using System;

namespace Bridge.Data.Dtos.Auth
{
    public record SuccessfulLoginResponseDto(string AccessToken, DateTime Expiration);
}
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;

namespace Shared_Memory
{
    class Program
    {
        public static DataMonitor data;
        public static SortedResultMonitor result;
        static void Main(string[] args)
        {
            int threadsNumber = 5;
            bool last = false;
            List<HDD> HddList = new List<HDD>();
            string json = File.ReadAllText(@"C:\Users\lt0mtls\Documents\Parallel programming\dat_1.json");
            HddList = JsonConvert.DeserializeObject<List<HDD>>(json);
            data= new DataMonitor(HddList.Count);
            result = new SortedResultMonitor(HddList.Count);
            var readers = Enumerable.Range(0, threadsNumber).Select(i => new Executor(data, result, i)).ToList();
            var threads = readers.Select(b => new Thread(b.Launch)).ToList();

            foreach (var thread in threads)
            {
                thread.Start();
            }
            for (int i=0; i<HddList.Count; i++)
            {
                if(i==HddList.Count-1)
                {
                    last = true;
                }
                data.addHDD(HddList[i], last);
            }
            foreach (var thread in threads)
            {
                thread.Join();
            }
            WriteToFile(result.getItems());
        }
        public static void WriteToFile(List<HDDWithComputedValue> result)
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\Users\lt0mtls\Documents\Parallel programming\rez.txt"))
            {
                file.WriteLine(String.Format("{0,30}|{1,15}|{2,15}|{3,20}", "Brand", "Capacity", "Price", "Value"));
                file.WriteLine(string.Concat(Enumerable.Repeat('-', 85)));
                foreach (HDDWithComputedValue hdd in result)
                {
                    file.WriteLine(String.Format("{0,30}|{1,15}|{2,15}|{3,20}", hdd.item.brand, hdd.item.capacity, hdd.item.price, Math.Round(hdd.computedData)));
                }
            }
        }
    }
}

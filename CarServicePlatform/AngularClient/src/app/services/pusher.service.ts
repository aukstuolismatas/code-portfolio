import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import Pusher from 'pusher-js';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiConstants } from '../api-constants';
import { ConversationMessage, CreateConversationMessage } from '../shared/conversation';
import { AuthService } from './auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class PusherService {

  pusher: Pusher;
  messagesChannel: any;
  sendMessageUrl = environment.apiUrl+ApiConstants.conversationsGroups;
  authUrl = environment.apiUrl+ApiConstants.pusherAuth;
  


  constructor(private http: HttpClient, private authService: AuthService) { 
    // const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    const headers = new HttpHeaders().set('Authorization', `Bearer ${this.authService.decodedToken}`);
    this.pusher = new Pusher(environment.pusher.key, {
      authEndpoint: this.authUrl,
      auth: { headers: headers},
      cluster: 'eu'      
    });
    // this.messagesChannel = this.pusher.subscribe('private-3');
  }
  postMessage(message: ConversationMessage): Observable<ConversationMessage>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    const createMessage : CreateConversationMessage = {
      sendBy: message.sendBy,
      message: message.message,
      socketId: this.pusher.connection.socket_id
   }
    return this.http.post<ConversationMessage>(`${this.sendMessageUrl}/${message.conversationGroupId}/messages`, createMessage, {headers: headers})
  }
  getPusher() {
    return this.pusher;
  }
  changeChannel(requestId : number){
    if(!this.pusher.channel('private-'+requestId)){
      this.messagesChannel = this.pusher.subscribe(`private-${requestId}`);
    }
  }
}

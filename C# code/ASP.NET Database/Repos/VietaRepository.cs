using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NT_Csharp.Models;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace NT_Csharp.Repos
{
    public class VietaRepository
    {
        public List<Vieta> getVietos()
        {
            List<Vieta> vietos = new List<Vieta>();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "SELECT * FROM vieta";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                vietos.Add(new Vieta
                {
                    miestas = Convert.ToString(item["miestas"]),
                    gatve = Convert.ToString(item["gatve"]),
                    namoNR= Convert.ToInt32(item["namo_numeris"]),
                    rajonas = Convert.ToString(item["rajonas"]),
                    atstumasIkiCentro = Convert.ToDouble(item["atstumas_iki_centro"]),
                    atstumasIkiParko = Convert.ToDouble(item["atstumas_iki_parko"]),
                    idVieta = Convert.ToInt32(item["id_VIETA"]),
                });
            }
            return vietos;
        }
    }
}
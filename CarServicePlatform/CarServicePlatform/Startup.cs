﻿using System.Text;
using CarServicePlatform.Auth;
using CarServicePlatform.Auth.Handlers;
using CarServicePlatform.Auth.Model;
using CarServicePlatform.Data;
using CarServicePlatform.Entities.Users;
using CarServicePlatform.Repository;
using CarServicePlatform.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace CarServicePlatform;

public class Startup
{
    private IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddIdentity<AppUser, IdentityRole>()
                .AddEntityFrameworkStores<CarPlatformDbContext>()
                .AddDefaultTokenProviders();
        services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters.ValidAudience = Configuration["JWT:ValidAudience"];
                        options.TokenValidationParameters.ValidIssuer = Configuration["JWT:ValidIssuer"];
                        options.TokenValidationParameters.IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]));
                    }
                );
        services.AddAuthorization(options =>
        {
            options.AddPolicy(PolicyNames.SameUser, policy => policy.Requirements.Add(new SameUserRequirement()));
            options.AddPolicy(PolicyNames.Chat, policy => policy.Requirements.Add(new ChatConversationRequirement()));
            options.AddPolicy(PolicyNames.User, policy => policy.Requirements.Add(new OperationAuthorizationRequirement()));
        });
        services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
        services.AddSingleton<IAuthorizationHandler, SameUserAuthorizationHandler>();
        services.AddSingleton<IAuthorizationHandler, ConversationAuthorizationHandler>();
        services.AddTransient<IAuthorizationHandler, UserAuthorizationHandler>();

        services.AddTransient<ITokenManager, TokenManager>();
        services.AddDbContext<CarPlatformDbContext>();
        services.AddControllers();
        services.AddTransient<IClientRequestRepository, ClientRequestRepository>();
        services.AddTransient<IPriceOfferRepository, PriceOfferRepository>();
        services.AddTransient<ITransferOfferRepository, TransferOfferRepository>();
        services.AddTransient<IRepairServiceCategoryRepository, RepairServiceCategoryRepository>();
        services.AddTransient<IRepairServiceRepository, RepairServiceRepository>();
        services.AddTransient<IClientRepository, ClientRepository>();
        services.AddTransient<IServiceRepository, ServiceRepository>();
        services.AddTransient<ITrawlRepository, TrawlRepository>();
        services.AddTransient<IAdministratorRepository, AdministratorRepository>();
        services.AddTransient<IRequestedRepairServiceRepository, RequestedRepairServiceRepository>();
        services.AddTransient<IAvailableRepairServiceRepository, AvailableRepairServiceRepository>();
        services.AddTransient<IFileInformationRepository, FileInformationRepository>();
        services.AddTransient<IConversationRepository, ConversationRepository>();
        services.AddTransient<IFileService, FileService>();
        services.AddTransient<DatabaseSeeder, DatabaseSeeder>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        // app.UseStaticFiles(new StaticFileOptions
        // {
        //     FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "Data/Resources/fa96e477-1bd0-45c0-8015-922b075dba97/Profile_Image")),
        //     RequestPath = "/Images"
        // });
        app.UseCors(x => x
                         .AllowAnyOrigin()
                         .AllowAnyMethod()
                         .AllowAnyHeader());
        // app.UseCors(options => options
        //                        .WithOrigins("http://localhost:4200")
        //                        .AllowAnyHeader()
        //                        .AllowAnyMethod()
        // );
        app.UseHttpsRedirection();

        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
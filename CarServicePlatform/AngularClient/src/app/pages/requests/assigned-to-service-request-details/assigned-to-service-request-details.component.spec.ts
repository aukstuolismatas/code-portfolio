import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedToServiceRequestDetailsComponent } from './assigned-to-service-request-details.component';

describe('AssignedToServiceRequestDetailsComponent', () => {
  let component: AssignedToServiceRequestDetailsComponent;
  let fixture: ComponentFixture<AssignedToServiceRequestDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignedToServiceRequestDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedToServiceRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

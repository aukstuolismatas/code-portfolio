import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { AppConstants } from "src/app/app-constants";
import { RolesType } from "../enum.service";
import { AuthService } from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class ServiceGuard {
    constructor(private auth: AuthService, private router: Router){}
    canActivate(): boolean | Observable<boolean>{
        if (this.auth.isLoggedOut() || !this.auth.isService()) {
            this.router.navigate([AppConstants.Login]);
            return false;
        } else {
            return true;
        }
    } 
}

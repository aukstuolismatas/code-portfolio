﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bridge.Auth;
using Bridge.Data.Dtos.Board;
using Bridge.Data.Entities;
using Bridge.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.Controllers
{
    [ApiController]
    [Route("api/boards")]
    public class BoardController : ControllerBase
    {
        private readonly IBoardRepository _boardRepository;
        private readonly IMapper _mapper;

        public BoardController(IBoardRepository boardRepository, IMapper mapper)
        {
            _boardRepository = boardRepository;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<BoardDto>> GetAll()
        {
            List<Board> boards = await _boardRepository.GetAllAsync().ConfigureAwait(false);
            return boards.Select(x => _mapper.Map<BoardDto>(x));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<BoardDto>> Get(int id)
        {
            Board board = await _boardRepository.GetAsync(id).ConfigureAwait(false);
            if (board == null)
            {
                return NotFound($"Board with id {id} not exist");
            }

            return Ok(_mapper.Map<BoardDto>(board));
        }

        [HttpPost]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<BoardDto>> Post(CreateBoardDto boardDto)
        {
            Board board = _mapper.Map<Board>(boardDto);
            await _boardRepository.InsertAsync(board).ConfigureAwait(false);
            return Created($"/api/boards/{board.Id}", _mapper.Map<BoardDto>(board));
        }

        [HttpPut("{id}")]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<BoardDto>> Put(int id, UpdateBoardDto boardDto)
        {
            Board board = await _boardRepository.GetAsync(id).ConfigureAwait(false);
            if (board == null)
            {
                return NotFound($"Board with id {id} not exist");
            }

            _mapper.Map(boardDto, board);
            await _boardRepository.UpdateAsync(board).ConfigureAwait(false);

            return Ok(_mapper.Map<BoardDto>(board));
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult<BoardDto>> Delete(int id)
        {
            Board board = await _boardRepository.GetAsync(id).ConfigureAwait(false);
            if (board == null)
            {
                return NotFound($"Board with id {id} not exist");
            }

            await _boardRepository.DeleteAsync(board).ConfigureAwait(false);

            return NoContent();
        }
    }
}
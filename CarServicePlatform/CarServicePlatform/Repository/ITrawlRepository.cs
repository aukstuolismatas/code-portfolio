﻿using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Repository;

public interface ITrawlRepository
{
    Task<Dictionary<string, TrawlAccount>> GetAllTrawlAccountsAsync();
    Task<TrawlAccount?> GetTrawlAccountAsync(string userId);
    Task UpdateTrawlAccountAsync(TrawlAccount serviceAccount);
    Task DeleteTrawlAccountAsync(TrawlAccount serviceAccount);
}
﻿namespace CarServicePlatform.Data.Dtos.Chat;

public record CreateConversationMessageDto(string SendBy, string Message, string SocketId);
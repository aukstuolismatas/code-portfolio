﻿export class Board {
    id: number;
    boardNr: number;
    deal: string;
    dealer: number;
    vulnerable?: any;
    minimax?: any;
    minimaxScore: number;
}
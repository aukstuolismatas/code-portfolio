﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CarServicePlatform.Auth;
using CarServicePlatform.Entities.RepairServices;
using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Entities.Request;

public class ClientRequest : IUserOwnedResources
{
    public ClientRequest()
    {
        Created = DateTime.Now;
        Status = RequestStatus.Waiting;
    }

    [Required]
    public int ClientRequestId { get; set; }

    public string? Title { get; set; }

    [Required]
    public string Description { get; set; }

    [Required]
    public string Address { get; set; }

    [EnumDataType(typeof(City))]
    [Required]
    public City City { get; set; }

    [EnumDataType(typeof(RequestStatus))]
    [Required]
    public RequestStatus Status { get; set; }

    [Required]
    public string DomainUserId { get; set; }

    public DomainUser DomainUser { get; set; }

    public DateTime Created { get; set; }

    [NotMapped] public List<RequestedRepairService> RequestedServices { get; set; } = new();
    [NotMapped] public int Price { get; set; }
    [NotMapped] public int Duration { get; set; }
    [NotMapped] public OfferStatus OfferStatus { get; set; }
    [NotMapped] public DateTime ArrivalTime { get; set; }
    [NotMapped] public string ServiceAccountId { get; set; }
    [NotMapped] public string ServiceAddress { get; set; }
    [NotMapped] public string ServiceCompanyName { get; set; }
    [NotMapped] public string ServicePhone { get; set; }
    [NotMapped] public string ClientPhone { get; set; }
}
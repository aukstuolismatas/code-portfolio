﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Bridge.Auth;
using Bridge.Auth.Model;
using Bridge.Data.Dtos.Tables;
using Bridge.Data.Entities;
using Bridge.Data.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Bridge.Controllers
{
    [ApiController]
    [Route("api/tournaments/{tournamentId}/tables")]
    public class TableController : ControllerBase
    {
        private readonly ITablesRepository _tablesRepository;
        private readonly ITournamentsRepository _tournamentsRepository;
        private readonly IBoardRepository _boardRepository;
        private readonly IAuthorizationService _authorizationService;
        private readonly IMapper _mapper;

        public TableController(ITablesRepository tablesRepository, IMapper mapper, ITournamentsRepository tournamentsRepository, IBoardRepository boardRepository, IAuthorizationService authorizationService)
        {
            _tablesRepository = tablesRepository;
            _tournamentsRepository = tournamentsRepository;
            _boardRepository = boardRepository;
            _authorizationService = authorizationService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<TableDto>> GetAllAsync(int tournamentId)
        {
            List<Table> tables = await _tablesRepository.GetAllAsync(tournamentId).ConfigureAwait(false);
            return tables.Select(x => _mapper.Map<TableDto>(x));
        }

        [HttpGet("{tableId}")]
        public async Task<ActionResult<TableDto>> GetAsync(int tournamentId, int tableId)
        {
            Table table = await _tablesRepository.GetAsync(tournamentId, tableId).ConfigureAwait(false);
            if (table == null)
            {
                return NotFound($"Table with id {tableId} not exists");
            }

            return Ok(_mapper.Map<TableDto>(table));
        }

        [HttpPost]
        [Authorize(Roles = BridgeRestUserRoles.Table)]
        public async Task<ActionResult<TableDto>> PostAsync(int tournamentId, CreateTableDto tableDto)
        {
            Tournament tournament = await _tournamentsRepository.GetAsync(tournamentId).ConfigureAwait(false);
            if (tournament == null)
            {
                return NotFound($"Tournament with id {tournamentId} not exists");
            }

            Board board = await _boardRepository.GetAsync(tableDto.BoardId).ConfigureAwait(false);
            if (board == null)
            {
                return NotFound($"Board with id {tableDto.BoardId} not exists");
            }

            Table table = _mapper.Map<Table>(tableDto);
            table.TournamentId = tournamentId;
            table.BoardId = board.Id;
            table.UserId = User.FindFirst(CustomClaims.UserId).Value;
            await _tablesRepository.InsertAsync(table).ConfigureAwait(false);

            return Created($"/api/tournaments/{tournamentId}/tables/{table.Id}", _mapper.Map<TableDto>(table));
        }

        [HttpPut("{tableId}")]
        [Authorize(Roles = BridgeRestUserRoles.Table)]
        public async Task<ActionResult<TableDto>> Put(int tournamentId, int tableId, UpdateTableDto tableDto)
        {
            Tournament tournament = await _tournamentsRepository.GetAsync(tournamentId).ConfigureAwait(false);
            if (tournament == null)
            {
                return NotFound($"Tournament with id {tournamentId} not exists");
            }

            Table oldTable = await _tablesRepository.GetAsync(tournamentId, tableId).ConfigureAwait(false);
            if (oldTable == null)
            {
                return NotFound($"Tournament with id {tournamentId} and table with {tableId} not exists");
            }

            AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, oldTable, PolicyNames.SameUser).ConfigureAwait(false);
            if (!authorizationResult.Succeeded)
            {
                return Forbid();
            }

            Board board = await _boardRepository.GetAsync(tableDto.BoardId).ConfigureAwait(false);
            if (board == null)
            {
                return NotFound($"Board with id {tableDto.BoardId} not exists");
            }

            _mapper.Map(tableDto, oldTable);

            await _tablesRepository.UpdateAsync(oldTable).ConfigureAwait(false);

            return Ok(_mapper.Map<TableDto>(oldTable));
        }

        [HttpDelete("{tableId}")]
        [Authorize(Roles = BridgeRestUserRoles.Director)]
        public async Task<ActionResult> Delete(int tournamentId, int tableId)
        {
            Tournament tournament = await _tournamentsRepository.GetAsync(tournamentId).ConfigureAwait(false);
            if (tournament == null)
            {
                return NotFound($"Tournament with id {tournamentId} not exists");
            }

            Table table = await _tablesRepository.GetAsync(tournamentId, tableId).ConfigureAwait(false);
            if (table == null)
            {
                return NotFound($"Tournament with id {tournamentId} and table with id {tableId} not exists");
            }

            await _tablesRepository.DeleteAsync(table).ConfigureAwait(false);

            return NoContent();
        }
    }
}
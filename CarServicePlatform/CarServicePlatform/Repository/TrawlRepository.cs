﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.Users;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class TrawlRepository :ITrawlRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public TrawlRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<Dictionary<string, TrawlAccount>> GetAllTrawlAccountsAsync()
    {
        return await _carPlatformDbContext.TrawlAccounts.ToDictionaryAsync(key => key.Id, x => x).ConfigureAwait(false);
    }

    public async Task<TrawlAccount?> GetTrawlAccountAsync(string userId)
    {
        return await _carPlatformDbContext.TrawlAccounts.FirstOrDefaultAsync(x => x.Id == userId).ConfigureAwait(false);
    }

    public async Task UpdateTrawlAccountAsync(TrawlAccount serviceAccount)
    {
        _carPlatformDbContext.TrawlAccounts.Update(serviceAccount);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }

    public async Task DeleteTrawlAccountAsync(TrawlAccount serviceAccount)
    {
        _carPlatformDbContext.TrawlAccounts.Remove(serviceAccount);
        await _carPlatformDbContext.SaveChangesAsync().ConfigureAwait(false);
    }
}
﻿using CarServicePlatform.Data;
using CarServicePlatform.Entities.Users;
using Microsoft.EntityFrameworkCore;

namespace CarServicePlatform.Repository;

public class AdministratorRepository : IAdministratorRepository
{
    private readonly CarPlatformDbContext _carPlatformDbContext;

    public AdministratorRepository(CarPlatformDbContext carPlatformDbContext)
    {
        _carPlatformDbContext = carPlatformDbContext;
    }

    public async Task<AdministratorAccount?> GetAdministratorUserAsync()
    {
        return await _carPlatformDbContext.AdministratorAccounts.FirstOrDefaultAsync().ConfigureAwait(false);
    }
}
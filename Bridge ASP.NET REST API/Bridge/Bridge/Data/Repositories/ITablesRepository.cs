﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Bridge.Data.Entities;

namespace Bridge.Data.Repositories
{
    public interface ITablesRepository
    {
        Task<Table> GetAsync(int tournamentId, int tableId);
        Task<List<Table>> GetAllAsync(int tournamentId);
        Task InsertAsync(Table table);
        Task UpdateAsync(Table table);
        Task DeleteAsync(Table table);
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NT_Csharp.Models;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace NT_Csharp.Repos
{
    public class IrengimoBusenaRepository
    {
        public List<IrengimoBusena> getBusenos()
        {
            List<IrengimoBusena> busenos = new List<IrengimoBusena>();

            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "SELECT * FROM irengimo_busena";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                busenos.Add(new IrengimoBusena
                {
                    id = Convert.ToInt32(item["id_irengimo_busena"]),
                    pavadinimas = Convert.ToString(item["name"])
                });
            }

            return busenos;
        }
    }
}

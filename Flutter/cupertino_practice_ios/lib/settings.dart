import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './util.dart';

class Settings extends StatefulWidget {
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  int unit = 0;
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    final Map<int, Widget> systems = const <int, Widget>{
      0: Text('Decimal'),
      1: Text('Imperial')
    };
    Util.getSettings().then(updateControl);
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Settings'),
      ),
      child: SafeArea(
          child: Column(
        children: [
          Image.network(
              'https://images.unsplash.com/photo-1451187580459-43490279c0fa?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1472&q=80'),
          Util.paddingTop,
          Row(
            children: [
              SizedBox(
                width: width,
                child: CupertinoSegmentedControl(
                  children: systems,
                  groupValue: unit,
                  onValueChanged: (int value) {
                    Util.saveSettings(value);
                    updateControl(value);
                  },
                ),
              )
            ],
          )
        ],
      )),
    );
  }

  void updateControl(int value) {
    if (value != unit) {
      setState(() {
        unit = value;
      });
    }
  }
}

﻿using System.ComponentModel.DataAnnotations;

namespace CarServicePlatform.Data.Dtos.Account;

public record UpdateTrawlAccountDto([EmailAddress] string Email, [Phone] string PhoneNumber, string Name, string Surname, int MinimalPrice, string? CurrentPassword, string? NewPassword, string City);
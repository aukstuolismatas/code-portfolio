﻿namespace CarServicePlatform.Data.Dtos.ClientRequest;

public record AssignedClientRequestToTrawlDto(int ClientRequestId, string Title, string Description, string Address, string City, string Created, string Status, int Price,
    string ArrivalTime, string OfferStatus, string ServiceCompanyName, string ServiceAddress, string ServicePhone, string ClientPhone);
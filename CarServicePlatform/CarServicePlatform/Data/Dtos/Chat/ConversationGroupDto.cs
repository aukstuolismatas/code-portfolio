﻿namespace CarServicePlatform.Data.Dtos.Chat;

public record ConversationGroupDto(int Id, string ConversationTitle, int ClientRequestId, string UserName);

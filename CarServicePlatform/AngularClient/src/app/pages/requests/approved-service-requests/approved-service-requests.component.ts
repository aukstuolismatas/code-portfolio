import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { ApprovedClientRequest, RegisteredClientRequest } from 'src/app/shared/client-request';
import { RequestStatus } from 'src/app/shared/request-status';

@Component({
  selector: 'app-approved-service-requests',
  templateUrl: './approved-service-requests.component.html',
  styleUrls: ['./approved-service-requests.component.css']
})
export class ApprovedServiceRequestsComponent implements OnInit {
  tabName:string=HeaderConstants.AssignedTab;
  frontTitle:string='Priskirti užsakymai'
  titles:string[]=['Užsakymo nr.', 'Miestas', 'Užsakymo pateikimo data', 'Trukmė', 'Kaina', 'Statusas']
  repairRequestList:ApprovedClientRequest[]=[];

  constructor(private repairRequestService: RepairRequestService, private dataSharingService: DataSharingService, private router: Router) {
    dataSharingService.approvedRequestList.subscribe(value => this.repairRequestList = value);
      this.repairRequestService.retrieveAllAssignedClientRequestForService().subscribe((data:ApprovedClientRequest[])=>{
        this.dataSharingService.approvedRequestList.next(data)
      })
   }

  ngOnInit(): void {
  }

  openItem(index:number){
    this.router.navigate([`${AppConstants.AllAssignedClientRequestForService}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  }

  getStatusDescription(enumString:string){
    return (<any>RequestStatus)[enumString];
  }
}

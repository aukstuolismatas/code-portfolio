﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Bridge.Data.Dtos.Tournaments
{
    public record CreateTournamentDto([Required] string Name, [Required] DateTime Date);

}
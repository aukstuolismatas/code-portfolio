﻿using System.Threading.Tasks;
using Bridge.Auth.Model;
using Microsoft.AspNetCore.Authorization;

namespace Bridge.Auth
{
    public class SameUserAuthorizationHandler : AuthorizationHandler<SameUserRequirement, IUserOwnedResources>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, SameUserRequirement requirement, IUserOwnedResources resource)
        {
            if (context.User.IsInRole(BridgeRestUserRoles.Director) || context.User.FindFirst(CustomClaims.UserId).Value == resource.UserId)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }

    public record SameUserRequirement : IAuthorizationRequirement;
}
﻿import { Component, OnInit } from "@angular/core";
import { TournamentService } from "../services/tournamentService";
import { AuthService } from "../services/authService";
import { Router } from "@angular/router";
import { Tournament } from "../shared/Tournament";

@Component({
    selector: "tournament-list",
    templateUrl: "tournamentListView.html",
    styleUrls: ["tournamentListView.css"]
})
export default class TournamentListView implements OnInit{
    constructor(public  tournamentService: TournamentService, private router: Router, public authService: AuthService) {
    }

    ngOnInit(): void {
        this.tournamentService.loadTournaments().subscribe();
    }

    onDelete(tournamentId: number) {
        this.tournamentService.deleteTournament(tournamentId).subscribe(() => {
            this.ngOnInit();
            this.router.navigate(['']);
        });
    }
    onEdit(tournamentId: number) {
        this.router.navigate(['edit/' + tournamentId]);
    }
    goTo(tournamentId: number) {
        this.router.navigate(['tournaments/', tournamentId, '/tables']);
    }

}
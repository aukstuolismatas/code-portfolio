import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrawlProfileComponent } from './trawl-profile.component';

describe('TrawlProfileComponent', () => {
  let component: TrawlProfileComponent;
  let fixture: ComponentFixture<TrawlProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrawlProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrawlProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

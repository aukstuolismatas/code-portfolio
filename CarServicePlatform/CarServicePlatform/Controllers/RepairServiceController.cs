﻿using AutoMapper;
using CarServicePlatform.Auth;
using CarServicePlatform.Data.Dtos.RepairService;
using CarServicePlatform.Entities.RepairServices;
using CarServicePlatform.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CarServicePlatform.Controllers;

[ApiController]
[Authorize(Roles = AppUserRoles.Admin)]
[Route("/categories/{categoryId}/services")]
public class RepairServiceController : ControllerBase
{
    private readonly IMapper _mapper;
    private readonly IRepairServiceRepository _repairServiceRepository;
    private readonly IRepairServiceCategoryRepository _repairServiceCategoryRepository;

    public RepairServiceController(IMapper mapper, IRepairServiceRepository repairServiceRepository, IRepairServiceCategoryRepository repairServiceCategoryRepository)
    {
        _mapper = mapper;
        _repairServiceRepository = repairServiceRepository;
        _repairServiceCategoryRepository = repairServiceCategoryRepository;
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<IEnumerable<RepairServiceDto>> GetAllServices(int categoryId)
    {
        List<RepairService> services = await _repairServiceRepository.GetAllServicesAsync(categoryId).ConfigureAwait(false);
        return services.Select(x => _mapper.Map<RepairServiceDto>(x));
    }

    [HttpGet("/repair/services")]
    [AllowAnonymous]
    public async Task<IEnumerable<RepairServiceDto>> GetAllServicesGroupedByCategory()
    {
        List<RepairService> services = await _repairServiceRepository.GetAllServicesGroupByCategoriesAsync().ConfigureAwait(false);
        return services.Select(x => _mapper.Map<RepairServiceDto>(x));
    }

    [HttpGet("/repair/services/{clientRequestId}")]
    [AllowAnonymous]
    public async Task<IEnumerable<RepairServiceDto>> GetAllServicesForClientRequest(int clientRequestId)
    {
        List<RepairService> services = await _repairServiceRepository.GetAllServicesForClientRequestAsync(clientRequestId).ConfigureAwait(false);
        return services.Select(x => _mapper.Map<RepairServiceDto>(x));
    }

    [HttpGet("/repair/services/available/{serviceUserId}")]
    [AllowAnonymous]
    public async Task<IEnumerable<RepairServiceDto>> GetAllRepairServicesAvailableForService(string serviceUserId)
    {
        List<RepairService> services = await _repairServiceRepository.GetAllRepairServicesAvailableForServiceAsync(serviceUserId).ConfigureAwait(false);
        return services.Select(x => _mapper.Map<RepairServiceDto>(x));
    }

    [HttpGet("{serviceId}")]
    [AllowAnonymous]
    public async Task<ActionResult<RepairServiceDto>> GetService(int categoryId, int serviceId)
    {
        RepairService? service = await _repairServiceRepository.GetServiceAsync(categoryId, serviceId).ConfigureAwait(false);
        if (service == null)
        {
            return NotFound($"Service with id {serviceId} was not found");
        }

        return Ok(_mapper.Map<RepairServiceDto>(service));
    }

    [HttpPost]
    public async Task<ActionResult<RepairServiceDto>> InsertService(int categoryId, CreateRepairServiceDto createRepairServiceDto)
    {
        RepairServiceCategory? category = await _repairServiceCategoryRepository.GetCategoryAsync(categoryId).ConfigureAwait(false);
        if (category == null)
        {
            return NotFound($"Category with id {categoryId} was not found");
        }

        RepairService service = _mapper.Map<RepairService>(createRepairServiceDto);
        service.RepairServiceCategoryId = categoryId;
        await _repairServiceRepository.InsertServiceAsync(service).ConfigureAwait(false);
        return Created($"/categories/{service.RepairServiceCategoryId}/services/{service.Id}", _mapper.Map<RepairServiceDto>(service));
    }

    [HttpPut("{serviceId}")]
    public async Task<ActionResult<RepairServiceDto>> UpdateService(int categoryId, int serviceId, UpdateRepairServiceDto updateRepairServiceDto)
    {
        RepairServiceCategory? category = await _repairServiceCategoryRepository.GetCategoryAsync(categoryId).ConfigureAwait(false);
        if (category == null)
        {
            return NotFound($"Category with id {categoryId} was not found");
        }

        RepairService? service = await _repairServiceRepository.GetServiceAsync(categoryId, serviceId).ConfigureAwait(false);
        if (service == null)
        {
            return NotFound($"Service with id {serviceId} was not found");
        }

        _mapper.Map(updateRepairServiceDto, service);
        await _repairServiceRepository.UpdateServiceAsync(service).ConfigureAwait(false);
        return Ok(_mapper.Map<RepairServiceDto>(service));
    }

    [HttpDelete("{serviceId}")]
    public async Task<ActionResult<RepairServiceDto>> DeleteService(int categoryId, int serviceId)
    {
        RepairServiceCategory? category = await _repairServiceCategoryRepository.GetCategoryAsync(categoryId).ConfigureAwait(false);
        if (category == null)
        {
            return NotFound($"Category with id {categoryId} was not found");
        }

        RepairService? service = await _repairServiceRepository.GetServiceAsync(categoryId, serviceId).ConfigureAwait(false);
        if (service == null)
        {
            return NotFound($"Service with id {serviceId} was not found");
        }

        await _repairServiceRepository.DeleteServiceAsync(service).ConfigureAwait(false);
        return NoContent();
    }
}
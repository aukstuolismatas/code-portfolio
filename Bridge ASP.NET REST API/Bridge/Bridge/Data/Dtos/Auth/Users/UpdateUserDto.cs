﻿using System.ComponentModel.DataAnnotations;
using Bridge.Data.Entities;

namespace Bridge.Data.Dtos.Users
{
    public record UpdateUserDto([Required] string Password, [Required] UserRole Role);
}
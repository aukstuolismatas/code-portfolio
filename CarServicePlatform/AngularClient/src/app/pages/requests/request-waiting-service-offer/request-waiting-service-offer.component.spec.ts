import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestWaitingServiceOfferComponent } from './request-waiting-service-offer.component';

describe('RequestWaitingServiceOfferComponent', () => {
  let component: RequestWaitingServiceOfferComponent;
  let fixture: ComponentFixture<RequestWaitingServiceOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestWaitingServiceOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestWaitingServiceOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

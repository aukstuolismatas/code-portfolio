﻿using System.ComponentModel.DataAnnotations.Schema;
using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Entities.Chat;

public class ConversationMessage
{
    public ConversationMessage()
    {
        CreatedAt = DateTime.Now;
    }

    public int Id { get; set; }

    [ForeignKey(nameof(DomainUser))]
    public string SendBy { get; set; }

    public string Message { get; set; }
    public DateTime CreatedAt { get; }
    public int ConversationGroupId { get; set; }
    public ConversationGroup ConversationGroup { get; set; }
}
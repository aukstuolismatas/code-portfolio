import scrapy
import scrapy.signals
import scrapy.exceptions
import scrapy.core.engine
from scrapy.spiders import SitemapSpider
from scrapy.spiders import Spider
from scrapy.http import Request, XmlResponse, TextResponse
from scrapy.utils.sitemap import Sitemap, sitemap_urls_from_robots
from scrapy.utils.gz import gunzip, is_gzipped
import re
from bs4 import BeautifulSoup
import requests
import time
import csv
from datetime import datetime
import json
import os

DATE_FILE='date.json'
URL_FILE='url.json'
URL_FILE_SCRAPED='url_scraping.json'
WEBSITE_URL = "https://www.firmy.cz"

class GetpagesfromsitemapSpider(SitemapSpider):
    name = "siteSpyder"
    handle_httpstatus_list = [404]
    # download_delay=1000
    allowed_domains = ["firmy.cz"] # crawl only on this domain
    fieldnames = ['City', 'Company', 'Category', 'Website', 'Phone', 'Email'] # titles of columns in csv file

    # Going throw all sitemap links available
    def _parse_sitemap(self, response):
        dic={}
        if response.url.endswith('/robots.txt'):
            for url in sitemap_urls_from_robots(response.body):
                yield Request(url, callback=self._parse_sitemap)
        else:
            body = self._get_sitemap_body( response)
            if body is None:
                self.logger.info('Ignoring invalid sitemap: %s', response.url)
                return
            s = Sitemap(body)
            it=self.sitemap_filter(s) # filtering the entries in order to get the most useful entries
            if s.type == 'sitemapindex': # points to sitemaps.xml
                for loc in iterloc(s, self.sitemap_alternate_links):
                    if any(x.search(loc) for x in self._follow):
                        yield Request(loc, callback=self._parse_sitemap)
            elif s.type == 'urlset': # going deeper in with every link in xml
                dates_file=open(DATE_FILE, mode='a', encoding='utf-8')
                url_file=open(URL_FILE, mode='a', encoding='utf-8')
                for item in it:
                    date_time = datetime.strptime(item['lastmod'], '%Y-%m-%dT%H:%M:%S%z')
                    loc = item['loc']
                    temp_url={}
                    temp_date={}
                    temp_date[loc] = str(date_time)
                    temp_url[loc] = False
                    if loc in self.urlList and loc in self.dateList and date_time.year>2018:
                        if date_time > self.dateList[loc]:
                            dates_file.write(json.dumps(temp_date) + '\n')
                            url_file.write(json.dumps(temp_url) + '\n')
                        else:
                            dates_file.write(json.dumps(temp_date) + '\n')
                            temp_url[loc] = self.urlList[loc]
                            url_file.write(json.dumps(temp_url) + '\n')
                    elif date_time.year>2018:
                        dates_file.write(json.dumps(temp_date) + '\n')
                        url_file.write(json.dumps(temp_url) + '\n')

                dates_file.close()
                url_file.close()

    # Filter sites only take which have higher priority than 0.5
    def sitemap_filter(self, entries):
        for entry in entries:
            priority = entry['priority']
            loc = entry['loc']
            if float(priority) >= 0.5 and loc.startswith(WEBSITE_URL+"/detail"):
                yield entry

    # Merge url files to one
    def merge_files(self, filename):
        scraped_urls = loadToDictionary(filename)
        for url in scraped_urls:
            self.urlList[url] = scraped_urls[url]

    def __init__(self, spider=None, *a, **kw):
            super(GetpagesfromsitemapSpider, self).__init__(*a, **kw)
            self.spider = spider
            # Loads data from files
            self.dateList={}
            self.urlList={}
            if os.path.isfile(DATE_FILE) and os.path.isfile(URL_FILE):
                self.dateList=loadToDictionary(DATE_FILE)
                self.urlList=loadToDictionary(URL_FILE)
            if os.path.isfile(URL_FILE_SCRAPED):
                self.merge_files(URL_FILE_SCRAPED)
            with open(DATE_FILE, mode='w', encoding='utf-8') as file:pass
            with open(URL_FILE, mode='w', encoding='utf-8') as file:pass
            self.counter=0
            l = []
            resp = requests.head(WEBSITE_URL + "/sitemap.xml")
            if (resp.status_code != 404):
                l.append(resp.url)
            else:
                resp = requests.head(WEBSITE_URL + "/robots.txt")
                if (resp.status_code == 200):
                    l.append(resp.url)
            self.sitemap_urls = l
            print(self.sitemap_urls)

# Returning the link in <loc> element
def iterloc(it, alt=False):
    for d in it:
        yield d['loc']

        # Also consider alternate URLs (xhtml:link rel="alternate")
        if alt and 'alternate' in d:
            for l in d['alternate']:
                yield l

def loadToDictionary(filename):
    d={}
    with open(filename, mode='r', encoding='utf-8') as file:
        for line in file:
            js = json.loads(line)
            d.update(js)
            if filename==DATE_FILE:
                row = line.rstrip().split("\":")
                key = row[0].strip('{"')
                d[key]=datetime.strptime(d[key], '%Y-%m-%d %H:%M:%S%z')
    return d



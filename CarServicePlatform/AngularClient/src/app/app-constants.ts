export class AppConstants {
    public static Profile = "profilis"
    public static Login = "prisijungti"
    public static Register = "registracija"
    public static Home = "";
}

export class RegexPatterns{
    public static Phone = "^\\+[0-9]{11}$";
    public static Email = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
    public static Passowrd = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$";

}

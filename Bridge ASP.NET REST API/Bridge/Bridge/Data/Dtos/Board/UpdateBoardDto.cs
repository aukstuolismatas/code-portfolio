﻿using System.ComponentModel.DataAnnotations;
using Bridge.Data.Entities;

namespace Bridge.Data.Dtos.Board
{
    public record UpdateBoardDto(string Deal, [Required] PairPosition Dealer, string Vulnerable, string Minimax, int MinimaxScore);
}
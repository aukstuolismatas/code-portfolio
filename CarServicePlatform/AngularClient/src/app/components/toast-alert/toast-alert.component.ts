import { Component, Inject, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-toast-alert',
  templateUrl: './toast-alert.component.html',
  styleUrls: ['./toast-alert.component.css']
})

export class ToastAlertComponent implements OnInit {
  @Input('message') errorMessage ='';
  @Input('color') backgroundColor ='bg-red-500';
  constructor() {
   }

  ngOnInit(): void {
  }

}

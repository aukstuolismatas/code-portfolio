﻿namespace CarServicePlatform.Entities;

public enum City
{
    Kaunas,
    Vilnius,
    Klaipėda,
    Šiauliai,
    Panevežys
}
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderConstants } from 'src/app/app-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { TrawlTransferRequestService } from 'src/app/services/trawl-transfer-request.service';
import { RejectedClientRequestTrawl } from 'src/app/shared/client-request';

@Component({
  selector: 'app-rejected-trawl',
  templateUrl: './rejected-trawl.component.html',
  styleUrls: ['./rejected-trawl.component.css']
})
export class RejectedTrawlComponent implements OnInit {
  tabName:string=HeaderConstants.InActiveTab;
  frontTitle:string='Nepatvirtinti užsakymai'
  titles:string[]=['Miestas', 'Data', 'Laikas', 'Kaina','Statusas']
  rejectedRepairRequestList:RejectedClientRequestTrawl[]=[];

  constructor(private trawlTransferService: TrawlTransferRequestService, private dataSharingService: DataSharingService, private router: Router) {
    dataSharingService.rejectedTrawlRequestList.subscribe(value => this.rejectedRepairRequestList = value);
      this.trawlTransferService.retrieveAllRejectedClientRequestForTrawl().subscribe((data:RejectedClientRequestTrawl[])=>{
        this.dataSharingService.rejectedTrawlRequestList.next(data)
      })
   }

  ngOnInit(): void {
  }

  openItem(index:number){
    // this.router.navigate([`${AppConstants.AllClientRequest}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  }
}

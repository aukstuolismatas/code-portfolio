﻿namespace Bridge.Data.Entities
{
    public class Board
    {
        public int Id { get; set; }
        public int BoardNr { get; set; }
        public string Deal { get; set; }
        public PairPosition Dealer { get; set; }
        public string Vulnerable { get; set; }
        public string Minimax { get; set; }
        public int MinimaxScore { get; set; }
    }
}
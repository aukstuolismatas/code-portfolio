﻿using System.Collections.Generic;

namespace Bridge.Auth
{
    public static class BridgeRestUserRoles
    {
        public const string Director = nameof(Director);
        public const string Table = nameof(Table);

        public static readonly IReadOnlyCollection<string> All = new[] { Director, Table };
    }
}
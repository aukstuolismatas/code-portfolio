import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrawlRequestDetailsComponent } from './trawl-request-details.component';

describe('TrawlRequestDetailsComponent', () => {
  let component: TrawlRequestDetailsComponent;
  let fixture: ComponentFixture<TrawlRequestDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrawlRequestDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrawlRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

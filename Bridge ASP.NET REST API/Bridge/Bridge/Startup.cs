using System.Text;
using Bridge.Auth;
using Bridge.Auth.Model;
using Bridge.Data;
using Bridge.Data.Dtos.Auth;
using Bridge.Data.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;

namespace Bridge
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddIdentity<BridgeRestUser, IdentityRole>()
                    .AddEntityFrameworkStores<BridgeRestContext>()
                    .AddDefaultTokenProviders();
            services.AddAuthentication(options =>
                    {
                        options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                        options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                    })
                    .AddJwtBearer(options =>
                        {
                            options.TokenValidationParameters.ValidAudience = Configuration["JWT:ValidAudience"];
                            options.TokenValidationParameters.ValidIssuer = Configuration["JWT:ValidIssuer"];
                            options.TokenValidationParameters.IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:Secret"]));
                        }
                    );
            services.AddAuthorization(options =>
            {
                options.AddPolicy(PolicyNames.SameUser, policy => policy.Requirements.Add(new SameUserRequirement()));
            });
            services.AddDbContext<BridgeRestContext>();

            services.AddSingleton<IAuthorizationHandler, SameUserAuthorizationHandler>();

            services.AddAutoMapper(typeof(Startup));
            services.AddTransient<ITokenManager, TokenManager>();
            services.AddControllers();
            services.AddTransient<ITournamentsRepository, TournamentsRepository>();
            services.AddTransient<ITablesRepository, TablesRepository>();
            services.AddTransient<ITablePairRepository, TablePairRepository>();
            services.AddTransient<IUsersRepository, UsersRepository>();
            services.AddTransient<IPairsRepository, PairsRepository>();
            services.AddTransient<IBoardRepository, BoardRepository>();
            services.AddTransient<DatabaseSeeder, DatabaseSeeder>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors(x => x
                             .AllowAnyOrigin()
                             .AllowAnyMethod()
                             .AllowAnyHeader());
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

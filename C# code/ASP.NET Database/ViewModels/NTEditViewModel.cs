using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using NT_Csharp.Models;

namespace NT_Csharp.ViewModels
{
    public class NTEditViewModel
    {
        [DisplayName("NR")]
        [Required]
        public int id { get; set; }
        [DisplayName("Pastatymo metai")]
        [Required]
        public int pastatymoMetai { get; set; }
        [DisplayName("Kambariu skaicius")]
        [Required]
        public int kambariuSkaicius { get; set; }
        [DisplayName("Bendras plotas")]
        [Required]
        public double bendrasPlotas { get; set; }
        [DisplayName("Aukštų skaičius")]
        [Required]
        public int aukstuSkaicius { get; set; }
        [DisplayName("Baldai")]
        [Required]
        public bool baldai { get; set; }
        [DisplayName("Signalizacija")]
        public bool signalizacija { get; set; }
        [DisplayName("Garažas")]
        public bool garazas { get; set; }
        [DisplayName("Irengimo būsena")]
        public int busena { get; set; }
        [DisplayName("Tipas")]
        [Required]
        public int tipas { get; set; }
        [DisplayName("Butas")]
        public Nullable<int> fk_butas { get; set; }
        [DisplayName("Vieta")]
        public int fk_vieta { get; set; }
        [DisplayName("Namas")]
        public Nullable<int> fk_namas { get; set; }

        public IList<SelectListItem> IrengimoBusenosList { get; set; }
        public IList<SelectListItem> TipasList { get; set; }
        public IList<SelectListItem> ButasList { get; set; }
        public IList<SelectListItem> VietaList { get; set; }
        public IList<SelectListItem> NamasList { get; set; }
    }
}
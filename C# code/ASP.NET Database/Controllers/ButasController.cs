using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Csharp.Repos;
using NT_Csharp.Models;

namespace NT_Csharp.Controllers
{
    public class ButasController : Controller
    {
        // GET: Butas
        ButasRepository butasRepository = new ButasRepository();
        public ActionResult Index()
        {
            return View(butasRepository.getButai());
        }


        // GET: Butas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Butas/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Butas/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Butas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Butas/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Butas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NT_Csharp.Repos;
using NT_Csharp.Models;

namespace NT_Csharp.Controllers
{
    
    public class NTAgenturaController : Controller
    {
        NTAgenturaRepository nTAgenturaRepository = new NTAgenturaRepository();
        // GET: NTAgentura
        public ActionResult Index()
        {
            return View(nTAgenturaRepository.getNTAgenturas());
        }

        // GET: NTAgentura/Edit/5
        public ActionResult Edit(string id)
        {
            return View(nTAgenturaRepository.getNTAgentura(id));
        }

        // POST: NTAgentura/Edit/5
        [HttpPost]
        public ActionResult Edit(string id, NTAgentura collection)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    nTAgenturaRepository.updateAgentura(collection);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View(collection);
            }
        }

        // GET: NTAgentura/Delete/5
        public ActionResult Delete(string id)
        {
            return View(nTAgenturaRepository.getNTAgentura(id));
        }

        // POST: NTAgentura/Delete/5
        [HttpPost]
        public ActionResult Delete(string id, NTAgentura collection)
        {
            try
            {
                bool naudojama = false;
                if (nTAgenturaRepository.getAgenturaSutarciuCount(id) > 0)
                {
                    naudojama = true;
                    ViewBag.naudojama = "Negalima pašalinti agentūra turėjo sudarytų sutarčių";
                    return View(nTAgenturaRepository.getNTAgentura(id));
                }
                if (!naudojama)
                {
                    nTAgenturaRepository.deleteAgentura(id);
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿namespace Bridge.Data.Dtos.Tables
{
    public record TableDto(int Id, int TournamentId, int TableNr, int BoardId, string UserId);
}
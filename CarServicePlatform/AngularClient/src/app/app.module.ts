import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './services/auth/auth.interceptor';
import { JwtService } from './services/auth/jwt.service';
import { AuthGuard } from './services/auth/auth-guard';
import { AuthService } from './services/auth/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { ToastAlertComponent } from './components/toast-alert/toast-alert.component';
import { ChatComponent } from './pages/chat/chat.component';
import { PusherService } from './services/pusher.service';
import { ChatService } from './services/chat.service';
import { ProfileService } from './services/profile.service';
import { ErrorHandleService } from './services/error-handle.service';
import { FileService } from './services/file.service';
import { DataSharingService } from './services/data-sharing.service';
import { RegistrationCategoryComponent } from './pages/registration-category/registration-category.component';
import { RegisterServiceComponent } from './pages/register-service/register-service.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { EnumService } from './services/enum.service';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { LoginGuard } from './services/auth/login-guard';
import { AdminGuard } from './services/auth/admin-guard';
import { ModalComponent } from './components/modal/modal.component';
import { CommonModule } from '@angular/common';
import { ConfirmationAlertComponent } from './components/confirmation-alert/confirmation-alert.component';
import { RepairRequestComponent } from './pages/repair-request/repair-request.component';
import { ClientGuard } from './services/auth/client-guard';
import { AllClientRequestComponent } from './pages/all-client-request/all-client-request.component';
import { RequestDetailsComponent } from './pages/request-details/request-details.component';
import { ServiceTabComponent } from './components/service-tab/service-tab.component';
import { RequestWaitingServiceOfferComponent } from './pages/requests/request-waiting-service-offer/request-waiting-service-offer.component';
import { RequestWaitingClientAcceptanceComponent } from './pages/requests/request-waiting-client-acceptance/request-waiting-client-acceptance.component';
import { ApprovedServiceRequestsComponent } from './pages/requests/approved-service-requests/approved-service-requests.component';
import { EndedServiceRequestsComponent } from './pages/requests/ended-service-requests/ended-service-requests.component';
import { ServiceGuard } from './services/auth/service-guard';
import { AssignedToServiceRequestDetailsComponent } from './pages/requests/assigned-to-service-request-details/assigned-to-service-request-details.component';
import { ServiceProfileComponent } from './pages/service-profile/service-profile.component';
import { RegisterTrawlComponent } from './pages/register-trawl/register-trawl.component';
import { WaitingForTrawlOfferComponent } from './pages/requests/trawl-requests/waiting-for-trawl-offer/waiting-for-trawl-offer.component';
import { WaitingForClientResponseComponent } from './pages/requests/trawl-requests/waiting-for-client-response/waiting-for-client-response.component';
import { AssignedToTrawlComponent } from './pages/requests/trawl-requests/assigned-to-trawl/assigned-to-trawl.component';
import { RejectedTrawlComponent } from './pages/requests/trawl-requests/rejected-trawl/rejected-trawl.component';
import { TrawlGuard } from './services/auth/trawl-guard';
import { TrawlTabComponent } from './components/trawl-tab/trawl-tab.component';
import { TrawlTransferRequestService } from './services/trawl-transfer-request.service';
import { TrawlRequestDetailsComponent } from './pages/requests/trawl-requests/trawl-request-details/trawl-request-details.component';
import { AssignedTrawlRequestDetailsComponent } from './pages/requests/trawl-requests/assigned-trawl-request-details/assigned-trawl-request-details.component';
import { AdminTabComponent } from './components/admin-tab/admin-tab.component';
import { ServiceAccountsComponent } from './pages/accounts/service-accounts/service-accounts.component';
import { TrawlAccountsComponent } from './pages/accounts/trawl-accounts/trawl-accounts.component';
import { DetailTrawlAccountsComponent } from './pages/accounts/detail-trawl-accounts/detail-trawl-accounts.component';
import { TrawlProfileComponent } from './pages/trawl-profile/trawl-profile.component';
import { DetailsServiceAccountsComponent } from './pages/accounts/details-service-accounts/details-service-accounts.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { ImageViewerComponent } from './components/image-viewer/image-viewer.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    PageNotFoundComponent,
    ProfileComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ToastAlertComponent,
    ChatComponent,
    RegistrationCategoryComponent,
    RegisterServiceComponent,
    DropdownComponent,
    AccountsComponent,
    ModalComponent,
    ConfirmationAlertComponent,
    RepairRequestComponent,
    AllClientRequestComponent,
    RequestDetailsComponent,
    ServiceTabComponent,
    RequestWaitingServiceOfferComponent,
    RequestWaitingClientAcceptanceComponent,
    ApprovedServiceRequestsComponent,
    EndedServiceRequestsComponent,
    AssignedToServiceRequestDetailsComponent,
    ServiceProfileComponent,
    RegisterTrawlComponent,
    WaitingForTrawlOfferComponent,
    WaitingForClientResponseComponent,
    AssignedToTrawlComponent,
    RejectedTrawlComponent,
    TrawlTabComponent,
    TrawlRequestDetailsComponent,
    AssignedTrawlRequestDetailsComponent,
    AdminTabComponent,
    ServiceAccountsComponent,
    TrawlAccountsComponent,
    DetailTrawlAccountsComponent,
    TrawlProfileComponent,
    DetailsServiceAccountsComponent,
    CarouselComponent,
    ImageViewerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    BrowserAnimationsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    JwtService,
    AuthGuard,
    LoginGuard,
    AdminGuard,
    AuthService,
    ClientGuard,
    ServiceGuard,
    TrawlGuard,
    PusherService,
    ChatService,
    ProfileService,
    ErrorHandleService,
    FileService,
    DataSharingService,
    EnumService,
    TrawlTransferRequestService
  ],
  bootstrap: [AppComponent],
  exports: [ModalComponent],
})
export class AppModule { }

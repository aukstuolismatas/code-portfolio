﻿using System;
using System.Linq;
using System.Security.Cryptography;

namespace Bridge.Data
{
    public class Hasher
    {
        private const int SaltLenght = 128;
        private const int SubkeyLength = 256;
        private const int IterCount = 770;

        public static string HashPassword(string password)
        {
            byte[] salt;
            byte[] subkey;
            using (Rfc2898DeriveBytes deriveBytes = new(password, SaltLenght, IterCount))
            {
                salt = deriveBytes.Salt;
                subkey = deriveBytes.GetBytes(SubkeyLength);
            }

            byte[] outputBytes = new byte[1 + SaltLenght + SubkeyLength];
            Buffer.BlockCopy(salt, 0, outputBytes, 1, SaltLenght);
            Buffer.BlockCopy(subkey, 0, outputBytes, 1 + SaltLenght, SubkeyLength);
            return Convert.ToBase64String(outputBytes);
        }

        public static bool VerifyHashedPassword(string hashedPassword, string password)
        {
            byte[] hashedPasswordBytes = Convert.FromBase64String(hashedPassword);

            if (hashedPasswordBytes.Length != (1 + SaltLenght + SubkeyLength) || hashedPasswordBytes[0] != 0x00)
            {
                return false;
            }

            byte[] salt = new byte[SaltLenght];
            Buffer.BlockCopy(hashedPasswordBytes, 1, salt, 0, SaltLenght);
            byte[] storedSubkey = new byte[SubkeyLength];
            Buffer.BlockCopy(hashedPasswordBytes, 1 + SaltLenght, storedSubkey, 0, SubkeyLength);

            byte[] generatedSubkey;
            using (Rfc2898DeriveBytes deriveBytes = new(password, salt, IterCount))
            {
                generatedSubkey = deriveBytes.GetBytes(SubkeyLength);
            }

            return storedSubkey.SequenceEqual(generatedSubkey);
        }
    }
}
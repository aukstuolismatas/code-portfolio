﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using CarServicePlatform.Auth.Model;
using CarServicePlatform.Data.Dtos.Auth;
using CarServicePlatform.Entities.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using JwtRegisteredClaimNames = Microsoft.IdentityModel.JsonWebTokens.JwtRegisteredClaimNames;

namespace CarServicePlatform.Auth;

public interface ITokenManager
{
    Task<SuccessfulLoginResponseDto> CreateAccessTokenAsync(AppUser user);
}

public class TokenManager : ITokenManager
{
    private readonly UserManager<AppUser> _userManager;
    private readonly string _validIssuer;
    private readonly string _validAudience;
    private readonly SymmetricSecurityKey _authSigningKey;

    public TokenManager(IConfiguration configuration, UserManager<AppUser> userManager)
    {
        _userManager = userManager;
        _validAudience = configuration["JWT:ValidAudience"];
        _validIssuer = configuration["JWT:ValidIssuer"];
        _authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]));
    }

    //TODO CHECK IF IT IS SAFE TO HAVE ALL CLAIMSTYPES
    public async Task<SuccessfulLoginResponseDto> CreateAccessTokenAsync(AppUser user)
    {
        IList<string> userRoles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
        List<Claim> authClaims = new()
        {
            // new(ClaimTypes.Name, user.Name),
            // new(ClaimTypes.Surname, user.Surname),
            new(ClaimTypes.Email, user.Email),
            new(ClaimTypes.MobilePhone, user.PhoneNumber),
            new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            new(CustomClaims.UserId, user.Id.ToString()),
        };

        authClaims.AddRange(userRoles.Select(userRole => new Claim(ClaimTypes.Role, userRole)));
        JwtSecurityToken accessSecurityToken = new(
            issuer: _validIssuer,
            audience: _validAudience,
            expires: DateTime.UtcNow.AddHours(1),
            claims: authClaims,
            signingCredentials: new SigningCredentials(_authSigningKey, SecurityAlgorithms.HmacSha256)
        );
        return new SuccessfulLoginResponseDto(new JwtSecurityTokenHandler().WriteToken(accessSecurityToken), accessSecurityToken.ValidTo);
    }
}
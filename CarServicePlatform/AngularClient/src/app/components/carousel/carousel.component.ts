import { animate, style, transition, trigger, useAnimation } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { fadeIn, fadeOut} from "../../shared/animations-types";
@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css'],
  animations: [
    trigger('carouselAnimation', [
      transition('void => *', [useAnimation(fadeIn, {params:{time:'600ms'}})]),
      transition('* => void', [useAnimation(fadeOut, {params:{time:'600ms'}})])
      ])
  ]
})
export class CarouselComponent implements OnInit {
  currentSlide=0;
  @Input('slides') slides : SafeUrl[]=[]
  @Input('open') open: boolean = false;
  @Output() onSlideChange = new EventEmitter<number>();

  @Output('clickOnImage') clickOnImage = new EventEmitter();
  constructor() { 
  }

  ngOnInit(): void {
  }

  onPreviousClick() {
    const previous = this.currentSlide - 1;
    this.currentSlide = previous < 0 ? this.slides.length - 1 : previous;
    console.log("previous clicked, new current slide is: ", this.currentSlide);
    this.onSlideChange.emit(this.currentSlide);
  }

  onNextClick() {
    const next = this.currentSlide + 1;
    this.currentSlide = next === this.slides.length ? 0 : next;
    console.log("next clicked, new current slide is: ", this.currentSlide);
    this.onSlideChange.emit(this.currentSlide);
  }

}

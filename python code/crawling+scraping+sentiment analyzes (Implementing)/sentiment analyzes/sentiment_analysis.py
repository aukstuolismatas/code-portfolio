import stop_words
from polyglot.downloader import downloader
from polyglot.text import Text
import nltk
from string import punctuation
import math
import csv
from collections import Counter
import operator

AFINN_FILE= './cz_vocabulary/afinn.cz.txt'
POS_FILE='./cz_vocabulary/readable_pos_words_list.txt'
NEG_FILE='./cz_vocabulary/readable_neg_words_list.txt'
CSV_FILE='./cz_vocabulary/sublex_1_0.csv'
REVIEWS_FILE='companies_info_with_reviews.csv'
RESULT_FILE='companies_final.csv'
fieldnames = ['Firmy.cz', 'City', 'Company', 'Category', 'Website', 'Phone', 'Email', 'Rate', 'Reviews']

def read_affinn():
    with open(AFINN_FILE, mode='r', encoding='utf-8') as input:
        for line in input.readlines()[1:]:
            line_parts=line.split(',')
            if line_parts[2].strip()=="1":
                positive_vocab.add(line_parts[1])
            elif line_parts[2].strip()=="-1":
                negative_vocab.add(line_parts[1])

def read_pos_neg(filename):
    temp_set=set()
    with open(filename, mode='r', encoding='utf-8') as input:
        for line in input.readlines():
            line_parts=line.split()
            if line_parts[1].strip()=='cs' or line_parts[1].strip()=='ce' or line_parts[1].strip()=='ms' or line_parts[1].strip()=='hr' or line_parts[1].strip()=='eu':
                temp_set.add(line_parts[0])

    return temp_set

def read_csv():
    with open(CSV_FILE, mode='r', encoding='utf-8') as input:
        for line in input.readlines():
            line_parts=line.split()
            if line_parts[3].split()=='NEG':
                negative_vocab.add(line_parts[2])
            else:
                positive_vocab.add(line_parts[2])

def load_data_from_csv(filename):
    with open(filename, 'r', newline='', encoding="utf-8") as input_file:
        reader = csv.DictReader(input_file, fieldnames=fieldnames, delimiter=',')
        data = list(reader)

# Converting reviews to python array
    for i in range(len(data)-1,-1,-1 ):
        arr = data[i]['Reviews'].strip('[\'').strip('\']').split('\', \'')
        data[i]['Reviews'] = arr
        data[i]['Rate']=float(data[i]['Rate'])
    return data


def PMI(t1, t2, fTuples, fTokens):
    if fTuples.freq((t1,t2))>0:
        pp = fTuples.freq((t1, t2))
    else:
        pp=fTuples.freq((t2,t1))
    den=(fTokens.freq(t1)*fTokens.freq(t2))
    return (math.log2(pp/den)) if den>0 and pp>0 else 0

def SO(t):
    text = Text(t)
    # print(text)
    tokens = [w.lower() for w in text.words if w.lower() not in stops and w not in punctuation]
    sentences = text.sentences
    tuples = []
    for sentence in sentences:
        for word in set(sentence.words):
            if word.lower() not in stops and word not in punctuation:
                for w2 in set(sentence.words):
                    if w2.lower() not in stops and w2 not in punctuation:
                        tuples.append((word, w2))

    fTuples = nltk.FreqDist(tuples)
    fTokens = nltk.FreqDist(tokens)

    results = {}
    for t1 in set(tokens):
        positives = sum(PMI(t1, t2, fTuples, fTokens) for t2 in positive_vocab)
        negatives = sum(PMI(t1, t2, fTuples, fTokens) for t2 in negative_vocab)
        results[t1] = positives - negatives

    results_sum = sum(results.values())
    return results_sum

def evaluate_rate():
    max_item=max(list_companies, key=lambda x:x['Rate'])
    min_item=min(list_companies, key=lambda x:x['Rate'])
    max_rate = max_item['Rate']
    min_rate = min_item['Rate']
    print(max_rate)
    print(min_rate)
    positive_step=max_rate/5
    negative_step=min_rate/5
    print('nstep:'+str(negative_step))
    print('pstep'+str(positive_step))
    for company in list_companies:
        if company['Rate']!=0:
            interval=0
            if company['Rate']>0:
                interval += positive_step
                while True:
                    if company['Rate']<=interval:
                        rate=((company['Rate']/max_rate)*5)+5
                        company['Rate']=round(rate, 3)
                        break
                    interval += positive_step
            else:
                interval += negative_step
                while True:
                    if company['Rate']>=interval:
                        rate=5-((company['Rate']/min_rate)*5)
                        company['Rate'] = round(rate, 2)
                        break
                    interval += negative_step

        else:
            company['Rate']=5

def write_to_file(list):
    with open(RESULT_FILE, 'w', newline='', encoding="utf-8") as outputfile:
        writer=csv.DictWriter(outputfile, fieldnames=fieldnames, delimiter=',')
        for item in list:
            writer.writerow(item)

# Reading from file
list_companies=load_data_from_csv(REVIEWS_FILE)

positive_vocab=set()
negative_vocab=set()
# Making the vocabulary of negative and positive words from files
read_affinn()
positive_vocab.update(read_pos_neg(POS_FILE))
negative_vocab.update(read_pos_neg(NEG_FILE))
# read_csv()
#----------------------------------------------------

stops =stop_words.get_stop_words('cz') #Stop words in czech language using PyPI library
print(list_companies[1]['Reviews'][2])

with open("rate.txt", mode='w') as out:
    for company in list_companies:
        for review in company['Reviews']:
            company['Rate'] += SO(review)
        out.write(str(company['Rate'])+'\n')
        print(company['Rate'])

print('------------------------------------------')
evaluate_rate()

sorted_list=sorted(list_companies, key=lambda x: x['Rate'],reverse=False)
write_to_file(sorted_list)


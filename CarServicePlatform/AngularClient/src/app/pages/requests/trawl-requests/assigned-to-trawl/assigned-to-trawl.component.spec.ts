import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedToTrawlComponent } from './assigned-to-trawl.component';

describe('AssignedToTrawlComponent', () => {
  let component: AssignedToTrawlComponent;
  let fixture: ComponentFixture<AssignedToTrawlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignedToTrawlComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedToTrawlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

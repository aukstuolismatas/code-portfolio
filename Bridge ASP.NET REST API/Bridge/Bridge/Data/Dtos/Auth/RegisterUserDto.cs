﻿using System.ComponentModel.DataAnnotations;

namespace Bridge.Data.Dtos.Auth
{
    public record RegisterUserDto([Required] string UserName, [Required] string Password);
}
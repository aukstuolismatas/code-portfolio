import { transition, trigger, useAnimation } from '@angular/animations';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SafeUrl } from '@angular/platform-browser';
import { fadeIn, fadeOut, flipIn, scaleIn, scaleOut} from "../../shared/animations-types";


@Component({
  selector: 'app-image-viewer',
  templateUrl: './image-viewer.component.html',
  styleUrls: ['./image-viewer.component.css'],
  animations:[
    trigger('viewerAnimation', [
      transition('void => *', [useAnimation(flipIn, {params:{time:'100ms'}})]),
      transition('* => void', [useAnimation(flipIn, {params:{time:'100ms'}})])
      ])
  ]
})
export class ImageViewerComponent implements OnInit {

  @Input() open: boolean = false;
  @Input() imageURL: SafeUrl | undefined;

  @Output() close = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}

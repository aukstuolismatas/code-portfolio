﻿namespace CarServicePlatform.Data.Dtos.ClientRequest;

public record ApprovedClientRequestDto(int ClientRequestId, string Title, string Description, string Address, string City, string Created, string Status, int Price, int Duration);
import 'package:drift/drift.dart';

class BlogPosts extends Table {
  IntColumn get id => integer().autoIncrement().call();
  TextColumn get name => text().withLength(min: 1, max: 80).call();
  TextColumn get content => text().nullable().call();
  DateTimeColumn get date => dateTime().nullable().call();
}

﻿using CarServicePlatform.Data.Dtos.Account;

namespace CarServicePlatform.Data.Dtos.Auth.User;

public record AppClientUserDto(string Email, string PhoneNumber, ClientAccountDto DomainUser);
// public record AppClientUserDto(string Id, string UserName, string Email, string PhoneNumber, bool IsAccountActivated, ClientAccountDto DomainUser);
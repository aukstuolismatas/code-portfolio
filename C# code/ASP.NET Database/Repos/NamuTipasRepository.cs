using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NT_Csharp.Models;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace NT_Csharp.Repos
{
    public class NamuTipasRepository
    {
        public List<NamuTipas> getTipai()
        {
            List<NamuTipas> tipai = new List<NamuTipas>();

            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "SELECT * FROM namu_tipai";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                tipai.Add(new NamuTipas
                {
                    id = Convert.ToInt32(item["id_namu_tipai"]),
                    pavadinimas = Convert.ToString(item["name"])
                });
            }

            return tipai;
        }
    }
}
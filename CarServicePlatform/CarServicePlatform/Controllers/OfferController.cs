﻿using System.Security.Claims;
using AutoMapper;
using CarServicePlatform.Auth;
using CarServicePlatform.Auth.Model;
using CarServicePlatform.Data.Dtos.PriceOffer;
using CarServicePlatform.Data.Dtos.TransferOffer;
using CarServicePlatform.Entities.Request;
using CarServicePlatform.Entities.Users;
using CarServicePlatform.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace CarServicePlatform.Controllers;

[ApiController]
[Authorize]
[Route("/offers")]
public class OfferController : ControllerBase
{
    private readonly UserManager<AppUser> _userManager;
    private readonly IMapper _mapper;
    private readonly IClientRequestRepository _clientRequestRepository;
    private readonly IPriceOfferRepository _priceOfferRepository;
    private readonly ITransferOfferRepository _transferOfferRepository;
    private readonly IAuthorizationService _authorizationService;

    public OfferController(IMapper mapper, IClientRequestRepository clientRequestRepository, IPriceOfferRepository priceOfferRepository, IAuthorizationService authorizationService, UserManager<AppUser> userManager, ITransferOfferRepository transferOfferRepository)
    {
        _mapper = mapper;
        _clientRequestRepository = clientRequestRepository;
        _priceOfferRepository = priceOfferRepository;
        _authorizationService = authorizationService;
        _userManager = userManager;
        _transferOfferRepository = transferOfferRepository;
    }

    [HttpGet("{clientRequestId}")]
    [Authorize(Roles = AppUserRoles.Client)]
    public async Task<ActionResult<IEnumerable<PriceOfferDto>>> GetAllClientRequestPriceOffer(int clientRequestId)
    {
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(clientRequestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return NotFound("This request does not exists");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, clientRequest, PolicyNames.SameUser).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<PriceOffer> priceOffers = await _priceOfferRepository.GetAllClientRequestPriceOffersAsync(clientRequestId).ConfigureAwait(false);
        return Ok(priceOffers.Select(x => _mapper.Map<PriceOfferDto>(x)));
    }

    [HttpGet("transfer/{clientRequestId}")]
    [Authorize(Roles = AppUserRoles.Client)]
    public async Task<ActionResult<IEnumerable<TransferOfferDto>>> GetAllClientRequestTransferOffer(int clientRequestId)
    {
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(clientRequestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return NotFound("This request does not exists");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, clientRequest, PolicyNames.SameUser).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<TransferOffer> transferOffers = await _transferOfferRepository.GetAllClientRequestTransferOffersAsync(clientRequestId).ConfigureAwait(false);
        return Ok(transferOffers.Select(x => _mapper.Map<TransferOfferDto>(x)));
    }

    [HttpGet("{clientRequestId}/service/{serviceAccountId}")]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<PriceOfferDto>> GetServicePriceOffer(int clientRequestId, string serviceAccountId)
    {
        PriceOffer? priceOffer = await _priceOfferRepository.GetPriceOfferByServiceIdAndRequestIdAsync(serviceAccountId, clientRequestId).ConfigureAwait(false);
        if (priceOffer == null)
        {
            return NotFound("This price offer does not exist");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, priceOffer.ServiceAccountId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        return Ok(_mapper.Map<PriceOfferDto>(priceOffer));
    }

    [HttpGet("transfer/{clientRequestId}/trawl/{trawlAccountId}")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<TransferOfferDto>> GetTrawlTransferOffer(int clientRequestId, string trawlAccountId)
    {
        TransferOffer? transferOffer = await _transferOfferRepository.GetTransferOfferByTrawlIdAndRequestIdAsync(trawlAccountId, clientRequestId).ConfigureAwait(false);
        if (transferOffer == null)
        {
            return NotFound("This price offer does not exist");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, transferOffer.TrawlAccountId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        return Ok(_mapper.Map<TransferOfferDto>(transferOffer));
    }

    [HttpPost("transfer")]
    [Authorize(Roles = AppUserRoles.Trawl)]
    public async Task<ActionResult<TransferOfferDto>> InsertTransferOffer(CreateTransferOfferDto createTransferOfferDto)
    {
        string trawlUserId = User.FindFirstValue(CustomClaims.UserId);
        AppUser trawlUser = await _userManager.FindByIdAsync(trawlUserId).ConfigureAwait(false);
        if (!trawlUser.IsAccountActivated)
        {
            return Forbid();
        }

        TransferOffer transferOffer = _mapper.Map<TransferOffer>(createTransferOfferDto);
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(transferOffer.ClientRequestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return NotFound("This client request does not exist");
        }

        if (clientRequest.Status != RequestStatus.WaitingTransfer)
        {
            return BadRequest($"This request is already in {clientRequest.Status} status");
        }

        transferOffer.TrawlAccountId = trawlUserId;
        TransferOffer? checkIfSameOfferExists =
            await _transferOfferRepository.GetTransferOfferByTrawlIdAndRequestIdAsync(transferOffer.TrawlAccountId, transferOffer.ClientRequestId).ConfigureAwait(false);
        if (checkIfSameOfferExists != null)
        {
            return BadRequest("The offer already placed");
        }

        await _transferOfferRepository.InsertTransferOfferAsync(transferOffer).ConfigureAwait(false);
        return Ok(_mapper.Map<TransferOfferDto>(transferOffer));
    }

    [HttpPost]
    [Authorize(Roles = AppUserRoles.Service)]
    public async Task<ActionResult<PriceOfferDto>> InsertPriceOffer(CreatePriceOfferDto createPriceOfferDto)
    {
        string serviceUserId = User.FindFirstValue(CustomClaims.UserId);
        AppUser serviceUser = await _userManager.FindByIdAsync(serviceUserId).ConfigureAwait(false);
        if (!serviceUser.IsAccountActivated)
        {
            return Forbid();
        }

        PriceOffer priceOffer = _mapper.Map<PriceOffer>(createPriceOfferDto);
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(priceOffer.ClientRequestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return NotFound("This client request does not exist");
        }

        if (clientRequest.Status != RequestStatus.Waiting)
        {
            return BadRequest($"This request is already in {clientRequest.Status} status");
        }

        priceOffer.ServiceAccountId = serviceUserId;
        PriceOffer? checkIfSamePriceOfferExists =
            await _priceOfferRepository.GetPriceOfferByServiceIdAndRequestIdAsync(priceOffer.ServiceAccountId, priceOffer.ClientRequestId).ConfigureAwait(false);
        if (checkIfSamePriceOfferExists != null)
        {
            return BadRequest("The offer already placed");
        }

        priceOffer.Status = OfferStatus.Waiting;
        await _priceOfferRepository.InsertPriceOfferAsync(priceOffer).ConfigureAwait(false);
        return Ok(_mapper.Map<PriceOfferDto>(priceOffer));
    }

    [HttpPut("approve/{offerId}")]
    [Authorize(Roles = AppUserRoles.Client)]
    public async Task<ActionResult<IEnumerable<PriceOfferDto>>> ApprovePriceOffer(int offerId, UpdatePriceOfferDto updatePriceOfferDto)
    {
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(updatePriceOfferDto.ClientRequestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return NotFound("This client request does not exist");
        }

        if (clientRequest.Status != RequestStatus.Waiting)
        {
            return BadRequest($"This request is already in {clientRequest.Status} status");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, clientRequest.DomainUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<PriceOffer> priceOffers = await _priceOfferRepository.GetAllClientRequestPriceOffersAsync(updatePriceOfferDto.ClientRequestId).ConfigureAwait(false);
        PriceOffer? priceOffer = priceOffers.FirstOrDefault(x => x.Id == offerId);
        if (priceOffer == null)
        {
            return NotFound("This price offer does not exist");
        }

        foreach (PriceOffer offer in priceOffers)
        {
            offer.Status = offer.Id == offerId ? OfferStatus.Approved : OfferStatus.Rejected;
        }

        clientRequest.Status = RequestStatus.WaitingTransfer;
        await _priceOfferRepository.UpdatePriceOffersRangeAsync(priceOffers).ConfigureAwait(false);
        await _clientRequestRepository.UpdateRequestAsync(clientRequest).ConfigureAwait(false);
        return Ok(priceOffers.Select(x => _mapper.Map<PriceOfferDto>(x)));
    }

    [HttpPut("approve/transfer/{offerId}")]
    [Authorize(Roles = AppUserRoles.Client)]
    public async Task<ActionResult<IEnumerable<TransferOfferDto>>> ApproveTransferOffer(int offerId, UpdateTransferOfferDto updateTransferOfferDto)
    {
        ClientRequest? clientRequest = await _clientRequestRepository.GetRequestAsync(updateTransferOfferDto.ClientRequestId).ConfigureAwait(false);
        if (clientRequest == null)
        {
            return NotFound("This client request does not exist");
        }

        if (clientRequest.Status != RequestStatus.WaitingTransfer)
        {
            return BadRequest($"This request is already in {clientRequest.Status} status");
        }

        AuthorizationResult authorizationResult = await _authorizationService.AuthorizeAsync(User, clientRequest.DomainUserId, PolicyNames.User).ConfigureAwait(false);
        if (!authorizationResult.Succeeded)
        {
            return Forbid();
        }

        List<TransferOffer> transferOffers = await _transferOfferRepository.GetAllClientRequestTransferOffersAsync(updateTransferOfferDto.ClientRequestId).ConfigureAwait(false);
        TransferOffer? transferOffer = transferOffers.FirstOrDefault(x => x.Id == offerId);
        if (transferOffer == null)
        {
            return NotFound("This transfer offer does not exist");
        }

        foreach (TransferOffer offer in transferOffers)
        {
            offer.Status = offer.Id == offerId ? OfferStatus.Approved : OfferStatus.Rejected;
        }

        clientRequest.Status = RequestStatus.Accepted;
        await _transferOfferRepository.UpdateTransferOffersRangeAsync(transferOffers).ConfigureAwait(false);
        await _clientRequestRepository.UpdateRequestAsync(clientRequest).ConfigureAwait(false);
        return Ok(transferOffers.Select(x => _mapper.Map<TransferOfferDto>(x)));
    }
}
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EndedServiceRequestsComponent } from './ended-service-requests.component';

describe('EndedServiceRequestsComponent', () => {
  let component: EndedServiceRequestsComponent;
  let fixture: ComponentFixture<EndedServiceRequestsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EndedServiceRequestsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EndedServiceRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

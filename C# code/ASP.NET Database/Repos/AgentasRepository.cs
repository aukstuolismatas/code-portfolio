using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NT_Csharp.Models;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using NT_Csharp.ViewModels;

namespace NT_Csharp.Repos
{
    public class AgentasRepository
    {
        public List<Agentas> getAgentai()
        {
            List<Agentas> agentai = new List<Agentas>();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"SELECT agentas.vardas, agentas.pavarde, agentas.pardavimu_skaicius, agentas.telefonas, agentas.id_AGENTAS, m.pavadinimas AS agentura from agentas LEFT JOIN nt_agentura m ON m.imones_kodas=agentas.fk_NT_AGENTURAimones_kodas";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                agentai.Add(new Agentas
                {
                    id = Convert.ToInt32(item["id_AGENTAS"]),
                    vardas = Convert.ToString(item["vardas"]),
                    pavarde = Convert.ToString(item["pavarde"]),
                    pardavimai= Convert.ToInt32(item["pardavimu_skaicius"]),
                    telefonas = Convert.ToString(item["telefonas"]),
                    agentura= Convert.ToString(item["agentura"])
                });
            }
            return agentai;
        }
        public bool addAgentas(AgentasEditViewModel agentas)
        {
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
                MySqlConnection mySqlConnection = new MySqlConnection(conn);
                string sqlquery = @"INSERT INTO agentas(vardas,pavarde,pardavimu_skaicius,telefonas,id_AGENTAS,fk_NT_AGENTURAimones_kodas)VALUES(?vardas,?pavarde,?pardavimai,?tel,?id,?imoneskod);";
                MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
                mySqlCommand.Parameters.Add("?vardas", MySqlDbType.VarChar).Value = agentas.vardas;
                mySqlCommand.Parameters.Add("?pavarde", MySqlDbType.VarChar).Value = agentas.pavarde;
                mySqlCommand.Parameters.Add("?pardavimai", MySqlDbType.VarChar).Value = agentas.pardavimai;
                mySqlCommand.Parameters.Add("?tel", MySqlDbType.VarChar).Value = agentas.telefonas;
                mySqlCommand.Parameters.Add("?id", MySqlDbType.VarChar).Value = agentas.id;
                mySqlCommand.Parameters.Add("?imoneskod", MySqlDbType.VarChar).Value = agentas.fk_agentura;
                mySqlConnection.Open();
                mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool updateAgentas(AgentasEditViewModel agentas)
        {
            try
            {
                string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
                MySqlConnection mySqlConnection = new MySqlConnection(conn);
                string sqlquery = @"UPDATE agentas a SET a.vardas=?vardas, a.pavarde=?pavarde, a.pardavimu_skaicius=?pardavimai, a.telefonas=?tel, a.fk_NT_AGENTURAimones_kodas=?imoneskod WHERE a.id_AGENTAS=?id";
                MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
                mySqlCommand.Parameters.Add("?vardas", MySqlDbType.VarChar).Value = agentas.vardas;
                mySqlCommand.Parameters.Add("?pavarde", MySqlDbType.VarChar).Value = agentas.pavarde;
                mySqlCommand.Parameters.Add("?pardavimai", MySqlDbType.VarChar).Value = agentas.pardavimai;
                mySqlCommand.Parameters.Add("?tel", MySqlDbType.VarChar).Value = agentas.telefonas;
                mySqlCommand.Parameters.Add("?id", MySqlDbType.VarChar).Value = agentas.id;
                mySqlCommand.Parameters.Add("?imoneskod", MySqlDbType.VarChar).Value = agentas.fk_agentura;
                mySqlConnection.Open();
                mySqlCommand.ExecuteNonQuery();
                mySqlConnection.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public AgentasEditViewModel getAgentas(int id)
        {
            AgentasEditViewModel agentas = new AgentasEditViewModel();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "select * from agentas where id_AGENTAS="+id;
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                agentas.id = Convert.ToInt32(item["id_AGENTAS"]);
                agentas.vardas = Convert.ToString(item["vardas"]);
                agentas.pavarde = Convert.ToString(item["pavarde"]);
                agentas.pardavimai = Convert.ToInt32(item["pardavimu_skaicius"]);
                agentas.telefonas = Convert.ToString(item["telefonas"]);
                agentas.fk_agentura = Convert.ToInt32(item["fk_NT_AGENTURAimones_kodas"]);
            }
            return agentas;
        }
        public int getKlientasSutarciuCount(int id)
        {
            int naudota = 0;
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"SELECT count(nr) as kiekis from sutartis where fk_AGENTASid_AGENTAS=" + id;
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                naudota = Convert.ToInt32(item["kiekis"] == DBNull.Value ? 0 : item["kiekis"]);
            }
            return naudota;
        }
        public void deleteKlientas(int id)
        {
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = @"DELETE FROM agentas where id_AGENTAS=?id";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlCommand.Parameters.Add("?id", MySqlDbType.VarChar).Value = id;
            mySqlConnection.Open();
            mySqlCommand.ExecuteNonQuery();
            mySqlConnection.Close();
        }
    }
}

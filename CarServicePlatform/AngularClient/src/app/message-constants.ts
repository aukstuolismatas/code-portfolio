export class MessageConstants {
    public static LoginError = "Prisijungti nepavyko";
    public static LoginSuccess = "Sveikiname prisijungus";
    public static RegisterError = "Registracija nesėkminga";
    public static RegisterSuccess = "Paskyra sėkmingai užregistruota";
    public static ProfileUpdateError = "Atnaujinti duomenų nepavyko";
    public static ProfileUpdateSuccess = "Duomenys atnaujinti sėkmingai";
    public static ProfilePictureSuccess = "Profilio nuotrauka įkelta sėkmingai";
    public static ProfilePictureFailed = "Profilio nuotraukos įkelti nepavyko";
    public static CompanyDocumentsSuccess = "Dokumentas įkeltas sėkmingai";
    public static CompanyDocumentsFailed = "Dokumento įkelti nepavyko";
    public static DeleteAccountSuccess = "Paskyra ištrinta sėkmingai";
    public static DeleteAccountFailed = "Paskyros ištrinti nepavyko";
    public static PlaceClientRequestSuccess = "Jūsų užklausa sėkmingai užregistruota";
    public static PlaceClientRequestFailed = "Jūsų užklausos užregistruoti nepavyko";
    public static PlacePriceOfferFailed = "Jūsų pasiūlymas jau pateiktas";
    public static PlacePriceOfferSuccess = "Jūsų pasiūlymas pateiktas sėkmingai";
    public static TransferringClientRequestSuccess = "Šiuo metu Jūs esate atsakingas už automobilio pristatymą";
    public static ClientRequestUpdateFailed = "Klaida: nepavyko pakeisti statuso";
    public static ArrivedClientRequestSuccess = "Automobilis sėkmingai pristatytas";
    public static DeliveredClientRequestSuccess = "Automobilis sėkmingai grąžintas klientui";
    public static InProgressClientRequestSuccess = "Remonto darbai pradėti";
    public static DoneClientRequestSuccess = "Remonto darbai baigti";
    public static AccountDeactivated ="Paskyra išjungta"
    public static AccountActivated ="Paskyra aktyvuota"
    public static AccountActivatedFailed ="Statusas nepasikeitė"
    public static ChatFailed ="Pokalbis jau sukurtas"
}

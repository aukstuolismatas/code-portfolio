﻿namespace CarServicePlatform.Data.Dtos.TransferOffer;

public record CreateTransferOfferDto(int Price, string DateAndTime, int ClientRequestId);
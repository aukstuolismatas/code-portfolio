import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { ProfileService } from 'src/app/services/profile.service';
import { TrawlAccount, UserAccount } from 'src/app/shared/account';
import { City } from 'src/app/shared/city';

@Component({
  selector: 'app-trawl-accounts',
  templateUrl: './trawl-accounts.component.html',
  styleUrls: ['./trawl-accounts.component.css']
})
export class TrawlAccountsComponent implements OnInit {

  public errorMessage ='';
  public successMessage ='';
  trawlAccounts: TrawlAccount[]=[];
  tabName = HeaderConstants.TrawlTab
  constructor(private profileService: ProfileService, private router: Router, private dataSharingService: DataSharingService) { 
    dataSharingService.trawlAccountsList.subscribe(value => this.trawlAccounts = value);
    profileService.loadTrawlAccounts().subscribe((data:TrawlAccount[])=>this.dataSharingService.trawlAccountsList.next(data))
  }

  ngOnInit(): void {
  }

  activateAccount(arrayIndex: number, userId:string){
    this.profileService.activateAccount(userId).subscribe({
        next:(data:UserAccount)=>{
          this.trawlAccounts[arrayIndex].isAccountActivated=data.isAccountActivated
          this.successMessage = MessageConstants.AccountActivated
        },
        error: () => this.errorMessage = MessageConstants.AccountActivatedFailed
      });
    this.successMessage='';
    this.errorMessage ='';
  }

  deactivateAccount(arrayIndex: number, userId:string){
    this.profileService.deactivateAccount(userId).subscribe({
      next:(data:UserAccount)=>{
        this.trawlAccounts[arrayIndex].isAccountActivated=data.isAccountActivated
        this.successMessage = MessageConstants.AccountDeactivated
      },
      error: () => this.errorMessage = MessageConstants.AccountActivatedFailed
    });
    this.successMessage='';
    this.errorMessage ='';
  }
  getCity(enumNumber:number){
    return City[enumNumber];
  }
  deleteTrawl(arrayIndex: number, userId:string){
    this.profileService.deleteTrawlAccount(userId).subscribe({
      next: () => {
        this.successMessage = MessageConstants.DeleteAccountSuccess;
        delete this.dataSharingService.trawlAccountsList.value[arrayIndex]
        // delete this.trawlAccounts[arrayIndex];
      },
      error: () => this.errorMessage = MessageConstants.DeleteAccountFailed
    });
    this.successMessage='';
    this.errorMessage ='';
  }

  openItem(index:number){
    this.router.navigate([`${AppConstants.AccountsTrawl}/${this.trawlAccounts[index].email}/${index}`]);
  }
}

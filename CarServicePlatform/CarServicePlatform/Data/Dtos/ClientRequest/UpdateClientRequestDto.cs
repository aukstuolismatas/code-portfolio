﻿using CarServicePlatform.Entities;

namespace CarServicePlatform.Data.Dtos.ClientRequest;

public record UpdateClientRequestDto(string Title, string Description, string Address, City City);
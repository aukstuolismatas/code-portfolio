import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable, Sanitizer } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { catchError, map, Observable, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ApiConstants } from '../api-constants';
import { FileData, FileInformation } from '../shared/file-information';
import { AuthService } from './auth/auth.service';
import { DataSharingService } from './data-sharing.service';
import { ErrorHandleService } from './error-handle.service';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  readonly profileImageUrl = `${environment.apiUrl}${ApiConstants.uploadProfileImage}`;
  readonly companyDocumentUrl = `${environment.apiUrl}${ApiConstants.companyDocument}`;
  readonly companyDocumentDataUrl = `${environment.apiUrl}${ApiConstants.companyDocumentData}`;
  readonly companyDocumentDataForAdminUrl = `${environment.apiUrl}${ApiConstants.companyDocumentDataForAdmin}`;
  readonly repairRequestImagesUrl = `${environment.apiUrl}${ApiConstants.uploadRepairRequestImage}/`;
  fileUrl: any
  images: SafeUrl[]=[]
  constructor(private http: HttpClient, private authService: AuthService, private errorHandle: ErrorHandleService,private sanitizer: DomSanitizer) { }

  retrieveAllDocumentsForUser(): Observable<FileInformation[]>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.get<FileInformation[]>(this.companyDocumentUrl+this.authService.getUserId(), {headers: headers})
    .pipe(map((data:FileInformation[]) => data), catchError(this.errorHandle.errorHandle));
  }

  retrieveAllDocumentsByUserId(userId: string): Observable<FileInformation[]>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.get<FileInformation[]>(this.companyDocumentUrl+userId, {headers: headers})
    .pipe(map((data:FileInformation[]) => data), catchError(this.errorHandle.errorHandle));
  }

  uploadFile(formFile: FormData): Observable<FileInformation>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.post<FileInformation>(this.profileImageUrl, formFile, {headers: headers})
  }

  uploadDocument(formFile: FormData): Observable<FileInformation>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.post<FileInformation>(this.companyDocumentUrl, formFile, {headers: headers})
  }

  uploadRepairRequestImages(formFile: FormData, clientRepairRequestId: number): Observable<FileInformation>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.post<FileInformation>(this.repairRequestImagesUrl+clientRepairRequestId, formFile, {headers: headers})
    .pipe(map(data=>data), catchError(this.errorHandle.errorHandle))
  }

  downloadProfilePicture(): Observable<string>{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.get(`${this.profileImageUrl}/${this.authService.getUserId()}`, {headers: headers, responseType: 'blob'})
    .pipe(switchMap(response => this.readFile(response)))
  }

  downloadRequestImages(requestId: number): Observable<SafeUrl[]>{
    this.images=[];
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.get<any[]>(`${this.repairRequestImagesUrl}${requestId}`, {headers: headers})
    .pipe(switchMap(response => this.readFiles(response)))
  }

  downloadDocument(fileId: number): any{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.get(`${this.companyDocumentDataUrl}${fileId}`, {headers: headers, responseType: 'blob'});
  }

  downloadDocumentAdmin(fileId: number): any{
    const headers = new HttpHeaders().set("Authorization", `Bearer ${this.authService.decodedToken}`);
    return this.http.get(`${this.companyDocumentDataForAdminUrl}${fileId}`, {headers: headers, responseType: 'blob'});
  }

  readFile(blob: Blob): Observable<string> {
    return Observable.create((obs:any) => {
      const reader = new FileReader();

      reader.onerror = err => obs.error(err);
      reader.onabort = err => obs.error(err);
      reader.onload = () => obs.next(reader.result);
      reader.onloadend = () => obs.complete();

      return reader.readAsDataURL(blob);
    });
  }

  readFiles(fileDataArray: FileData[]): Observable<any[]> {
    return Observable.create((obs:any[]) => {
      fileDataArray.forEach(el =>{
        let objectURL = 'data:'+ el.fileExtension +';base64,' + el.fileBase64;
        this.images.push(this.sanitizer.bypassSecurityTrustUrl(objectURL));
      })
      return this.images
    });
  }
    
  //   const reader = new FileReader();
  //   fileDataArray.forEach(element => {
  //   this.images.push(Observable.create((obs:any) => {
  //     reader.onerror = err => obs.error(err);
  //       reader.onabort = err => obs.error(err);
  //       reader.onload = () => obs.next(reader.result);
  //       reader.onloadend = () => obs.complete();
  //       return reader.readAsDataURL(new Blob([element.fileContent]));
  //   }))
  // });
  // return Observable<this.images>;
}

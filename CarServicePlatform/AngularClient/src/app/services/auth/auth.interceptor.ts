import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const idToken = localStorage.getItem("id_token");

    if (idToken) {
        const cloned = request.clone({
            headers: request.headers.set("Authorization","Bearer " + idToken)
        });

        return next.handle(cloned);
    }
    else {
        return next.handle(request);
    }
  }
}
// export class AuthInterceptor implements HttpInterceptor {

//     constructor(private authService: AuthService, @Inject(AUTH_STRATEGY) private jwt: JwtAuthStrategy) { }
  
//     intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  
//       if (config.auth === 'token' && this.jwt && this.jwt.getToken()) {
//         request = this.addToken(request, this.jwt.getToken());
//       }
  
//       return next.handle(request).pipe(catchError(error => {
//         if (error.status === 401) {
//           this.authService.doLogoutAndRedirectToLogin();
//         }
//         return throwError(error);
//       }));
  
//     }
  
//     private addToken(request: HttpRequest<any>, token: string) {
//       return request.clone({
//         setHeaders: { 'Authorization': `Bearer ${token}` }
//       });
//     }
import nltk
import numpy
from collections import Counter
from nltk.corpus import stopwords
from string import punctuation
import wikipedia
nltk.download('punkt')
nltk.download('stopwords')
nltk.download('averaged_perceptron_tagger')
nltk.download('maxent_ne_chunker')
nltk.download('words')
stops=stopwords.words('english')

def extractEntities(ne_chunked):
    data={}
    for entity in ne_chunked.subtrees():
        print(entity.label())
        if isinstance(entity, nltk.tree.Tree):
            t=" ".join([word for word, tag in entity.leaves()])
            print(entity.label())
            print(entity.leaves())
            ent=entity.label()
            data[t]=ent
        else:
            continue
        return data

def tokenCounts(tokens):
    counts = Counter(tokens)
    # print(counts.items().)
    sortedCounts = sorted(counts.items(), key=lambda count:count[1], reverse=True)
    # print(sortedCounts.count:count[1])
    return sortedCounts

# Wikipedia implementation
def wikipediaClassification(entity, title):
    wResults = {}
    print(title)
    for item in entity:
        try:
            search = wikipedia.search(item)
            page = wikipedia.page(search[0])
        except:
            page = 'Thing'

        if page != 'Thing':
            sentences = nltk.sent_tokenize(page.summary)
            words = nltk.word_tokenize(sentences[0])
            nopunc_words = [w for w in words if w not in punctuation]  # removing punctuation
            filtered_words = [w for w in nopunc_words if w not in stops]  # removing stopwords
            pos = nltk.pos_tag(filtered_words)
            counter = 0
            description = []
            print(item, end=": ")
            for tag in pos:
                if tag[1].startswith('NN') and not tag[1].startswith('NNP') and counter < 4:
                    print(tag[0], end=" ")
                    description.append(tag[0])
                    counter += 1

            wResults[item] = description
            print("")
        else:
            wResults[item] = 'Thing'
            print(item + ": " + wResults[item])
    print("")
    return wResults

text=None
with open("textOfSpeaches.txt", "r", encoding="utf8") as file:
    text=file.read()

#POS
sentences = nltk.sent_tokenize(text) #sentence spliting
tokens = nltk.word_tokenize(text) #word spliting
nopunc_tokens = [token for token in tokens if token not in punctuation] #removing punctuation
filtered_tokens = [token for token in nopunc_tokens if token not in stops] #removing stopwords
# tokens = [nltk.word_tokenize(sent) for sent in sentences]
# print(tokenCounts(filtered_tokens))



tagged = nltk.pos_tag(filtered_tokens) #POS
pos_counts = Counter((item[1] for item in tagged)) #count every tag
Counter((item[1] for item in tagged))
print("The seventh most common tags are", pos_counts.most_common(7))

# #NER
# tag_list= [nltk.pos_tag(item) for item in tokens]
# ne_chunked = nltk.ne_chunk(tag_list, binary=False)
# for item in ne_chunked.subtrees():

# print(ne_chunked)
# da=extractEntities(ne_chunked)
# print(ne_chunked)
data=[]
repeated=[]
GPE=[]
print("The most common organization is ")
for sent in sentences:
    for chunk in nltk.ne_chunk(nltk.pos_tag(nltk.word_tokenize(sent))):
        if hasattr(chunk, 'label'):
            NE=chunk.label(), ' '.join(c[0] for c in chunk)
            if NE[1] not in data:
                data.append(NE[1])
                print(NE)
            elif NE[1] not in repeated:
                repeated.append(NE[1])
                if chunk.label() == 'ORGANIZATION' and NE[1] not in GPE:
                    GPE.append(NE[1])
                    # print(NE)
print("")
#Custom NER
# tokens=nltk.word_tokenize(text)
tagged = nltk.pos_tag(tokens)
entity=[]
required=[]
previous=''
cusEntity=[]
wAdjective=[]
print("Custom pattern cusEntity".upper())
for item in tagged:
    if previous!='.':
        if (item[1].startswith('JJ') or (entity and item[1].startswith('NNP'))):
            entity.append(item)
        else:
            if not entity and item[1].startswith('NNP') and len(item[0])>3:
                required.append(item)
            elif required:
                # print(" ".join(r[0] for r in required)) #show the NNP* which is not goes with adjectives
                cusEntity.append(" ".join(r[0] for r in required))
                required = []
            elif entity and len(entity) > 1 and entity[-1][0][0].isupper() and len(entity[0][0])>3:
                print(" ".join(e[0] for e in entity)) #shows only those NNP* which comes with adjective
                wAdjective.append(" ".join(e[0] for e in entity))
            entity = []
            # print()
    previous=item[0]

#Removing dublicated entries
# sortedCusEntity=sorted(cusEntity)
finalCusEntity=list(dict.fromkeys(cusEntity))

# print(tokenCounts(cusEntity))


#nltk ORGANIZATION entities classification
wikipediaClassification(GPE, "nltk ne_chunk ORGANIZATION classification results".upper())
#nltk entities classification
wikipediaClassification(data, "nltk ne_chunk classification results".upper())
#Custom pattern classification
wikipediaClassification(wAdjective, "Custom pattern classification results".upper())
#Custom pattern classification without adjectives
wikipediaClassification(finalCusEntity,"Custom pattern without adjectives classification results".upper())

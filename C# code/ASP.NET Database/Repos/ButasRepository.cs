using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using MySql.Data.MySqlClient;
using NT_Csharp.Models;
using System.Configuration;


namespace NT_Csharp.Repos
{
    public class ButasRepository
    {
        public List<Butas> getButai()
        {
            List<Butas> butai = new List<Butas>();
            string conn = ConfigurationManager.ConnectionStrings["MysqlConnection"].ConnectionString;
            MySqlConnection mySqlConnection = new MySqlConnection(conn);
            string sqlquery = "SELECT * FROM butas";
            MySqlCommand mySqlCommand = new MySqlCommand(sqlquery, mySqlConnection);
            mySqlConnection.Open();
            MySqlDataAdapter mda = new MySqlDataAdapter(mySqlCommand);
            DataTable dt = new DataTable();
            mda.Fill(dt);
            mySqlConnection.Close();

            foreach (DataRow item in dt.Rows)
            {
                butai.Add(new Butas
                {
                    laiptine = Convert.ToInt32(item["laiptines_numeris"]),
                    butoNR = Convert.ToInt32(item["buto_numeris"]),
                    butoAukstas = Convert.ToInt32(item["buto_aukstas"]),
                    vietaPark = Convert.ToBoolean(item["isskirta_parkavimo_vieta"]),
                    zaidimuAikst = Convert.ToBoolean(item["vaiku_zaidimu_aikstele"]),
                    rusys = Convert.ToBoolean(item["rusys"]),
                    id = Convert.ToInt32(item["id_BUTAS"]),
                });
            }
            return butai;
        }
    }
}
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppConstants, RegexPatterns } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { AuthService } from 'src/app/services/auth/auth.service';
import { mustMatch } from 'src/app/shared/form-validation';
import { RegisterTrawl } from 'src/app/shared/login-data';

@Component({
  selector: 'app-register-trawl',
  templateUrl: './register-trawl.component.html',
  styleUrls: ['./register-trawl.component.css']
})
export class RegisterTrawlComponent implements OnInit {

  public registerForm : FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    surname: new FormControl('', [Validators.required]),
    email: new FormControl('', [Validators.required ,Validators.email, Validators.pattern(RegexPatterns.Email)]),
    phone: new FormControl('', [Validators.required, Validators.pattern(RegexPatterns.Phone)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6),Validators.maxLength(40), Validators.pattern(RegexPatterns.Passowrd)]),
    confirmPassword: new FormControl('', [Validators.required]),
    city: new FormControl('', [Validators.required]),
    minPrice: new FormControl('', [Validators.required]),
    companyCode: new FormControl('', [Validators.required]),
    companyName: new FormControl('', [Validators.required]),
  }, {
    validators: mustMatch
  });

  public submitted =false;
  public errorMessage = '';
  public successMessage = '';
  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
  }
  get f(): {[key:string]:AbstractControl} { return this.registerForm.controls; }

  changeCity(event:number){
    this.f['city'].setValue(event);
    console.log(this.f['city'].value);
  }

  register(){
    const registerRequest: RegisterTrawl = {
      name: this.f['name'].value,
      surname: this.f['surname'].value,
      email: this.f['email'].value,
      phoneNumber: this.f['phone'].value,
      password: this.f['password'].value,
      minimalPrice: this.f['minPrice'].value,
      city: this.f['city'].value,
      companyCode: this.f['companyCode'].value,
      companyName: this.f['companyName'].value,
    };
    this.submitted=false;
     if (this.registerForm.invalid) {
       this.submitted = true;
        return;
     }
    this.authService.registerTrawl(registerRequest).subscribe({
      next: (user) => this.router.navigate([AppConstants.Login], {queryParams: { registered: 'true' } }),
      error: (err) => this.errorMessage = MessageConstants.RegisterError
    })
    this.registerForm.reset;
    this.errorMessage ='';
  }

}

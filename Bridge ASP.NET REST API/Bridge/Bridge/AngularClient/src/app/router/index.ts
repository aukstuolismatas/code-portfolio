﻿import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { TournamentPage } from "../pages/tournamentPage";
import { BoardPage } from "../pages/boardPage";
import { LoginPage } from "../pages/loginPage";
import { TournamentCreatePage } from "../pages/tournamentCreatePage";
import { TournamentEditView } from "../views/tournamentEditView";
import TableListView from "../views/tableListView";
import { TableCreateView } from "../views/tableCreateView";
import { AuthActivator } from "../services/authActivatorService";


const routes: Routes = [
    { path: "", component: TournamentPage, data: {animation:'isTournament'}},
    { path: "tournaments/:id/tables", component: TableListView },
    { path: "tableCreate", component: TableCreateView, canActivate: [AuthActivator] },
    { path: "boards", component: BoardPage},
    { path: "login", component: LoginPage },
    { path: "tournamentCreate", component: TournamentCreatePage, canActivate: [AuthActivator]  },
    { path: "edit/:id", component: TournamentEditView, canActivate: [AuthActivator]  },
    { path: "**", redirectTo: "" }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class Router {
}
//const router = RouterModule.forRoot(routes,
//    {
//       useHash: false
//    });
//export default router;
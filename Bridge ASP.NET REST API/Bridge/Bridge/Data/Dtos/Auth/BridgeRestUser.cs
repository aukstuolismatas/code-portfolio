﻿using Microsoft.AspNetCore.Identity;

namespace Bridge.Data.Dtos.Auth
{
    public class BridgeRestUser : IdentityUser
    {
        [PersonalData]
        public string AdditionalInfo { get; set; }
    }
}
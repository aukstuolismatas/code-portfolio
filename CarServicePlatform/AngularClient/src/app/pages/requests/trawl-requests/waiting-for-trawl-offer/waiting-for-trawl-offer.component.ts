import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants, HeaderConstants } from 'src/app/app-constants';
import { MessageConstants } from 'src/app/message-constants';
import { DataSharingService } from 'src/app/services/data-sharing.service';
import { RepairRequestService } from 'src/app/services/repair-request.service';
import { TrawlTransferRequestService } from 'src/app/services/trawl-transfer-request.service';
import { RegisteredClientRequest, RegisteredClientRequestTrawl } from 'src/app/shared/client-request';
import { RequestStatus } from 'src/app/shared/request-status';

@Component({
  selector: 'app-waiting-for-trawl-offer',
  templateUrl: './waiting-for-trawl-offer.component.html',
  styleUrls: ['./waiting-for-trawl-offer.component.css']
})
export class WaitingForTrawlOfferComponent implements OnInit {

  successMessage:string='';
  tabName:string=HeaderConstants.WaitingPriceOfferTab;
  frontTitle:string='Laukiantys užsakymai pervežimui'
  titles:string[]=['Užsakymo nr.', 'Miestas', 'Atvykimo adresas', 'Pristatymo adresas','Užklauso pateikimo data', 'Statusas']
  repairRequestList:RegisteredClientRequestTrawl[]=[];

  constructor(private trawlTransferService: TrawlTransferRequestService, private dataSharingService: DataSharingService, private router: Router,
    private route: ActivatedRoute) {
    dataSharingService.trawlRequestList.subscribe(value => this.repairRequestList = value);
      this.trawlTransferService.retrieveAllAvailableClientRequestForTrawl().subscribe((data:RegisteredClientRequestTrawl[])=>{
        this.dataSharingService.trawlRequestList.next(data)
      })
   }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params =>{
      if(params['login'] !== undefined && params['login'] === 'true'){
        this.successMessage = MessageConstants.LoginSuccess;
      }
    });
  }

  openItem(index:number){
    this.router.navigate([`${AppConstants.AllTrawlRequests}/${this.repairRequestList[index].clientRequestId}/${index}`]);
  }

  getStatusDescription(enumString:string){
    return (<any>RequestStatus)[enumString];
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(ChangeNotifierProvider(
    create: (_) => _MyAppState(),
    child: MyApp(),
  ));
}

const List<String> urls = [
  'https://images.unsplash.com/photo-1542553458-79a13aebfda6?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=723&q=80',
  'https://images.unsplash.com/photo-1542438408-abb260104ef3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=735&q=80',
  'https://images.unsplash.com/photo-1542466500-dccb2789cbbb?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=686&q=80',
  'https://images.unsplash.com/photo-1542370285-b8eb8317691c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1126&q=80'
];

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Photo Viewer', home: GalleryPage(title: 'Image Gallery'));
  }
}

// class MyInheritedWidge extends InheritedWidget {
//   final _MyAppState state;
//   const MyInheritedWidge({super.key, required this.child, required this.state})
//       : super(child: child);

//   final Widget child;

//   static MyInheritedWidge? of(BuildContext context) {
//     return context.dependOnInheritedWidgetOfExactType<MyInheritedWidge>();
//   }

//   @override
//   bool updateShouldNotify(MyInheritedWidge oldWidget) {
//     return true;
//   }
// }

class _MyAppState with ChangeNotifier {
  bool isTagging = false;
  List<PhotoState> photoStates =
      List.of(urls.map((url) => PhotoState(url: url)));
  Set<String> tags = {"all", "nature", "city"};

  void selectTag(String tag) {
    if (isTagging) {
      if (tag != 'all') {
        photoStates.forEach((element) {
          if (element.selected) {
            element.tags.add(tag);
          }
        });
      }
      toggelTagging(null);
    } else {
      photoStates.forEach((element) {
        element.display = tag == 'all' ? true : element.tags.contains(tag);
      });
    }
    notifyListeners();
  }

  void toggelTagging(String? url) {
    isTagging = !isTagging;
    photoStates.forEach((element) {
      if (isTagging && element.url == url) {
        element.selected = true;
      } else {
        element.selected = false;
      }
    });
    notifyListeners();
  }

  void onPhotoSelect(String url, bool? selected) {
    photoStates.forEach((element) {
      if (element.url == url) {
        element.selected = selected ?? true;
      }
    });
    notifyListeners();
  }
}

class PhotoState {
  String url;
  bool selected;
  bool display;
  Set<String> tags = {};
  PhotoState(
      {required this.url, this.selected = false, this.display = true, tags});
}

class GalleryPage extends StatelessWidget {
  final String title;

  GalleryPage({required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(this.title)),
      body: GridView.count(
        crossAxisCount: 2,
        primary: false,
        children: List.of(context
            .watch<_MyAppState>()
            .photoStates
            .where((ps) => ps.display ?? true)
            .map((ps) => Photo(photoState: ps))),
      ),
      drawer: Drawer(
        child: ListView(
            children:
                List.of((context.watch<_MyAppState>().tags.map((e) => ListTile(
                      title: Text(e),
                      onTap: () {
                        context.read<_MyAppState>().selectTag(e);
                        Navigator.of(context).pop();
                      },
                    ))))),
      ),
    );
  }
}

class Photo extends StatelessWidget {
  final PhotoState photoState;
  const Photo({required this.photoState});

  @override
  Widget build(BuildContext context) {
    List<Widget> children = [
      GestureDetector(
        child: Image.network(
          photoState.url,
          fit: BoxFit.cover,
        ),
        onLongPress: () =>
            context.read<_MyAppState>().toggelTagging(photoState.url),
      )
    ];
    if (context.watch<_MyAppState>().isTagging) {
      children.add(Positioned(
        child: Theme(
          data: Theme.of(context)
              .copyWith(unselectedWidgetColor: Colors.grey[200]),
          child: Checkbox(
            onChanged: (value) {
              context.read<_MyAppState>().onPhotoSelect(photoState.url, value);
            },
            value: photoState.selected,
            activeColor: Colors.white,
            checkColor: Colors.black,
          ),
        ),
        left: 20,
        top: 0,
      ));
    }

    return Container(
      padding: EdgeInsets.all(5),
      child: Stack(
        alignment: Alignment.center,
        children: children,
      ),
    );
  }
}

import { Component, OnInit } from "@angular/core";
import { TournamentService } from "../services/tournamentService";

@Component({
    selector: "board-list",
    templateUrl: "boardListView.html",
    styleUrls: []
})
export default class BoardListView implements OnInit {

    constructor(public tournamentService: TournamentService) {
    }


    ngOnInit(): void {
        this.tournamentService.loadBoards().subscribe();
    }
}
﻿using CarServicePlatform.Entities.Request;

namespace CarServicePlatform.Repository;

public interface IPriceOfferRepository
{
    Task<PriceOffer?> GetPriceOfferAsync(int id);
    Task<PriceOffer?> GetClientRequestApprovedPriceOfferAsync(int clientRequestId);
    Task<List<PriceOffer>> GetAllClientRequestPriceOffersAsync(int clientRequestId);
    Task<PriceOffer?> GetPriceOfferByServiceIdAndRequestIdAsync(string serviceAccountId, int clientRequestId);
    Task InsertPriceOfferAsync(PriceOffer priceOffer);
    Task UpdatePriceOfferAsync(PriceOffer priceOffer);
    Task UpdatePriceOffersRangeAsync(List<PriceOffer> priceOffers);
    Task DeletePriceOfferAsync(PriceOffer priceOffer);
}
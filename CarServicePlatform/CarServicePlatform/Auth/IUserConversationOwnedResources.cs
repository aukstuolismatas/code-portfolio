﻿namespace CarServicePlatform.Auth;

public interface IUserConversationOwnedResources
{
    string SenderId { get; }
}
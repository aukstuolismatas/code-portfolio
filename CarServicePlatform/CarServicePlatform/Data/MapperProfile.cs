﻿using AutoMapper;
using CarServicePlatform.Data.Dtos.Account;
using CarServicePlatform.Data.Dtos.Auth;
using CarServicePlatform.Data.Dtos.Auth.User;
using CarServicePlatform.Data.Dtos.AvailableRepairService;
using CarServicePlatform.Data.Dtos.Chat;
using CarServicePlatform.Data.Dtos.ClientRequest;
using CarServicePlatform.Data.Dtos.File;
using CarServicePlatform.Data.Dtos.PriceOffer;
using CarServicePlatform.Data.Dtos.RepairService;
using CarServicePlatform.Data.Dtos.RepairServiceCategory;
using CarServicePlatform.Data.Dtos.RequestedRepairService;
using CarServicePlatform.Data.Dtos.TransferOffer;
using CarServicePlatform.Entities;
using CarServicePlatform.Entities.Chat;
using CarServicePlatform.Entities.RepairServices;
using CarServicePlatform.Entities.Request;
using CarServicePlatform.Entities.Users;

namespace CarServicePlatform.Data;

public class MapperProfile : Profile
{
    public MapperProfile()
    {
        CreateMap<ClientRequest, ClientRequestDto>();
        CreateMap<ClientRequest, ClientRequestForTrawlDto>();
        CreateMap<ClientRequest, ApprovedClientRequestDto>();
        CreateMap<ClientRequest, RejectedClientRequestDto>();
        CreateMap<ClientRequest, AssignedClientRequestToTrawlDto>();
        CreateMap<ClientRequest, RejectedTrawlClientRequestDto>();
        CreateMap<CreateClientRequestDto, ClientRequest>();
        CreateMap<CreateClientRequestDto, RequestedRepairService>();
        CreateMap<UpdateClientRequestDto, ClientRequest>();

        CreateMap<PriceOffer, PriceOfferDto>();
        CreateMap<CreatePriceOfferDto, PriceOffer>();

        CreateMap<TransferOffer, TransferOfferDto>();
        CreateMap<CreateTransferOfferDto, TransferOffer>();

        CreateMap<RequestedRepairService, RequestedRepairServiceDto>();
        CreateMap<AvailableRepairService, AvailableRepairServiceDto>();

        CreateMap<RepairServiceCategory, RepairServiceCategoryDto>();
        CreateMap<CreateRepairServiceCategoryDto, RepairServiceCategory>();
        CreateMap<UpdateRepairServiceCategoryDto, RepairServiceCategory>();

        CreateMap<RepairService, RepairServiceDto>();
        CreateMap<CreateRepairServiceDto, RepairService>();
        CreateMap<UpdateRepairServiceDto, RepairService>();

        CreateMap<AppUser, AppUserDto>();

        CreateMap<ServiceAccount, ServiceAccountDto>();
        CreateMap<AppUser, AppServiceUserDto>();
        CreateMap<UpdateServiceAccountDto, AppUser>();
        CreateMap<UpdateServiceAccountDto, ServiceAccount>();
        CreateMap<RegisterServiceUserDto, ServiceAccount>();

        CreateMap<AdministratorAccount, ClientAccountDto>();
        CreateMap<UpdateClientAccountDto, AdministratorAccount>();

        CreateMap<ClientAccount, ClientAccountDto>();
        CreateMap<AppUser, AppClientUserDto>();
        CreateMap<UpdateClientAccountDto, AppUser>();
        CreateMap<UpdateClientAccountDto, ClientAccount>();
        CreateMap<RegisterClientUserDto, ClientAccount>();

        CreateMap<TrawlAccount, TrawlAccountDto>();
        CreateMap<AppUser, AppTrawlUserDto>();
        CreateMap<UpdateTrawlAccountDto, AppUser>();
        CreateMap<UpdateTrawlAccountDto, TrawlAccount>();
        CreateMap<RegisterTrawlUserDto, TrawlAccount>();

        CreateMap<FileInformation, FileInformationDto>();
        CreateMap<FileInformation, FileDataDto>();

        CreateMap<ConversationGroup, ConversationGroupDto>();
        CreateMap<CreateConversationMessageDto, ConversationMessage>();
        CreateMap<ConversationMessage, ConversationMessageDto>();
    }
}
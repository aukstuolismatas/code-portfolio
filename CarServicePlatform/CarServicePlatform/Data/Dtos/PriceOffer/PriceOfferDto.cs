﻿namespace CarServicePlatform.Data.Dtos.PriceOffer;

public record PriceOfferDto(int Id, int Price, int Duration, string Status, string ServiceAccountId, int ClientRequestId);
﻿using CarServicePlatform.Data.Dtos.Account;

namespace CarServicePlatform.Data.Dtos.Auth.User;

public record AppServiceUserDto(string Id, string Email, string PhoneNumber, bool IsAccountActivated, ServiceAccountDto DomainUser);
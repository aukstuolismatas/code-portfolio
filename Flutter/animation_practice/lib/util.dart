import 'package:animation_practice/radila.dart';
import 'package:flutter/material.dart';

class Util {
  static const String BEANS_URL =
      'https://images.unsplash.com/photo-1587049016823-69ef9d68bd44?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80';
  static const String COFFE_URL =
      'https://images.unsplash.com/photo-1606791405792-1004f1718d0c?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80';
  static const String COCKTAIL_URL =
      'https://images.unsplash.com/photo-1461023058943-07fcbe16d735?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1469&q=80';
  static Widget buildHeroIcon(String path, String tag) {
    return Hero(
        tag: tag,
        child: Container(
          width: 80,
          height: 80,
          child: Image.network(
            path,
            fit: BoxFit.cover,
          ),
        ));
  }

  static Widget buildHeroDestination(String path, String tag, double width) {
    return Hero(
        tag: tag,
        child: Container(
          width: width,
          child: Image.network(
            path,
            fit: BoxFit.cover,
          ),
        ));
  }

  static Widget buildHeroRadialICon(
      String path, String tag, double minRadius, double maxRadius) {
    return Container(
      child: Hero(
        tag: tag,
        createRectTween: _createTween,
        child: Container(
          height: minRadius,
          width: minRadius,
          child: RadialTransition(
            maxRadius: maxRadius,
            child: Image.network(
              path,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }

  static Tween<Rect?> _createTween(Rect? begin, Rect? end) {
    return MaterialRectArcTween(begin: begin, end: end);
  }

  static Widget buildHeroRadialDestination(String path, String tag,
      double maxWidth, double maxHeight, VoidCallback pop) {
    return GestureDetector(
      child: Hero(
        createRectTween: _createTween,
        tag: tag,
        child: Container(
          height: maxHeight,
          width: maxWidth,
          child: RadialTransition(
            maxRadius: maxWidth / 2,
            child: Image.network(
              path,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
      onTap: pop,
    );
  }
}
